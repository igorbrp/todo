<?php
namespace FileManager\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;

use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

use Cake\Utility\Inflector;
use Cake\Utility\Text;

use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;

use Cake\Log\Log;

/**
 * Files Model
 *
 * @property \FileManager\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @method \FileManager\Model\Entity\File get($primaryKey, $options = [])
 * @method \FileManager\Model\Entity\File newEntity($data = null, array $options = [])
 * @method \FileManager\Model\Entity\File[] newEntities(array $data, array $options = [])
 * @method \FileManager\Model\Entity\File|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \FileManager\Model\Entity\File saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \FileManager\Model\Entity\File patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \FileManager\Model\Entity\File[] patchEntities($entities, array $data, array $options = [])
 * @method \FileManager\Model\Entity\File findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class FilesTable extends Table
{
    public $tmp_name_file = '';

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('files');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

		$this->addBehavior('Burzum/Imagine.Imagine');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'className' => 'FileManager.Users'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('model')
            ->maxLength('model', 128)
            ->allowEmptyString('model');

        $validator
            ->allowEmptyFile('submittedfile');

        $validator
            ->add('submittedfile', [
                'extension' => [
                    'rule' => ['extension', ['jpeg', 'jpg', 'png', 'gif', 'bmp']],
                    'message' => __('Invalid extension.')
                ]]);

        $validator
            ->add('submittedfile', [
                'uploadError' => [
                    'rule' => ['uploadError', true],
                    'message' => __('An error occurred while uploading the image.'),
                ]])
            ->add('submittedfile', [
                'mimeType' => [
                    'rule' => ['mimeType', ['image/png', 'image/jpg', 'image/jpeg', 'image/gif', 'image/bmp']],
                    'message' => __('Invalid type'),
                ]])
            ->add('submittedfile', [
                'fileSize' => [
                    'rule' => ['fileSize', '<=', '2MB'],
                    'message' => __('The maximum allowed size is 2MB'),
                ]]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function beforeSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        // Log::write('debug', '----------------------------------------------------');
        // Log::write('debug', 'FilesTable->beforeSave');
        // Log::write('debug', $entity->getOriginalValues());
        // Log::write('debug', '----------------------------------------------------');

        // if ($entity->isNew())
        //     Log::write('debug', 'beforeSave->isNew');
        // else
        //     Log::write('debug', 'beforeSave->isNOTNew');
            // $this->removeFiles($entity->getOriginalValues());

        global $tmp_name_file;

        if (!empty($entity['hash'])) {
            $folder = new Folder();
            $path = WWW_ROOT . $entity['path'];
            // $path = $entity['path'];
            $folder->create($path);

            // mini image
            $imageOperations = [
                'squareCenterCrop' => [
                    'size' => 48,
                ],
            ];
            $this->processImage(
                $tmp_name_file,
                $path . DS . $entity['hash'] . '.mini.' . $entity['extension'],
                [],
                $imageOperations
            );
            // media image
            $imageOperations = [
                'squareCenterCrop' => [
                    'size' => 144,
                ],
            ];
            $this->processImage(
                $tmp_name_file,
                $path . DS . $entity['hash'] . '.media.' . $entity['extension'],
                [],
                $imageOperations
            );

            // main image
            $imageOperations = [
                'squareCenterCrop' => [
                    'size' => 480,
                ],
            ];
            $this->processImage(
                $tmp_name_file,
                $path . DS . $entity['hash'] . '.main.' . $entity['extension'],
                [],
                $imageOperations
            );

            // original image
            $file = new File($tmp_name_file);
            $file->copy($path . DS . $entity['name'], true);

            // tratar caso haja erro
            // if($folder->errors())
        }

        // aqui nao da erro
        // $files = $this->find()->all();


    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        global $tmp_name_file;

        if($data['submittedfile']['error'] === (int)0) {
            $dt = getdate();
            $tmp_name_file = $data['submittedfile']['tmp_name'];
            $nm = pathinfo($data['submittedfile']['name']);
            $data['name'] = Text::slug($nm['filename']) . '.' . $nm['extension'];
            $data['size'] = $data['submittedfile']['size'];
            $data['type'] = $data['submittedfile']['type'];
            $data['hash'] = hash('md5', $data['name']);

            $data['path'] = Inflector::underscore('FileManager') . DS . strtolower($data['model']) . DS . ($dt['seconds'] < 10 ? '0' . $dt['seconds'] : $dt['seconds']) . DS . ($dt['minutes'] < 10 ? '0' . $dt['minutes'] : $dt['minutes']);

            switch($data['type']) {
            case 'image/png':
                $data['extension'] = 'png';
                break;
            case 'image/gif':
                $data['extension'] = 'gif';
                break;
            case 'image/bmp':
                $data['extension'] = 'bmp';
                break;
            case 'image/jpg':
            case 'image/jpeg':
            case 'image/pjpeg':
                $data['extension'] = 'jpg';
                break;
            }
        }
    }

    public function afterSave(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        if (!$entity->isNew()) {
            $this->removeFiles($entity->getOriginalValues());
        }
    }

    public function deleteFiles($params)
    {
        $files = TableRegistry::getTableLocator()->get('Files');
        $result = $files->find()
                ->select(['id', 'foreign_key', 'model', 'name', 'hash', 'path', 'extension'])
                ->where(['foreign_key' => $params['id']])
                ->andWhere(['model' => $params['model']]);
        foreach ($result as $rec) {
            $this->removeFiles($rec);
        }
        $result->delete()->execute();
    }

    public function removeFiles($f)
    {
        $dir = new Folder(WWW_ROOT . $f['path']);
        $list = $dir->find($f['hash'] . '.*.' . $f['extension']);
        foreach ($list as $file) {
            $arq = new File(WWW_ROOT . $f['path'] . DS . $file);
            $arq->delete();
        }
        $arq = new File(WWW_ROOT . $f['path'] . DS . $f['name']);
        $arq->delete();
    }
}
