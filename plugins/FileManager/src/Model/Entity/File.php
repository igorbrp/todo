<?php
namespace FileManager\Model\Entity;

use Cake\ORM\Entity;

/**
 * File Entity
 *
 * @property int $id
 * @property string|null $user_id
 * @property int $subsidiary_id
 * @property string|null $model
 * @property string|null $filename
 * @property int|null $filesize
 * @property string|null $mime_type
 * @property string|null $extension
 * @property string|null $hash
 * @property string|null $path
 * @property string|null $adapter
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \FileManager\Model\Entity\User $user
 * @property \FileManager\Model\Entity\Subsidiary $subsidiary
 */
class File extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'subsidiary_id' => true,
        'model' => true,
        'name' => true,
        'size' => true,
        'type' => true,
        'extension' => true,
        'hash' => true,
        'path' => true,
        'adapter' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'subsidiary' => true
    ];
}
