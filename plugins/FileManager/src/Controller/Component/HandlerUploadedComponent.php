<?php
namespace FileManager\Controller\Component;

use Cake\Controller\Component;

class HandlerFiles extends Component
{
    public function make_path()
    {
        $dt = getdate();
        return $dt['seconds'] . DS . $dt['minutes'];
    }
}
