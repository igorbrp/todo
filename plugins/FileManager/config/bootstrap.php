<?php
use Cake\Event\Event;
use Cake\Event\EventManager;

use FileManager\Model\Table\FilesTable as Files;

EventManager::instance()->on(
    'Model.afterDeleteAssociated',
    function ($event, $params) {
        $files = new Files();
        $files->deleteFiles($params);
    }
);
