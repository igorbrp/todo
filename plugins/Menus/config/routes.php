<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'Menus',
    ['path' => '/menus'],
    function (RouteBuilder $routes) {
        $routes->fallbacks(DashedRoute::class);
    }
);

Router::connect('/menus/', ['plugin' => 'Menus', 'controller' => 'Menus', 'action' => 'index']);
Router::connect('/menus/add/', ['plugin' => 'Menus', 'controller' => 'Menus', 'action' => 'add']);
Router::connect('/menus/edit/*', ['plugin' => 'Menus', 'controller' => 'Menus', 'action' => 'edit']);
Router::connect('/menus/view/*', ['plugin' => 'Menus', 'controller' => 'Menus', 'action' => 'view']);
Router::connect('/menus/delete/*', ['plugin' => 'Menus', 'controller' => 'Menus', 'action' => 'delete']);

Router::connect('/menu-items/', ['plugin' => 'Menus', 'controller' => 'MenuItems', 'action' => 'index']);
Router::connect('/menu-items/index', ['plugin' => 'Menus', 'controller' => 'MenuItems', 'action' => 'index']);
Router::connect('/menu-items/add/', ['plugin' => 'Menus', 'controller' => 'MenuItems', 'action' => 'add']);
Router::connect('/menu-items/edit/*', ['plugin' => 'Menus', 'controller' => 'MenuItems', 'action' => 'edit']);
Router::connect('/menu-items/view/*', ['plugin' => 'Menus', 'controller' => 'MenuItems', 'action' => 'view']);
Router::connect('/menu-items/delete/*', ['plugin' => 'Menus', 'controller' => 'MenuItems', 'action' => 'delete']);
