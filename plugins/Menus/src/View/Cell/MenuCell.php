<?php
namespace Menus\View\Cell;

use Cake\View\Cell;
use Cake\Log\Log;
use Cake\ORM\Query;

/**
 * Menu cell
 */
class MenuCell extends Cell
{
    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {

    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        $model = $this->loadModel('Menus.Menus');

        $query = $model->find()
                       ->contain('MenuItems',  function (Query $q) {
                           return $q->select()
                                    ->order(['title' => 'ASC']);
                       });
        $menus = [];
        foreach ($query as $item) {
            $submenu = [];
            if (!empty($item->menu_items)) {
                foreach ($item->menu_items as $menu_item) {

                        $submenu[] = $menu_item;
                }
                $menus[$item->name] =  [
                    'name' => $item->name,
                    'title' => $item->title,
                    'icon' => $item->icon,
                    'menu_items' => $submenu
                ];
            }
        }
        $this->set(compact('menus', 'query'));
    }
}
