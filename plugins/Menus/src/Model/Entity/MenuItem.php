<?php
namespace Menus\Model\Entity;

use Cake\ORM\Entity;

/**
 * MenuItem Entity
 *
 * @property int $id
 * @property int $menu_id
 * @property string $title
 * @property string $controller
 * @property string $action
 * @property string|null $icon
 * @property string|null $icon_type
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \Menus\Model\Entity\Menu $menu
 */
class MenuItem extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'menu_id' => true,
        'title' => true,
        'controller' => true,
        'action' => true,
        'icon' => true,
        'icon_type' => true,
        'created' => true,
        'modified' => true,
        'menu' => true
    ];
}
