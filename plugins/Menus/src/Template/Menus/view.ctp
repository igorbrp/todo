<section class="content-header">
  <h1>
    Menu
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Name') ?></dt>
            <dd><?= h($menu->name) ?></dd>
            <dt scope="row"><?= __('Url') ?></dt>
            <dd><?= h($menu->url) ?></dd>
            <dt scope="row"><?= __('Icon') ?></dt>
            <dd><?= h($menu->icon) ?></dd>
            <dt scope="row"><?= __('Icon Type') ?></dt>
            <dd><?= h($menu->icon_type) ?></dd>
            <dt scope="row"><?= __('Title') ?></dt>
            <dd><?= h($menu->title) ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($menu->id) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($menu->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($menu->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-share-alt"></i>
          <h3 class="box-title"><?= __('Menu Items') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php if (!empty($menu->menu_items)): ?>
          <table class="table table-hover">
              <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Menu Id') ?></th>
                    <th scope="col"><?= __('Title') ?></th>
                    <th scope="col"><?= __('Controller') ?></th>
                    <th scope="col"><?= __('Action') ?></th>
                    <th scope="col"><?= __('Icon') ?></th>
                    <th scope="col"><?= __('Icon Type') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
              <?php foreach ($menu->menu_items as $menuItems): ?>
              <tr>
                    <td><?= h($menuItems->id) ?></td>
                    <td><?= h($menuItems->menu_id) ?></td>
                    <td><?= h($menuItems->title) ?></td>
                    <td><?= h($menuItems->controller) ?></td>
                    <td><?= h($menuItems->action) ?></td>
                    <td><?= h($menuItems->icon) ?></td>
                    <td><?= h($menuItems->icon_type) ?></td>
                    <td><?= h($menuItems->created) ?></td>
                    <td><?= h($menuItems->modified) ?></td>
                      <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['controller' => 'MenuItems', 'action' => 'view', $menuItems->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['controller' => 'MenuItems', 'action' => 'edit', $menuItems->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['controller' => 'MenuItems', 'action' => 'delete', $menuItems->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menuItems->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
              </tr>
              <?php endforeach; ?>
          </table>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
