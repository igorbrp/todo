<section class="content-header">
  <h1>
    Menu Item
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><i class="fa fa-dashboard"></i> <?php echo __('Home'); ?></a></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Menu') ?></dt>
            <dd><?= $menuItem->has('menu') ? $this->Html->link($menuItem->menu->name, ['controller' => 'Menus', 'action' => 'view', $menuItem->menu->id]) : '' ?></dd>
            <dt scope="row"><?= __('Title') ?></dt>
            <dd><?= h($menuItem->title) ?></dd>
            <dt scope="row"><?= __('Controller') ?></dt>
            <dd><?= h($menuItem->controller) ?></dd>
            <dt scope="row"><?= __('Action') ?></dt>
            <dd><?= h($menuItem->action) ?></dd>
            <dt scope="row"><?= __('Icon') ?></dt>
            <dd><?= h($menuItem->icon) ?></dd>
            <dt scope="row"><?= __('Icon Type') ?></dt>
            <dd><?= h($menuItem->icon_type) ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($menuItem->id) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($menuItem->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($menuItem->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
