<ul>

    <li class="treeview">
	<?=$this->Html->link('Dashboarddd', [ 'controller' => 'dashboard', 'action' => 'welcome' ])?>
    </li>
    <?php
    foreach($menus as $menu) {

	if(!empty($menu['menu_items'])) {

            $linkTitle = $this->Html->link('#', $menu['title']);
            $itemsCount = 0;
            $arrayItems = [];

	    foreach($menu['menu_items'] as $menu_item) {
                $urlIndex = $this->Url->build(['controller' => $menu_item['controller'], 'action' => $menu_item['action']]);
                if ($this->AuthLink->isAuthorized()) {
                    $linkIndex = $this->Html->link(
                        $menu_item['icon'] . $menu_item['title'],
                        $urlIndex,
                        ['escape' => false]
                    );
                    $itemAdd = $class = '';
                    $urlAdd = $this->Url->build(['controller' => $menu_item['controller'], 'action' => 'add']);
                    if($this->AuthLink = $this->Html->link($urlAdd)) {
                        $linkAdd = $this->Html->link(
                            '<i class="fa fa-circle-o text-success"></i>' . __('new'),
                            $urlAdd,
                            ['escape' => false]
                        );
                        $class = 'treeview';
                        $itemAdd = $this->Html->tag('ul', $this->Html->tag('li', $linkAdd), ['class' => 'treeview-menu']);
                    }
                    $arrayItems[] = $this->Html->tag('li', $urlIndex . $itemAdd, ['class' = $class]);
                    $itemsCount++;
                }
            }
            // endforeach

        }
        // endif
    }
    // endforeach
    ?>

    <li class="treeview">
	<a href="#">
	    <?php echo $menu['title']; ?>
	</a>

	<ul class="treeview-menu">
	    <?php foreach($menu['menu_items'] as $menu_item): ?>

		<li>
		    <?=$this->Html->link(
			$menu_item['icon'] . ' ' . $menu_item['title'],
                        /* '/' . $menu_item['controller'] . '/' . $menu_item['action'], */
			$this->Url->build([
                            'controller' => $menu_item['controller'],
                            'action' => $menu_item['action']
                        ]),
			['escape' => false])?>
		</li>

	    <?php  endforeach; ?>
	</ul>

    </li>

</ul>
