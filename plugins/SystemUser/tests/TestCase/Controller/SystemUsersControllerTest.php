<?php
namespace SystemUser\Test\TestCase\Controller;

use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;
use SystemUser\Controller\SystemUsersController;

/**
 * SystemUser\Controller\SystemUsersController Test Case
 *
 * @uses \SystemUser\Controller\SystemUsersController
 */
class SystemUsersControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.SystemUser.SystemUsers',
        'plugin.SystemUser.SocialAccounts',
        'plugin.SystemUser.Files'
    ];

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
