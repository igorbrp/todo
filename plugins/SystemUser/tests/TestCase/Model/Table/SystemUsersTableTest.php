<?php
namespace SystemUser\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use SystemUser\Model\Table\SystemUsersTable;

/**
 * SystemUser\Model\Table\SystemUsersTable Test Case
 */
class SystemUsersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \SystemUser\Model\Table\SystemUsersTable
     */
    public $SystemUsers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.SystemUser.SystemUsers',
        'plugin.SystemUser.Files',
        'plugin.SystemUser.SocialAccounts'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SystemUsers') ? [] : ['className' => SystemUsersTable::class];
        $this->SystemUsers = TableRegistry::getTableLocator()->get('SystemUsers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SystemUsers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
