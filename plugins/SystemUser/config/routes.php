<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'SystemUser',
    ['path' => '/system-user'],
    function (RouteBuilder $routes) {
        $routes->fallbacks(DashedRoute::class);
    }
);

Router::connect('/system-users/', ['plugin' => 'SystemUser', 'controller' => 'SystemUsers', 'action' => 'index']);
Router::connect('/system-users/index/', ['plugin' => 'SystemUser', 'controller' => 'SystemUsers', 'action' => 'index']);
Router::connect('/system-users/add/', ['plugin' => 'SystemUser', 'controller' => 'SystemUsers', 'action' => 'add']);
Router::connect('/system-users/edit/*', ['plugin' => 'SystemUser', 'controller' => 'SystemUsers', 'action' => 'edit']);
Router::connect('/system-users/view/*', ['plugin' => 'SystemUser', 'controller' => 'SystemUsers', 'action' => 'view']);
Router::connect('/system-users/delete/*', ['plugin' => 'SystemUser', 'controller' => 'SystemUsers', 'action' => 'delete']);
Router::connect('/system-users/profile/', ['plugin' => 'SystemUser', 'controller' => 'SystemUsers', 'action' => 'profile']);
//Router::connect('/system-users/delete-some-records/*', ['plugin' => 'SystemUser', 'controller' => 'SystemUsers', 'action' => 'delete-some-records']);
