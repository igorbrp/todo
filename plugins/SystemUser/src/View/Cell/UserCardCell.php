<?php
namespace SystemUser\View\Cell;

use Cake\View\Cell;

/**
 * UserCard cell
 */
class UserCardCell extends Cell
{
    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {
    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        $currentUser = $this->request->getSession()->read('Auth.User');

        $systemusers = $this->loadModel('SystemUser.SystemUsers');

        //        $query = $model->find()->contain(['Files']);
        // $systemuser = $systemusers->get('efda77a0-c730-4ab5-87b9-593b207f6aed')
        //        ->contain(['Files']);
        $systemuser = $systemusers->get($currentUser['id'], [
              'contain' => ['Files']
        ]);

        $this->set(compact('systemuser'));
    }
}
