<?php
namespace SystemUser\Model\Table;

// use Cake\ORM\Query;
// use Cake\ORM\RulesChecker;
// use Cake\ORM\Table;
// use Cake\Validation\Validator;

use CakeDC\Users\Model\Table\UsersTable;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;

/**
 * SystemUsers Model
 *
 * @property \SystemUser\Model\Table\FilesTable&\Cake\ORM\Association\HasMany $Files
 * @property \SystemUser\Model\Table\SocialAccountsTable&\Cake\ORM\Association\HasMany $SocialAccounts
 *
 * @method \SystemUser\Model\Entity\SystemUser get($primaryKey, $options = [])
 * @method \SystemUser\Model\Entity\SystemUser newEntity($data = null, array $options = [])
 * @method \SystemUser\Model\Entity\SystemUser[] newEntities(array $data, array $options = [])
 * @method \SystemUser\Model\Entity\SystemUser|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \SystemUser\Model\Entity\SystemUser saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \SystemUser\Model\Entity\SystemUser patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \SystemUser\Model\Entity\SystemUser[] patchEntities($entities, array $data, array $options = [])
 * @method \SystemUser\Model\Entity\SystemUser findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SystemUsersTable extends UsersTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        //        $this->setTable('system_users');
        $this->setPrimaryKey('id');
        $this->setDisplayField('username');

        $this->hasOne('Files', [
            'foreignKey' => 'foreign_key',
            'bindingKey' => 'id',
            'className' => 'FileManager.Files',
            'conditions' => ['Files.model' => 'SystemUser.SystemUsers'],
            'cascadeCallbacks' => true,
            // 'dependent' => true
        ]);
        // friendsofcake / search
        // Add the behaviour to your table
        $this->addBehavior('Search.Search');
        // Setup search filter using search manager
        $this->searchManager()
             ->value('user_id')
        // Here we will alias the 'q' query param to search the `Articles.title`
        // field and the `Articles.content` field, using a LIKE match, with `%`
        // both before and after.
             ->add('q', 'Search.Like', [
                 'before' => true,
                 'after' => true,
                 'mode' => 'or',
                 'comparison' => 'LIKE',
                 'wildcardAny' => '*',
                 'wildcardOne' => '?',
                 'field' => [
                     'username', 'nick_name',
                 ]
             ]);
    }

    public function afterDelete(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        $event = new Event('Model.afterDeleteAssociated', $this, ['params' => ['id' => $entity->id, 'model' => $this->registryAlias()]]);
        $this->getEventManager()->dispatch($event);
    }
}
