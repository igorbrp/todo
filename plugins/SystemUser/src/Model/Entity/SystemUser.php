<?php
namespace SystemUser\Model\Entity;

use CakeDC\Users\Model\Entity\User;

/**
 * SystemUser Entity
 *
 * @property string $id
 * @property string $username
 * @property string|null $email
 * @property string $password
 * @property string|null $nick_name
 * @property string|null $last_name
 * @property string|null $token
 * @property \Cake\I18n\FrozenTime|null $token_expires
 * @property string|null $api_token
 * @property \Cake\I18n\FrozenTime|null $activation_date
 * @property string|null $secret
 * @property bool|null $secret_verified
 * @property \Cake\I18n\FrozenTime|null $tos_date
 * @property bool $active
 * @property bool $is_superuser
 * @property string|null $role
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property string|null $additional_data
 *
 * @property \SystemUser\Model\Entity\File[] $files
 * @property \SystemUser\Model\Entity\SocialAccount[] $social_accounts
 */
class SystemUser extends User
{
    /**
     * Map CakeDC's User.nick_name field to User.first_name when getting
     *
     * @return mixed The value of the mapped property.
     */
    protected function _getFirstName()
    {
        return $this->_properties['nick_name'];
    }

    /**
     * Map CakeDC's User.nick_name field to User.first_name when setting
     *
     * @param mixed $value The value to set.
     * @return static
     */
    protected function _setFirstName($value)
    {
        $this->set('nick_name', $value);
        return $value;
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */

    protected $_accessible = [
        '*' => true,
        // 'id' => false,
        // 'is_superuser' => false,
        // 'role' => false,
        // 'file' => true,
        // 'submittedfile' => true,
        // 'type' => true,
        // 'extension' => true,
        // 'hash' => true,
        // 'path' => true
    ];
}
