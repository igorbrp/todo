<?php

namespace SystemUser\Controller;

use App\Controller\AppController as BaseController;

class AppController extends BaseController
{
    /**
     * Initialize
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        //        $this->loadComponent('Security');
        if ($this->request->getParam('_csrfToken') === false) {
            $this->loadComponent('Csrf');
        }
        $this->loadComponent('CakeDC/Users.UsersAuth');
    }
}
