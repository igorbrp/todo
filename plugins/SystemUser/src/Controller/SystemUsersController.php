<?php
namespace SystemUser\Controller;

use SystemUser\Controller\AppController;

use App\Model\Table\SystemUsersTable;
use Cake\Event\Event;
//use CakeDC\Users\Controller\Component\UsersAuthComponent;

use CakeDC\Users\Controller\Traits\LoginTrait;
use CakeDC\Users\Controller\Traits\RegisterTrait;
use CakeDC\Users\Controller\Traits\ProfileTrait;

use Cake\Utility\Inflector;

use Cake\Core\Configure;
/**
 * SystemUsers Controller
 *
 * @property \SystemUser\Model\Table\SystemUsersTable $SystemUsers
 */
class SystemUsersController extends AppController
{
    use LoginTrait;
    use RegisterTrait;
    //    use SimpleCrudTrait;
    use ProfileTrait;

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $table = $this->loadModel();
        //        $tableAlias = $table->getAlias();
        $tableAlias = $table->getAlias('search', ['search' => $this->request->getQuery()]);
        $this->set($tableAlias, $this->paginate($table, [
            'contain' => 'Files'
        ]));

        
        $this->set('tableAlias', $tableAlias);
        $this->set('_serialize', [$tableAlias, 'tableAlias']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return void
     * @throws NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $table = $this->loadModel();
        $tableAlias = $table->getAlias();
        $entity = $table->get($id, [
            'contain' => [
                'Files'
            ]
        ]);
        $this->set($tableAlias, $entity);
        $this->set('tableAlias', $tableAlias);
        $this->set('_serialize', [$tableAlias, 'tableAlias']);
    }

    /**
     * Add method
     *
     * @return mixed Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $table = $this->loadModel();
        $tableAlias = $table->getAlias();
        $entity = $table->newEntity();

        $this->set($tableAlias, $entity);
        $this->set('tableAlias', $tableAlias);
        $this->set('_serialize', [$tableAlias, 'tableAlias']);
        if (!$this->request->is('post')) {
            $this->render('addedit');
            return;
        }

		$userId = $this->Auth->user('id');
        $data = $this->request->getData();
        
        if (!empty($data['file']['submittedfile']) && $data['file']['submittedfile']['error'] == 0) {
            $data['file']['user_id'] = $userId; // Optional
            $data['file']['model'] = $this->SystemUsers->registryAlias();
        }
        
        $entity = $table->patchEntity($entity, $data, [
            'associated' => [
                'Files'
            ]
        ]);
                
        $singular = Inflector::singularize(Inflector::humanize($tableAlias));
        if ($table->save($entity)) {
            $this->Flash->success(__d('CakeDC/Users', 'The {0} has been saved', $singular));
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error('O usuário não pode ser salvo, verifique os erros e tente novamente.');
        
        $this->render('addedit');
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return mixed Redirects on successful edit, renders view otherwise.
     * @throws NotFoundException When record not found.
     */
    public function edit($id = null)
    {
		$userId = $this->Auth->user('id');

        $table = $this->loadModel();
        $tableAlias = $table->getAlias();
        $entity = $table->get($id, [
            'contain' => [
            'Files'
            ]
        ]);
        
        $this->set($tableAlias, $entity);
        $this->set('tableAlias', $tableAlias);
        $this->set('_serialize', [$tableAlias, 'tableAlias']);
        if (!$this->request->is(['patch', 'post', 'put'])) {
            $this->render('addedit');
            return;
        }

        $data = $this->request->getData();
             
        if (!empty($data['file']['submittedfile']) && $data['file']['submittedfile']['error'] == 0) {
            $data['file']['user_id'] = $userId; // Optional
            $data['file']['model'] = $this->SystemUsers->registryAlias();
        }

        $entity = $table->patchEntity($entity, $data, [
            'associated' => [
                'Files'
            ]
        ]);

        $singular = Inflector::singularize(Inflector::humanize($tableAlias));
        if ($table->save($entity)) {
            $this->Flash->success(__d('CakeDC/Users', 'The {0} has been saved', $singular));
            
            return $this->redirect(['action' => 'index']);
        }
        $this->Flash->error(__d('CakeDC/Users', 'The {0} could not be saved', $singular));
        $this->render('addedit');
    }

    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $table = $this->loadModel();
        $tableAlias = $table->getAlias();
        $entity = $table->get($id, [
            'contain' => []
        ]);
        $singular = Inflector::singularize(Inflector::humanize($tableAlias));
        if ($table->delete($entity)) {
            $this->Flash->success(__d('CakeDC/Users', 'The {0} has been deleted', $singular));
        } else {
            $this->Flash->error(__d('CakeDC/Users', 'The {0} could not be deleted', $singular));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function profile()
    {
        $currentUser = $this->request->getSession()->read('Auth.User');
		$userId = $this->Auth->user('id');

        $table = $this->loadModel();
        $tableAlias = $table->getAlias();
        $entity = $table->get($userId, [
            'contain' => [
            'Files'
            ]
        ]);

        $this->set($tableAlias, $entity);
        $this->set('tableAlias', $tableAlias);
        $this->set('_serialize', [$tableAlias, 'tableAlias']);
        if (!$this->request->is(['patch', 'post', 'put'])) {
            $this->render('profile');
            return;
        }

        $data = $this->request->getData();

        if (!empty($data['file']['submittedfile']) && $data['file']['submittedfile']['error'] == 0) {
            $data['file']['user_id'] = $userId; // Optional
            $data['file']['model'] = $this->SystemUsers->registryAlias();
        }

        $entity = $table->patchEntity($entity, $data, [
            'associated' => [
                'Files'
            ]
        ]);

        $singular = Inflector::singularize(Inflector::humanize($tableAlias));
        if ($table->save($entity)) {
            $this->Flash->success('O seu perfil foi atualizado.');

            return $this->redirect(['action' => 'profile']);
        }
        $this->Flash->error('Seu perfil não pode ser atualizado, vefirique os erros e tente novamente.');

    }

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        $this->Security->setConfig('unlockedActions', ['add']);
    }
}
