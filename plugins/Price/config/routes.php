<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'Price',
    ['path' => '/price'],
    function (RouteBuilder $routes) {
        $routes->fallbacks(DashedRoute::class);
    }
);

Router::connect('/prices/', ['plugin' => 'Price', 'controller' => 'Prices', 'action' => 'index']);
Router::connect('/prices/index/', ['plugin' => 'Price', 'controller' => 'Prices', 'action' => 'index']);
Router::connect('/prices/add/', ['plugin' => 'Price', 'controller' => 'Prices', 'action' => 'add']);
Router::connect('/prices/edit/*', ['plugin' => 'Price', 'controller' => 'Prices', 'action' => 'edit']);
Router::connect('/prices/view/*', ['plugin' => 'Price', 'controller' => 'Prices', 'action' => 'view']);
Router::connect('/prices/delete/*', ['plugin' => 'Price', 'controller' => 'Prices', 'action' => 'delete']);
Router::connect('/prices/delete-some-records/*', ['plugin' => 'Price', 'controller' => 'Prices', 'action' => 'delete-some-records']);
