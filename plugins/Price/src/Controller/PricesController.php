<?php
namespace Price\Controller;

use Price\Controller\AppController;
use Cake\Collection\Collection;

use Cake\Log\Log;

/**
 * Prices Controller
 *
 * @property \Price\Model\Table\PricesTable $Prices
 *
 * @method \Price\Model\Entity\Price[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PricesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $prices = $this->paginate($this->Prices, [
            'order' => ['sequence']
        ]);

        $this->set(compact('prices'));
    }

    /**
     * View method
     *
     * @param string|null $id Price id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $price = $this->Prices->get($id, [
            'contain' => []
        ]);

        $this->set('price', $price);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $price = $this->Prices->newEntity();

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if (!empty($data['taxes'])) {
                $collection = new Collection($data['taxes']);
                $valids = $collection->reject(function ($t, $k) {
                    return trim($t['name'] . $t['value']) === '';
                });

                $data['taxes'] = $valids->toList();
            }

            $data['sequence'] = $this->buildSequence();

            $price = $this->Prices->patchEntity($price, $data, [
                'associated' => [
                    'Taxes'
                ]
            ]);

            if ($this->Prices->save($price)) {
                $this->Flash->success(__('The price has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The price could not be saved. Please, try again.'));
        }
        $this->set(compact('price'));
        $this->render('addedit');
    }

    /**
     * Edit method
     *
     * @param string|null $id Price id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $price = $this->Prices->get($id, [
            'contain' => ['Taxes']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            if (!empty($data['taxes'])) {
                $collection = new Collection($data['taxes']);
                $valids = $collection->reject(function ($t, $k) {
                    return trim($t['name'] . $t['value']) === '';
                });

                $data['taxes'] = $valids->toList();
            }

            $price = $this->Prices->patchEntity($price, $data, [
                'associated' => [
                    'Taxes'
                ]
            ]);

            if ($this->Prices->save($price)) {
                $this->Flash->success(__('The price has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The price could not be saved. Please, try again.'));
        }
        $this->set(compact('price'));
        $this->render('addedit');
    }

    /**
     * Delete method
     *
     * @param string|null $id Price id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $price = $this->Prices->get($id);
        if ($this->Prices->delete($price)) {
            $this->Flash->success(__('The price has been deleted.'));
        } else {
            $this->Flash->error(__('The price could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function changeActivity()
    {
        $this->request->allowMethod(['post']);
        $id = $this->request->getData('change-id');
        $price = $this->Prices->get($id);
        $price->active = ($price->active == 1 ? 0 : 1);
        if ($this->Prices->save($price)) {
            $this->Flash->success(__('The status of price has been updated.'));
        } else {
            $this->Flash->error(__('The price could not be updated. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function up($id = null)
    {
        $this->request->allowMethod(['post']);
        $price = $this->Prices->get($id);
        $sequence = $price->sequence;

        $result = $this->Prices->find()
               ->select(['id', 'sequence'])
               ->where(['sequence <' => $sequence])
               ->order(['sequence' => 'DESC'])
               ->first();

        $id2 = $result->id;
        $sequence2 = $result->sequence;

        $this->changeOrder($id, $sequence, $id2, $sequence2);
        return $this->redirect(['action' => 'index']);
    }

    public function down($id = null)
    {
        $this->request->allowMethod(['post']);
        $price = $this->Prices->get($id);
        $sequence = $price->sequence;

        $result = $this->Prices->find()
               ->select(['id', 'sequence'])
               ->where(['sequence >' => $sequence])
               ->order(['sequence' => 'ASC'])
               ->first();

        $id2 = $result->id;
        $sequence2 = $result->sequence;

        $this->changeOrder($id, $sequence, $id2, $sequence2);
        return $this->redirect(['action' => 'index']);
    }

    public function changeOrder($id1, $sequence1, $id2, $sequence2)
    {
        $query1 = $this->Prices->query();
        $query2 = $this->Prices->query();

        $query1->update()
            ->set(['sequence' => $sequence2])
            ->where(['id' => $id1])
            ->execute();

        $query2->update()
            ->set(['sequence' => $sequence1])
            ->where(['id' => $id2])
            ->execute();
    }

    private function buildSequence()
    {
        $prices = $this->Prices->find();
        $lastPrice = $prices->max(function ($max) {
            return $max->sequence;
        });

        return empty($lastPrice) ? 1 : $lastPrice['sequence'] + 1;
    }

}
