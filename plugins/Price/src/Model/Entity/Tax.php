<?php
namespace Price\Model\Entity;

use Cake\ORM\Entity;

/**
 * Tax Entity
 *
 * @property int $id
 * @property int $price_id
 * @property int|null $foreign_key
 * @property string $model
 * @property string $name
 * @property float|null $value
 * @property string $calculation
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Price $price
 */
class Tax extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'price_id' => true,
        'foreign_key' => true,
        'model' => true,
        'name' => true,
        'value' => true,
        'calculation' => true,
        'created' => true,
        'modified' => true,
        'price' => true
    ];
}
