<?php
namespace Price\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Event\Event;
use ArrayObject;

use Cake\Log\Log;

/**
 * Prices Model
 *
 * @method \Price\Model\Entity\Price get($primaryKey, $options = [])
 * @method \Price\Model\Entity\Price newEntity($data = null, array $options = [])
 * @method \Price\Model\Entity\Price[] newEntities(array $data, array $options = [])
 * @method \Price\Model\Entity\Price|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Price\Model\Entity\Price saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Price\Model\Entity\Price patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Price\Model\Entity\Price[] patchEntities($entities, array $data, array $options = [])
 * @method \Price\Model\Entity\Price findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PricesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('prices');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Taxes', [
            'foreignKey' => 'price_id',
            'bindingKey' => 'id',
            'className' => 'Price.Taxes',
            //            'conditions' => ['Taxes.model' => 'Price.Prices'],
            'cascadeCallbacks' => true,
            'dependent' => true,
            'saveStrategy' => 'replace'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 180)
            ->requirePresence('name', 'create')
            ->notEmptyString('name')
            ->add('name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        // $validator
        //     ->integer('sequence')
        //     ->requirePresence('sequence', 'create')
        //     ->notEmptyString('sequence');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name']));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        // $prices = $this->find();
        // $lastPrice = $prices->max(function ($max) {
        //     return $max->sequence;
        // });

        // $data['sequence'] = $lastPrice['sequence'] + 1;
    }
}
