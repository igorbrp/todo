<?php
namespace Price\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Taxes Model
 *
 * @property \App\Model\Table\PricesTable&\Cake\ORM\Association\BelongsTo $Prices
 *
 * @method \Price\Model\Entity\Tax get($primaryKey, $options = [])
 * @method \Price\Model\Entity\Tax newEntity($data = null, array $options = [])
 * @method \Price\Model\Entity\Tax[] newEntities(array $data, array $options = [])
 * @method \Price\Model\Entity\Tax|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Price\Model\Entity\Tax saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \Price\Model\Entity\Tax patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \Price\Model\Entity\Tax[] patchEntities($entities, array $data, array $options = [])
 * @method \Price\Model\Entity\Tax findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TaxesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('taxes');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Prices', [
            'foreignKey' => 'price_id',
            'joinType' => 'INNER',
            'className' => 'Price.Prices'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        // $validator
        //     ->nonNegativeInteger('foreign_key')
        //     ->allowEmptyString('foreign_key');

        // $validator
        //     ->scalar('model')
        //     ->maxLength('model', 128)
        //     ->requirePresence('model', 'create')
        //     ->notEmptyString('model');

        $validator
            ->scalar('name')
            ->maxLength('name', 180)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->decimal('value')
            ->allowEmptyString('value');

        $validator
            ->scalar('calculation')
            ->requirePresence('calculation', 'create')
            ->notEmptyString('calculation');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['price_id'], 'Prices'));

        return $rules;
    }
}
