<?php
    /**
     * @var \App\View\AppView $this
     * @var \Cake\Datasource\EntityInterface $price
     */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	    <?=__('Price')?>
	    <small><?= __('Edit'); ?></small>
    </h1>
    <?=$this->element('MenuHeader')?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <span><?=__('Identification'); ?></span>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create($price, ['role' => 'form']); ?>
                <div class="box-body">
                    <?php
                        echo $this->Form->control('name');
                        /* echo $this->Form->control('sequence');*/
                    ?>
                    <span><?=__('Taxes'); ?></span>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col"><?=__('name')?></th>
                                    <th scope="col"><?=__('value')?></th>
                                    <th scope="col"><?=__('calculation')?></th>
                                    <th scope="col" class="text-right"><?=__('actions')?></th>
                                </tr>

                                <tr>
                                    <td>
                                        <?=$this->Form->input('new_name', ['type' => 'text', 'label' => false, 'placeholder' => __('new name')]);?>
                                    </td>
                                    <td>
                                        <?=$this->Form->input('new_tax', ['type' => 'number', 'label' => false, 'placeholder' => __('new tax')]);?>
                                    </td>
                                    <td>
                                        <!-- <div class="form-group input number"> -->
                                        <?=$this->Form->select('new_calculation', [
                                            '%' => __('percentage'),
                                            '$' => __('currency')
                                            ], [
                                                'id' => 'new-calculation',
                                                'class' => 'select2',
                                                'style' => 'width: 100%',
                                                'value' => '%'
                                            ]);?>
                                        <!-- </div> -->
                                    </td>
                                    <th class="text-right">
                                        <div class="form-group">
                                            <?=$this->Html->link(__('add'), '#', ['id' => 'bt-add', 'class' => 'btn btn-info', 'onclick' => 'addElement();'])?>
                                            <?=$this->Html->link(__('cancel'), '#', ['id' => 'bt-cancel', 'class' => 'btn btn-danger', 'onclick' => 'cancelEditElement();', 'disabled' => true])?>
                                        </div>
                                    </th>
                                </tr>
                            </thead>
                            <tbody id="tbody">
                                <?php $i = 0; ?>
                                <?php foreach ($price->taxes as $tax) : ?>
                                    <tr id="taxes-<?=$i?>">
                                        <td><?=$this->Form->control('taxes.' . $i . '.name', ['label' => false, 'required' => false, 'readonly' => true]);?></td>
                                        <td><?=$this->Form->control('taxes.' . $i . '.value', ['label' => false, 'required' => false, 'readonly' => true]);?></td>
                                        <td><?=$this->Form->control('taxes.' . $i . '.calculation', ['label' => false, 'required' => false, 'readonly' => true]);?></td>
                                        <td class="text-right">
                                            <div class="form-group">
                                                <?=$this->Html->link(__('edit'), '#', ['id' => 'bt-edit-' . $i, 'class' => 'btn btn-warning', 'onclick' => 'editElement(' . $i . ');'])?>
                                                <?=$this->Html->link(__('delete'), '#', ['id' => 'bt-delete-' . $i,'class' => 'btn btn-danger', 'onclick' => 'deleteElement(' . $i . ');'])?>
                                            </div>
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                <?php endforeach; ?>
                                <?php for (; $i < 20; $i++) : ?>
                                    <tr id="taxes-<?=$i?>" style="display:none">
                                        <td><?=$this->Form->control('taxes.' . $i . '.name', ['label' => false, 'required' => false, 'readonly' => true]);?></td>
                                        <td><?=$this->Form->control('taxes.' . $i . '.value', ['label' => false, 'required' => false, 'readonly' => true]);?></td>
                                        <td><?=$this->Form->control('taxes.' . $i . '.calculation', ['label' => false, 'required' => false, 'readonly' => true]);?></td>
                                        <td class="text-right">
                                            <div class="form-group">
                                                <?=$this->Html->link(__('edit'), '#', ['id' => 'bt-edit-' . $i, 'class' => 'btn btn-warning', 'onclick' => 'editElement(' . $i . ');'])?>
                                                <?=$this->Html->link(__('delete'), '#', ['id' => 'bt-delete-' . $i,'class' => 'btn btn-danger', 'onclick' => 'deleteElement(' . $i . ');'])?>
                                            </div>
                                        </td>
                                    </tr>
                                <?php endfor; ?>
                            </tbody>

                        </table>
                    </div>
                </div>
                <!-- /.box-body -->

                <?php echo $this->Form->submit(__('Submit')); ?>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>
<script>
    const newName = document.getElementById('new-name');
    const newValue = document.getElementById('new-tax');
    const newCalculation = document.getElementById('new-calculation');
    const tbody_container = document.getElementById('tbody');
    const btCancel = document.getElementById('bt-cancel');

    var edit = {
        index: false,
        targetTr: undefined,
        targetName: undefined,
        targetValue: undefined,
        targetCalculation: undefined,
    };

    function editElement(i)
    {
        if(edit.index !== false)
            endEditElement();
        edit.index = i;
        edit.targetTr = document.getElementById('taxes-' + i);
        edit.targetName = document.getElementById('taxes-' + i + '-name');
        edit.targetValue = document.getElementById('taxes-' + i + '-value');
        edit.targetCalculation = document.getElementById('taxes-' + i + '-calculation');
        edit.targetTr.style.backgroundColor = '#ffffe6';
        document.getElementById('bt-edit-' + i).setAttribute('disabled', true);
        document.getElementById('bt-delete-' + i).setAttribute('disabled', true);
        btCancel.removeAttribute('disabled');
        newName.value = edit.targetName.value;
        newValue.value = edit.targetValue.value;
        newCalculation = edit.targetCalculation.value;
        newName.select();
    }

    function endEditElement()
    {
        document.getElementById('bt-edit-' + edit.index).removeAttribute('disabled');
        document.getElementById('bt-delete-' + edit.index).removeAttribute('disabled');
        btCancel.setAttribute('disabled', true);
        edit.targetTr.style.removeProperty('background-color');
        edit.index = false;
    }

    function cancelEditElement()
    {
        endEditElement();
        newName.value = newValue.value = '';
    }

    function deleteElement(i)
    {
        document.getElementById('taxes-' + i + '-name').value = '';
        document.getElementById('taxes-' + i + '-value').value = '';
        document.getElementById('taxes-' + i + '-calculation').value = '%';
        document.getElementById('taxes-' + i).style.setProperty('display', 'none');
    }

    function getFirstHiddenTr(obj)
    {
        for (i = 0; i < obj.length; i++) {
            if (obj.item(i).attributes.getNamedItem("style").value == 'display:none')
                return obj.item(i).id;
        }
    }

    function getIndex(tr)
    {
        var s = 'taxes-';
        var i = tr.id.substr(s.length, tr.id.length);
        return i;
    }

    function addElement()
    {
        console.log('nome: ' + newName.value);
        console.log('valor: ' + newValue.value);
        if (newName.value === '' || newValue.value === '') {
            alert('Nome e valor da taxa devem ser digitados.');
            return;
        }

        if (edit.index === false) {
            var listTrElements = tbody_container.getElementsByTagName('TR');
            var firstHiddenTr = document.getElementById(getFirstHiddenTr(listTrElements));
            var index = getIndex(firstHiddenTr);

            var targetName = document.getElementById('taxes-' + index + '-name');
            var targetValue = document.getElementById('taxes-' + index + '-value');
            var targetCalculation = document.getElementById('taxes-' + index + '-calculation');

            targetName.value = newName.value;
            targetValue.value = newValue.value;
            //            targetValue.value = newCalculation.value;
            targetCalculation.value = getSelect2Value(newCalculation);
            // firstHiddenTr.attributes.getNamedItem("style").value = '';
            firstHiddenTr.style.removeProperty('display');
        } else {
            edit.targetName.value = newName.value;
            edit.targetValue.value = newValue.value;
            edit.targetCalculation.value = newCalculation.value;
            endEditElement();
        }
        newName.value = newValue.value = '';
        newName.focus();
    }
</script>
<?php
    echo $this->Html->css([
        'AdminLTE./bower_components/select2/dist/css/select2.css'], [
            'block' => 'css'
        ]);
    echo $this->Html->script([
        'AdminLTE./bower_components/select2/dist/js/select2.js',
        'apply-select2',
    ], [
        'block' => 'script'
    ]);
?>
