<style>
    .user-mini {
        margin:auto 10px auto auto;
        width: 25px;
        height: 25px;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Prices')?>
        <small><?=__('List'); ?></small>
    </h1>
    <?=$this->element('MenuHeader')?>
</section>
<!-- Main content -->
<h1>Template do plugin</h1>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('List'); ?></h3>
                    <div class="box-tools">
                        <?=$this->element('Search')?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">&nbsp;</th>
                                <th scope="col" class="text-center"><?= $this->Paginator->sort(__('active')) ?></th>
                                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('sequence') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                                <th scope="col" class="actions text-right"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $recs = 1; ?>
                            <?php $first = true; ?>
                            <?php foreach ($prices as $price): ?>
                                <tr>
                                    <td>
                                        <?php if($first) : ?>
                                            <span style="color:#cce0ff"><i class="fa fa-arrow-up"></i></span>
                                            <?php $first = false; ?>
                                        <?php else: ?>
                                            <?=$this->Form->postLink('<i class="fa fa-arrow-up"></i>', ['action' => 'up', $price->id], ['escape' => false])?>
                                        <?php endif; ?>
                                        &nbsp;
                                        <?php if($recs == count($prices)) : ?>
                                            <span style="color:#cce0ff"><i class="fa fa-arrow-down"></i></span>
                                        <?php else : ?>
                                            <?=$this->Form->postLink('<i class="fa fa-arrow-down"></i>', ['action' => 'down', $price->id], ['escape' => false])?>
                                        <?php endif; ?>
                                    </td>
                                    <td class="text-center">
                                        <?=$this->Form->create('', [
                                            'id' => 'change-active-' . $price->id,
                                            'action' => 'change-activity',
                                            ]);?>
                                        <span style="display:none;"><?=$this->Form->input('change-id', ['value' => $price->id]);?></span>
                                        <?=$this->Form->checkbox('id', [
                                            'id' => 'chk-activity',
                                            'name' => 'chk_activity',
                                            'class' => 'chk-box',
                                            'checked' => $price->active == 1 ? true : false,
                                            'value' => $price->id,
                                            'onclick' => 'document.getElementById("change-active-' . $price->id . '").submit()'
                                            ]);?>
                                        <?=$this->Form->end(); ?>
                                    </td>
                                    <td><?= h($price->name) ?></td>
                                    <td><?= $this->Number->format($price->sequence) ?></td>
                                    <td><?= h($price->created) ?></td>
                                    <td><?= h($price->modified) ?></td>
                                    <td class="actions text-right">
                                        <?=$this->Element('Actions', ['cod_id' => $price->id]);?>
                                    </td>
                                </tr>
                                <?php $recs++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<script>
    function changeActivity(form, id)
    {
        console.log('form: ' + form);
        console.log('id: ' + id);
    }
</script>
<?php
    /* echo $this->Html->css([
     *     'AdminLTE./plugins/iCheck/all'
     * ],[
     *     'block' => 'css'
     * ]);
     * echo $this->Html->script([
     *     'AdminLTE./plugins/iCheck/icheck.min',
     * ], [
     *     'block' => 'script'
     * ]); */
    echo $this->Html->script([
        /* 'apply-icheck', */
        'js-base'
    ], [
        'block' => 'scriptBottom'
    ]);
?>
