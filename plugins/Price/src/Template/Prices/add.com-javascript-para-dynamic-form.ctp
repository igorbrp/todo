<?php
    /**
     * @var \App\View\AppView $this
     * @var \Cake\Datasource\EntityInterface $price
     */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	    <?=__('Price')?>
	    <small><?= __('Add'); ?></small>
    </h1>
    <?=$this->element('MenuHeader')?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <span><?=__('Identification'); ?></span>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create($price, ['role' => 'form']); ?>
                <div class="box-body">
                    <?php
                        echo $this->Form->control('name');
                        echo $this->Form->control('sequence');
                    ?>
                    <span><?=__('Taxes'); ?></span>
                    <div class="box-body table-responsive no-padding">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col"><?=__('name')?></th>
                                    <th scope="col"><?=__('value')?></th>
                                    <th scope="col" class="text-right"><?=__('actions')?></th>
                                </tr>
                                <tr>
                                    <th><input name="new_name" class="form-control" maxlength="180" id="new-name" type="text" placeholder="<?=__('new name')?>"></th>
                                    <th><input name="new_tax" class="form-control" maxlength="180" id="new-tax" type="number" placeholder="<?=__('new tax')?>"></th>
                                    <th class="text-right"><a href="#" id="add_tax" class="btn btn-info" onclick="addElement();"><?=__('add')?></a></th>
                            </thead>

                            <tbody id="tbody">
                                <?php $i = 0; ?>
                                <?php if (!empty($price['taxes'])) : ?>
                                    <?php foreach ($price['taxes'] as $tax) : ?>
                                        <tr class="taxes-<?=$i?>">
                                            <td><?=$this->Form->control('taxes.' . $i . '.name', ['label' => false]);?></td>
                                            <td><?=$this->Form->control('taxes.' . $i . '.value', ['label' => false]);?></td>
                                        </tr>
                                        <?php $i++; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->

                <?php echo $this->Form->submit(__('Submit')); ?>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>
<script>
    function nextIndex(prntElement, tgElement)
    {
        console.log('prntElement.childElementCount: ' + prntElement.childElementCount);

        if (prntElement.childElementCount === 0)
            return 0;

        var p = prntElement.lastChild.id; // id do elemento pai

        console.log('p: ' + p);

        var i = p.substr(tgElement.length, p.length);

        console.log('p.length: ' + p.length);
        console.log('tgElement.length: ' + tgElement.length);
        console.log('i: ' + i);

        if (i == '')
            i = 0;
        else
        ++i;

        return i;
    }

    //    function createInput(tag, index, name, className)
    function createInput(properties)
    {
        var input = document.createElement('INPUT');
        if (properties.id) input.id = properties.id;
        if (properties.name) input.name = properties.name;
        if (properties.className) input.className = properties.className;
        if (properties.type) input.type = properties.type;
        if (properties.maxLength) input.maxLength = properties.maxLength;
        if (properties.value) input.value = properties.value;
        if (properties.required) {
            var attrib = document.createAttribute('required');
            attrib.value = 'required';
            input.attributes.setNamedItem(attrib);
        }

        return input;
    }

    function createTd(properties)
    {
        var td = document.createElement('TD');
        if (properties.id) td.id = properties.id;
        return td;
    }

    function createTr(properties)
    {
        var tr = document.createElement('TR');
        if (properties.id) tr.id = properties.id;
        return tr;
    }

    function createButton(properties)
    {
        console.log(properties.innerHtml);
        var bt = document.createElement('BUTTON');
        if (properties.id) bt.id = properties.id;
        if (properties.className) bt.className = properties.className;
        if (properties.innerHTML) bt.innerHTML = properties.innerHTML;
        return bt;
    }

    function addElement()
    {
        var newName = document.getElementById('new-name');
        var newValue = document.getElementById('new-tax');

        if (newName.value === '' || newValue.value === '') {
            alert('Nome e valor da taxa devem ser digitados.');
            return;
        }

        var tbody_container = document.getElementById('tbody');
        var index = nextIndex(tbody_container, 'taxes-');
        var trProp = {
            id: 'taxes-' + index
        };
        var tdProp = {};
        var tr = createTr(trProp);
        var nameProp = {
            id: 'taxes-' + index + '-name',
            name: 'taxes[' + index + '][name]',
            className: 'form-control',
            maxLength: 180,
            type: 'text',
            value: newName.value,
            required: true
        };
        var valueProp = {
            id: 'taxes-' + index + '-value',
            name: 'taxes[' + index + '][value]',
            className: 'form-control',
            maxLength: 180,
            type: 'text',
            value: newValue.value,
            required: true
        };

        var buttonProp = {
            id: 'botao',
            className: 'btn btn-info btn-xs',
        };

        var inputName = createInput(nameProp);
        var td = createTd(tdProp);
        td.append(inputName);
        tr.append(td);

        var inputValue = createInput(valueProp);
        td = createTd(tdProp);
        td.append(inputValue);
        tr.append(td);

        buttonProp.innerHTML = 'edit';
        var buttonEdit = createButton(buttonProp);
        td = createTd(tdProp);
        td.append(buttonEdit);
        tr.append(td);

        tbody.append(tr);

        newName.value = '';
        newValue.value = '';
            newName.focus();
        }
</script>
