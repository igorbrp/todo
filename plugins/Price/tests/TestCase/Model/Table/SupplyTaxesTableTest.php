<?php
namespace Price\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Price\Model\Table\SupplyTaxesTable;

/**
 * Price\Model\Table\SupplyTaxesTable Test Case
 */
class SupplyTaxesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Price\Model\Table\SupplyTaxesTable
     */
    public $SupplyTaxes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Price.SupplyTaxes',
        'plugin.Price.Supplies'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SupplyTaxes') ? [] : ['className' => SupplyTaxesTable::class];
        $this->SupplyTaxes = TableRegistry::getTableLocator()->get('SupplyTaxes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SupplyTaxes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
