<?php
namespace Price\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Price\Model\Table\TaxTypesTable;

/**
 * Price\Model\Table\TaxTypesTable Test Case
 */
class TaxTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Price\Model\Table\TaxTypesTable
     */
    public $TaxTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Price.TaxTypes',
        'plugin.Price.PriceTypes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TaxTypes') ? [] : ['className' => TaxTypesTable::class];
        $this->TaxTypes = TableRegistry::getTableLocator()->get('TaxTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TaxTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
