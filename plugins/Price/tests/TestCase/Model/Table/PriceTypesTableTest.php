<?php
namespace Price\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use Price\Model\Table\PriceTypesTable;

/**
 * Price\Model\Table\PriceTypesTable Test Case
 */
class PriceTypesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \Price\Model\Table\PriceTypesTable
     */
    public $PriceTypes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.Price.PriceTypes',
        'plugin.Price.TaxTypes'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PriceTypes') ? [] : ['className' => PriceTypesTable::class];
        $this->PriceTypes = TableRegistry::getTableLocator()->get('PriceTypes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PriceTypes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
