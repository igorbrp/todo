<section class="content-header">
    <h1>
        <?='Manutenção'?> <!-- TRADUZIR -->
        <small>&nbsp;</small>
    </h1>
    &nbsp;
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
			Requisições
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col" width="10%">&nbsp;</th>
                                <th scope="col" width="15%">Itens pedidos</th>
                                <th scope="col" width="15%">Compras estoque</th>
                                <th scope="col" width="15%">Soma</th>
                                <th scope="col" width="15%">Total de requisições</th>
				<th scope="col" width="15%">Diferença</th>
                                <th scope="col" width="15%" class="actions text-right"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
				<td></td>
                                <td><?= $countRequisitionItems ?></td>
                                <td><?= $countRequisitionPurchases ?></td>
                                <td><?= ($countRequisitionItems + $countRequisitionPurchases) ?></td>
                                <td><?= $countRequisitions ?></td>
				<td><?= $countRequisitions - ($countRequisitionItems + $countRequisitionPurchases)?></td>
                                <td class="actions text-right"></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
	    </div>

	    <div class="row">
		<div class="col-xs-12">
		    <div class="box">
			<div class="box-header">
			    <div>
				Requisições
			    </div>
			</div>
			<div class="box-body table-responsive no-padding">
			    <table class="table table-hover">
				<thead>
				    <tr>
					<th scope="col" width="10%">&nbsp;</th>
					<th scope="col" width="15%">registros em ItemsRequisitions</th>
					<th scope="col" width="15%">registros em PurchasesRequisitions</th>
					<th scope="col" width="45%">&nbsp;</th>
					<th scope="col" width="15%" class="actions text-right"><?= __('Actions') ?></th>
				    </tr>
				</thead>
				<tbody>
				    <tr>
					<td></td>
					<td><?= $countItemsRequisitions ?></td>
					<td><?= $countPurchasesRequisitions ?></td>
					<td class="actions text-right">
					    
					</td>
				    </tr>
				</tbody>
			    </table>
			</div>
		    </div>
		</div>
	    </div>

	    <div class="row">
		<div class="col-xs-12">
		    <div class="box">
			<div class="box-header">
			    <div>
				Estoque
			    </div>
			</div>
			<div class="box-body table-responsive no-padding">
			    <table class="table table-hover">
				<thead>
				    <tr>
					<th scope="col" width="10%">&nbsp;</th>
					<th scope="col" width="15%">entradas</th>
					<th scope="col" width="15%">saídas</th>
					<th scope="col" width="45%">diferença</th>
					<th scope="col" width="45%">estoque</th>
					<th scope="col" width="15%" class="actions text-right"><?= __('Actions') ?></th>
				    </tr>
				</thead>
				<tbody>
				    <tr>
					<td></td>
					<td><?= $countReceiveds ?></td>
					<td><?= $countOutputs ?></td>
					<td><?= ($countReceiveds - $countOutputs) ?></td>
					<td><?= $countStocks ?></td>
					<td class="actions text-right">
					    
					</td>
				    </tr>
				</tbody>
			    </table>
			</div>
		    </div>
		</div>
	    </div>
        </div>
            <!-- /.box -->
        </div>
    </div>
