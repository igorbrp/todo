<?php
namespace Maintenance\Controller;

use Maintenance\Controller\AppController;

/**
 * Maintenance Controller
 *
 *
 * @method \Maintenance\Model\Entity\Maintenance[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MaintenanceController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Requisitions');
        $this->loadModel('ItemsRequisitions');
        $this->loadModel('PurchasesRequisitions');
        $this->loadModel('Receiveds');
        $this->loadModel('Outputs');
        $this->loadModel('Stocks');
    }        
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        // total de requisicoes
        $requisitions = $this->Requisitions->find();
        $countRequisitions = $requisitions->count();
        $this->set(compact('countRequisitions'));

        // total de requisicoes de itens
        $requisitionItems = $this->Requisitions->find()->contain(['Items'])->innerJoinWith('Items');
        $countRequisitionItems = $requisitionItems->count();
        $this->set(compact('countRequisitionItems'));

        // total de requisicoes de compras de estoque
        $requisitionPurchases = $this->Requisitions->find()->contain(['Purchases'])->innerJoinWith('Purchases');
        $countRequisitionPurchases = $requisitionPurchases->count();
        $this->set(compact('countRequisitionPurchases'));

        // total de requisicoes de itens em itens requisitions
        $itemsRequisitions = $this->ItemsRequisitions->find();
        $countItemsRequisitions = $itemsRequisitions->count();
        $this->set(compact('countItemsRequisitions'));

        // total de requisicoes de estoque em purchases requisitions
        $purchasesRequisitions = $this->PurchasesRequisitions->find();
        $countPurchasesRequisitions = $purchasesRequisitions->count();
        $this->set(compact('countPurchasesRequisitions'));

        // entradas
        $receiveds = $this->Receiveds->find();
        $countReceiveds = $receiveds->count();
        $this->set(compact('countReceiveds'));

        // saídas
        $outputs = $this->Outputs->find();
        $countOutputs = $outputs->count();
        $this->set(compact('countOutputs'));

        // estoque
        $stocks = $this->Stocks->find();
        $countStocks = $stocks->count();
        $this->set(compact('countStocks'));
    }

    public function stockIndex()
    {
        
    }
}
