<?php
namespace Widgets\View\Cell;

use Cake\View\Cell;

/**
 * Items cell
 */
class ItemsCell extends Cell
{
    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {
    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function uncovereds()
    {
        $this->loadModel('Orders');

        $totalUncovereds = $this->Orders
                    ->find()
                    ->contain([
                        'Items'
                    ])
                    ->where(['Items.total_uncovered >' => 0])
                    ->innerJoinWith('Items')
                    ->count();
        $this->set('totalUncovereds', $totalUncovereds);
    }
}
