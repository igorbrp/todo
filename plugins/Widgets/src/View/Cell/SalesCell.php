<?php
namespace Widgets\View\Cell;
use Cake\I18n\Time;
use Cake\I18n\Date;

use Cake\View\Cell;

/**
 * Sales cell
 */
class SalesCell extends Cell
{
    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {
    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function ordersThisMonth()
    {
        $this->loadModel('Orders');

        $firstDay = new Date();
        $onLastMonth = new Date();
        $firstDayOnLastMonth = new Date();

        $firstDay->setDate(2020, $firstDay->month, 1);

        $onLastMonth->subMonth();

        $firstDayOnLastMonth->subMonth();
        $firstDayOnLastMonth->setDate($firstDayOnLastMonth->year, $firstDayOnLastMonth->month, 1);

        $salesMonth = $this->Orders
                    ->find()
                    ->where(['date_order >=' => $firstDay])
                    ->count();

        $salesLastMonth = $this->Orders
                        ->find()
                        ->where(['date_order >=' => $firstDayOnLastMonth])
                        ->andWhere(['date_order <=' => $onLastMonth])
                        ->count();

        $this->set('firstDayOnLastMonth', $firstDayOnLastMonth);
        $this->set('firstDay', $firstDay);
        $this->set('onLastMonth', $onLastMonth);
        
        $this->set('salesMonth', $salesMonth);
        $this->set('salesLastMonth', $salesLastMonth);
    }

    public function totalOrdersThisMonth()
    {
        $totalOrdersThisMonth = $this->Orders
                              ->find()
                              ->select(function (\Cake\ORM\Query $query) {
                                  return [
                                      'order_amount' => $query->func()->sum(
                                          $query->identifier('Items.price')
                                      ),
                                      'order_total_qtd' => $query->func()->sum(
                                          $query->identifier('Items.qtd')
                                      ),
                                  ];
                              })
                              ->innerJoinWith('Items')
                              ->group('Orders.id');

    }
}
