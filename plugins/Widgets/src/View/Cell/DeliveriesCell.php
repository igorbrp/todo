<?php
namespace Widgets\View\Cell;
use Cake\I18n\Time;
use Cake\I18n\Date;

use Cake\View\Cell;

/**
 * Deliveries cell
 */
class DeliveriesCell extends Cell
{
    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {
    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function todayScheduled()
    {
        $this->loadModel('Deliveries');

        $today = new Date();
        $today->setDate(2020, 2, 12);

        $scheduledDeliveries = $this->Deliveries
                    ->find()
                    ->contain([
                        'DeliveryMades'
                    ])
                    ->where(['DeliveryMades.scheduled_date' => $today])
                    ->group('Deliveries.id')
                    ->innerJoinWith('DeliveryMades')
                    ->count();
        $this->set('scheduledDeliveries', $scheduledDeliveries);
    }

    public function todayForeseen()
    {
        $this->loadModel('Deliveries');

        $today = new Date();
        $today->setDate(2020, 2, 12);

        $foreseenDeliveries = $this->Deliveries
                    ->find()
                    // ->contain([
                    //     'DeliveryMades'
                    // ])
                            //                    ->where(['DeliveryMades.foreseen_date' => $today])
                    ->where(['foreseen_date' => $today])
                            //                    ->group('Deliveries.id')
                            //                    ->innerJoinWith('DeliveryMades')
                    ->count();
        $this->set('foreseenDeliveries', $foreseenDeliveries);
    }
}
