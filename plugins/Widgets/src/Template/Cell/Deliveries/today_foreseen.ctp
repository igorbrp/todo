<div class="info-box bg-yellow">
    <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

    <div class="info-box-content">
        <span class="info-box-text">Entregas previstas hoje</span>
        <span class="info-box-number"><?=$foreseenDeliveries?></span>

        <div class="progress">
            <div class="progress-bar" style="width: 100%;"></div>
        </div>
        <span class="progress-description">
            previsão de entrega no pedido
        </span>
    </div>
    <!-- /.info-box-content -->
</div>
