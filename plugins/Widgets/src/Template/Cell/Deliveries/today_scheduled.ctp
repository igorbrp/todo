<div class="info-box bg-aqua">
    <span class="info-box-icon"><i class="fa fa-truck"></i></span>

    <div class="info-box-content">
        <span class="info-box-text">Entregas hoje</span>
        <span class="info-box-number"><?=$scheduledDeliveries?></span>

        <div class="progress">
            <div class="progress-bar" style="width: 100%;"></div>
        </div>
        <span class="progress-description">
            entregas marcadas por telefone
        </span>
    </div>
    <!-- /.info-box-content -->
</div>
