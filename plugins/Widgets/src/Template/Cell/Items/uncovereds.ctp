<div class="info-box bg-red">
    <span class="info-box-icon"><i class="fa fa-warning"></i></span>

    <div class="info-box-content">
        <span class="info-box-text">Itens pendentes</span>
        <span class="info-box-number"><?=$totalUncovereds?></span>

        <div class="progress">
            <div class="progress-bar" style="width: 100%;"></div>
        </div>
        <span class="progress-description">
            itens aguardando requisição
        </span>
    </div>
    <!-- /.info-box-content -->
</div>
