<?php
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'ProductLine',
    ['path' => '/product-line'],
    function (RouteBuilder $routes) {
        $routes->fallbacks(DashedRoute::class);
    }
);

Router::connect('/product-lines/', ['plugin' => 'ProductLine', 'controller' => 'ProductLines', 'action' => 'index']);
Router::connect('/product-lines/add/', ['plugin' => 'ProductLine', 'controller' => 'ProductLines', 'action' => 'add']);
Router::connect('/product-lines/edit/*', ['plugin' => 'ProductLine', 'controller' => 'ProductLines', 'action' => 'edit']);
Router::connect('/product-lines/view/*', ['plugin' => 'ProductLine', 'controller' => 'ProductLines', 'action' => 'view']);
Router::connect('/product-lines/delete/*', ['plugin' => 'ProductLine', 'controller' => 'ProductLines', 'action' => 'delete']);
Router::connect('/product-lines/delete-some-records/*', ['plugin' => 'ProductLine', 'controller' => 'ProductLines', 'action' => 'delete-some-records']);

Router::connect('/product-lines/teste/', ['plugin' => 'ProductLine', 'controller' => 'ProductLines', 'action' => 'teste']);
