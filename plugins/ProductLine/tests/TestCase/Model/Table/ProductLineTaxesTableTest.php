<?php
namespace ProductLine\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use ProductLine\Model\Table\ProductLineTaxesTable;

/**
 * ProductLine\Model\Table\ProductLineTaxesTable Test Case
 */
class ProductLineTaxesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \ProductLine\Model\Table\ProductLineTaxesTable
     */
    public $ProductLineTaxes;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.ProductLine.ProductLineTaxes',
        'plugin.ProductLine.ProductLines'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProductLineTaxes') ? [] : ['className' => ProductLineTaxesTable::class];
        $this->ProductLineTaxes = TableRegistry::getTableLocator()->get('ProductLineTaxes', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductLineTaxes);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
