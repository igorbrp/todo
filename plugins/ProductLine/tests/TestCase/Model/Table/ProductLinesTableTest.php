<?php
namespace ProductLine\Test\TestCase\Model\Table;

use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;
use ProductLine\Model\Table\ProductLinesTable;

/**
 * ProductLine\Model\Table\ProductLinesTable Test Case
 */
class ProductLinesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \ProductLine\Model\Table\ProductLinesTable
     */
    public $ProductLines;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'plugin.ProductLine.ProductLines',
        'plugin.ProductLine.Supplies'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('ProductLines') ? [] : ['className' => ProductLinesTable::class];
        $this->ProductLines = TableRegistry::getTableLocator()->get('ProductLines', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProductLines);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
