<?php
namespace ProductLine\Controller;

use ProductLine\Controller\AppController;

use Cake\Collection\Collection;

/**
 * ProductLines Controller
 *
 * @property \ProductLine\Model\Table\ProductLinesTable $ProductLines
 *
 * @method \ProductLine\Model\Entity\ProductLine[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductLinesController extends AppController
{
    // public function teste()
    // {
    //     $productLine = $this->ProductLines->get(3, [
    //         'contain' => ['ProductLineTaxes']
    //     ]);

    //     $productLineTaxes = $this->mergeWithTaxes(3, $productLine->product_line_taxes);

    //     $this->set(compact('productLineTaxes'));
    // }

    public function mergeWithTaxes($supply_id, $childTaxes)
    {
        $taxes = [];

        $globalModel = $this->loadModel('Price.Taxes');
        $globalTaxes = $globalModel
                     ->find()
                     ->contain(['Prices' => ['fields' => ['id', 'name', 'sequence', 'active']]])
                     ->order(['Prices.sequence'])
                     ->where(['Prices.active' => true])
                     ->all();

        $supplyModel = $this->loadModel('Supply.SupplyTaxes');
        $supplyTaxes = $supplyModel
                      ->find()
                     //                      ->contain(['Prices' => ['fields' => ['id', 'name', 'sequence', 'active']]])
                     //                      ->order(['Prices.sequence'])
                      ->where(['supply_id' => $supply_id])
                      ->all();

        foreach ($globalTaxes as $tax) {
            $tax['inherited'] = 1;
            $tax['inherited_value'] = $tax['value'];
            $tax['cost_name'] = $tax['price']['name'];
            $taxes[] = $tax;
        }

        $names = array_column($taxes, 'name');

        foreach ($supplyTaxes as $supplyTax) {
            $key = array_search(trim($supplyTax['name']), $names);
            if ($key !== false) {
                //                $taxes[$key]['price_id'] = $supplyTax['price_id'];
                $taxes[$key]['model'] = $supplyTax['model'];
                $taxes[$key]['name'] = $supplyTax['name'];
                $taxes[$key]['value'] = $supplyTax['value'];
                $taxes[$key]['calculation'] = $supplyTax['calculation'];
                //                $taxes[$key]['inherited'] = 0;
            }
        }

        foreach ($childTaxes as $childTax) {
            $key = array_search(trim($childTax['name']), $names);
            if ($key !== false) {
                //                $taxes[$key]['price_id'] = $childTax['price_id'];
                $taxes[$key]['model'] = $childTax['model'];
                $taxes[$key]['name'] = $childTax['name'];
                $taxes[$key]['value'] = $childTax['value'];
                $taxes[$key]['calculation'] = $childTax['calculation'];
                $taxes[$key]['inherited'] = 0;
            }
        }

        return $taxes;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Supplies']
        ];
        $productLines = $this->paginate($this->ProductLines);

        $this->set(compact('productLines'));
    }

    /**
     * View method
     *
     * @param string|null $id Product Line id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $productLine = $this->ProductLines->get($id, [
            'contain' => ['Supplies']
        ]);

        $this->set('productLine', $productLine);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $productLine = $this->ProductLines->newEntity();
        if ($this->request->is('post')) {
            $productLine = $this->ProductLines->patchEntity($productLine, $this->request->getData());
            if ($this->ProductLines->save($productLine)) {
                $this->Flash->success(__('The product line has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product line could not be saved. Please, try again.'));
        }
        $supplies = $this->ProductLines->Supplies->find('list', ['limit' => 200]);
        $this->set(compact('productLine', 'supplies'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Product Line id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $productLine = $this->ProductLines->get($id, [
            'contain' => ['ProductLineTaxes']
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            debug($data);
            if (!empty($data['product_line_taxes'])) {
                $collection = new Collection($data['product_line_taxes']);
                $valids = $collection->reject(function ($t, $k) {
                    return $t['inherited'] == '1';
                });

                $data['product_line_taxes'] = $valids->toList();

                // for($i = 0; $i < count($data['supply_taxes']); $i++)
                //     $data['supply_taxes'][$i]['model'] = $this->Supplies->registryAlias();
            }

            //            debug($data);

            $productLine = $this->ProductLines->patchEntity($productLine, $data, [
                'associated' => [
                    'ProductLineTaxes'
                    ]
                ]);

            //            debug($productLine);die();

            if ($this->ProductLines->save($productLine)) {
                $this->Flash->success(__('The product line has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product line could not be saved. Please, try again.'));
        }

        $productLine->product_line_taxes = $this->mergeWithTaxes($productLine->supply_id, $productLine->product_line_taxes);

        $supplies = $this->ProductLines->Supplies->find('list', ['limit' => 200]);
        $this->set(compact('productLine', 'supplies'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Product Line id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $productLine = $this->ProductLines->get($id);
        if ($this->ProductLines->delete($productLine)) {
            $this->Flash->success(__('The product line has been deleted.'));
        } else {
            $this->Flash->error(__('The product line could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deleteSomeRecords($result = null)
    {
        if ($this->request->is('post')) {
            $ids = explode(",", $this->request->getData('ids'));

            $query = $this->ProductLines->query();
            $query->delete();

            foreach($ids as $id)
                $query->orWhere(['id' => $id]);

            if($query->execute()) {
                $this->Flash->success(__('The product lines has been deleted.'));
            } else {
                $this->Flash->error(__('The product lines could not be deleted. Please, try again.'));
            }
        } else {
            $data = '';
        }

        return $this->redirect(['action' => 'index']);
    }
}
