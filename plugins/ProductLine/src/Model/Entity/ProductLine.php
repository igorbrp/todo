<?php
namespace ProductLine\Model\Entity;

use Cake\ORM\Entity;

/**
 * ProductLine Entity
 *
 * @property int $id
 * @property int|null $supply_id
 * @property string $name
 * @property int|null $deadline
 * @property bool $working_days
 * @property bool $deadline_inherited
 * @property float|null $maximum_discount
 * @property bool $maximum_discount_inherited
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \ProductLine\Model\Entity\Supply $supply
 */
class ProductLine extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'supply_id' => true,
        'name' => true,
        'deadline' => true,
        'working_days' => true,
        'deadline_inherited' => true,
        'maximum_discount' => true,
        'maximum_discount_inherited' => true,
        'created' => true,
        'modified' => true,
        'supply' => true,
        'product_line_taxes' => true
    ];
}
