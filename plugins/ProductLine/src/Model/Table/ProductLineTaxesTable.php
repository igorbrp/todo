<?php
namespace ProductLine\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProductLineTaxes Model
 *
 * @property \ProductLine\Model\Table\ProductLinesTable&\Cake\ORM\Association\BelongsTo $ProductLines
 *
 * @method \ProductLine\Model\Entity\ProductLineTax get($primaryKey, $options = [])
 * @method \ProductLine\Model\Entity\ProductLineTax newEntity($data = null, array $options = [])
 * @method \ProductLine\Model\Entity\ProductLineTax[] newEntities(array $data, array $options = [])
 * @method \ProductLine\Model\Entity\ProductLineTax|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \ProductLine\Model\Entity\ProductLineTax saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \ProductLine\Model\Entity\ProductLineTax patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \ProductLine\Model\Entity\ProductLineTax[] patchEntities($entities, array $data, array $options = [])
 * @method \ProductLine\Model\Entity\ProductLineTax findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductLineTaxesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('product_line_taxes');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ProductLines', [
            'foreignKey' => 'product_line_id',
            'joinType' => 'INNER',
            'className' => 'ProductLine.ProductLines'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->nonNegativeInteger('foreign_key')
            ->allowEmptyString('foreign_key');

        $validator
            ->scalar('model')
            ->maxLength('model', 128)
            ->allowEmptyString('model');

        $validator
            ->scalar('name')
            ->maxLength('name', 180)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->decimal('value')
            ->allowEmptyString('value');

        $validator
            ->scalar('calculation')
            ->requirePresence('calculation', 'create')
            ->notEmptyString('calculation');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_line_id'], 'ProductLines'));

        return $rules;
    }
}
