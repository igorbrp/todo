<?php
namespace ProductLine\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ProductLines Model
 *
 * @property \ProductLine\Model\Table\SuppliesTable&\Cake\ORM\Association\BelongsTo $Supplies
 *
 * @method \ProductLine\Model\Entity\ProductLine get($primaryKey, $options = [])
 * @method \ProductLine\Model\Entity\ProductLine newEntity($data = null, array $options = [])
 * @method \ProductLine\Model\Entity\ProductLine[] newEntities(array $data, array $options = [])
 * @method \ProductLine\Model\Entity\ProductLine|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \ProductLine\Model\Entity\ProductLine saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \ProductLine\Model\Entity\ProductLine patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \ProductLine\Model\Entity\ProductLine[] patchEntities($entities, array $data, array $options = [])
 * @method \ProductLine\Model\Entity\ProductLine findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductLinesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('product_lines');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Supplies', [
            'foreignKey' => 'supply_id',
            'className' => 'ProductLine.Supplies'
        ]);

        $this->hasMany('ProductLineTaxes', [
            'foreignKey' => 'product_line_id',
            'bindingKey' => 'id',
            'className' => 'ProductLine.ProductLineTaxes',
            // 'conditions' => ['Taxes.model' => 'Supply.Supplies'],
            'cascadeCallbacks' => true,
            'dependent' => true,
            'saveStrategy' => 'replace'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 120)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator
            ->integer('deadline')
            ->allowEmptyString('deadline');

        $validator
            ->boolean('working_days')
            ->notEmptyString('working_days');

        $validator
            ->boolean('deadline_inherited')
            ->notEmptyString('deadline_inherited');

        $validator
            ->decimal('maximum_discount')
            ->allowEmptyString('maximum_discount');

        $validator
            ->boolean('maximum_discount_inherited')
            ->notEmptyString('maximum_discount_inherited');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['supply_id'], 'Supplies'));

        return $rules;
    }
}
