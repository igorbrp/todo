<?php
    /**
     * @var \App\View\AppView $this
     * @var \Cake\Datasource\EntityInterface $productLine
     */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	    <?=__('Product Line')?>
	    <small><?= __('Edit'); ?></small>
    </h1>
    <?=$this->element('MenuHeader')?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <?php echo $this->Form->create($productLine, ['role' => 'form']); ?>
            <div class="nav-tabs-custom">
	            <ul class="nav nav-tabs">
	                <li class="active">
                        <a href="#identity" data-toggle="tab" aria-expanded="true"><?=__('Identification')?></a>
                    </li>
	                <li class="">
                        <a href="#properties" data-toggle="tab" aria-expanded="false"><?=__('Properties')?></a>
                    </li>
	                <li class="">
                        <a href="#prices" data-toggle="tab" aria-expanded="false"><?=__('Prices')?></a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="identity">
		                <!-- identity content -->
                        <?php
                            echo $this->Form->select('supply_id', $supplies, [
                                'class' => 'select2',
                                'style' => 'width: 100%',
                            ]);
                            echo $this->Form->control('name');
                        ?>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="properties">
		                <!-- properties content -->
                        <?php
                            echo $this->Form->control('deadline');
                            echo $this->Form->control('working_days');
                            echo $this->Form->control('deadline_inherited');
                            echo $this->Form->control('maximum_discount');
                            echo $this->Form->control('maximum_discount_inherited');
                        ?>
                    </div>
                    <div class="tab-pane" id="prices">
                        <div class="box-body table-responsive no-padding">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th scope="col"><?=__('price')?></th>
                                        <th scope="col"><?=__('inherited')?></th>
                                        <th scope="col"><?=__('name')?></th>
                                        <th scope="col"><?=__('value')?></th>
                                        <th scope="col"><?=__('calculation')?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 0; ?>
                                    <?php $oldPrice = '' ?>
                                    <?php foreach ($productLine->product_line_taxes as $tax): ?>
                                        <?php if ($tax['price_id'] != $oldPrice): ?>
                                            <tr>
                                                <td><h5><?=$tax['price']['name']?></h5></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                            </tr>
                                            <?php $oldPrice = $tax['price_id']; ?>
                                        <?php endif; ?>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>
                                                <?=$this->Form->checkbox('product_line_taxes.' . $i . '.inherited', [
                                                    'id' => 'product_line_taxes-' . $i . '-inherited',
                                                    'checked' => $tax['inherited'] == 1 ? true : false,
                                                    'value' => $tax['inherited'],
                                                    'onclick' => 'toggleHeritage("' . $i . '")'
                                                    ]);?>
                                            </td>
                                            <td>
                                                <?=$tax->name?>
                                                <?=$this->Form->input('product_line_taxes.' . $i . '.name', ['type' => 'hidden'])?>
                                            </td>
                                            <td><?=$this->Form->control('product_line_taxes.' . $i . '.value', [
                                                    'id' => 'product_line_taxes-' . $i . '-value',
                                                    'label' => false,
                                                    'readonly' => $tax['inherited'] ? true : false
                                                    ])?>
                                                <input type="hidden" id="inherited-value-<?=$i?>" value="<?=$tax->inherited_value?>">
                                                <?=$this->Form->input('product_line_taxes.' . $i . '.price_id', ['type' => 'hidden'])?>
                                                <?=$this->Form->input('product_line_taxes.' . $i . '.id', ['type' => 'hidden'])?>
                                                <?=$this->Form->input('product_line_taxes.' . $i . '.foreign_key', ['type' => 'hidden'])?>
                                            </td>
                                            <td>
                                                <?php
                                                    echo $this->Form->select('product_line_taxes.' . $i . '.calculation', [
                                                        '%' => __('percentage'),
                                                        '$' => __('currency')
                                                    ], [
                                                        //                                                        'id' => 'calculation',
                                                        'class' => 'select2',
                                                        'style' => 'width: 100%',
                                                    ]);
                                                ?>
                                            </td>
                                        </tr>
                                        <?php $i++; ?>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <!-- /.tab-content -->
                <?php echo $this->Form->submit(__('Submit')); ?>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>
<script>
    function toggleHeritage(i)
    {
        var chkb = document.getElementById('product_line_taxes-' + i + '-inherited');
        var inputValue = document.getElementById('product_line_taxes-' + i + '-value');
        if (chkb.checked) {
            inputValue.setAttribute('readonly', true);
            //            inputValue.readonly = true;
            inputValue.value = document.getElementById('inherited-value-' + i).value;
            chkb.value = 1;
        } else {
            inputValue.removeAttribute('readonly');
            //            inputValue.readonly = false;
            chkb.value = 0;
            inputValue.value = '';
            inputValue.focus();
        }
    }
</script>
<?php
    echo $this->Html->css([
        'AdminLTE./bower_components/select2/dist/css/select2.css'], [
            'block' => 'css'
        ]);
    echo $this->Html->script([
        'AdminLTE./bower_components/select2/dist/js/select2.js',
        'apply-select2',
    ], [
        'block' => 'script'
    ]);
?>
