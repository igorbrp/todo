<section class="content-header">
  <h1>
    Product Line
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
      <?=$this->element('MenuHeader')?>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Name') ?></dt>
            <dd><?= h($productLine->name) ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($productLine->id) ?></dd>
            <dt scope="row"><?= __('Supply Id') ?></dt>
            <dd><?= $this->Number->format($productLine->supply_id) ?></dd>
            <dt scope="row"><?= __('Deadline') ?></dt>
            <dd><?= $this->Number->format($productLine->deadline) ?></dd>
            <dt scope="row"><?= __('Maximum Discount') ?></dt>
            <dd><?= $this->Number->format($productLine->maximum_discount) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($productLine->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($productLine->modified) ?></dd>
            <dt scope="row"><?= __('Working Days') ?></dt>
            <dd><?= $productLine->working_days ? __('Yes') : __('No'); ?></dd>
            <dt scope="row"><?= __('Deadline Inherited') ?></dt>
            <dd><?= $productLine->deadline_inherited ? __('Yes') : __('No'); ?></dd>
            <dt scope="row"><?= __('Maximum Discount Inherited') ?></dt>
            <dd><?= $productLine->maximum_discount_inherited ? __('Yes') : __('No'); ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
