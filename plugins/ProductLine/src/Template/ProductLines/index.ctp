<style>
    .user-mini {
        margin:auto 10px auto auto;
        width: 25px;
        height: 25px;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Product Lines')?>
        <small><?=__('List'); ?></small>
    </h1>
    <?=$this->element('MenuHeader')?>
</section>
<!-- Main content -->
<h1>template do plugin</h1>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('List'); ?></h3>
                    <div class="box-tools">
                        <?=$this->element('Search')?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col"><label><input id="chk-all" type="checkbox" class="minimal" onchange="chkAllChange();"></label></th>
                                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('supply_id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('deadline') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('working_days') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                                <th scope="col" class="actions text-right"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($productLines as $productLine): ?>
                                <tr>
                                    <td>
                                        <?=$this->Form->checkbox('id', [
                                            'id' => 'chk-b',
                                            'name' => 'chkb',
                                            'class' => 'chk-box',
                                            'value' => $productLine->id,
                                            'onclick' => 'checkOrUncheck(this)'
                                            ]);?>
                                    </td>
                                    <td><?= h($productLine->name) ?></td>
                                    <td><?= h(!empty($productLine->supply->name) ? $productLine->supply->name : '') ?></td>
                                    <td><?= $this->Number->format($productLine->deadline) ?></td>
                                    <td><?= h($productLine->working_days == 1 ? __('yes') : __('no')) ?></td>
                                    <td><?= h($productLine->created) ?></td>
                                    <td><?= h($productLine->modified) ?></td>
                                    <td class="actions text-right">
                                        <?=$this->element('Actions', ['cod_id' => $productLine->id,])?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
<?php
    echo $this->Html->script([
        'js-base',
        'index',
    ], [
        'block' => 'scriptBottom'
    ]);
?>
