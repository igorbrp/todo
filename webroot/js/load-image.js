document.getElementById("bt-upload").onclick = function(e) {
    document.getElementById("submittedfile").click();
};

document.getElementById("submittedfile").onchange = function(e) {
    loadImage(
        e.target.files[0],
        function(img) {
            if(Number(e.target.files[0].size) > 2097152) {
                alert('O tamanho maximo para arquivos de imagem e de 2MB.');
                return;
            }
            var types = ['image/png', 'image/jpg', 'image/jpeg'];
            if(types.indexOf(e.target.files[0].type) === -1) {
                alert('O tipo do arquivo e invalido');
                return;
            }
            var target = document.getElementById("target_img");
            var old_image = document.getElementById("preview_image");
            img.removeAttribute("height");
            img.removeAttribute("width");
            img.setAttribute("class", "profile-user-img img-thumbnail");
            img.setAttribute("id", "preview_image");
            old_image.remove();
            target.appendChild(img);
        }
    );
};
