// js-base.js
var func, param;
function delSomeRecs(tag_id, ids, event)
{
    var container = document.getElementById(tag_id);
    var target = document.getElementById(ids);

    var arr = $(container).find('input:checkbox:checked').toArray();
    var plural = '';

    if(arr.length > 0) {
        if (arr.length > 1)
            plural = 's';
        if (confirm('Tem certeza de que deseja excluir este' + plural + ' registro' + plural + '?')) {
            var data = [];
            for(i = 0; i < arr.length; i++)
	            data[i] = $(arr[i]).val();

            $(target).val(data);
            return true;
        }
    }
    event.preventDefault();
    return false;
}

// novo ajax_post
function ajax_post(targeturl, data, ext, f, param)
{
    console.log('entrou em ajax_post');
    if(ext == 'json')
	    targeturl = targeturl.concat('.json');
    ajax_request(targeturl, data, ext, f, param, 'post', '');
}

// novo ajax_request
function ajax_request(targeturl, data, ext, f, param, type, enctype)
{
    console.log('csrfToken: ' + csrfToken);

    console.log('ajax_request: ' + targeturl);
    enctype = (enctype == '') ?  'application/x-www-form-urlencoded' : enctype;
    $.ajax({
     	type: type,
        headers: {'X-CSRF-Token': csrfToken},
        //	_ext: 'json',
        //	_ext: ext,
	    url: targeturl,
	    // inseri linha abaixo para upload de arquivos
        //	enctype: (enctype == '') ? 'application/x-www-form-urlencoded' : enctype,
	    enctype: enctype,
	    // data: (type == 'post') ? data : '',
	    data: data,
     	beforeSend: function(xhr) {
	        // coloquei enctype para upload de arquivo
	        // xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
     	    xhr.setRequestHeader('Content-type', enctype);
	    xhr.setRequestHeader('X-CSRF-Token', csrfToken);
	        // alert(enctype);
     	},
     	success: function(response) {
    	    if(response.error) {
                //    	    	modal_err_msg(response.error);
    	    	alert(response.error);
    	    } else {
		f(response, param);
    	    }
     	},
     	error: function(e) {
     	    alert("An error occurred: " + e.responseText.message);
     	    console.log('error: ' + e);
     	}
    });
//    console.log('csrfToken: ' + getCookie('csrfToken'));
}

// function getCookie(cname) {
//     var name = cname + "=";
//     var decodedCookie = decodeURIComponent(document.cookie);
//     var ca = decodedCookie.split(';');
//     for(var i = 0; i <ca.length; i++) {
//         var c = ca[i];
//         while (c.charAt(0) == ' ') {
//             c = c.substring(1);
//         }
//         if (c.indexOf(name) == 0) {
//             return c.substring(name.length, c.length);
//         }
//     }
//     return "";
// }

/**********************************************************************************
 * modal_confirm, modal_msg, modal_err_msg, modal_ok_msg
 **********************************************************************************/
function modal_dispatch(title, frm, f, p) {
// frm: formulário em html
    func = f;
    param = p;
    $('#bt_modal_confirm').text('ok');
    $('#bt_modal_confirm').show();
    $('#bt_modal_close').text('cancelar');
    $('#bt_modal_close').show();
    $('#modal_title').text(title);
    $('#modal_msg_body').html(frm);
    $('#main_modal').modal();
    $('#modal-datepicker').datepicker({
        autoclose: true,
        language: 'pt-BR'
    });
    // $('.select2-modal').select2({
    //     theme: 'classic',
    // });
    // $('#modal-vehicle').select2({
    //     theme: 'classic',
    // });
}

function modal_invoice(title, frm, f, p) {
// frm: formulário em html
    func = f;
    param = p;
    $('#bt_modal_confirm').text('ok');
    $('#bt_modal_confirm').show();
    $('#bt_modal_close').text('cancelar');
    $('#bt_modal_close').show();
    $('#modal_title').text(title);
    $('#modal_msg_body').html(frm);
    $('#main_modal').modal();
    // $('#modal-datepicker').datepicker({
    //     autoclose: true,
    //     language: 'pt-BR'
    // });
    // $('.select2-modal').select2({
    //     theme: 'classic',
    // });
    // $('#modal-vehicle').select2({
    //     theme: 'classic',
    // });
}

function modal_form(title, frm, f, p) {
// frm: formulário em html
    func = f;
    param = p;
    $('#bt_modal_confirm').text('ok');
    $('#bt_modal_confirm').show();
    $('#bt_modal_close').text('cancelar');
    $('#bt_modal_close').show();
    $('#modal_title').text(title);
    $('#modal_msg_body').html(frm);
    $('#main_modal').modal();
}

function modal_confirm(title, message, f, p) {
    func = f;
    param = p;
    $('#bt_modal_confirm').text('sim');
    // $('#bt_modal_confirm').attr('data-function', f);
    // $('#bt_modal_confirm').attr('data-param', p);
    $('#bt_modal_confirm').show();
    $('#bt_modal_close').text('não');
    $('#bt_modal_close').show();
    $('#modal_title').text(title);
    $('#modal_msg_body').html(message);
    $('#main_modal').modal();
}

function modal_confirm_danger(title, message, f, p) {
    $('#main_modal').removeClass('modal-default');
    $('#main_modal').addClass('modal-danger');
    $('#modal_icon').html('<i class="fa fa-trash"></i>');
    modal_confirm(title, message, f, p);
}

function modal_confirm_warning(title, message, f, p) {
    $('#main_modal').removeClass('modal-default');
    $('#main_modal').addClass('modal-warning');
    $('#modal_icon').html('<i class="fa fa-warning"></i>');
    modal_confirm(title, message, f, p);
}

function modal_warning(title, message) {
    $('#main_modal').removeClass('modal-info');
    $('#main_modal').removeClass('modal-danger');
    $('#main_modal').removeClass('modal-default');
    $('#main_modal').addClass('modal-warning');
    $('#modal_icon').html('<i class="fa fa-warning"></i>');
    modal_msg(title, message);
}

function modal_danger(title, message) {
    $('#main_modal').removeClass('modal-info');
    $('#main_modal').removeClass('modal-warning');
    $('#main_modal').removeClass('modal-default');
    $('#main_modal').addClass('modal-danger');
    $('#modal_icon').html('<i class="fa fa-frown-o"></i>');
    modal_msg(title, message);
}

function modal_msg(title, message) {
    $('#bt_modal_confirm').hide();
    $('#bt_modal_close').text('OK');
    $('#bt_modal_close').show();
    $('#modal_title').text(title);
    $('#modal_msg_body').html(message);
    setTimeout(function(){$('#main_modal').modal();}, 500);
}

function modal_err_msg(message) {
    $('#main_modal').removeClass('modal-info');
    $('#main_modal').removeClass('modal-warning');
    $('#main_modal').removeClass('modal-default');
    $('#main_modal').addClass('modal-danger');
    $('#modal_icon').html('<i class="fa fa-frown-o"></i>');

    var title = "Erro";
    modal_msg(title, message);
}

function modal_ok_msg(message) {
    $('#main_modal').removeClass('modal-default');
    $('#main_modal').addClass('modal-success');
    $('#modal_icon').html('<i class="fa fa-hand-peace-o"></i>');
    var title = "Sucesso";
    modal_msg(title, message);
}

function modal_upload_file() {
    $('#mdalert').removeClass('alert-danger alert-warning');
    $('#mdalert').addClass('alert-success');
    $('#main_modalLabel').text('Selecione um arquivo para enviar');
    $('#bt_modal_confirm').text('escolher');
    $('#bt_modal_confirm').show();
    $('#bt_modal_close').text('cancelar');
    $('#bt_modal_close').show();
    $('#msgModal').text('Aqui entra o texto');
    setTimeout(function(){$('#main_modal').modal();}, 500);
}

$(document).ready(function() {
//    console.log('document.ready func: ' + func);
    $('#bt_modal_confirm').on( 'click', function(){
	console.log('clicou em modal confirm');
	$('#main_modal').modal('hide');
//	alert('param: ' + param);
	if (func !== undefined && param !== undefined) {
	    param.response = true;
	    console.log(func);
	    func(param);
	}
    });
    $('#bt_modal_close').on( 'click', function(){
	console.log('clicou em modal close');
	$('#main_modal').modal('hide');
//	alert('param: ' + param);
	if (func !== undefined && param !== undefined) {
	    param.response = false;
	    console.log(func);
//	    func(param);
	}
    });
});
/* final de funcoes modal
 **********************************************************************************/

/**********************************************************************************
 * ajax_request, ajax_post, ajax_get
 * envia requisições ajax post
 * targeturl: url
 * data: array de dados
 * func: funcao de retorno
 * param: parametros da funcao de retorno
 **********************************************************************************/
// inseri variavel enctype para upload de arquivo
// function ajax_request(targeturl, data, ext, func, param, type, enctype)
// {
//     console.log('entrou em ajax_request');
//     console.log(targeturl);
//     console.log(data);

//     enctype = (enctype == '') ?  'application/x-www-form-urlencoded' : enctype;
//     $.ajax({
//      	type: type,
//         'X-CSRF-Token': csrfToken,
// //	_ext: 'json',
// //	_ext: ext,
// 	url: targeturl,
// 	// inseri linha abaixo para upload de arquivos
// //	enctype: (enctype == '') ? 'application/x-www-form-urlencoded' : enctype,
// 	enctype: enctype,
// 	// data: (type == 'post') ? data : '',
// 	data: data,
//      	beforeSend: function(xhr) {
// 	    // coloquei enctype para upload de arquivo
// 	    // xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
//      	    xhr.setRequestHeader('Content-type', enctype);
// 	    // alert(enctype);
//      	},
//      	success: function(response) {
//     	    if(response.error) {
// //    	    	modal_err_msg(response.error);
//     	    	alert(response.error);
//     	    } else {
// 		func(response, param);
//     	    }
//      	},
//      	error: function(e) {
//      	    alert("An error occurred: " + e.responseText.message);
//      	    console.log('error: ' + e);
//      	}
//     });
// }
function ajax_get(targeturl, data, ext, f, param)
{
    console.log('ajax_get: ' + targeturl);
    if(ext == 'json')
	    targeturl = targeturl.concat('.json?');
    else
	    targeturl = targeturl.concat('?');
//    targeturl = targeturl.concat($.param(data));

    console.log('ajax_get 2: ' + targeturl);
    ajax_request(targeturl, data, ext, f, param, 'get', '');
}
// function ajax_post(targeturl, data, ext, func, param)
// {
//     console.log('entrou em ajax_post');
//     if(ext == 'json')
// 	targeturl = targeturl.concat('.json');
//     ajax_request(targeturl, data, ext, func, param, 'post', '');
// }
/* final de ajax_post
 **********************************************************************************/

function validator(arr_input)
{
    var valid = true;
    for(i = 0; i < arr_input.length; i++)
	if($(arr_input[i]).val() == '')
	    valid = false;
    return valid;
}

function decimalComma2Point(num)
{
    var res = num.replace(",", ".");
    return Number(res);
}

function totalPayments()
{
    var sum = 0;
    var obj_prices = $('#payments').find("[class='payment_value']");
    console.log(obj_prices);
    for(k = 0; k < obj_prices.length; k++) {
	console.log(obj_prices[k].value);
    	sum += Number(obj_prices[k].value);
	//	     console.log(sum);
    }
    return sum;
}

function totalItems()
{
    var sum = 0;
    var obj_prices = $('#items').find("[class='price']");
    //	console.log(obj_prices);
    for(k = 0; k < obj_prices.length; k++) {
	console.log(obj_prices[k].value);
    	sum += Number(obj_prices[k].value);
	//	    console.log(sum);
    }
    return sum;
    //	console.log(formatCurrency(sum));
}

// function formatCurrency(num)
// {
//     // https://stackoverflow.com/questions/6124644/format-as-currency-in-javascript
//     // adaptei para formato R$ com virgula decimal e pontos
//     if(typeof num !== 'undefined')
// 	num = num.toString().replace(/\$|\,/g, '');
//     if (isNaN(num)) {
//         num = "0";
//     }

//     sign = (num == (num = Math.abs(num)));
//     num = Math.floor(num * 100 + 0.50000000001);
//     cents = num % 100;
//     num = Math.floor(num / 100).toString();

//     if (cents < 10) {
//         cents = "0" + cents;
//     }
//     for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++) {
//         num = num.substring(0, num.length - (4 * i + 3)) + '.' + num.substring(num.length - (4 * i + 3));
//     }

//     return (((sign) ? '' : '-') + 'R$' + num + ',' + cents);
// }

// tooltip
$(document).ready(function() {
    $(document).ready(function(){
	$('[data-toggle="tooltip"]').tooltip();
    });
});

// ============================================================================================================
// esse trecho deverá sair daqui para um logar mais apropriado
if (document.getElementById('sented') !== null) {
    if (document.getElementById('sented').checked) {
	$('#collapseExample').collapse('show');
    }
}

if(document.getElementById('selectsupply') !== null && document.getElementById('selectsupply').value != '') {
    $('#collapseExample').collapse('show');
}

if(document.getElementById('select-type') !== null && document.getElementById('select-type').value != '') {
    $('#collapseExample').collapse('show');
}

if(document.getElementById('items-tot-uncovered') !== null) {
    if(document.getElementById('items-tot-uncovered').checked ||
       document.getElementById('items-tot-required').checked ||
       document.getElementById('items-tot-received').checked ||
       document.getElementById('items-tot-dispatched').checked ||
       document.getElementById('items-partially').checked ||
       document.getElementById('items-completed').checked) {
        $('#collapseExample').collapse('show');
    }
}

if(document.getElementById('foreseen') !== null) {
    if(document.getElementById('foreseen').value != '' ||
       document.getElementById('scheduled').value != '' ||
       document.getElementById('delivery').value != '' ||
       document.getElementById('vehicle').value != '' ||
       document.getElementById('completeds').checked != '')  {
        $('#collapseExample').collapse('show');
    }
}

if(document.getElementById('select-employee') !== null && document.getElementById('select-employee').value != '') {
    $('#collapseExample').collapse('show');
}

if(document.getElementById('select-subsidiary') !== null && document.getElementById('select-subsidiary').value != '') {
    $('#collapseExample').collapse('show');
}

if(document.getElementById('order-total-uncovered') !== null) {
    if(document.getElementById('order-total-uncovered').checked ||
       document.getElementById('order-total-required').checked ||
       document.getElementById('order-total-received').checked ||
       document.getElementById('order-total-dispatched').checked ||
       document.getElementById('order-partially').checked ||
       document.getElementById('order-completed').checked) {
        $('#collapseExample').collapse('show');
    }
}

$(function () {
  $('[data-toggle="popover"]').popover()
})
//
// ============================================================================================================
