// common_masks.js
$(function() {
    // Inputmask.extendAliases({
    //     real: {
    //         prefix: "₱ ",
    //         groupSeparator: ".",
    //         alias: "numeric",
    //         placeholder: "0",
    //         autoGroup: !0,
    //         digits: 2,
    //         digitsOptional: !1,
    //         clearMaskOnLostFocus: !1
    //     }
    // });

    $(".date").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/aaaa"});
    $(".phone").inputmask('(99) 9999-9999');
    $(".mobile").inputmask('(99) 9 9999-9999');
    $(".cpf").inputmask('999.999.999-99');
    $(".cnpj").inputmask('99.999.999/9999-99');
//    $(".cpf_cnpj").inputmask(['999.999.999-99', '99.999.999/9999-99']);
    //    $(".cpf_cnpj").inputmask('999.999.999-99'|'99.999.999/9999-99');
    $(".cpf_cnpj").inputmask({
	mask: ['999.999.999-99', '99.999.999/9999-99'],
//	keepStatic: true
    });
    $(".cep").inputmask('99999-999');
    $(".currency").inputmask("currency", {radixPoint: ",", prefix: ""});

    // email mask
    $(".email").inputmask({
	mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
	greedy: false,
	onBeforePaste: function (pastedValue, opts) {
	    pastedValue = pastedValue.toLowerCase();
	    return pastedValue.replace("mailto:", "");
	},
	definitions: {
	    '*': {
		validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]", casing: "lower"
	    }
	}
    });
    // select2
    //    $(".select2").select2();
});
