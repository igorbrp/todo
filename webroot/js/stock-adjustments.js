function rSelectProductsBySupplyHtml(response, param)
{
//    console.log(response);
    $('#product-id').html('<option value="">escolha...</option>' + response);
    $('#span-products-count').text('(' + (Number($('#product-id option').length) - 1) + ')');
    $('#current-stock').val('');
    console.log('size of product-id: ' + $('#product-id option').length);
}

function rStockQtd(response, param)
{
    console.log('retorno em rStockQtd: ' + response);
    $('#current-stock').val(response);
    $('#qtd').attr('min', Number(response) * -1);
}

$(function() {
    $('#supply-id').change(function() {
	    var targeturl = '/products/select_stock_products_by_supply_html';
	    var data = {supply_id: $( '#supply-id' ).val()};
	    ajax_get(targeturl, data, '', rSelectProductsBySupplyHtml, '');
    });

    $('#product-id').change(function() {
	    var targeturl = '/stocks/stock_qtd';
	    var data = {product_id: $( '#product-id' ).val()};
	    ajax_get(targeturl, data, '', rStockQtd, '');
    });

    if ($('#current-stock').val() != '') {
	$('#qtd').attr('min', Number($('#current-stock').val()) * -1);
    } else {
	console.log('current_stock está vazio');
    }
    // $('#supply-name').blur(function() {
    // 	    document.getElementById('supply-name').value = document.getElementById('supply-selected').value;
    // });

});
