function getPrice(targeturl, data, field)
{
    var field_target = document.getElementById(field);
    targeturl = targeturl.concat($.param(data));
    console.log(targeturl);
    $.ajax({
        type: 'get',
        url: targeturl,
        success: function(response) {
            if(response.error) {
                alert(response.error);
            } else {
                if (typeof(response) == 'string')
                    response = JSON.parse(response);
                field_target.value = response.price;
                console.log(response.price);
            }
        },
        error: function(e) {
            alert("An error occurred: " + e.responseText.message);
     	    console.log('error: ' + e);
     	}
    });
}
