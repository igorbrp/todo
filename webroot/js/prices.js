var newName = document.getElementById('new-name');
var newValue = document.getElementById('new-tax');
var newCalculation = document.getElementById('new-calculation');
var tbody_container = document.getElementById('tbody');
var btCancel = document.getElementById('bt-cancel');

var edit = {
    index: false,
    targetTr: undefined,
    targetName: undefined,
    targetValue: undefined,
    targetCalculation: undefined,
};

function editElement(i)
{
    if(edit.index !== false)
        endEditElement();
    edit.index = i;
    edit.targetTr = document.getElementById('taxes-' + i);
    edit.targetName = document.getElementById('taxes-' + i + '-name');
    edit.targetValue = document.getElementById('taxes-' + i + '-value');
    edit.targetCalculation = document.getElementById('taxes-' + i + '-calculation');
    edit.targetTr.style.backgroundColor = '#ffffe6';
    document.getElementById('bt-edit-' + i).setAttribute('disabled', true);
    document.getElementById('bt-delete-' + i).setAttribute('disabled', true);
    btCancel.removeAttribute('disabled');
    newName.value = edit.targetName.value;
    newValue.value = edit.targetValue.value;
    newCalculation = edit.targetCalculation.value;
    newName.select();
}

function endEditElement()
{
    document.getElementById('bt-edit-' + edit.index).removeAttribute('disabled');
    document.getElementById('bt-delete-' + edit.index).removeAttribute('disabled');
    btCancel.setAttribute('disabled', true);
    edit.targetTr.style.removeProperty('background-color');
    edit.index = false;
}

function cancelEditElement()
{
    endEditElement();
    newName.value = newValue.value = '';
}

function deleteElement(i)
{
    document.getElementById('taxes-' + i + '-name').value = '';
    document.getElementById('taxes-' + i + '-value').value = '';
    document.getElementById('taxes-' + i + '-calculation').value = '%';
    document.getElementById('taxes-' + i).style.setProperty('display', 'none');
}

function getFirstHiddenTr(obj)
{
    for (i = 0; i < obj.length; i++) {
        //        if (obj.item(i).attributes.getNamedItem("style").value == 'display:none')
        if (obj.item(i).style.getPropertyValue("display") == 'none'){
            return obj.item(i).id;
        }
    }
}

function getIndex(tr)
{
    var s = 'taxes-';
    var i = tr.id.substr(s.length, tr.id.length);
    return i;
}

function addElement()
{
//    console.log('nome: ' + newName.value);
//    console.log('valor: ' + newValue.value);
    if (newName.value === '' || newValue.value === '') {
        alert('Nome e valor da taxa devem ser digitados.');
        return;
    }

    if (edit.index === false) {
        var listTrElements = tbody_container.getElementsByTagName('TR');
        var firstHiddenTr = document.getElementById(getFirstHiddenTr(listTrElements));
        var index = getIndex(firstHiddenTr);

        var targetName = document.getElementById('taxes-' + index + '-name');
        var targetValue = document.getElementById('taxes-' + index + '-value');
        var targetCalculation = document.getElementById('taxes-' + index + '-calculation');

        targetName.value = newName.value;
        targetValue.value = newValue.value;
        //            targetValue.value = newCalculation.value;
        targetCalculation.value = getSelect2Value(newCalculation);
        // firstHiddenTr.attributes.getNamedItem("style").value = '';
        firstHiddenTr.style.removeProperty('display');
    } else {
        edit.targetName.value = newName.value;
        edit.targetValue.value = newValue.value;
	//        edit.targetCalculation.value = newCalculation.value;
//	console.log(1);
	edit.targetCalculation.value = getSelect2Value(document.getElementById('new-calculation'));
//	console.log(2);
        endEditElement();
    }
    newName.value = newValue.value = '';
    newName.focus();
}
