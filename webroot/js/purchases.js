var workingDays =  document.getElementById('working-days');
var forecastDays = document.getElementById('forecast-days');
var forecastDate = document.getElementById('forecast-date');
var datePurchase = document.getElementById('date-purchase');

var dialog, items, form;
var itemsRecIndex;
var qtdRec, dataRec;

function rSelectProductsBySupplyHtml(response, param)
{
    console.log(response);
    $('#product-id').html(response);
}

function requestReceived(q) {
    var p = $("#id").val();
    //    var qtd = $("#inputQtd").val();
    var qtd = q;

    var targeturl = '/receiveds/insertreceived';
    var data = {p: p, q: qtd};
    ajax_get(targeturl, data, '', insertLine, '');
}

function insertLine(response, param) {
    console.log('voltou de insertreceived')
    if (typeof(response) == 'string') {
        response = JSON.parse(response);
    }

    qtdRec = response.qtd;
    dataRec = response.created;
    itemsRecIndex = $( '#receiveds li').length;

    var valid = true;
    var label = '';
    if(response.qtd > 0) {
	    label = 'success';
	    text = 'green';
	    status = 'recebido';
    } else {
	    label = 'warning';
	    text = 'yellow';
	    status = 'cancelado';
    }
    $( "#receiveds" ).append(
	    '<li id="li-receiveds-' + itemsRecIndex + '">' +
	        '<input id="recs-' + itemsRecIndex + '-id" value="' + response.id + '" type="hidden">' +
	        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	    // '<input id="recs-' + itemsRecIndex + '-product_id" name="recs[' + itemsRecIndex + '][product_id]" value="' + response.product_id + '" type="hidden">' +	    //
	    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	    '<input id="recs-' + itemsRecIndex + '-qtd" name="recs[' + itemsRecIndex + '][qtd]" value="' + response.qtd + '" type="hidden">' +
	        '<input id="recs-' + itemsRecIndex + '-created" name="recs[' + itemsRecIndex + '][created]" value="' + response.created + '" type="hidden">' +
	        '<span class="label label-' + label  + '">' +
	        response.qtd +
	        '</span>' +
	        '<span class="text-' + text + '">' +
	        '  ' + status + ' em ' + response.created_formated +
	        '</span>' +
	        // '<div class="tools">' +
	        // '<a href="#" data-toggle="tooltip" title="" data-original-title="cancelar" onclick="cancelConfirm(' + itemsRecIndex + ');">' +
	        // '<span class="text-red"><i class="fa fa-remove"></i></span>' +
	        // '</a>' +
	        // '</div>' +
	        '</li>'
    );

    if(response.recSum > 0) {
	document.getElementById('li-recsum').style.setProperty('display', 'contents');
    } else {
	document.getElementById('li-recsum').style.setProperty('display', 'none');
    }

    if(response.recSum > 0) {
	document.getElementById('li-recsum').style.setProperty('display', 'contents');
    } else {
	document.getElementById('li-recsum').style.setProperty('display', 'none');
    }

    if((response.total - response.recSum) > 0) {
	document.getElementById('li-qtd').style.setProperty('display', 'contents');
    } else {
	document.getElementById('li-qtd').style.setProperty('display', 'none');
    }
    
    $('#label-receiveds').text(response.recSum);
    $('#label-to-receive').text((response.total - response.recSum));
    itemsRecIndex++;

    return valid;
}

// function cancelConfirm(i) {
//     modal_confirm_warning("Cancelamento", "Confirma o cancelamento?", rCancelConfirm, i);
// }

function cancelReceived(q) {
    var p = $("#id").val();
    var qtd = q;

    var targeturl = '/receiveds/cancelreceived';
    var data = {p: p, q: qtd};
    ajax_get(targeturl, data, '', insertLine, '');

    // var id = $('#recs-' + i + '-id').val();
    // var targeturl = '/receiveds/cancelreceived'
    // var data = {id: id};
    // //    var param = $('#li-receiveds-' + i);
    // ajax_get(targeturl, data, '', insertLine, '');
}

function calcForessen(wd)
{
    if (forecastDays.value == '' && forecastDate.value == '') {
	return;
    }


    if(forecastDays.value != '')
	daysToForeseen();
    else
	foreseenToDays();
}

function daysToForeseen()
{
    if (datePurchase.value == '') {
	modal_warning('Alerta', 'Você precisa digitar a data da compra para calcular a data de recebimento');
	return;
    }

    if (forecastDays.value == '' || forecastDays.value == null)
	return;

    var dtstart = new Date(brDateToShortFormat(datePurchase.value));
    var dttarget = new Date(brDateToShortFormat(datePurchase.value));
    var days = Number(forecastDays.value);

    if (workingDays.checked) {
	console.log('entrou');
	var wd = 0;
	for (i = 0; i < days; i++) {
	    dttarget.setDate(dttarget.getDate() + 1);
	    if (dttarget.getDay() == 6 || dttarget.getDay() == 0)
		wd++;
	}
	days = days + wd;
    }

    dtstart.setDate(dtstart.getDate() + days);
    if(dtstart.getDay() == 6) {
	dtstart.setDate(dtstart.getDate() + 2);
    } else {
	if(dtstart.getDay() == 0) {
	    dtstart.setDate(dtstart.getDate() + 1);
	}
    }

    forecastDate.value = dtstart.toLocaleDateString();
}

function foreseenToDays()
{
    if (datePurchase.value == '') {
	modal_warning('Alerta', 'Você precisa digitar a data da compra para calcular a data do recebimento');
	return;
    }

    if (forecastDate.value == '' || forecastDate.value == null)
	return;

    var dtstart = new Date(brDateToShortFormat(datePurchase.value));
    var dtend = new Date(brDateToShortFormat(forecastDate.value));

    var days = (dtend.getTime() - dtstart.getTime()) / 86400000;
    if((parseFloat(days) - parseInt(days)) > 0)
	days = parseInt(days) + 1;

    if (workingDays.checked) {
	var wd = 0;
	for (i = 0; i < days; i++) {
	    dtstart.setDate(dtstart.getDate() + 1);
	    if (dtstart.getDay() == 6 || dtstart.getDay() == 0)
		wd++;
	}
	days = days - wd;
    }

    forecastDays.value = days;
}

function brDateToShortFormat(d)
{
    var a = d.split('/');
    return a[1] + '/' + a[0] + '/' + a[2];
}


// funcionando na chamada direta do elemento
$(function() {
    //masks
    // $("#date-purchase").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/aaaa"});
    // $("#forecast-date").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/aaaa"});

    //
    $('#working-days').on('change', function () {
	calcForessen($(this).prop('checked'));
    });

    $(forecastDays).on('change', function () {
	daysToForeseen();
    });

    $(forecastDate).on('change', function () {
	foreseenToDays();
    });	

    $('#supply-id').change(function() {
	    var targeturl = '/products/select_stock_products_by_supply_html';
	    var data = {supply_id: $( '#supply-id' ).val()};
	    ajax_get(targeturl, data, '', rSelectProductsBySupplyHtml, '');
    });

    $( "#btrecok" ).on( "click", function() {
	    modal_confirm_warning("Recebimento", "Confirma o recebimento?", requestReceived, $('#qtd-input').val());
	    return false;
    });

    $( "#btreccancel" ).on( "click", function() {
	    modal_confirm_warning("Cancelamento", "Confirma o cancelamento?", cancelReceived, $('#cancel-input').val());
	    return false;
    });

    $('#supply-name').blur(function() {
	    document.getElementById('supply-name').value = document.getElementById('supply-selected').value;
    });

});

    // $('#supply-id').change(function() {
    //     var supplyValue = $('#supply-id').val();
    //     var productElement = $('#product-id');
    //     if(supplyValue == '') {
    //         productElement.empty();
    //         return;
    //     }
    //     getProductsBySupply(supplyValue);
    // });

// function getProductsBySupply(element)
// {
//     alert(element.value);
// }

// function r_getProductsBySupply(response)
// {
//     productElement.append(response['results']);
// }


// new autoComplete({
//     selector: 'input[name="supply_name"]',
//     source: function(term, response){
//         $.getJSON('/supplies/find.json', { q: term }, function(data){ response(data); });
//     },
//     renderItem: function (item, search){
// 	    console.log(item);
// 	    return '<div class="autocomplete-suggestion" data-id="' + item.id + '" data-name="' + item.name + '">' + item.name + '</div>';
//     },
//     onSelect: function(e, term, item){
// 	    var supply = {};
// 	    supply.id = item.getAttribute('data-id');
// 	    supply.name = item.getAttribute('data-name');
// 	    document.getElementById("supply-id").value = supply.id;
// 	    document.getElementById("supply-name").value = supply.name;
// 	    document.getElementById("supply-selected").value = supply.name;
// 	    document.getElementById("product-id").value = '';
// 	    document.getElementById("product-title").value = '';
// 	    if(e.type == 'keydown' && e.keyCode == 13) {
// 	        e.preventDefault();
// 	        return false;
// 	    }
//     },
// });
// new autoComplete({
//     selector: 'input[name="product_title"]',
//     cache: false,
//     source: function(term, response){
// 	    var s = document.getElementById("supply-id").value;
// 	    if(s == '') return false;
//         $.getJSON('/products/find-short.json', { q: term, s: s }, function(data){ response(data); });
//     },
//     renderItem: function (item, search){
// 	    console.log(item);
// 	    return '<div class="autocomplete-suggestion" data-id="' + item.id + '" data-title="' + item.title + '">' + item.title + '</div>';
//     },
//     onSelect: function(e, term, item){
// 	    var product = {};
// 	    product.id = item.getAttribute('data-id');
// 	    product.title = item.getAttribute('data-title');
// 	    document.getElementById("product-id").value = product.id;
// 	    document.getElementById("product-title").value = product.title;
// 	    if(e.type == 'keydown' && e.keyCode == 13) {
// 	        e.preventDefault();
// 	        return false;
// 	    }
//     },
// });
