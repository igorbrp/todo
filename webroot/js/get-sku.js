function getSku(targeturl, data, field)
{
    var field_target = document.getElementById(field);
    targeturl = targeturl.concat($.param(data));
//    var url = encodeURI(targeturl + str);
    console.log(targeturl);
    $.ajax({
        type: 'get',
        url: targeturl,
        success: function(response) {
            if(response.error) {
                alert(response.error);
            } else {
                //                r_function(response);
                console.log(response);
                if (typeof(response) == 'string')
                    response = JSON.parse(response);
                field_target.value = response.code;
            }
        },
        error: function(e) {
            alert("An error occurred: " + e.responseText.message);
     	    console.log('error: ' + e);
     	}
    });
}
