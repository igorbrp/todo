//function changeScheduledDate(delivery_made_id, scheduled_date)
function rChangeScheduledDate(response)
{
    if (response) {
	modal_msg('Sucesso', 'A data agendada foi atualizada.');
    } else {
	modal_danger('Erro', 'Não foi possível atualizar a data agendada.');
    }
}

function rChangeVehicle(response)
{
    if (response) {
	modal_msg('Sucesso', 'O veículo foi atualizado.');
    } else {
	modal_danger('Erro', 'Não foi possível atualizar o veículo.');
    }
}

function rChangeScheduledHorary(response)
{
    if (response) {
	modal_msg('Sucesso', 'O turno agendado foi atualizado.');
    } else {
	modal_danger('Erro', 'Não foi possível atualizar o turno agendado.');
    }
}
// ----------------------------------------------------------------------------

$('.datepicker').datepicker({
    autoclose: true,
    language: 'pt-BR',
    format: 'dd/mm/yyyy',
});

$('.scheduled-date').on('change', function () {
    var trElement = $(this).parents().filter('tr');
    var data = {};
    data.delivery_id = $(this).attr('data-delivery-id');
    data.scheduled_date = $(this).attr('data-original-value');
    data.scheduled_new_date = this.value;
    data.scheduled_horary = trElement.find('.scheduled-horary').first().val();
    data.vehicle_id = trElement.find('.vehicle').first().val();
    $.ajax({
     	type: 'get',
    	data: data,
    	url: '/delivery-mades/update-scheduled-date/',
     	success: function(response) {
    	    if(response.error) {
    	    	alert(response.error);
    	    } else {
                rChangeScheduledDate(response);
    	    }
     	},
     	error: function(e) {
     	    alert("An error occurred: " + e.responseText.message);
     	}
    });
});

$('.vehicle').on('change', function () {
    var trElement = $(this).parents().filter('tr');
    var data = {};
    data.delivery_id = $(this).attr('data-delivery-id');
    data.vehicle_id = $(this).attr('data-original-value');
    data.vehicle_new_id = this.value;
    data.scheduled_horary = trElement.find('.scheduled-horary').first().val();
    data.scheduled_date = trElement.find('.scheduled-date').first().val();
    $.ajax({
     	type: 'get',
    	data: data,
    	url: '/delivery-mades/update-vehicle/',
     	success: function(response) {
    	    if(response.error) {
    	    	alert(response.error);
    	    } else {
		rChangeVehicle(response);
    	    }
     	},
     	error: function(e) {
     	    alert("An error occurred: " + e.responseText.message);
     	}
    });
});

$('.scheduled-horary').on('change', function () {
    var trElement = $(this).parents().filter('tr');
    var data = {};
    data.delivery_id = $(this).attr('data-delivery-id');
    data.scheduled_horary = $(this).attr('data-original-value');
    data.scheduled_new_horary = this.value;
    data.vehicle_id = trElement.find('.vehicle').first().val();
    data.scheduled_date = trElement.find('.scheduled-date').first().val();
    console.log('data para scheduled_horary:');
    console.log(data);
    $.ajax({
     	type: 'get',
    	data: data,
    	url: '/delivery-mades/update-scheduled-horary/',
     	success: function(response) {
    	    if(response.error) {
    	    	alert(response.error);
    	    } else {
		rChangeScheduledHorary(response);
    	    }
     	},
     	error: function(e) {
     	    alert("An error occurred: " + e.responseText.message);
     	}
    });
});
