//var trSpansTotals = document.getElementsByClassName('totals-item');
function loadInfo(id)
{
        $.ajax({
     	    type: 'get',
	        url: '/item-histories/get-info/' + id,
     	    success: function(response) {
    	        if(response.error) {
    	    	    alert(response.error);
    	        } else {
                    rLoadInfo(response, id);
    	        }
     	    },
     	    error: function(e) {
     	        alert("An error occurred: " + e.responseText.message);
     	        console.log('error: ' + e);
     	    }
        });
}

function rLoadInfo(response, id)
{
    $('#group-item-history-' + id).html(response);
    var arr = $('#actualized-totals-' + id).text().split(",");
    buildSparkline($('#graphic-big-' + id), arr);
    $('#totals-' + id).text($('#actualized-totals-' + id).text());
    arr = $('#totals-' + id).text().split(",");
    buildSparkline($('#graphic-' + id), arr); 
    var orderId = $("#group-item-history-" + id).parents().filter(".group-item").first().attr('data-order-id');
    loadOrderInfo(orderId);
}

function buildSparkline(targetElement, arrValues)
{
    /**********************************************************
    * essa funcção está escrita diferente aqui e no order.js
    * padronizar!!!!!
    **********************************************************/
//    var arrValues = valueElement.innerHTML.split(",");
    // var arrValues = valueElement.split(",");
    $(targetElement).sparkline(arrValues, {
        type: 'pie',
        sliceColors: [
            '#ff0000', /* pendente */
            '#f4c113', /* requisitado */
            '#008000', /* recebido */
            '#ff6680', /* despachado */
            '#3333ff'   /* concluído */
        ]});
}

// function confirmChangeStatus(item_id, from, to, t, msg = '')
function confirmChangeStatus(item_id, from, to, t)
{
    var recqtd = document.getElementById(from + '_recqtd_' + item_id).value;
    var maxqtd = document.getElementById(from + '_recqtd_' + item_id).getAttribute('max');
    if (Number(recqtd) > Number(maxqtd)) {
        alert('O número escolhido é maior que a quantidade disponível');
        return;
    }
    var data = {item_id: item_id, from: from, to: to, qtd: recqtd};
    var html = '';
    var msg = '';

    if(from == 'uncovered' && to == 'required') {
	recqtd = maxqtd;
	msg = 'Confirma a requisição do item?';
    } else {
	if (from == 'required' && to == 'uncovered') {
	    if ( Number(recqtd) < Number(maxqtd) ) {
		recqtd = maxqtd;
		msg = '<p>Você está tentando cancelar uma requisição parcialmente e isso não é possível, se você confirmar esse cancelamento, <b>TODAS AS ' + maxqtd + ' PEÇAS</b> do item terão a requisição cancelada.</p><p>Confirma o cancelamento da requisição?</p>';
	    } else {
		msg = 'Confirma o cancelamento da requisição?';
	    }
	} else {
	    if (from == 'required' && to == 'received') {
		msg = 'Entre com o número da nota fiscal e confirme o recebimento do item:';
		msg += document.getElementById('divModalFormInvoice').innerHTML;
	    } else {
		if (from == 'received' && to == 'required') {
		    msg = 'Confirma o cancelamento do recebimento?';
		} else {
		    if (from == 'received' && to == 'dispatched') {
			if (Number(recqtd) < Number(maxqtd) ){
			    recqtd = maxqtd;
			    msg = '<p>Você está tentando despachar parcialmente um item e isso não é possível, se você confirmar esse despacho, <b>TODAS AS ' + maxqtd + ' PEÇAS</b> do item serão deapachadas.</p><p>Confirma o despacho da(s) mercadoria(s)?</p><br>';
			}
			msg += document.getElementById('divModalFormDispatched').innerHTML;
		    } else {
			if (from == 'dispatched' && to == 'received') {
			    msg = 'Confirma o cancelamento da entrega?';
			} else {
			    if (from == 'dispatched' && to == 'completed') {
				msg = 'Confirma a conclusão da entrega do item?';
			    } else {
				if (from == 'completed' && to == 'dispatched') {
				    msg = '<p>Você está cancelando uma conclusão de envio.</p><p>Se o(s) produto(s) está(ão) voltando para o depósito, você deve torná-los recebidos novamente.</p>';
				}
			    }
			}
		    }
		}
	    }
	}
    }

    html = msg;
    if (from == 'received' && to == 'dispatched') {
        modal_dispatch('Confirme a ação', html, itemHistoriesDetails, data);
    } else {
	if (from == 'required' && to == 'received') {
	    modal_invoice('Entre com o número da nota fiscal e confirme a ação', html, itemHistoriesDetails, data);
	}
//	console.log('modal_form');
	modal_form('Confirme a ação.', html, itemHistoriesDetails, data);
    }
}

/**********************************************************************************
 * itemHistoriesDetails
 * chama item_history_details no server atraves da funcao ajax_post para alterar o status de um item
 * item_id: id do item
 * from: status atual do item
 * to: status do item apos alteracao
 * t: elemento (this) que fez o pedido
 * obs: a funcao rLoadInfo recebe o retorno vindo de ajax_post
 */
function itemHistoriesDetails(data)
{
    var dispatch_date;
    var vehicle;

    if(data.from == 'received' && data.to == 'dispatched') {
        if(document.getElementById('modal-datepicker').value == null) {
            alert('Escolha uma data válida para o despacho');
            return;
        }

        dispatch_date = document.getElementById('modal-datepicker').value;
        if(!Inputmask.isValid(dispatch_date, { alias: "datetime", inputFormat: "dd/mm/yyyy"})) {
            alert('Escolha uma data válida para o despacho');
            return;
        }

        vehicle = document.getElementById('modal-vehicle').value;
        if(vehicle == null || vehicle == '') {
            alert('Escolha um veículo para o despacho');
            return;
        }

        data.vehicle = vehicle;
        data.dispatch_date = dispatch_date;
    } else {
	if (data.from == 'required' && data.to == 'received') {
	    data.invoice = document.getElementById('modal-invoice').value;
	}
    }

    var returnFunction = rLoadInfo;
    var targeturl = '/itemHistories/change_status';

    $('#modal_msg_body').html();
    ajax_get(targeturl, data, '', returnFunction, data.item_id);
}

function insertInvoice(itemHistoryId, itemId, t)
{
    var data = {};
    data.item_history_id = itemHistoryId;
    data.item_id = itemId;
    
    html = document.getElementById('divModalFormInvoice').innerHTML;
    modal_invoice('Entre com o número da nota fiscal e confirme a ação', html, updateInvoice, data);    
}

function updateInvoice(data)
{
    var returnFunction = rLoadInfo;
    var targeturl = '/itemHistories/update_invoice';

    data.invoice_number = document.getElementById('modal-invoice').value;

    $('#modal_msg_body').html();
    ajax_get(targeturl, data, '', returnFunction, data.item_id);
}

function registryEvents()
{
    $(".group-item-history").on('show.bs.collapse', function () {
	alert('.group-item-history.on(show.bs.collapse)'); 
	loadInfo($(this).attr('data-item-id'));
    });
}

$(function() {
// atualiza historico do item
    registryEvents();
    
    var groupItems = $('#panel-orders').find('.group-item');
    groupItems.each(function () {
	var lineItems = $(this).find('.line-item');
	lineItems.each(function () {
	var arr = $('#totals-' + $(this).attr('data-item-id')).text().split(",");
	    buildSparkline($('#graphic-' + $(this).attr('data-item-id')), arr);
	});
    });
});

