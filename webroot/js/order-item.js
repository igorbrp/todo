// order functions
// var trSpansTotalOrder = '';
// var trSpansHeaderOrder = document.getElementsByClassName('totals-order');

function loadOrderInfo(id)
{
    var i;
    var lineItems = $('#group-item-' + id).find('.line-item');
    var arrTotalsOrder = [0, 0, 0, 0, 0];
    lineItems.each(function () {
	var arr = $('#totals-' + $(this).attr('data-item-id')).text().split(",");
	// drawGraphicItem($(this).attr('data-item-id'));
	buildSparkline($('#graphic-' + $(this).attr('data-item-id')), arr);
	for (i = 0; i < arrTotalsOrder.length; i++)
	    arrTotalsOrder[i] += Number(arr[i]);
    });
    buildSparkline($('#graphic-order-' + id), arrTotalsOrder);
}

// function calcTotalsOrder(arrTot, arr)
// {
//     for (var i = 0; i < arr.length; i++)
// 	arrTot[i] += Number(arrTot[i]) + Number(arr[i]);
// }

function confirmChangeOrderStatus(order_id, from, to)
{
    var data = {item_id: order_id, from: from, to: to};
    var html = '';

    if (to == 'dispatched') {
        html = '<div class="form-group required"><label class="control-label" for="modal-datepicker">data:&nbsp;</label><br><div class="input-group date"><div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="text" class="form-control pull-right" id="modal-datepicker"></div></div><br> ';
        html += document.getElementById('divVehicles').innerHTML;
        modal_dispatch('Despachar mercadoria', html, orderDetails, data);
        // }
    } else {
        html = msg == '' ? 'Confirma a alteracao do registro?' : msg;
        modal_form('Confirme a ação.', html, orderDetails, data);
    }
}

function confirmChangeBulk(order_id, to)
{
    var msg = '';
    var html = '';
    var data = {};
    data.order_id = order_id;
    data.to = to;
    var o, n;
    if (to == 'required') {
	o = 'pendentes';
	n = 'requisitados';
    } else if (to == 'received') {
	o = 'requisitados';
	n = 'recebidos';
	msg = '<p>O número da nota fiscal digitada aqui será atribuída a todos os items.</p><p>Entre com o número da nota fiscal e confirme o recebimento do item:</p>';
	msg += document.getElementById('divModalFormInvoice').innerHTML;
    } else if (to == 'dispatched') {
	o = 'recebidos';
	n = 'despachados';
	msg = document.getElementById('divModalFormDispatched').innerHTML;
    } else if (to == 'completed') {
	o = 'despachados';
	n = 'concluídos';
    }
    html = '<p>Todos os itens ' + o + ' do pedido serão ' + n + '.</p>';
    html += msg;
    if (to == 'dispatched') {
        modal_dispatch('Confirme a ação', html, rConfirmChangeBulk, data);
    } else {
	modal_confirm_warning('Confirme a ação', html, rConfirmChangeBulk, data);
    }
}

function rConfirmChangeBulk(data)
{
    var dispatch_date;
    var vehicle;

    if(data.to == 'dispatched') {
        if(document.getElementById('modal-datepicker').value == null) {
            modal_err_msg('Escolha uma data válida para o despacho');
            return;
        }

        dispatch_date = document.getElementById('modal-datepicker').value;
        if(!Inputmask.isValid(dispatch_date, { alias: "datetime", inputFormat: "dd/mm/yyyy"})) {
            modal_err_msg('Escolha uma data válida para o despacho');
            return;
        }

        vehicle = document.getElementById('modal-vehicle').value;
        if(vehicle == null || vehicle == '') {
            modal_err_msg('Escolha um veículo para o despacho');
            return;
        }

        data.vehicle = vehicle;
        data.dispatch_date = dispatch_date;
    }

    if (data.to == 'received') {
	data.invoice = document.getElementById('modal-invoice').value;
    }

    var returnFunction = rChangeStatusBulk;
    var targeturl = '/orders/change-status-bulk';

    $('#modal_msg_body').html();
    ajax_get(targeturl, data, '', returnFunction, data.order_id);
}

function rChangeStatusBulk(response, param)
{
    $('#li-order-' + param).html(response);
    registryEvents();
    var arr = $('#totals-order-' + param).text().split(",");
    buildSparkline($('#graphic-order-' + param), arr);
    $('#group-item-' + param).collapse('show');
    loadOrderInfo(param);
}

$(function() {
// atualiza graficos do pedido
    $(".group-item").on('shown.bs.collapse', function () {
	loadOrderInfo($(this).attr('data-order-id'));
    });
    var lineOrders = $('#panel-orders').find('.line-order');
    lineOrders.each(function () {
	var arr = $('#totals-order-' + $(this).attr('data-order-id')).text().split(",");
	buildSparkline($('#graphic-order-' + $(this).attr('data-order-id')), arr);
    });
});

// item functions
//var trSpansTotals = document.getElementsByClassName('totals-item');
function loadInfo(id)
{
        $.ajax({
     	    type: 'get',
	        url: '/item-histories/get-info/' + id,
     	    success: function(response) {
    	        if(response.error) {
    	    	    alert(response.error);
    	        } else {
                    rLoadInfo(response, id);
    	        }
     	    },
     	    error: function(e) {
     	        alert("An error occurred: " + e.responseText.message);
     	        console.log('error: ' + e);
     	    }
        });
}

function rLoadInfo(response, id)
{
    $('#group-item-history-' + id).html(response);
    // substituido por drawGraphicitem
    var arr = $('#actualized-totals-' + id).text().split(",");
    buildSparkline($('#graphic-big-' + id), arr);
    $('#totals-' + id).text($('#actualized-totals-' + id).text());
    arr = $('#totals-' + id).text().split(",");
    buildSparkline($('#graphic-' + id), arr); 
//    drawGraphicItem(id);
    var orderId = $("#group-item-history-" + id).parents().filter(".group-item").first().attr('data-order-id');
    loadOrderInfo(orderId);
}

function buildSparkline(targetElement, arrValues)
{
    /**********************************************************
    * essa funcção está escrita diferente aqui e no order.js
    * padronizar!!!!!
    **********************************************************/
//    var arrValues = valueElement.innerHTML.split(",");
    // var arrValues = valueElement.split(",");
    $(targetElement).sparkline(arrValues, {
        type: 'pie',
        sliceColors: [
            '#ff0000', /* pendente */
            '#f4c113', /* requisitado */
            '#008000', /* recebido */
            '#ff6680', /* despachado */
            '#3333ff'   /* concluído */
        ]});
}

// function confirmChangeStatus(item_id, from, to, t, msg = '')
function confirmChangeStatus(item_id, from, to, t)
{
    var recqtd = document.getElementById(from + '_recqtd_' + item_id).value;
    var maxqtd = document.getElementById(from + '_recqtd_' + item_id).getAttribute('max');
    if (Number(recqtd) > Number(maxqtd)) {
        alert('O número escolhido é maior que a quantidade disponível');
        return;
    }
    var data = {item_id: item_id, from: from, to: to, qtd: recqtd};
    var html = '';
    var msg = '';

    if(from == 'uncovered' && to == 'required') {
	recqtd = maxqtd;
	msg = 'Confirma a requisição do item?';
    } else {
	if (from == 'required' && to == 'uncovered') {
	    if ( Number(recqtd) < Number(maxqtd) ) {
		recqtd = maxqtd;
		msg = '<p>Você está tentando cancelar uma requisição parcialmente e isso não é possível, se você confirmar esse cancelamento, <b>TODAS AS ' + maxqtd + ' PEÇAS</b> do item terão a requisição cancelada.</p><p>Confirma o cancelamento da requisição?</p>';
	    } else {
		msg = 'Confirma o cancelamento da requisição?';
	    }
	} else {
	    if (from == 'required' && to == 'received') {
		msg = 'Entre com o número da nota fiscal e confirme o recebimento do item:';
		msg += document.getElementById('divModalFormInvoice').innerHTML;
	    } else {
		if (from == 'received' && to == 'required') {
		    msg = 'Confirma o cancelamento do recebimento?';
		} else {
		    if (from == 'received' && to == 'dispatched') {
			if (Number(recqtd) < Number(maxqtd) ){
			    recqtd = maxqtd;
			    msg = '<p>Você está tentando despachar parcialmente um item e isso não é possível, se você confirmar esse despacho, <b>TODAS AS ' + maxqtd + ' PEÇAS</b> do item serão deapachadas.</p><p>Confirma o despacho da(s) mercadoria(s)?</p><br>';
			}
			msg += document.getElementById('divModalFormDispatched').innerHTML;
		    } else {
			if (from == 'dispatched' && to == 'received') {
			    msg = 'Confirma o cancelamento da entrega?';
			} else {
			    if (from == 'dispatched' && to == 'completed') {
				msg = 'Confirma a conclusão da entrega do item?';
			    } else {
				if (from == 'completed' && to == 'dispatched') {
				    msg = '<p>Você está cancelando uma conclusão de envio.</p><p>Se o(s) produto(s) está(ão) voltando para o depósito, você deve torná-los recebidos novamente.</p>';
				}
			    }
			}
		    }
		}
	    }
	}
    }

    html = msg;
    if (from == 'received' && to == 'dispatched') {
        modal_dispatch('Confirme a ação', html, rConfirmChangeStatus, data);
    } else {
	if (from == 'required' && to == 'received') {
	    modal_invoice('Entre com o número da nota fiscal e confirme a ação', html, rConfirmChangeStatus, data);
	}
//	console.log('modal_form');
	modal_form('Confirme a ação.', html, rConfirmChangeStatus, data);
    }
}

/**********************************************************************************
 * rConfirmChangeStatus
 * chama item_history_details no server atraves da funcao ajax_post para alterar o status de um item
 * item_id: id do item
 * from: status atual do item
 * to: status do item apos alteracao
 * t: elemento (this) que fez o pedido
 * obs: a funcao rLoadInfo recebe o retorno vindo de ajax_post
 */
function rConfirmChangeStatus(data)
{
    var dispatch_date;
    var vehicle;

    if(data.from == 'received' && data.to == 'dispatched') {
        if(document.getElementById('modal-datepicker').value == null) {
            alert('Escolha uma data válida para o despacho');
            return;
        }

        dispatch_date = document.getElementById('modal-datepicker').value;
        if(!Inputmask.isValid(dispatch_date, { alias: "datetime", inputFormat: "dd/mm/yyyy"})) {
            alert('Escolha uma data válida para o despacho');
            return;
        }

        vehicle = document.getElementById('modal-vehicle').value;
        if(vehicle == null || vehicle == '') {
            alert('Escolha um veículo para o despacho');
            return;
        }

        data.vehicle = vehicle;
        data.dispatch_date = dispatch_date;
    } else {
	if (data.from == 'required' && data.to == 'received') {
	    data.invoice = document.getElementById('modal-invoice').value;
	}
    }

    var returnFunction = rLoadInfo;
    var targeturl = '/itemHistories/change_status';

    $('#modal_msg_body').html();
    ajax_get(targeturl, data, '', returnFunction, data.item_id);
}

function insertInvoice(itemHistoryId, itemId, t)
{
    var data = {};
    data.item_history_id = itemHistoryId;
    data.item_id = itemId;
    
    html = document.getElementById('divModalFormInvoice').innerHTML;
    modal_invoice('Entre com o número da nota fiscal e confirme a ação', html, updateInvoice, data);    
}

function updateInvoice(data)
{
    var returnFunction = rLoadInfo;
    var targeturl = '/itemHistories/update_invoice';

    data.invoice_number = document.getElementById('modal-invoice').value;

    $('#modal_msg_body').html();
    ajax_get(targeturl, data, '', returnFunction, data.item_id);
}

function registryEvents()
{
    $(".group-item-history").on('show.bs.collapse', function () {
	loadInfo($(this).attr('data-item-id'));
    });
}

// function drawGraphicItem(id)
// {
//     var arr = $('#actualized-totals-' + id).text().split(",");
//     buildSparkline($('#graphic-big-' + id), arr);
//     $('#totals-' + id).text($('#actualized-totals-' + id).text());
//     arr = $('#totals-' + id).text().split(",");
//     buildSparkline($('#graphic-' + id), arr); 
// }

registryEvents();

$(function() {
// atualiza historico do item
    
    var lineItems = $('#panel-items').find('.line-item');
    lineItems.each(function () {
	var arr = $('#totals-' + $(this).attr('data-item-id')).text().split(",");
	buildSparkline($('#graphic-' + $(this).attr('data-item-id')), arr);
    });
});

