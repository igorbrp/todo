var selecionado = {};
var img;
new autoComplete({
    selector: 'input[name="q"]',
    source: function(term, response){
      $.getJSON('/products/find', { q: term }, function(data){ response(data); });
    },
    renderItem: function (item, search){
        var currency_price = item.price;
        if (item.image == null)
            img = '/img/no-image.jpg';
        else
            img = item.image;
	    return '<div class="autocomplete-suggestion" data-id="' + item.id + '" data-sku="' + item.sku + '" data-title="' + item.title + '" data-supply_name="' +  item.supply.name + '" data-supply_id="' +  item.supply_id + '" data-supply_line_id="' + item.supply_line_id + '" data-price="' + item.price.toFixed(2) + '" data-price_calculated="' + item.price_calculated + '" data-deadline="' + item.deadline + '" data-working_days="' + item.working_days + '" data-maximum_discount="' + item.maximum_discount + '" data-minimum_unitary_price="' +  item.minimum_unitary_price.toFixed(2) + '" data-product_type_id="' + item.product_type_id + '" data-supply_code="' + item.supply.code + '"><div class="select2-result-repository clearfix">' +
	    '<div class="select2-result-repository__avatar"><img src="' + img + '" /></div>' +
	        '<div class="select2-result-repository__meta">' +
	        '<div>' + item.sku + '<span class="pull-right"><i class="fa fa-money"></i> ' + currency_price.toFixed(2) + '</span></div>' +
	        '<div>' + item.title + '</div>' +
	        '<div class="select2-result-repository__statistics">' +
	        '<div class="select2-result-repository__forks"><i class="fa fa-calendar"></i> ' + item.deadline + ' dias sob encomenda</div>' +
	        '</div>' +
	        '</div></div></div>';
    },
    onSelect: function(e, term, item){
        selecionado = {};
	selecionado.product_id = item.getAttribute('data-id');
	selecionado.sku = item.getAttribute('data-sku');
	selecionado.title = item.getAttribute('data-title');
	selecionado.supply_name = item.getAttribute('data-supply_name');
        selecionado.supply_code = item.getAttribute('data-supply_code');
	selecionado.supply_id = item.getAttribute('data-supply_id');
	selecionado.supply_line_id = item.getAttribute('data-supply_line_id');
	selecionado.price = item.getAttribute('data-price');
	selecionado.price_calculated = item.getAttribute('data-price_calculated');
	selecionado.deadline = item.getAttribute('data-deadline');
	selecionado.working_days = item.getAttribute('data-working_days');
	selecionado.maximum_discount = item.getAttribute('data-maximum_discount');
        selecionado.minimum_unitary_price = item.getAttribute('data-minimum_unitary_price');
	selecionado.product_type_id = item.getAttribute('data-product_type_id');
	document.getElementById("inputTitle").value = selecionado.title;
	selectedItem();
	if(e.type == 'keydown' && e.keyCode == 13) {
	    e.preventDefault();
	    return false;
	}
    },
});

function selectedItem()
{
    if(!Number(newQtd.value) || Number(newQtd.value) == 0)
        newQtd.value = 1;
    console.log('selected item');
    console.log('selecionado.price:');
    console.log(Number(selecionado.price));
    inputUnitPriceUnmasked.value = Number(selecionado.price);
    inputPriceUnmasked.value = Number(selecionado.price) * Number(newQtd.value);
    console.log('selecionado.price: ' + selecionado.price);
    newPrice.value = Number(selecionado.price) * Number(newQtd.value);
    return false;
}
