// ------------------------------------------------------------------
// items declarations
var selectedItem = {};
var index;
var imgProductSelected;
var newQtd = document.getElementById('inputQtd');
var newTitle = document.getElementById('inputTitle');
var newComplement = document.getElementById('inputComplement');
var newPrice = document.getElementById('inputPrice');
var tbody_container = document.getElementById('tbody_items');
var trSpansTotals = document.getElementsByClassName('totals-item');
var btCancel = document.getElementById('bt-cancel');

var edit = {
    index: false,
    targetTr: undefined,
    targetQtd: undefined,
    targetTitle: undefined,
    targetPrice: undefined,
    targetProductId: undefined,
    targetSku: undefined,
    targetSupplyName: undefined,
    targetSupplyCode: undefined,
    targetSupplyId: undefined,
    targetSupplyLineId: undefined,
    targetStatus: undefined,
    targetDeadline: undefined,
    targetWorkingDays: undefined,
    targetOrigin: undefined,
    targetMinimumUnitaryPrice: undefined,
    targetOrigin: undefined
};

// ------------------------------------------------------------------
// payments declarations
var newPaymentMethod = document.getElementById('inputPaymentMethod');
var newDetails = document.getElementById('inputDetails');
var newFinancier = '';
var newValue = document.getElementById('inputValue');
var newInstallments = document.getElementById('inputInstallments');
var tbody_container_payments = document.getElementById('tbody_payments');
var btCancelPayment = document.getElementById('bt-cancel-payment-payment');

var payEdit = {
    index: false,
    targetTr: undefined,
    targetPaymentMethod: undefined,
    targetDetails: undefined,
    targetFinancier: undefined,
    targetPaymentMethodText: undefined,
    targetValue: undefined,
    targetFinancierText: undefined,
    targetInstallments: undefined,
};

// ------------------------------------------------------------------
// common declarations
// var newCityId = false; // IMPORTANTE, TIREI DE addresses.js e passei pra cá
var dateOrder = document.getElementById('date-order');
var itemsSales = document.getElementById('tbody_items');
var itemsPayments = document.getElementById('tbody_payments');
var customerCity = document.getElementById('customer-address-city_id');
var customerState = document.getElementById('customer-address-state_id');
var customerThorougfare = document.getElementById('customer-address-thoroughfare');
var deliveryState = document.getElementById('delivery-address-state_id');
var deliveryCity = document.getElementById('delivery-address-city_id');
var deliveryThoroughfare = document.getElementById('delivery-address-thoroughfare');
var unnecessaryCustomerAddress = document.getElementById('unnecessary-customer-address');
var deliveryLess = document.getElementById('delivery-less');
var sameCustomerAddress = document.getElementById('delivery-same-customer-address');
var deliveryForeseenDate = document.getElementById('delivery-foreseen-date');
var divForeseenDays = document.getElementById('divForeseenDays');
var foreseenDays = document.getElementById('delivery-foreseen-days');
var workingDays = document.getElementById('delivery-working-days');
var divWorkingDays = document.getElementById('divWorkingDays');
var divForeseenDate = document.getElementById('divForeseenDate');
var divForeseenHorary = document.getElementById('divForeseenHorary');
var divSameCustomerAddress = document.getElementById('divSameCustomerAddress');
var divDeliveryAddress = document.getElementById('divDeliveryAddress');
var divCustomerAddress = document.getElementById('divCustomerAddress');
var cancelSearchCities = false;
// ------------------------------------------------------------------
// items functions
new autoComplete({
    selector: 'input[name="q"]',
    source: function(term, response){
        $.getJSON('/products/find', { q: term }, function(data){ response(data); });
    },
    renderItem: function (item, search){
        var currency_price = item.price;
        if (item.image == null)
            imgProductSelected = '/img/no-image.jpg';
        else
            imgProductSelected = item.image;

	if (item.stock_qtd > 0)
	    span_origin = '<small class="label bg-green"><i class="fa fa-bars"></i> ' + item.stock_qtd + ' em estoque</small></div>';
	else
	    span_origin = '<small class="label bg-yellow"><i class="fa fa-calendar"></i> ' + item.deadline + ' dias sob encomenda</small></div>';

	return '<div class="autocomplete-suggestion" data-id="' + item.id + '" data-sku="' + item.sku + '" data-title="' + item.title + '" data-supply_name="' +  item.supply.name + '" data-supply_id="' +  item.supply_id + '" data-supply_line_id="' + item.supply_line_id + '" data-price="' + item.price.toFixed(2) + '" data-price_calculated="' + item.price_calculated + '" data-deadline="' + item.deadline + '" data-working_days="' + item.working_days + '" data-maximum_discount="' + item.maximum_discount + '" data-minimum_unitary_price="' +  item.minimum_unitary_price.toFixed(2) + '" data-product_type_id="' + item.product_type_id + '" data-supply_code="' + item.supply.code + '" data-stock-qtd="' + item.stock_qtd + '" data-complement="' + item.complement + '"><div class="select2-result-repository clearfix">' +
	    '<div class="select2-result-repository__avatar"><img src="' + imgProductSelected + '" /></div>' +
	    '<div class="select2-result-repository__meta">' +
	    '<div>' + item.sku + '<span class="pull-right"><i class="fa fa-money"></i> ' + currency_price.toFixed(2) + '</span></div>' +
	    '<div>' + item.title + '</div>' +
	    '<div class="select2-result-repository__statistics">' +
	    '<div class="select2-result-repository__forks">' + span_origin + '</div>' +
//	    '<div class="select2-result-repository__forks">' + span_stock + '</div>' +
	    '</div>' +
	    '</div></div></div>';
    },
    onSelect: function(e, term, item){
        if(!Number(newQtd.value) || Number(newQtd.value) == 0)
            newQtd.value = 1;
        selectedItem = {};
	selectedItem.product_id = item.getAttribute('data-id');
	selectedItem.sku = item.getAttribute('data-sku');
	selectedItem.title = item.getAttribute('data-title');
	selectedItem.supply_name = item.getAttribute('data-supply_name');
        selectedItem.supply_code = item.getAttribute('data-supply_code');
	selectedItem.supply_id = item.getAttribute('data-supply_id');
	selectedItem.supply_line_id = item.getAttribute('data-supply_line_id');
	selectedItem.price = item.getAttribute('data-price');
        selectedItem.unitPriceUnmasked = Number(selectedItem.price); // substitui inputUnitPriceUnmasked
        selectedItem.price = Number(selectedItem.unitPriceUnmasked) * Number(newQtd.value);
	selectedItem.price_calculated = item.getAttribute('data-price_calculated');
	selectedItem.deadline = item.getAttribute('data-deadline');
	selectedItem.working_days = item.getAttribute('data-working_days');
	selectedItem.maximum_discount = item.getAttribute('data-maximum_discount');
        selectedItem.minimum_unitary_price = item.getAttribute('data-minimum_unitary_price');
	selectedItem.product_type_id = item.getAttribute('data-product_type_id');
	selectedItem.stock_qtd = item.getAttribute('data-stock-qtd');
	selectedItem.is_stock = selectedItem.stock_qtd > 0 ? true : false;
	selectedItem.origin = selectedItem.is_stock > 0 ? 'stock' : 'request';
	selectedItem.complement = selectedItem.is_stock ? item.getAttribute('data-complement') : '';

	// document.getElementById("inputTitle").value = selectedItem.title;
	// document.getElementById("inputComplement").value = selectedItem.complement;
	newTitle.value = selectedItem.title;
	newComplement.value = selectedItem.complement;

	if (selectedItem.is_stock) {  // se houver estoque, campo fica readonlyn
	    newComplement.setAttribute('readonly', true);
	} else {
	    document.getElementById('inputComplement').removeAttribute('readonly');
	    //newComplement.removeAttribute('readonly');
	}

        newPrice.value = Number(selectedItem.unitPriceUnmasked) * Number(newQtd.value);

	if(e.type == 'keydown' && e.keyCode == 13) {
	    e.preventDefault();
	    return false;
	}
    },
});

// javascript do ProductsAdd.ctp
function changedQtd()
{
    //    newPrice.value = selectedItem.priceUnmasked = Number(selectedItem.unitPriceUnmasked) * Number(newQtd.value);
    newPrice.value = selectedItem.price = Number(selectedItem.unitPriceUnmasked) * Number(newQtd.value);
}

function changedPrice()
{
    if(selectedItem.product_id){
        selectedItem.price = newPrice.inputmask.unmaskedvalue().toString().replace(/\$|\,/g, '.');
        //        testeShowSelected();
    }
}

function editItem(i)
{
    /*
      criar ainda:
      se total_required ou total_received ou total_complement forem <> 0 ou origin == stock
      não permite editar e dá mensagem avisando que o status do produto tem que mudar
    */

    if(edit.index !== false)
        endEditItem();
    edit.index = i;

    edit.targetTr = document.getElementById('items-' + i);
    edit.targetTr.style.backgroundColor = '#ffffe6';

    document.getElementById('bt-edit-' + i).setAttribute('disabled', true);
    document.getElementById('bt-delete-' + i).setAttribute('disabled', true);
    //    btCancel.removeAttribute('disabled');

    newQtd.value = document.getElementById('items-' + i + '-qtd').value;
    newTitle.value = document.getElementById('items-' + i + '-title').value;
    newPrice.value = Number(document.getElementById('items-' + i + '-price').value);
    newComplement.value = document.getElementById('items-' + i + '-complement').value;

    selectedItem = {};
    selectedItem.product_id = document.getElementById('items-' + i + '-product-id').value;
    selectedItem.sku = document.getElementById('items-' + i + '-sku').value;
    selectedItem.title = document.getElementById('items-' + i + '-title').value;
    selectedItem.complement = document.getElementById('items-' + i + '-title').value;
    selectedItem.supply_name = document.getElementById('items-' + i + '-supply-name').value;
    selectedItem.supply_code = document.getElementById('items-' + i + '-supply-code').value;
    selectedItem.supply_id = document.getElementById('items-' + i + '-supply-id').value;
    selectedItem.supply_line_id = document.getElementById('items-' + i + '-supply-line-id').value;
    selectedItem.status = document.getElementById('items-' + i + '-status').value;
    selectedItem.total_uncovered = document.getElementById('items-' + i + '-total-uncovered').value;
    selectedItem.total_required = document.getElementById('items-' + i + '-total-required').value;
    selectedItem.total_received = document.getElementById('items-' + i + '-total-received').value;
    selectedItem.total_dispatched = document.getElementById('items-' + i + '-total-dispatched').value;
    selectedItem.total_completed = document.getElementById('items-' + i + '-total-completed').value;
    selectedItem.origin = document.getElementById('items-' + i + '-origin').value;
    selectedItem.price = document.getElementById('items-' + i + '-price').value.toString().replace(/\$|\,/g, '.');;
    selectedItem.deadline = document.getElementById('items-' + i + '-deadline').value;
    selectedItem.working_days = document.getElementById('items-' + i + '-working-days').value;
    selectedItem.minimum_unitary_price = document.getElementById('items-' + i + '-minimum-unitary-price').value;
    selectedItem.unitPriceUnmasked = document.getElementById('items-' + i + '-unit-original-price').value;

    // document.getElementById("inputTitle").value = selectedItem.title;
    // document.getElementById("inputComplement").value = selectedItem.complement;
    // selectedItem.origin = document.getElementById('items-' + i + '-origin').value;
    //    selectedItem.unitPriceUnmasked = Number(selectedItem.price); // substitui inputUnitPriceUnmasked
    //    selectedItem.priceUnmasked = Number(selectedItem.price) * Number(newQtd.value); // substitui inputPriceUnmasked

    // if(!Number(newQtd.value) || Number(newQtd.value) == 0)
    //     newQtd.value = 1;
    // newPrice.value = Number(selectedItem.price); // * Number(newQtd.value);

    //    selectedItem.priceUnmasked = document.getElementById('items-' + i + '-price').value.toString().replace(/\$|\,/g, '.');
    //    selectedItem.unitPriceUnmasked = Number(document.getElementById('items-' + i + '-minimum-unitary-price').value);
    //    testeShowSelected();
    if (selectedItem.origin == 'stock') {
	newComplement.setAttribute('readonly', true);
    } else {
	newComplement.removeAttribute('readonly');
    }
    newTitle.select();
}

function endEditItem()
{
    document.getElementById('bt-edit-' + edit.index).removeAttribute('disabled');
    document.getElementById('bt-delete-' + edit.index).removeAttribute('disabled');
    //    btCancel.setAttribute('disabled', true);
    edit.targetTr.style.removeProperty('background-color');
    selectedItem = {};
    edit.index = false;
    newQtd.value = 1;
    newComplement.removeAttribute('readonly');
    newTitle.value = newPrice.value = newComplement.value = '';
    //    testeShowSelected();
}

function endAddItem()
{
    selectedItem = {};
    newQtd.value = 1;
    newComplement.removeAttribute('readonly');
    newTitle.value = newPrice.value = newComplement.value = '';
    newTitle.focus();
    //    testeShowSelected();
}

function cancelEditItem()
{
    if(edit.index !== false)
        endEditItem();
    else
        endAddItem();
}

function deleteItem(i)
{
    document.getElementById('items-' + i + '-id').value = '';
    document.getElementById('items-' + i + '-product-id').value = '';
    document.getElementById('items-' + i + '-sku').value = '';
    document.getElementById('items-' + i + '-supply-name').value = '';
    document.getElementById('items-' + i + '-supply-id').value = '';
    document.getElementById('items-' + i + '-supply-code').value = '';
    document.getElementById('items-' + i + '-supply-line-id').value = '';
    document.getElementById('items-' + i + '-status').value = '';
    document.getElementById('items-' + i + '-total-uncovered').value = '0';
    document.getElementById('items-' + i + '-total-required').value = '0';
    document.getElementById('items-' + i + '-total-received').value = '0';
    document.getElementById('items-' + i + '-total-completed').value = '0';
    document.getElementById('items-' + i + '-deadline').value = '';
    document.getElementById('items-' + i + '-working-days').value = '';
    document.getElementById('items-' + i + '-unit-original-price').value = '';
    document.getElementById('items-' + i + '-minimum-unitary-price').value = '';
    document.getElementById('items-' + i + '-complement').value = '';
    document.getElementById('items-' + i + '-origin').value = '';
    document.getElementById('items-' + i + '-qtd').value = '';
    document.getElementById('items-' + i + '-title').value = '';
    document.getElementById('items-' + i + '-price').value = '%';
    document.getElementById('items-' + i).style.setProperty('display', 'none');
    showTotalItems();
    showTotalPayments();
}

function getIndex(tr)
{
    var s = 'items-';
    var i = tr.id.substr(s.length, tr.id.length);
    return i;
}

function addItem() // adiciona item selectedItem na grade
{
    if (!selectedItem.product_id) {
        modal_warning('Alerta', 'Voce deve selecionar um produto.');
        return;
    }

    if (inputQtd.value === '' || inputQtd.value < 1) {
        modal_warning('Alerta', 'Voce deve digitar uma quantidade.');
        return;
    }

    if (inputPrice.value === '') {
        modal_warning('Alerta', 'Voce deve digitar um preco.');
        return;
    }

    if (Number(selectedItem.price) < Number(selectedItem.minimum_unitary_price)) {
        modal_warning('Alerta', 'O preço digitado é menor que o valor mínimo permitido para o produto');
        newPrice.select();
        return;
    }

    if (edit.index === false) {
        var listTrElements = tbody_container.getElementsByTagName('TR');
        var firstHiddenTr = document.getElementById(getFirstHiddenTr(listTrElements));
        index = getIndex(firstHiddenTr);
        firstHiddenTr.style.removeProperty('display');
        moveSelectedToItem(index);
        endAddItem();
    } else {
        moveSelectedToItem(edit.index);
        endEditItem();
    }
    showTotalItems();
    showTotalPayments();
}

function moveSelectedToItem(i)
{
    document.getElementById('items-' + i + '-qtd').value = newQtd.value;
    document.getElementById('qtd-' + i + '-show').innerHTML = newQtd.value;
    // document.getElementById('items-' + i + '-title').value = newTitle.value;
    // document.getElementById('title-' + i + '-show').innerHTML = newTitle.value;
    document.getElementById('items-' + i + '-title').value = selectedItem.title;
    document.getElementById('title-' + i + '-show').innerHTML = selectedItem.title;
    document.getElementById('items-' + i + '-price').value = Number(selectedItem.price);
    document.getElementById('price-' + i + '-show').innerHTML = Number(selectedItem.price).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
    document.getElementById('items-' + i + '-product-id').value = selectedItem.product_id;
    document.getElementById('items-' + i + '-sku').value = selectedItem.sku;
    document.getElementById('items-' + i + '-supply-name').value = selectedItem.supply_name;
    document.getElementById('items-' + i + '-supply-code').value = selectedItem.supply_code;
    document.getElementById('items-' + i + '-supply-id').value = selectedItem.supply_id;
    document.getElementById('items-' + i + '-supply-line-id').value = selectedItem.supply_line_id;
    document.getElementById('items-' + i + '-status').value = 'uncovered';
    document.getElementById('items-' + i + '-deadline').value = selectedItem.deadline;
    document.getElementById('items-' + i + '-working-days').value = selectedItem.working_days;
    document.getElementById('items-' + i + '-unit-original-price').value = selectedItem.unitPriceUnmasked;
    document.getElementById('items-' + i + '-minimum-unitary-price').value = selectedItem.minimum_unitary_price;
    document.getElementById('items-' + i + '-complement').value = newComplement.value;
    document.getElementById('complement-' + i + '-show').innerHTML = newComplement.value;
    document.getElementById('items-' + i + '-origin').value = selectedItem.origin;
    if(selectedItem.origin == 'stock') {
	document.getElementById('totals-' + i).innerHTML = '0,0,1,0,0';
	buildSparkline(document.getElementById('graphic-' + i), '0,0,1,0,0');
    } else {
	document.getElementById('totals-' + i).innerHTML = '1,0,0,0,0';
	buildSparkline(document.getElementById('graphic-' + i), '1,0,0,0,0');
    }
}

// function testeShowSelected()
// {
//     document.getElementById('span_product_id').innerHTML = 'product_id: ' + selectedItem.product_id;
//     document.getElementById('span_sku').innerHTML = 'sku: ' + selectedItem.sku;
//     document.getElementById('span_title').innerHTML = 'title: ' + selectedItem.title;
//     document.getElementById('span_supply_name').innerHTML = 'supply_name: ' + selectedItem.supply_name;
//     document.getElementById('span_supply_code').innerHTML = 'supply_code: ' + selectedItem.supply_code;
//     document.getElementById('span_supply_id').innerHTML = 'supply_id: ' + selectedItem.supply_id;
//     document.getElementById('span_supply_line_id').innerHTML = 'supply_line_id: ' + selectedItem.supply_line_id;

//     document.getElementById('span_status').innerHTML = 'status: ' + selectedItem.status;

//     document.getElementById('span_total_uncovered').innerHTML = 'total_uncovered: ' + selectedItem.total_uncovered;
//     document.getElementById('span_total_required').innerHTML = 'total_required: ' + selectedItem.total_required;
//     document.getElementById('span_total_received').innerHTML = 'total_received: ' + selectedItem.total_received;
//     document.getElementById('span_total_dispatched').innerHTML = 'total_dispatched: ' + selectedItem.total_dispatched;
//     document.getElementById('span_total_completed').innerHTML ='total_completed: ' + selectedItem.total_completed;

//     document.getElementById('span_price').innerHTML = 'price: ' + selectedItem.price;
//     document.getElementById('span_unitary_price_unmasked').innerHTML = 'unitary_price_unmasked: ' + selectedItem.unitPriceUnmasked;
//     document.getElementById('span_price_calculated').innerHTML = 'price_calculated: ' + selectedItem.price_calculated;
//     document.getElementById('span_deadline').innerHTML = 'deadline: ' + selectedItem.deadline;
//     document.getElementById('span_working_days').innerHTML = 'working_days: ' + selectedItem.working_days;
//     document.getElementById('span_maximum_discount').innerHTML = 'maximum_discount: ' + selectedItem.maximum_discount;
//     document.getElementById('span_minimum_unitary_price').innerHTML = 'minimum_unitary_price: ' + selectedItem.minimum_unitary_price;
//     document.getElementById('span_product_type_id').innerHTML = 'product_type_id: ' + selectedItem.product_type_id;
// }

function showTotalItems()
{
    document.getElementById('totalOrder').innerHTML = '<span style="font-weight: bold; color: blue;">' + Number(calcTotalItems()).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) + '</span>';
}

function calcTotalItems()
{
    var listTrElements = tbody_container.getElementsByTagName('TR');
    var listInputElements;
    var totalOrder = 0;
    for (i = 0; i < listTrElements.length; i++) {
        if (listTrElements.item(i).style.getPropertyValue("display") != 'none'){
            totalOrder += Number(document.getElementById('items-' + i + '-price').value);
        }
    }
    return totalOrder;
}

// ------------------------------------------------------------------
// payments functions
function editPayment(i)
{
    if(payEdit.index !== false)
        endEditPayment();
    payEdit.index = i;

    payEdit.targetTr = document.getElementById('payments-' + i);
    payEdit.targetTr.style.backgroundColor = '#ffffe6';
    document.getElementById('bt-edit-payment-' + i).setAttribute('disabled', true);
    document.getElementById('bt-delete-payment-' + i).setAttribute('disabled', true);
    btCancelPayment.removeAttribute('disabled');

    payEdit.targetPaymentMethod = document.getElementById('payments-' + i + '-payment-methods-id');
    payEdit.targetDetails = document.getElementById('payments-' + i + '-details');
    payEdit.targetFinancier = document.getElementById('payments-' + i + '-financier-id');
    payEdit.targetFinancierText = document.getElementById('payments-' + i + '-financier-text');
    payEdit.targetValue = document.getElementById('payments-' + i + '-value');
    payEdit.targetInstallments = document.getElementById('payments-' + i + '-installments');

    newFinancier = document.getElementById(buildPayment(payEdit.targetPaymentMethod)); // torno visivel select da financeira e pego o id do elemento

    $(newPaymentMethod).val(payEdit.targetPaymentMethod.value).change();
    $(newFinancier).val(payEdit.targetFinancier.value).change();

    newDetails.value = payEdit.targetDetails.value;

    newValue.value = Number(payEdit.targetValue.value);
    newInstallments.value = payEdit.targetInstallments.value;
    newValue.select();
}

function endEditPayment()
{
    document.getElementById('bt-edit-payment-' + payEdit.index).removeAttribute('disabled');
    document.getElementById('bt-delete-payment-' + payEdit.index).removeAttribute('disabled');
    btCancelPayment.setAttribute('disabled', true);
    payEdit.targetTr.style.removeProperty('background-color');
    payEdit.index = false;
    clearPaymentFields();
}

function endAddPayment()
{
    clearPaymentFields();
}

function clearPaymentFields()
{
    newValue.value = newInstallments.value = newDetails = '';
    $(newPaymentMethod).prop('selectedIndex', '').change();
    $(newFinancier).prop('selectedindex', '').change();
}

function cancelEditPayment()
{
    if(payEdit.index !== false)
        endEditPayment();
    else
        endAddPayment();
}

function deletePayment(i)
{
    document.getElementById('payments-' + i + '-payment-methods-id').value = '';
    document.getElementById('payments-' + i + '-details').value = '';
    document.getElementById('payments-' + i + '-financier-id').value = '';
    document.getElementById('payments-' + i + '-value').value = '';
    document.getElementById('payments-' + i + '-installments').value = '';
    document.getElementById('payments-' + i + '-payment-methods-text').value = '';
    document.getElementById('payments-' + i + '-financier-text').value = '';
    document.getElementById('payments-' + i).style.setProperty('display', 'none');
    showTotalPayments()
}

// function getFirstHiddenTr(obj)
// {
//     for (i = 0; i < obj.length; i++) {
//         // if (obj.item(i).attributes.getNamedItem("style").value == 'display:none')
//         if (obj.item(i).style.getPropertyValue("display") == 'none'){
//             return obj.item(i).id;
//         }
//     }
// }

function getPaymentIndex(tr)
{
    var s = 'payments-';
    var i = tr.id.substr(s.length, tr.id.length);
    return i;
}

function addPayment()
{
    if (inputPaymentMethod.value === '' || inputPaymentMethod.value < 1) {
        modal_warning('Alerta', 'Voce deve digitar um método de pagamento.');
        return;
    }

    if (inputValue.value === '') {
        modal_warning('Alerta', 'Voce deve selecionar um valor.');
        return;
    }

    if (inputInstallments.value === '') {
        modal_warning('Alerta', 'Voce deve digitar as parcelas.');
        return;
    }

    if (payEdit.index === false) { // add
        var listTrElements = tbody_container_payments.getElementsByTagName('TR');
        var firstHiddenTr = document.getElementById(getFirstHiddenTr(listTrElements));
        var indexPayment = getPaymentIndex(firstHiddenTr);
        var targetPaymentMethodText = document.getElementById('payments-' + indexPayment + '-payment-methods-text');
        var targetPaymentMethod = document.getElementById('payments-' + indexPayment + '-payment-methods-id');
        var targetDetails = document.getElementById('payments-' + indexPayment + '-details');
        var targetFinancier = document.getElementById('payments-' + indexPayment + '-financier-id');
        var targetFinancierText = document.getElementById('payments-' + indexPayment + '-financier-text');
        var targetValue = document.getElementById('payments-' + indexPayment + '-value');
        var targetInstallments = document.getElementById('payments-' + indexPayment + '-installments');

        targetPaymentMethodText.value = newPaymentMethod.options[newPaymentMethod.selectedIndex].innerHTML
        document.getElementById('payment-methods-text-' + indexPayment + '-show').innerHTML = targetPaymentMethodText.value;

        targetPaymentMethod.value = newPaymentMethod.value;
        targetDetails.value = newDetails.value;

        newFinancier = getValueOfFinancier();
        targetFinancier.value = newFinancier;

        if (targetFinancier.value !== '' && targetFinancier.value !== undefined)
            targetFinancierText.value = getTextOfFinancier(); // newFinancier.options[newFinancier.selectedIndex].innerHTML
        else
            targetFinancierText.value = '';
        document.getElementById('financier-text-' + indexPayment + '-show').innerHTML = targetFinancierText.value;

        targetDetails.value = '{"chave":"valor"}'; // tirar

        targetValue.value = newValue.inputmask.unmaskedvalue().toString().replace(/\$|\,/g, '.');
        //.toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });

        document.getElementById('value-' + indexPayment + '-show').innerHTML = Number(targetValue.value).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
        targetInstallments.value = newInstallments.value;
        document.getElementById('installments-' + indexPayment + '-show').innerHTML = targetInstallments.value;
        firstHiddenTr.style.removeProperty('display');
        endAddPayment()
    } else { // edit
        payEdit.targetPaymentMethod.value = newPaymentMethod.value;
        payEdit.targetDetails.value = newDetails.value;
        payEdit.targetFinancier.value = getValueOfFinancier();
        payEdit.targetFinancierText.value = getTextOfFinancier();
        document.getElementById('financier-text-' + payEdit.index + '-show').innerHTML = payEdit.targetFinancierText.value;
        payEdit.targetValue.value = Number(newValue.value).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' });
        document.getElementById('value-' + payEdit.index + '-show').innerHTML = payEdit.targetValue.value;
        payEdit.targetInstallments.value = newInstallments.value;
        document.getElementById('installments-' + payEdit.index + '-show').innerHTML = payEdit.targetInstallments.value;
        endEditPayment();
    }

    // $(newPaymentMethod).prop('selectedIndex', '').change();
    // newValue.value = newInstallments.value = newDetails = '';
    // cleanFinancier();
    // newValue.focus();

    showTotalPayments();
}

function cleanFinancier()
{
    document.getElementById('credcard').value = '';
    document.getElementById('debitcard').value = '';
    document.getElementById('checkbook').value = '';

}

function getValueOfFinancier()
{
    switch(newPaymentMethod.value) {
    case '2':
        return document.getElementById('inputCredcard').value;
        break;
    case '3':
        return document.getElementById('inputDebitcard').value;
        break;
    case '4':
        return document.getElementById('inputCheckbook').value;
        break;
    }
}

function getTextOfFinancier()
{
    switch(newPaymentMethod.value) {
    case '2':
        return document.getElementById('inputCredcard').options[document.getElementById('inputCredcard').selectedIndex].innerHTML;
        break;
    case '3':
        return document.getElementById('inputDebitcard').options[document.getElementById('inputDebitcard').selectedIndex].innerHTML;
        break;
    case '4':
        return document.getElementById('inputCheckbook').options[document.getElementById('inputCheckbook').selectedIndex].innerHTML;
        break;
    }
}

function hiddenAllSpans()
{
    document.getElementById('credcard').style.setProperty('display', 'none');
    document.getElementById('debitcard').style.setProperty('display', 'none');
    document.getElementById('checkbook').style.setProperty('display', 'none');
}

function buildPayment(t)
{
    var ret = '';
    hiddenAllSpans();
    if(t.value == '') {
        ret = '';
    }


    if(t.value == 1) {
        inputInstallments.value = 1;
        inputInstallments.setAttribute('disabled', true);
        ret = '';
    }
    if(t.value == 5) {
        inputInstallments.value = 1;
        inputInstallments.setAttribute('disabled', true);
        ret = '';
    }
    if(t.value == 2) {
        document.getElementById('credcard').style.removeProperty('display');
        inputInstallments.removeAttribute('disabled');
        $('#inputCredcard').prop('selectedIndex', '').change();
        ret = 'inputCredcard';
    }
    if(t.value == 3) {
        document.getElementById('debitcard').style.removeProperty('display');
        inputInstallments.value = 1;
        inputInstallments.setAttribute('disabled', true);
        $('#inputDebitcard').prop('selectedIndex', '').change();
        ret = 'inputDebitcard';
    }
    if(t.value == 4) {
        document.getElementById('checkbook').style.removeProperty('display');
        inputInstallments.removeAttribute('disabled');
        $('#inputCheckbook').prop('selectedIndex', '').change();
        ret = 'inputCheckbook';
    }
    return ret;
}

function getVisibleFinancier()
{
    var display = '';
    var financierElements = document.getElementById('divFinanciers');
    var listSpans = financierElements.getElementsByClassName('financier');

    for(var i = 0; i < listSpans.length; i++) {
        display = listSpans.item(i).style.getPropertyValue("display");
        if (display != 'none') {
            return listSpans.item(i).id;
        }
    }
}

function showTotalPayments()
{
    var totalPaid = Number(calcTotalPayments()) - Number(calcTotalItems());

    var color;

    if (totalPaid < 0) {
        color = 'red';
    } else {
        if (totalPaid == 0) {
            color = 'green';
        } else {
            if (totalPaid > 0) {
                color = 'blue';
            }
        }
    }

    document.getElementById('totalPaid').innerHTML = '<span style="font-weight: bold; color: ' + color + ';">' + Number(totalPaid).toLocaleString('pt-BR', { style: 'currency', currency: 'BRL' }) + '</span>';
}

function calcTotalPayments()
{
    var listTrElements = tbody_container_payments.getElementsByTagName('TR');
    var listInputElements;
    var totalPayments = 0;
    for (i = 0; i < listTrElements.length; i++) {
        if (listTrElements.item(i).style.getPropertyValue("display") != 'none'){
            totalPayments += Number(document.getElementById('payments-' + i + '-value').value);
        }
    }
    return totalPayments;
}

// ------------------------------------------------------------------
// common functions
function existItems(obj)
{
    var existVisible = false;
    //        var obj = tbody_container.getElementsByTagName('TR');
    for (i = 0; i < obj.length; i++) {
        // if (obj.item(i).attributes.getNamedItem("style").value != 'display:none') {
        if (obj.item(i).style.getPropertyValue("display") != 'none'){
            existVisible = true;
        }
    }
    return existVisible;
}

function validateOrder(e)
{
    if(!existItems(itemsSales.getElementsByTagName('TR'))) {
        e.returnValue = false;
	modal_warning('Alerta', 'O pedido precisa de pelo menos um item vendido');
        return;
    }

    if(!existItems(itemsPayments.getElementsByTagName('TR'))) {
        e.returnValue = false;
	modal_warning('Alerta', 'Não consta nenhum pagamento no pedido.');
        return;
    }

    if(!unnecessaryCustomerAddress.checked) {
        if(!existsCustomerAddress()){
            e.returnValue = false;
            modal_warning('Alerta', 'O endereço do cliente deve ser informado.\nCaso não seja necessário, marque "endereço desnecessário" antes de salvar');;
            return;
        }
    }

    if(!deliveryLess.checked && deliveryForeseenDate.value == '') {
        e.returnValue = false;
        modal_warning('Alerta', 'A data de previsão da entrega deve ser informada.');
        return;
    }

    if(!deliveryLess.checked && !sameCustomerAddress.checked) {
        if(!existsDeliveryAddress()) {
            e.returnValue = false;
            modal_warning('Alerta', 'O endereço de entrega deve ser informado.\nCaso não haja entrega, marque "sem entrega",\n ou caso seja o mesmo do cliente, marque "mesmo endereço do cliente".');
            return;
        }
    }

    if(sameCustomerAddress.checked && (unnecessaryCustomerAddress.checked || !existsCustomerAddress())) {
        e.returnValue = false;
        modal_warning('Alerta', 'Se a entrega é no endereço do cliente, o mesmo deve ser informado');
        return;
    }

    // muito importante. quando o campo está desabilitado, o formulario não é
    // aceito no CsrfProtectionMiddleware
    inputInstallments.removeAttribute('disabled');
    //        removeAttributesOfAddress();
    e.returnValue =  true;
}

function getFirstHiddenTr(obj)
{
    for (i = 0; i < obj.length; i++) {
        if (obj.item(i).style.getPropertyValue("display") == 'none'){
            return obj.item(i).id;
        }
    }
}

function existsCustomerAddress()
{
    if(customerCity.value == '' || customerState.value == '' || customerThorougfare == '') {
        return false;
    } else {
        return true;
    }
}

function existsDeliveryAddress()
{
    if(deliveryCity.value == '' || deliveryState.value == '' || deliveryThoroughfare == '') {
        return false;
    } else {
        return true;
    }
}

function changeUnnecessaryCustomerAddress()
{
    if(unnecessaryCustomerAddress.checked) {
        cleanAddress('customer');
        divCustomerAddress.style.setProperty('display', 'none');
	sameCustomerAddress.checked = false;
	divSameCustomerAddress.style.setProperty('display', 'none');
    } else {
        divCustomerAddress.style.removeProperty('display');
	divSameCustomerAddress.style.removeProperty('display');
    }
}

function cleanAddress(t)
{
    document.getElementById(t + '-address-postal-code').value = '';
    document.getElementById(t + '-address-state_id').value = '';
    document.getElementById(t + '-address-city_id').value = '';
    document.getElementById(t + '-address-neighborhood').value = '';
    document.getElementById(t + '-address-thoroughfare').value = '';
    document.getElementById(t + '-address-number').value = '';
    document.getElementById(t + '-address-complement').value = '';
    document.getElementById(t + '-address-reference').value = '';
}

function changeSameCustomerAddress()
{
    if (sameCustomerAddress.checked) { // mesmo endereco do cliente
	cleanAddress('delivery');
        divDeliveryAddress.style.setProperty('display', 'none');
    } else {
        divDeliveryAddress.style.removeProperty('display');
    }
}

function changeDeliveryLess()
{
    if (deliveryLess.checked) {
        divForeseenDate.style.setProperty('display', 'none');
        divForeseenHorary.style.setProperty('display', 'none');
        divSameCustomerAddress.style.setProperty('display', 'none');
        divDeliveryAddress.style.setProperty('display', 'none');
	divWorkingDays.style.setProperty('display', 'none');
	divForeseenDays.style.setProperty('display', 'none');
	document.getElementById('delivery-same-customer-address').checked = false;
    } else {
        divForeseenDate.style.removeProperty('display');
        divForeseenHorary.style.removeProperty('display');
        divSameCustomerAddress.style.removeProperty('display');
	divWorkingDays.style.removeProperty('display');
	divForeseenDays.style.removeProperty('display');
	if(unnecessaryCustomerAddress.checked) {
            divSameCustomerAddress.style.setProperty('display', 'none');
	}
        divDeliveryAddress.style.setProperty('display', 'contents');
    }
    //    changeSameCustomerAddress(document.getElementById('delivery-same-customer-address').checked);
}

function checkCpfCnpj(cpfCnpj)
{
    cpfCnpj = cpfCnpj.inputmask.unmaskedvalue();

    if(cpfCnpj == '')
	return;

    if(Number(cpfCnpj.length) != 14 && Number(cpfCnpj.length) != 11)
        return;

    $.ajax({
     	type: 'get',
	url: '/customers/findcpfcnpj/' + cpfCnpj,
     	success: function(response) {
    	    if(response.error) {
    	    	alert(response.error);
    	    } else {
                if (typeof(response) == 'string') {
                    response = JSON.parse(response);
                }
                r_checkCpfCnpj(response);
    	    }
     	},
     	error: function(e) {
     	    alert("An error occurred: " + e.responseText.message);
     	}
    });
}

function r_checkCpfCnpj(response)
{
    if(response != null) {
        getCustomer(response);
    } // else {
    //     console.log('cep_cnpj não encontrado');
    // }
}

function getCustomer(response)
{
    document.getElementById('existing-customer').value = response.id;
    document.getElementById('customer-name').value = response.name;
    document.getElementById('customer-cpf-cnpj').value = response.cpf_cnpj;
    document.getElementById('customer-birth-date').value = response.birth_date !== null ? response.birth_date : '';
    document.getElementById('customer-phone').value = response.phone !== null ? response.phone : '';
    document.getElementById('customer-phone2').value = response.phone2 !== null ? response.phone2 : '';
    document.getElementById('customer-mobile').value = response.mobile !== null ? response.mobile : '';
    document.getElementById('customer-email').value = response.email !== null ? response.email : '';

    if (response.address != null) {
        document.getElementById('customer-address-postal-code').value = response.address.postal_code !== null ? response.address.postal_code : '';
        cancelCitySelect = true;
        $('#customer-address-state_id').val(response.address.state_id).change();
        document.getElementById('customer-address-neighborhood').value = response.address.neighborhood !== null ? response.address.neighborhood : '';
        document.getElementById('customer-address-thoroughfare').value = response.address.thoroughfare !== null ? response.address.thoroughfare : '';
        document.getElementById('customer-address-number').value = response.address.number !== null ? response.address.number : '';
        document.getElementById('customer-address-complement').value = response.address.complement !== null ? response.address.complement : '';
        document.getElementById('customer-address-reference').value = response.address.reference  !== null ? response.address.reference : '';

        var citySelect = $('#customer-address-city_id');
        $.ajax({
            type: 'GET',
            url: '/addresses/get-cities-by-state/' + response.address.state_id,
        }).then(function (data) {
            citySelect.empty();
            citySelect.append(data);
            citySelect.val(response.address.city_id);
            citySelect.change();
            //        newCityId = false;
            cancelCitySelect = false;
        });
    }
}

function buildSparkline(targetElement, str)
{
    /**********************************************************
    * essa funcção está escrita diferente aqui e no items.js
    * padronizar!!!!!
    **********************************************************/
    if(str != '') {
	var arrValues = str.split(",");
	$(targetElement).sparkline(arrValues, {
            type: 'pie',
            sliceColors: [
		'#ff0000', /* pendente */
		'#f4c113', /* requisitado */
		'#008000', /* recebido */
		'#ff6680', /* despachado */
		'#3333ff'   /* concluído */
            ]});
    }
}

function showAllGraphics()
{
        for (i = 0; i < trSpansTotals.length; i++) {
        var str = trSpansTotals.item(i).id;
        var arr = str.split('-');

        buildSparkline(document.getElementById('graphic-' + arr[1]), trSpansTotals.item(i).innerHTML);
    }
}

function calcForessen(wd)
{
    if (foreseenDays.value == '' && deliveryForeseenDate.value == '') {
	return;
    }


    if(foreseenDays.value != '')
	daysToForeseen();
    else
	foreseenToDays();
}

function daysToForeseen()
{
    if (dateOrder.value == '') {
	modal_warning('Alerta', 'Você precisa digitar a data do pedido para calcular a data de entrega');
	return;
    }

    if (foreseenDays.value == '' || foreseenDays.value == null)
	return;

    var dtstart = new Date(brDateToShortFormat(dateOrder.value));
    var dttarget = new Date(brDateToShortFormat(dateOrder.value));
    var days = Number(foreseenDays.value);

    if (workingDays.checked) {
	var wd = 0;
	for (i = 0; i < days; i++) {
	    dttarget.setDate(dttarget.getDate() + 1);
	    if (dttarget.getDay() == 6 || dttarget.getDay() == 0)
		wd++;
	}
	days = days + wd;
    }

    dtstart.setDate(dtstart.getDate() + days);
    if(dtstart.getDay() == 6) {
	dtstart.setDate(dtstart.getDate() + 2);
    } else {
	if(dtstart.getDay() == 0) {
	    dtstart.setDate(dtstart.getDate() + 1);
	}
    }

    deliveryForeseenDate.value = dtstart.toLocaleDateString();
}

function foreseenToDays()
{
    if (dateOrder.value == '') {
	modal_warning('Alerta', 'Você precisa digitar a data do pedido para calcular a data de entrega');
	return;
    }

    if (deliveryForeseenDate.value == '' || deliveryForeseenDate.value == null)
	return;

    var dtstart = new Date(brDateToShortFormat(dateOrder.value));
    var dtend = new Date(brDateToShortFormat(deliveryForeseenDate.value));

    var days = (dtend.getTime() - dtstart.getTime()) / 86400000;
    if((parseFloat(days) - parseInt(days)) > 0)
	days = parseInt(days) + 1;

    if (workingDays.checked) {
	var wd = 0;
	for (i = 0; i < days; i++) {
	    dtstart.setDate(dtstart.getDate() + 1);
	    if (dtstart.getDay() == 6 || dtstart.getDay() == 0)
		wd++;
	}
	days = days - wd;
    }

    foreseenDays.value = days;
}

function brDateToShortFormat(d)
{
    var a = d.split('/');
    return a[1] + '/' + a[0] + '/' + a[2];
}

// function shortFormatcDateToBr()
// {
//     var a = d.split('-');
//     return a[2] + '/' + a[1] + '/' + a[0];
// }

changeUnnecessaryCustomerAddress();
changeDeliveryLess();
changeSameCustomerAddress();
showTotalItems();
showTotalPayments();

$(function() {
    $('#delivery-working-days').on('change', function () {
	calcForessen($(this).prop('checked'));
    });

    $(foreseenDays).on('change', function () {
	daysToForeseen();
    });

    $(deliveryForeseenDate).on('change', function () {
	foreseenToDays();
    });	

    $('.nav-tabs a[href="#items"]').on('shown.bs.tab', function(){
	showAllGraphics();
    });

    $('.nav-tabs a[href="#delivery"]').on('shown.bs.tab', function(){
	changeDeliveryLess();
    });
});
