var tbodyContainer = document.getElementById('tbody');
var chkAll = document.getElementById('chk-all');
var btDel = document.getElementById('btdelete');
var inputIds = document.getElementById('ids');

var listCheckboxElements = document.getElementsByClassName('chk-box');

function checkOrUncheck(c)
{
    var existChecked = false;
    if (!c.checked) {
        chkAll.checked = false;
        for (i = 0; i < listCheckboxElements.length; i++)
            if (listCheckboxElements[i].checked)
                existChecked = true;
    } else {
        existChecked = true;
    }
    btDel.disabled = !existChecked;
}

function chkAllChange()
{
    for (i = 0; i < listCheckboxElements.length; i++)
        listCheckboxElements[i].checked = chkAll.checked;

    btDel.disabled = !chkAll.checked;
}

function delSomeRecs(e)
{
    var plural = '';
    var ids = [];
    for (i = 0; i < listCheckboxElements.length; i++) {
        if(listCheckboxElements[i].checked)
            ids.push(listCheckboxElements[i].attributes.getNamedItem('value').value);
    }
    if (ids.length > 0) {
        if (ids.length > 1)
            plural = 's';
        if (confirm('Tem certeza de que deseja excluir este' + plural + ' registro' + plural + '?')) {
            inputIds.value = ids;
            return true;
        }
    }
    e.preventDefault();
    return false;
}
