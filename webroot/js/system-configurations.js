function cleanDivFolders(event) {
    $('#list-sents-folder').html('');
    return false;
}

function setListSentsFolder() {
    console.log('entrou em setListSentsFolder');
    $('#configuration-sents-folder').val($('input:radio[name=folders]:checked').val());
}

function validateCheckForm()
{
    if ($('#configuration-imap-email').val() != '' &&
	$('#configuration-imap-user').val() != '' &&
	$('#configuration-imap-password').val() != '' &&
	$('#configuration-imap-server').val() != '' &&
	$('#configuration-imap-port').val() != '') {
	// $('#configuration-smtp-server').val() != '' &&
	// $('#configuration-smtp-port').val() != '') {
	
//	$('#waiting-check').html('<i class="fa fa-refresh fa-spin"></i>');
	
	$('#check-imap-email').val($('#configuration-imap-email').val());
	$('#check-imap-user').val($('#configuration-imap-user').val());
	$('#check-imap-password').val($('#configuration-imap-password').val());
	$('#check-imap-server').val($('#configuration-imap-server').val());
	$('#check-imap-port').val($('#configuration-imap-port').val());
	$('#check-imap-layer').val($('#configuration-imap-layer').is(":checked") ? '1' : '0');
	// $('#smtp-server').val($('#configuration-smtp-server').val());
	// $('#smtp-port').val($('#configuration-smtp-port').val());
	// $('#smtp-layer').val($('#configuration-smtp-layer').is(":checked") ? '1' : '0');
	return true;
    } else {
	return false;
    }
}

function validateTestForm()
{
    if ($('#configuration-imap-email').val() != '' &&
	$('#configuration-imap-user').val() != '' &&
	$('#configuration-imap-password').val() != '' &&
	$('#configuration-imap-server').val() != '' &&
	$('#configuration-imap-port').val() != '' &&
	$('#configuration-smtp-server').val() != '' &&
	$('#configuration-smtp-port').val() != '') {
	
//	$('#waiting-check').html('<i class="fa fa-refresh fa-spin"></i>');
	
	$('#test-imap-email').val($('#configuration-imap-email').val());
	$('#test-imap-user').val($('#configuration-imap-user').val());
	$('#test-imap-password').val($('#configuration-imap-password').val());

	$('#test-smtp-server').val($('#configuration-smtp-server').val());
	$('#test-smtp-port').val($('#configuration-smtp-port').val());
	$('#test-smtp-layer').val($('#configuration-smtp-layer').is(":checked") ? '1' : '0');
	
	$('#test-imap-server').val($('#configuration-imap-server').val());
	$('#test-imap-port').val($('#configuration-imap-port').val());
	$('#test-sents-folder').val($('#configuration-sents-folder').val());
	$('#test-imap-layer').val($('#configuration-imap-layer').is(":checked") ? '1' : '0');
	return true;
    } else {
	return false;
    }
}

function checkEmailConfiguration()
{
    if (validateCheckForm()) {
	var posting = $.post('/system-configurations/check-configuration', $('#form-check-configuration').serialize());

    	$('#waiting-check').html('<i class="fa fa-refresh fa-spin"></i>');

	posting.done(function( data ) {

	    if (data != 'false') {
		var listFolders = JSON.parse(data);
		var str = '';
		var checked;
		for (i = 0; i < listFolders.length; i++) {
		    checked = listFolders[i] == $('#configuration-sents-folder').val() ? 'checked' : '';
		    str += '<div class="checkbox"><label><input type="radio" name="folders" value="' + listFolders[i] + '" ' + checked + '>&nbsp;' + listFolders[i] + '</label></div>';
		}
//		$('#list-sents-folder').append(str);
		$('#list-sents-folder').html(str);
		$('.test-send').css("display","inline");
//		setListSentsFolder();		
	    } else {
    		modal_warning('Alerta', 'Não é possível acessar sua caixa de emails enviados com os dados fornecidos. Por favor, verifique as informações e tente novamente.');
	    }
	    $('#waiting-check').html('');
	    modal_msg('Informação', 'A verificação das configurações de email não retornaram erro. Faça um teste de envio.');
	});
    } else {
    	modal_warning('Alerta', 'Para verificar a configuração os campos devem estar preenchidos');
    }
}
// });

function testShipping()
{
    if (!validateTestForm()) {
    	modal_warning('Alerta', 'Para testar o envio de email os campos devem estar preenchidos');
	return;
    }

    if ($('#test-sents-folder').val() == '') {
    	modal_warning('Alerta', 'Você precisa selecionar uma pasta de emails enviados para fazer o teste de envio.');
	return;
    }
	
    if ($('#test-target-email').val() != '') {
	$('#target-email').val($('#test-target-email').val());
	var posting = $.post('/system-configurations/test-shipping', $('#form-test-shipping').serialize());

    	$('#waiting-shipping').html('<i class="fa fa-refresh fa-spin"></i>');

	posting.done(function( data ) {

	    if (data != 'false') {
		console.log(data);
	    } else {
    		modal_warning('Alerta', 'Não foi possível enviar o email, por favor verifique suas configurações e tente novamente.');
	    }
	    $('#waiting-shipping').html('');
	    modal_msg('Informação', 'O teste de envio não retornou nenhum erro. Você deve ter recebido um email em "' + $('#test-target-email').val() + '" e a pasta "' + $('#configuration-sents-folder').val() + '" da conta "' + $('#configuration-imap-user').val() + '" deve estar com um registro desse envio.');
	});
    } else {
    	modal_warning('Alerta', 'Você precisa digitar um email para fazer o teste.');
    }
}
