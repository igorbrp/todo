var paramType = '';
var cancelCitySelect = false;
$(document).ready(function() {
    var newCityId = false;
    var postal_code_orig = '';
    var state_id_orig = '';
    var neighborhood_orig = '';
    var thoroughfare_orig = '';

    $('.lnk-find-address').click(function(event) {
        paramType = this.getAttribute('param_type');
        var pc = $('#' + paramType + 'address-postal-code').val();
        if(pc.length < 8)
            return false;

        $.ajax({
     	    type: 'get',
	        url: '/addresses/find-cep/' + pc,
     	    success: function(response) {
    	        if(response.error) {
    	    	    alert(response.error);
    	        } else {
                    r_verifyCep(response, paramType);
    	        }
     	    },
     	    error: function(e) {
     	        modal_danger('Erro', "<p>Não foi possível se conectar ao servidor para checar o CEP.</p><p>Verifique sua conexão com a internet.</p>");
     	        console.log('error: ' + e);
     	    }
        });
    });

    function r_verifyCep(response, paramType)
    {
        if (typeof(response) == 'string')
            response = JSON.parse(response);

        // console.log('entrou em r_verifyCep');
        // console.log('typeof response: ' + typeof(response));
        // if (paramType != '')
        //     pramType = paramType + '-';
        if (response.cep) {
            // console.log('entrou em r_verifyCep if response cep');
            newCityId = response.ibge;
    	    $('#' + paramType + 'address-state_id').val(response.coduf);
    	    $('#' + paramType + 'address-state_id').change();
    	    $('#' + paramType + 'address-neighborhood').val(response.bairro);
    	    $('#' + paramType + 'address-neighborhood').change();
    	    $('#' + paramType + 'address-thoroughfare').val(response.logradouro);
    	    $('#' + paramType + 'address-thoroughfare').change();
            $('#' + paramType + 'address-valid-address').val(1);
            $('#' + paramType + 'msg-find-address').html('<small class="label bg-green"><i class="fa fa-check"></i></small>&nbsp;CEP verificado');
            postal_code_orig = $('#' + paramType + 'address-postal-code').val();
            state_id_orig = $('#' + paramType + 'address-state_id').val();
            neighborhood_orig = $('#' + paramType + 'address-neighborhood').val();
            thoroughfare_orig = $('#' + paramType + 'address-thoroughfare').val();
        }
        else
        {
            $('#' + paramType + 'address-valid-address').val(0);
            $('#' + paramType + 'msg-find-address').html('<small class="label bg-yellow"><i class="fa fa-warning"></i></small>&nbsp;CEP não verificado');
        }
    }

//    $('#' + paramType + 'address-state_id').change(function() {
    $('.address_state_id').change(function() {
        // console.log('entrou em address_state_id.change');
        if(cancelCitySelect) { // não atualiza pq será atualizado em outro lugar
            return;
        }
        paramType = this.getAttribute('param_type');
        // console.log('este e o param_type: '  + this.getAttribute('param_type'));
        // console.log('#' + paramType + 'address-state_id');
        var citySelect = $('#' + paramType + 'address-city_id');
        var selectedValue = $('#' + paramType + 'address-state_id').val();
        if(selectedValue == '') {
            citySelect.empty();
            return;
        }
        $.ajax({
            type: 'GET',
            url: '/addresses/get-cities-by-state/' + selectedValue,
        }).then(function (data) {
            citySelect.empty();
            citySelect.append(data);
            // console.log('newCityId em address: ' + newCityId);
            if (newCityId != false) {
                citySelect.val(newCityId);
                citySelect.change();
                newCityId = false;
            }
        });
    });

    $('.check_address').change(function() {
        // console.log('entrou em check address');
        if(postal_code_orig != $('#' + paramType + 'address-postal-code').val() || state_id_orig != $('#' + paramType + 'address-state_id').val() ||
           neighborhood_orig != $('#' + paramType + 'address-neighborhood').val() || thoroughfare_orig != $('#' + paramType + 'address-thoroughfare').val()) {
            $('#' + paramType + 'msg-find-address').html('<small class="label bg-yellow"><i class="fa fa-warning"></i></small>&nbsp;CEP não verificado');
        } else {
            $('#' + paramType + 'msg-find-address').html('<small class="label bg-green"><i class="fa fa-check"></i></small>&nbsp;CEP verificado');
        }
    });
});

    // function reloadSelectCities(element)
    // {
    //     paramType = element.getAttribute('param_type');
    //     console.log('este e o param_type: '  + element.getAttribute('param_type'));
    //     console.log('#' + paramType + 'address-state_id');
    //     var citySelect = $('#' + paramType + 'address-city_id');
    //     var selectedValue = $('#' + paramType + 'address-state_id').val();
    //     if(selectedValue == '') {
    //         citySelect.empty();
    //         return;
    //     }
    //     $.ajax({
    //         type: 'GET',
    //         url: '/addresses/get-cities-by-state/' + selectedValue,
    //     }).then(function (data) {
    //         citySelect.empty();
    //         citySelect.append(data);
    //         if (newCityId != false) {
    //             citySelect.val(newCityId);
    //             citySelect.change();
    //             newCityId = false;
    //         }
    //     });
    // }
