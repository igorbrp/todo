// var trSpansTotalOrder = '';
// var trSpansHeaderOrder = document.getElementsByClassName('totals-order');

function loadOrderInfo(id)
{
    var i;
    var lineItems = $('#group-item-' + id).find('.line-item');
    var arrTotalsOrder = [0, 0, 0, 0, 0];
    lineItems.each(function () {
	var arr = $('#totals-' + $(this).attr('data-item-id')).text().split(",");
	buildSparkline($('#graphic-' + $(this).attr('data-item-id')), arr);
	for (i = 0; i < arrTotalsOrder.length; i++)
	    arrTotalsOrder[i] += Number(arr[i]);
    });
    buildSparkline($('#graphic-order-' + id), arrTotalsOrder);
}

function calcTotalsOrder(arrTot, arr)
{
    for (var i = 0; i < arr.length; i++)
	arrTot[i] += Number(arrTot[i]) + Number(arr[i]);
}

function confirmChangeOrderStatus(order_id, from, to)
{
    var data = {item_id: order_id, from: from, to: to};
    var html = '';

    if (to == 'dispatched') {
        html = '<div class="form-group required"><label class="control-label" for="modal-datepicker">data:&nbsp;</label><br><div class="input-group date"><div class="input-group-addon"><i class="fa fa-calendar"></i></div><input type="text" class="form-control pull-right" id="modal-datepicker"></div></div><br> ';
        html += document.getElementById('divVehicles').innerHTML;
        modal_dispatch('Despachar mercadoria', html, orderDetails, data);
        // }
    } else {
        html = msg == '' ? 'Confirma a alteracao do registro?' : msg;
        modal_form('Confirme a ação.', html, orderDetails, data);
    }

}

function confirmChangeBulk(order_id, to)
{
    var msg = '';
    var html = '';
    var data = {};
    data.order_id = order_id;
    data.to = to;
    var o, n;
    if (to == 'required') {
	o = 'pendentes';
	n = 'requisitados';
    } else if (to == 'received') {
	o = 'requisitados';
	n = 'recebidos';
	msg = '<p>O número da nota fiscal digitada aqui será atribuída a todos os items.</p><p>Entre com o número da nota fiscal e confirme o recebimento do item:</p>';
	msg += document.getElementById('divModalFormInvoice').innerHTML;
    } else if (to == 'dispatched') {
	o = 'recebidos';
	n = 'despachados';
	msg = document.getElementById('divModalFormDispatched').innerHTML;
    } else if (to == 'completed') {
	o = 'despachados';
	n = 'concluídos';
    }
    html = '<p>Todos os itens ' + o + ' do pedido serão ' + n + '.</p>';
    html += msg;
    if (to == 'dispatched') {
        modal_dispatch('Confirme a ação', html, rConfirmChangeBulk, data);
    } else {
	modal_confirm_warning('Confirme a ação', html, rConfirmChangeBulk, data);
    }
}

function rConfirmChangeBulk(data)
{
    var dispatch_date;
    var vehicle;

    if(data.to == 'dispatched') {
        if(document.getElementById('modal-datepicker').value == null) {
            modal_err_msg('Escolha uma data válida para o despacho');
            return;
        }

        dispatch_date = document.getElementById('modal-datepicker').value;
        if(!Inputmask.isValid(dispatch_date, { alias: "datetime", inputFormat: "dd/mm/yyyy"})) {
            modal_err_msg('Escolha uma data válida para o despacho');
            return;
        }

        vehicle = document.getElementById('modal-vehicle').value;
        if(vehicle == null || vehicle == '') {
            modal_err_msg('Escolha um veículo para o despacho');
            return;
        }

        data.vehicle = vehicle;
        data.dispatch_date = dispatch_date;
    }

    if (data.to == 'received') {
	data.invoice = document.getElementById('modal-invoice').value;
    }

    var returnFunction = rChangeStatusBulk;
    var targeturl = '/orders/change-status-bulk';

    $('#modal_msg_body').html();
    ajax_get(targeturl, data, '', returnFunction, data.order_id);
}

function rChangeStatusBulk(response, param)
{

    // console.log('response: ' );
    // console.log(response);
    console.log('param');
    console.log(param);
    console.log('index: ' + $('#line-order-' + param).index());
    $('#li-order-' + param).html(response);
    regitryEvents();
    $('#group-item-' + param).collapse('show');
    // collapse 
    // var target = document.getElementById('tr-' + param + '-order');
    // var targetInfo = document.getElementById('tr-' + param + '-order-info');
    // var index = $(target).index();
    // $(target).remove();
    // $(targetInfo).remove();
    // var newTarget = $("tr").eq(index);
    // $(newTarget).after(response);

    // buildSparkline(document.getElementById('graphic-order-' + param), document.getElementById('totals-order-' + param));
}

$(function() {
// atualiza graficos do pedido
    $(".group-item").on('shown.bs.collapse', function () {
	loadOrderInfo($(this).attr('data-order-id'));
    });
    var lineOrders = $('#panel-orders').find('.line-order');
    lineOrders.each(function () {
	var arr = $('#totals-order-' + $(this).attr('data-order-id')).text().split(",");
	buildSparkline($('#graphic-order-' + $(this).attr('data-order-id')), arr);
    });
});

