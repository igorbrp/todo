$(function() {
    $('.sidebar-toggle').on('click', function () {
	if ($('body').attr('class').indexOf("fixed") < 0) {
	    var statusBar = $('body').attr('class').indexOf("sidebar-collapse") >= 0 ? 1 : 0;
	    var targetUrl = '/system-configurations/collapse_toggle/' + statusBar;
	    console.log(targetUrl);
            $.ajax({
     		type: 'get',
	        url: targetUrl,
     		success: function(response) {
    	            if(response.error) {
    	    		console.log(response.error);
    	            } else {
		    	console.log('Retorno de collapse-toggle: ' + response);
		    }
     		},
     		error: function(e) {
     	            console.log('error: ' + e);
     		}
            });
	}
    });
});
