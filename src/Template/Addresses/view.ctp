<section class="content-header">
  <h1>
    Address
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
      <?=$this->element('MenuHeader')?>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Model') ?></dt>
            <dd><?= h($address->model) ?></dd>
            <dt scope="row"><?= __('Postal Code') ?></dt>
            <dd><?= h($address->postal_code) ?></dd>
            <dt scope="row"><?= __('Country') ?></dt>
            <dd><?= $address->has('country') ? $this->Html->link($address->country->id, ['controller' => 'Countries', 'action' => 'view', $address->country->id]) : '' ?></dd>
            <dt scope="row"><?= __('State') ?></dt>
            <dd><?= $address->has('state') ? $this->Html->link($address->state->, ['controller' => 'States', 'action' => 'view', $address->state->]) : '' ?></dd>
            <dt scope="row"><?= __('City') ?></dt>
            <dd><?= $address->has('city') ? $this->Html->link($address->city->name, ['controller' => 'Cities', 'action' => 'view', $address->city->id]) : '' ?></dd>
            <dt scope="row"><?= __('City Name') ?></dt>
            <dd><?= h($address->city_name) ?></dd>
            <dt scope="row"><?= __('Neighborhood') ?></dt>
            <dd><?= h($address->neighborhood) ?></dd>
            <dt scope="row"><?= __('Thoroughfare') ?></dt>
            <dd><?= h($address->thoroughfare) ?></dd>
            <dt scope="row"><?= __('Complement') ?></dt>
            <dd><?= h($address->complement) ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($address->id) ?></dd>
            <dt scope="row"><?= __('Foreign Key') ?></dt>
            <dd><?= $this->Number->format($address->foreign_key) ?></dd>
            <dt scope="row"><?= __('Number') ?></dt>
            <dd><?= $this->Number->format($address->number) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($address->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($address->modified) ?></dd>
            <dt scope="row"><?= __('Valid Address') ?></dt>
            <dd><?= $address->valid_address ? __('Yes') : __('No'); ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-text-width"></i>
          <h3 class="box-title"><?= __('Reference') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <?= $this->Text->autoParagraph($address->reference); ?>
        </div>
      </div>
    </div>
  </div>
</section>
