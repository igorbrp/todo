<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Address $address
 */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	    <?=__('Address')?>
	    <small><?= __('Add'); ?></small>
    </h1>
    <?=$this->element('MenuHeader')?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <span><?=__('Identification'); ?></span>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create($address, ['role' => 'form']); ?>
                <div class="box-body">
                    <?php
                                                    echo $this->Form->control('model');
                                echo $this->Form->control('foreign_key');
                                echo $this->Form->control('postal_code');
                                echo $this->Form->control('country_id', ['options' => $countries, 'empty' => true]);
                                echo $this->Form->control('state_id', ['options' => $states, 'empty' => true]);
                                echo $this->Form->control('city_id', ['options' => $cities, 'empty' => true]);
                                echo $this->Form->control('city_name');
                                echo $this->Form->control('neighborhood');
                                echo $this->Form->control('thoroughfare');
                                echo $this->Form->control('number');
                                echo $this->Form->control('complement');
                                echo $this->Form->control('reference');
                                echo $this->Form->control('valid_address');
                    ?>
                </div>
                <!-- /.box-body -->

                <?php echo $this->Form->submit(__('Submit')); ?>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>
