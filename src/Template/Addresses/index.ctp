<style>
    .user-mini {
        margin:auto 10px auto auto;
        width: 25px;
        height: 25px;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Addresses')?>
        <small><?=__('List'); ?></small>
    </h1>
        <?=$this->element('MenuHeader')?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('List'); ?></h3>
                    <div class="box-tools">
                        <?=$this->element('Search')?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                                                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('model') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('foreign_key') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('postal_code') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('country_id') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('state_id') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('city_id') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('city_name') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('neighborhood') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('thoroughfare') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('number') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('complement') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('valid_address') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                                                                <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($addresses as $address): ?>
                            <tr>
                                                                                                                                                                                                                                                                                                                                                    <td><?= $this->Number->format($address->id) ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                <td><?= h($address->model) ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                <td><?= $this->Number->format($address->foreign_key) ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                <td><?= h($address->postal_code) ?></td>
                                                                                                                                                                                                                                                                                                                    <td><?= $address->has('country') ? $this->Html->link($address->country->id, ['controller' => 'Countries', 'action' => 'view', $address->country->id]) : '' ?></td>
                                                                                                                                                                                                                                                                                                                                                        <td><?= $address->has('state') ? $this->Html->link($address->state->, ['controller' => 'States', 'action' => 'view', $address->state->]) : '' ?></td>
                                                                                                                                                                                                                                                                                                                                                        <td><?= $address->has('city') ? $this->Html->link($address->city->name, ['controller' => 'Cities', 'action' => 'view', $address->city->id]) : '' ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                    <td><?= h($address->city_name) ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                <td><?= h($address->neighborhood) ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                <td><?= h($address->thoroughfare) ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                <td><?= $this->Number->format($address->number) ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                <td><?= h($address->complement) ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                <td><?= h($address->valid_address) ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                <td><?= h($address->created) ?></td>
                                                                                                                                                                                                                                                                                                                                                                                                                                <td><?= h($address->modified) ?></td>
                                                                                                                                                                            <td class="actions text-right">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $address->id], ['class'=>'btn btn-info btn-xs']) ?>
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $address->id], ['class'=>'btn btn-warning btn-xs']) ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $address->id], ['confirm' => __('Are you sure you want to delete # {0}?', $address->id), 'class'=>'btn btn-danger btn-xs']) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
    </div>
  </div>
</section>
<?php
    $this->Html->css(['AdminLTE./plugins/iCheck/all'],['block' => 'css']);
    $this->Html->script(['AdminLTE./plugins/iCheck/icheck.min', 'js-index', 'js-base'], ['block' => 'scriptBottom']);
?>
