<section class="content-header">
  <h1>
    Customer
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
      <?=$this->element('MenuHeader')?>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Name') ?></dt>
            <dd><?= h($customer->name) ?></dd>
            <dt scope="row"><?= __('Cpf Cnpj') ?></dt>
            <dd><?= h($customer->cpf_cnpj) ?></dd>
            <dt scope="row"><?= __('Phone') ?></dt>
            <dd><?= h($customer->phone) ?></dd>
            <dt scope="row"><?= __('Phone2') ?></dt>
            <dd><?= h($customer->phone2) ?></dd>
            <dt scope="row"><?= __('Mobile') ?></dt>
            <dd><?= h($customer->mobile) ?></dd>
            <dt scope="row"><?= __('Mobile2') ?></dt>
            <dd><?= h($customer->mobile2) ?></dd>
            <dt scope="row"><?= __('Email') ?></dt>
            <dd><?= h($customer->email) ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($customer->id) ?></dd>
            <dt scope="row"><?= __('Birth Date') ?></dt>
            <dd><?= h($customer->birth_date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($customer->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($customer->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-share-alt"></i>
          <h3 class="box-title"><?= __('Customer Addresses') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php if (!empty($customer->customer_addresses)): ?>
          <table class="table table-hover">
              <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Customer Id') ?></th>
                    <th scope="col"><?= __('Postal Code') ?></th>
                    <th scope="col"><?= __('Country Id') ?></th>
                    <th scope="col"><?= __('State Id') ?></th>
                    <th scope="col"><?= __('City Id') ?></th>
                    <th scope="col"><?= __('City Name') ?></th>
                    <th scope="col"><?= __('Neighborhood') ?></th>
                    <th scope="col"><?= __('Thoroughfare') ?></th>
                    <th scope="col"><?= __('Number') ?></th>
                    <th scope="col"><?= __('Complement') ?></th>
                    <th scope="col"><?= __('Reference') ?></th>
                    <th scope="col"><?= __('Valid Address') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
              <?php foreach ($customer->customer_addresses as $customerAddresses): ?>
              <tr>
                    <td><?= h($customerAddresses->id) ?></td>
                    <td><?= h($customerAddresses->customer_id) ?></td>
                    <td><?= h($customerAddresses->postal_code) ?></td>
                    <td><?= h($customerAddresses->country_id) ?></td>
                    <td><?= h($customerAddresses->state_id) ?></td>
                    <td><?= h($customerAddresses->city_id) ?></td>
                    <td><?= h($customerAddresses->city_name) ?></td>
                    <td><?= h($customerAddresses->neighborhood) ?></td>
                    <td><?= h($customerAddresses->thoroughfare) ?></td>
                    <td><?= h($customerAddresses->number) ?></td>
                    <td><?= h($customerAddresses->complement) ?></td>
                    <td><?= h($customerAddresses->reference) ?></td>
                    <td><?= h($customerAddresses->valid_address) ?></td>
                    <td><?= h($customerAddresses->created) ?></td>
                    <td><?= h($customerAddresses->modified) ?></td>
                      <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['controller' => 'CustomerAddresses', 'action' => 'view', $customerAddresses->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['controller' => 'CustomerAddresses', 'action' => 'edit', $customerAddresses->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['controller' => 'CustomerAddresses', 'action' => 'delete', $customerAddresses->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customerAddresses->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
              </tr>
              <?php endforeach; ?>
          </table>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-share-alt"></i>
          <h3 class="box-title"><?= __('Orders') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php if (!empty($customer->orders)): ?>
          <table class="table table-hover">
              <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Number') ?></th>
                    <th scope="col"><?= __('Date Order') ?></th>
                    <th scope="col"><?= __('Customer Id') ?></th>
                    <th scope="col"><?= __('Subsidiary Id') ?></th>
                    <th scope="col"><?= __('Employee Id') ?></th>
                    <th scope="col"><?= __('User Id') ?></th>
                    <th scope="col"><?= __('Discount') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
              <?php foreach ($customer->orders as $orders): ?>
              <tr>
                    <td><?= h($orders->id) ?></td>
                    <td><?= h($orders->number) ?></td>
                    <td><?= h($orders->date_order) ?></td>
                    <td><?= h($orders->customer_id) ?></td>
                    <td><?= h($orders->subsidiary_id) ?></td>
                    <td><?= h($orders->employee_id) ?></td>
                    <td><?= h($orders->user_id) ?></td>
                    <td><?= h($orders->discount) ?></td>
                    <td><?= h($orders->created) ?></td>
                    <td><?= h($orders->modified) ?></td>
                      <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['controller' => 'Orders', 'action' => 'view', $orders->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['controller' => 'Orders', 'action' => 'edit', $orders->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['controller' => 'Orders', 'action' => 'delete', $orders->id], ['confirm' => __('Are you sure you want to delete # {0}?', $orders->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
              </tr>
              <?php endforeach; ?>
          </table>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
