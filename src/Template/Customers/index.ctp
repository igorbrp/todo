<style>
    .user-mini {
        margin:auto 10px auto auto;
        width: 25px;
        height: 25px;
    }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Customers')?>
        <small><?=__('List'); ?></small>
    </h1>
        <?=$this->element('MenuHeader')?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('List'); ?></h3>
                    <div class="box-tools">
                        <?=$this->element('Search')?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                                                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('cpf_cnpj') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('birth_date') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('phone') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('phone2') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('mobile') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('mobile2') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                                                                    <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                                                                <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($customers as $customer): ?>
                            <tr>
                                                                                                                                                                                                                                                                        <td><?= $this->Number->format($customer->id) ?></td>
                                                                                                                                                                                                                                                                                                                                                    <td><?= h($customer->name) ?></td>
                                                                                                                                                                                                                                                                                                                                                    <td><?= h($customer->cpf_cnpj) ?></td>
                                                                                                                                                                                                                                                                                                                                                    <td><?= h($customer->birth_date) ?></td>
                                                                                                                                                                                                                                                                                                                                                    <td><?= h($customer->phone) ?></td>
                                                                                                                                                                                                                                                                                                                                                    <td><?= h($customer->phone2) ?></td>
                                                                                                                                                                                                                                                                                                                                                    <td><?= h($customer->mobile) ?></td>
                                                                                                                                                                                                                                                                                                                                                    <td><?= h($customer->mobile2) ?></td>
                                                                                                                                                                                                                                                                                                                                                    <td><?= h($customer->email) ?></td>
                                                                                                                                                                                                                                                                                                                                                    <td><?= h($customer->created) ?></td>
                                                                                                                                                                                                                                                                                                                                                    <td><?= h($customer->modified) ?></td>
                                                                                                                                                                            <td class="actions text-right">
                                    <?= $this->Html->link(__('View'), ['action' => 'view', $customer->id], ['class'=>'btn btn-info btn-xs']) ?>
                                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $customer->id], ['class'=>'btn btn-warning btn-xs']) ?>
                                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $customer->id], ['confirm' => __('Are you sure you want to delete # {0}?', $customer->id), 'class'=>'btn btn-danger btn-xs']) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
    </div>
  </div>
</section>
<?php
    $this->Html->css(['AdminLTE./plugins/iCheck/all'],['block' => 'css']);
    $this->Html->script(['AdminLTE./plugins/iCheck/icheck.min', 'js-index', 'js-base'], ['block' => 'scriptBottom']);
?>
