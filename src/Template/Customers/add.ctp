<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Customer $customer
 */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	    <?=__('Customer')?>
	    <small><?= __('Add'); ?></small>
    </h1>
    <?=$this->element('MenuHeader')?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <span><?=__('Identification'); ?></span>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create($customer, ['role' => 'form']); ?>
                <div class="box-body">
                    <?php
                                                    echo $this->Form->control('name');
                                echo $this->Form->control('cpf_cnpj');
                                echo $this->Form->control('birth_date', ['empty' => true]);
                                echo $this->Form->control('phone');
                                echo $this->Form->control('phone2');
                                echo $this->Form->control('mobile');
                                echo $this->Form->control('mobile2');
                                echo $this->Form->control('email');
                    ?>
                </div>
                <!-- /.box-body -->

                <?php echo $this->Form->submit(__('Submit')); ?>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>
