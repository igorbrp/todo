<?php
    echo $this->Form->control('name');
    echo $this->Form->control('cnpj');
?>
<div class="row">
    <div class="col-md-6">
        <?php
            echo $this->Form->control('phone');
        ?>
    </div>
    <div class="col-md-6">
        <?php
            echo $this->Form->control('mobile');
        ?>
    </div>
</div>
<?php
    echo $this->Form->control('fax');
    echo $this->Form->control('active');
?>
