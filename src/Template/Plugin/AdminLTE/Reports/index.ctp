<?php
use Cake\I18n\Time;
?>
<style>
 .user-mini {
     margin:auto 10px auto auto;
     width: 25px;
     height: 25px;
 }
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Reports')?>
        <small>&nbsp;</small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true,'nodelete' => true])?>
</section>
<?php
/* debug($chartArray);
 * debug($data); */
/* debug($combined->toArray());die();
debu
/* if (!empty($data)) {
 *     debug($data);
 *     // debug($employees->toArray());
 *     // die();
 * } */
//debug($selectEmployees);
/* $now = new Time();
 * echo $now . "<BR>";
 * echo $now->i18nFormat("MMMM") . '<br>';
 * echo $totalPeriod; */
?>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
                        <?=$this->element('ReportSearch', ['placeholder' => 'pesquise por número do pedido ou nome do cliente'])?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    /* echo 'sinceDate: ' . $sinceDate . '<br>';
     * echo 'untilDate: ' . $untilDate; */

    if (!empty($data)) {
        //    debug($data);
        /* debug($query->toArray());die(); */
        switch ($data['select_report']) {
            case 'employee':
                echo $this->element('ReportEmployees');
                break;
            case 'subsidiary':
                echo $this->element('ReportSubsidiaries');
                break;
            case 'supply':
                echo $this->element('ReportSupplies');
                break;
        }
    }
    ?>
</section>

<?php
echo $this->Html->script([
    'AdminLTE./bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min',
    'AdminLTE./bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min',
    /* ChartJS */
    'AdminLTE./bower_components/chart.js/Chart',
    //    'AdminLTE./bower_components/jquery-sparkline/dist/jquery.sparkline.min',
    '/node_modules/inputmask/dist/jquery.inputmask.min',
    '/node_modules/inputmask/dist/bindings/inputmask.binding',
], [
    'block' => 'script'
]);

echo $this->Html->script([
    'js-base',
    'index',
    'order-item',
    /* 'items',
     * 'order-index', */
], [
    'block' => 'scriptBottom'
]);
?>
