<?php
/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<section class="content-header">
    <h1>
	<?='Perfil'?>
	<small>
            <?=${$tableAlias}->nick_name;?>
        </small>
    </h1>
    <?=$this->element('MenuHeader')?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-body">

                    <a href="#" id="bt-upload" >
                        <div id="target_img" class="text-center">
                            <?php if (is_null(${$tableAlias}->file['hash'])): ?>
                                <?=$this->Html->image('avatar_placeholder.png', ['alt' => __('Image not available.'), 'class' => 'profile-user-img img-responsive', 'id' => 'preview_image']); ?>
                            <?php else : ?>
                                <?=$this->Html->image(${$tableAlias}->file['path'] . DS . ${$tableAlias}->file['hash'] . '.media.' . ${$tableAlias}->file['extension'], ['pathPrefix' => '', 'class' => 'profile-user-img img-responsive', 'id' => 'preview_image']);  ?>
                            <?php endif ?>
                        </div>
                    </a>
                    <h3 class="profile-username text-center"><?=${$tableAlias}->nick_name?></h3>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b><?=''; //__('products')?></b> <!-- <a class="pull-right">322</a> -->
                        </li>
                        <li class="list-group-item">
                            <b><?='';//__('lines')?></b> <!-- <a class="pull-right">8</a> -->
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /.col-md -->
	<div class="col-md-9">
            <!-- general form elements -->
            <div class="box box-primary">
		<div class="box-header with-border">
                    <span><?=__('Identification') ?></span>
		</div>
		<!-- /.box-header -->
		<!-- form start -->
                <?= $this->Form->create(${$tableAlias}, ['enctype' => 'multipart/form-data', 'role' => 'form']); ?>
		<div class="box-body">
		    <?php
                    echo $this->Form->control('username');
                    echo $this->Form->control('email', [
                        'type' => 'text',
                        'templateVars' => [
                            'otherClass' => 'email'
                        ]
                    ]);
                    echo $this->Form->control('password', ['label' => 'senha', 'type' => 'password']);
		    //		    echo $this->Form->control('confirm_password', ['label' => __('confirm password'), 'type' => 'password']);
                    echo $this->Form->control('nick_name');

                    ?>
                    <div class="form-group input text">
                        <?php
			echo $this->Form->control('role', ['readonly' => true]);
                        ?>
                    </div>
		</div>
		<!-- /.box-body -->
                <div style="display:none;">
                    <?php echo $this->Form->file('file.submittedfile', ['id' => 'submittedfile', 'class' => 'upload']); // Pay attention here! ?>
                    <?php echo ''; // $this->Form->file('submittedfile', ['id' => 'submittedfile', 'class' => 'upload']); // Pay attention here! ?>
                </div>

                <div class="text-right"><?= $this->Form->submit(__d('CakeDC/Users', 'Submit')) ?></div>
		<?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
	</div>
    </div>
    <!-- /.row -->
</section>
<?php
echo $this->Html->css([
    'AdminLTE./bower_components/select2/dist/css/select2.css',
], [
    'block' => 'css'
]);
echo $this->Html->script([
    '/node_modules/blueimp-load-image/js/load-image.all.min',
    'load-image',
    'AdminLTE./bower_components/select2/dist/js/select2.js',
    '/node_modules/inputmask/dist/jquery.inputmask.min',
    '/node_modules/inputmask/dist/bindings/inputmask.binding',
], [
    'block' => 'script'
]);
echo $this->Html->script([
    'apply-select2',
    'common_masks',
], [
    'block' => 'scriptBottom'
]);

?>
