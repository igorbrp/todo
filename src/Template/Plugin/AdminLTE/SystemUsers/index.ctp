<?php
/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>
<style>
 .user-mini {
     margin:auto 10px auto auto;
     width: 25px;
     height: 25px;
 }
</style>
<section class="content-header">
    <h1>
	<?=__('Users')?>
	<small>&nbsp;</small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
	<div class="col-xs-12">
	    <div class="box">
		<div class="box-header">
		    <div>
                        <?=$this->element('Search')?>
		    </div>
		</div>
		<!-- /.box-header -->
		<div class="box-body table-responsive no-padding">
		    <table class="table table-hover">
			<thead>
			    <tr>
                                <th scope="col"><label><input id="chk-all" type="checkbox" class="minimal"></label></th>
				<th scope="col"><?= $this->Paginator->sort('username') ?></th>
				<th scope="col"><?= $this->Paginator->sort('email', ['label' => __('email')]) ?></th>
                                <th scope="col"><?= $this->Paginator->sort('first_name', [__('nickname')]) ?></th>
				<th scope="col"><?= $this->Paginator->sort('role') ?></th>
				<th scope="col" class="actions text-right"><?= __('Actions') ?></th>
			    </tr>
			</thead>
			<tbody id="tbody">
                            <?php foreach (${$tableAlias} as $user) : ?>
				<?php if (!$user->is_superuser) : // coloquei aqui pq não achei onde muda em SystemUsers->index ?>
				    <tr>
					<td><input name="chkb" id="chk-b" value="<?=$user->id?>" type="checkbox" class="minimal"></td>
					<td>
                                            <span style="display:inline-block">
						<?php if (is_null($user->file['hash'])): ?>
                                                    <?=$this->Html->image('avatar_placeholder.png', ['class' => 'user-mini img-circle']);  ?>
						<?php else: ?>
                                                    <?=$this->Html->image($user->file['path'] . DS . $user->file['hash'] . '.mini.' . $user->file['extension'], ['pathPrefix' => '', 'class' => 'user-mini img-circle']);?>
						<?php endif; ?>
						<?= h($user->username) ?>
                                            </span>
					</td>
					<td><?= h($user->email) ?></td>
					<td><?= h($user->first_name) ?></td>
					<td><?= h($user->role) ?></td>
					<td class="actions text-right">
                                            <?=$this->Element('Actions', ['cod_id' => $user->id]);?>
					</td>
				    </tr>
				<?php endif; ?>
			    <?php endforeach; ?>
			</tbody>
		    </table>
		</div>
		<!-- /.box-body -->
	    </div>
	    <!-- /.box -->
	</div>
    </div>
    <div class="row">
	<div class="col-sm-5">
	    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
		<?php echo $this->Paginator->counter(['format' => 'range']);?>
	    </div>
	</div>
	<div class="col-sm-7">
	    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
		<!-- Paginator -->
		<?=
		$this->element('Paginator');
		?>
	    </div>
	</div>
    </div>
</section>
<?php
echo $this->Html->script([
    'js-base',
    'index',
], [
    'block' => 'scriptBottom'
]);
?>
