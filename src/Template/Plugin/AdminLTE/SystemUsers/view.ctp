<?php
    /**
     * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
     *
     * Licensed under The MIT License
     * Redistributions of files must retain the above copyright notice.
     *
     * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
     * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
     */

    $user = ${$tableAlias};
?>
<section class="content-header">
    <h1>
        Usuário
        <small><?php echo $user->username; ?></small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-body">



                    <a href="#" id="bt-upload" >
                        <div id="target_img" class="text-center">
                            <?php if (empty(${$tableAlias}->file)): ?>
                                <?=$this->Html->image('avatar_placeholder.png', ['alt' => __('Image not available.'), 'class' => 'profile-user-img img-responsive', 'id' => 'preview_image']); ?>
                            <?php else : ?>
                                <?=$this->Html->image(${$tableAlias}->file['path'] . DS . ${$tableAlias}->file['hash'] . '.media.' . ${$tableAlias}->file['extension'], ['pathPrefix' => '', 'class' => 'profile-user-img img-responsive', 'id' => 'preview_image']);  ?>
                            <?php endif ?>
                        </div>
                    </a>
                    <h3 class="profile-username text-center">&nbsp;</h3>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b><?=__('products')?></b> <a class="pull-right">322</a>
                        </li>
                        <li class="list-group-item">
                            <b><?=__('lines')?></b> <a class="pull-right">8</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /.col-md -->
        <div class="col-md-9">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-info"></i>
                    <h3 class="box-title"><?php echo __('Information'); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt scope="row"><?= __('username') ?></dt>
                        <dd><?= h($user->username) ?></dd>
                        <dt scope="row"><?= __('email') ?></dt>
                        <dd><?= h($user->email) ?></dd>
                        <dt scope="row"><?= __('name') ?></dt>
                        <dd><?= h($user->first_name) ?></dd>
                        <dt scope="row"><?= __('role') ?></dt>
                        <dd><?= h($user->role) ?></dd>
                        <dt scope="row"><?= __('Created') ?></dt>
                        <dd><?= h($user->created) ?></dd>
                        <dt scope="row"><?= __('Modified') ?></dt>
                        <dd><?= h($user->modified) ?></dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>

</section>
