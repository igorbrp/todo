<section class="content-header">
  <h1>
    Fornecedor
      <small><?php echo $supply->name; ?></small>
  </h1>
  <ol class="breadcrumb">
      <?=$this->element('MenuHeader', ['noprint' => true])?>
  </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
	<div class="col-md-12">
	    <div class="box box-solid">
		<div class="box-header with-border">
		    <i class="fa fa-info"></i>
		    <h3 class="box-title"><?php echo __('Information'); ?></h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		    <dl class="dl-horizontal">
			<dt scope="row"><?= __('Code') ?></dt>
			<dd><?= h($supply->code) ?></dd>
			<dt scope="row"><?= __('Name') ?></dt>
			<dd><?= h($supply->name) ?></dd>
			<dt scope="row"><?= __('Cnpj') ?></dt>
			<dd><?= h($supply->cnpj) ?></dd>
			<dt scope="row"><?= __('Phone') ?></dt>
			<dd><?= h($supply->phone) ?></dd>
			<dt scope="row"><?= __('Phone2') ?></dt>
			<dd><?= h($supply->phone2) ?></dd>
			<dt scope="row"><?= __('Email') ?></dt>
			<dd><?= h($supply->email) ?></dd>
			<dt scope="row"><?= __('Contact') ?></dt>
			<dd><?= h($supply->contact) ?></dd>
			<dt scope="row"><?= __('Mobile Contact') ?></dt>
			<dd><?= h($supply->mobile_contact) ?></dd>
			<dt scope="row"><?='';// __('Representante') ?></dt> <!-- TRADUZIR -->
			<dd><?= '';//$supply->has('agent') ? $this->Html->link($supply->agent->name, ['controller' => 'Agents', 'action' => 'view', $supply->agent->id]) : '' ?></dd>
			<dt scope="row"><?= __('Email Request') ?></dt>
			<dd><?= $supply->email_request ? __('Yes') : __('No'); ?></dd>
			<dt scope="row"><?= __('Print Request') ?></dt>
			<dd><?= $supply->print_request ? __('Yes') : __('No'); ?></dd>
		    </dl>
		</div>
	    </div>
	</div>
    </div>

    <div class="row">
	<div class="col-md-12">
	    <div class="box box-solid">
		<div class="box-header with-border">
		    <i class="fa fa-share-alt"></i>
		    <h3 class="box-title"><?= __('Endereço') ?></h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		    <?php if (!empty($supply->address)): ?>
			<table class="table table-hover">
			    <tr>
				<th scope="col"><?= __('CEP') ?></th>
				<th scope="col"><?= __('estado') ?></th>
				<th scope="col"><?= __('cidade') ?></th>
				<th scope="col"><?= __('Neighborhood') ?></th>
				<th scope="col"><?= __('Thoroughfare') ?></th>
				<th scope="col"><?= __('Number') ?></th>
				<th scope="col"><?= __('Complement') ?></th>
				<th scope="col"><?= __('Reference') ?></th>
			    </tr>
			    <?php echo ''; // foreach ($supply->address as $supplyAddresses): ?>
			    <tr>
				<td><?= h($supply->address->postal_code) ?></td>
				<td><?= h($supply->address->state_id) ?></td>
				<td><?= h($supply->address->city) ?></td>
				<td><?= h($supply->address->neighborhood) ?></td>
				<td><?= h($supply->address->thoroughfare) ?></td>
				<td><?= h($supply->address->number) ?></td>
				<td><?= h($supply->address->complement) ?></td>
				<td><?= h($supply->address->reference) ?></td>
			    </tr>
			    <?php echo ''; // endforeach; ?>
			</table>
		    <?php endif; ?>
		</div>
	    </div>
	</div>
    </div>
</section>
