<section class="content">
    <div class="row">
        <div class="col-md-12 text-center">
	    <?php
	    echo $this->Html->link(
		'voltar',
		['controller' => 'supplies', 'action' => 'edit', $supply->id],
		['type' => 'button']
	    );
	    ?>
	</div>
    </div>
</section>
