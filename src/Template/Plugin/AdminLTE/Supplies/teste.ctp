<?php
    debug($globalPrices);
    //debug($prices);
    //debug($result);
    /* foreach ($prices as $price)
     * debug($price); */
?>
<div class="box-body table-responsive no-padding">
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col">inherited</th>
                <th scope="col">cost name</th>
                <th scope="col">model</th>
                <th scope="col">name</th>
                <th scope="col">value</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($formTaxes as $tax): ?>
                <tr>
                    <td>
                        <?=$tax['inherited']?>
                    </td>
                    <td>
                        <?=$tax['cost_name']?>
                    </td>
                    <td>
                        <?=$tax['model']?>
                    </td>
                    <td>
                        <?=$tax['name']?>
                    </td>
                    <td>
                        <?=$tax['value']?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
