<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	<?=__('Importar tabela')?>
	<small>
            <?=$supply->name;?>
        </small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>


<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header">

		    <?php
		    if (isset($uploadedFile)) {
			if (empty($header)) {
			    $arrFields = $theTable[0];
			} else {
			    $arrFields = $header;
			}
			echo '<ul id="list-fields" class="list-group">';
			$i = 0;
			foreach ($arrFields as $field) {
			    $s = $this->Form->select('field_' . $i, makeSelect($fields), [
				'id' => 'field_' . $i,
				'empty' => true,
				'class' => 'select2',
				'onchange' => 'removeSelectedOption(' . $i . ');',
			    ]);
			    $i++;
			    echo '<li class="list-group-item">' . $field . ' <span class="pull-right">' . $s . '</span></li>';
			}
			echo '</ul>';
			echo $this->Form->button('confirmar', ['onclick' => 'submitForm()']);
		    }
		    ?>

		</div>
	    </div>
	</div>
    </div>
</section>
<?php 
echo '<div style="display:none">';
echo $this->Form->create('insert-table', ['id' => 'insert-table', 'controller' => 'supplies', 'action' => 'insert-table', 'role' => 'form']);
echo $this->Form->controller('withheader', ['value' => $withHeader]);
echo $this->Form->controller('supply_id', ['value' => $supply->id]);
/* echo $this->Form->controller('tmp_name', ['value' => $uploadedFile['tmp_name']]);
 * echo $this->Form->controller('name', ['value' => $uploadedFile['name']]);
 * echo $this->Form->controller('type', ['value' => $uploadedFile['type']]);
 * echo $this->Form->controller('size', ['value' => $uploadedFile['size']]); */
echo $this->Form->controller('sku', ['id' => 'sku']);
echo $this->Form->controller('title', ['id' =>'title']);
echo $this->Form->controller('description', ['id' =>'description']);
echo $this->Form->controller('long_description', ['id' =>'long_description']);
echo $this->Form->controller('root_price', ['id' =>'root_price']);
echo $this->Form->controller('price', ['id' =>'price']);
echo $this->Form->controller('deadline', ['id' =>'deadline']);
echo $this->Form->controller('working_days', ['id' =>'working_days']);
echo $this->Form->end();
echo '</div>';

function makeSelect($f) {
    $arrSelect = [];
    foreach ($f as $key => $field) {
	$arrSelect[$key] = $field['description'];
    }
    return $arrSelect;
}
?>
<script>
 function submitForm() {
     var formSubmit = document.getElementById('insert-table');
     for (var i = 0; i < document.getElementById("list-fields").children.length; i++) {
	 var f = document.getElementById('field_' + i).value;
	 console.log('field_' + i + ': ' + f)
	 if (f != '') {
	     console.log('f: ' + f);
	     document.getElementById(f).value = i;
	 }
     }
     formSubmit.submit();
 }

 function removeSelectedOption(indice) {
     var arrKeys=['', 'sku', 'title', 'description', 'long_description', 'root_price', 'price', 'deadline', 'working_days'];
     var arrValues=['', 'código', 'título', 'descrição', 'descrição longa', 'preço de custo', 'preço de venda', 'prazo de entrega', 'entrega em dias úteis'];
     var arrSelected = [];
     for (var i = 0; i < document.getElementById("list-fields").children.length; i++) {
	 var v = document.getElementById('field_' + i).value;
	 if (v != '')
	     arrSelected.push(v);
     }

     for (var i = 0; i < document.getElementById("list-fields").children.length; i++) {
	 if (i != indice) {
	     var selectElement = document.getElementById('field_' + i);
	     var valor = selectElement.value;

	     while (selectElement.hasChildNodes()) {  
		 selectElement.removeChild(selectElement.firstChild);
	     }

	     for (j = 0; j < arrKeys.length; j++) {
		 var option = document.createElement('option');
		 option.value = arrKeys[j];
		 option.text = arrValues[j];
		 selectElement.add(option);
	     }

	     arrSelected.forEach(function(value) {
		 var op = selectElement.querySelector('option[value="' + value + '"]');
		 if (op && op.value != '' && op.value != valor) {
		     selectElement.removeChild(op);
		 }
	     });

	     selectElement.value = valor;
	 }
     }
 }
</script>
