<style>
 .user-mini {
     margin:auto 10px auto auto;
     width: 25px;
     height: 25px;
 }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Supplies')?>
        <small>&nbsp;</small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
                        <?=$this->element('Search', ['placeholder' => 'pesquise por nome ou código de fornecedor'])?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col"><label><input id="chk-all" type="checkbox" class="minimal" onchange="chkAllChange();"></label></th>
                                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('code') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('contact') ?></th>
                                <th scope="col" class="actions text-right"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            <?php foreach ($supplies as $supply): ?>
                                <tr>
                                    <td>
                                        <?=$this->Form->checkbox('id', [
                                            'id' => 'chk-b',
                                            'name' => 'chkb',
                                            'class' => 'chk-box',
                                            'value' => $supply->id,
                                            'onclick' => 'checkOrUncheck(this)'
                                        ]);?>
                                    </td>
                                    <td>
                                        <span class="image" style="display:inline-block">
                                            <?php if (is_null($supply->file['hash'])): ?>
                                                <?=$this->Html->image('no-image.jpg', ['class' => 'user-mini img-circle', 'style' => 'width:25px;height:25px']);  ?>
                                            <?php else: ?>
                                                <?=$this->Html->image($supply->file['path'] . DS . $supply->file['hash'] . '.mini.' . $supply->file['extension'], ['pathPrefix' => '', 'class' => 'user-mini img-circle', 'style' => 'width:25px;height:25px;margin: auto 10px auto auto;']);  ?>
                                            <?php endif; ?>
                                            <?= h($supply->name) ?>
                                        </span>
                                    </td>
                                    <td><?= h($supply->code) ?></td>
                                    <td><?= h($supply->email) ?></td>
                                    <td><?= h($supply->contact) ?></td>
                                    <td><?= h($supply->created) ?></td>
                                    <td class="actions text-right">
                                        <?=$this->element('Actions', ['cod_id' => $supply->id,])?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
	<div class="col-sm-5">
	    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
		<?php echo $this->Paginator->counter(['format' => 'range']);?>
	    </div>
	</div>
	<div class="col-sm-7">
	    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
		<!-- Paginator -->
		<?=
		$this->element('Paginator');
		?>
	    </div>
	</div>
    </div>
</section>
<?php
echo $this->Html->script([
    'js-base',
    'index',
], [
    'block' => 'script'
]);
?>
