<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	<?=__('Importar tabela')?>
	<small>
            <?=$supply->name;?>
        </small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>

<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
		<?php echo $this->Form->create($supply, ['controller' => 'supplies', 'action' => 'validate-table/' . $supply->id, 'enctype' => 'multipart/form-data', 'role' => 'form']); ?>
                <div class="box-body">
		    <!-- <div class="row"> -->
		    <div>
			<div class="row">
			    <div class="col-md-12"><?php echo $this->Form->checkbox('withheader') . ' a primeira linha é cabeçalho';?></div>
			</div>
			<div class="row">
			    <br/>
			    <div class="col-md-12">
				<?php echo $this->Form->button('arquivo', ['type' => 'button', 'onclick' => 'uploadFile()']);?>
				&nbsp;&nbsp;<span id="uploaded-file-name">
				</span>
			    </div>
			</div>
			<div class="row">
			    <div class="col-md-12 text-right"><?php echo $this->Form->submit(__('Submit')); ?></div>
			</div>
			<!-- <div class="row"> -->
			<!-- </div> -->
		    </div>
		</div>
		<div style="display:none;"><?php echo $this->Form->file('submittedfile', ['id' => 'submittedfile', 'class' => 'upload']);?></div>
		<?php echo $this->Form->end(); ?>
	    </div>
	</div>
    </div>
</section>

<script>
 var control = document.getElementById("submittedfile");
 control.addEventListener("change", function(event) {
     // When the control has changed, there are new files
     var files = control.files;
     for (var i = 0; i < files.length; i++) {
         console.log("Filename: " + files[i].name);
         console.log("Type: " + files[i].type);
         console.log("Size: " + files[i].size + " bytes");
	 document.getElementById('uploaded-file-name').innerHTML = files[i].name + ' (' + files[i].size + ' bytes)';
 }}, false);
 function uploadFile() {
     document.getElementById('submittedfile').click();
 }
</script>
