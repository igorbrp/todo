<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $supply
 */
$template = $this->template == 'add' ? __('Add') : __('Edit');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	<?=__('Supply')?>
	<small>
            <?php if(strtolower($this->request->getParam('action')) == 'edit') : ?>
                <?=$supply->name;?>
            <?php else : ?>
                <?='novo'?> <!-- TRADUZIR -->
            <?php endif; ?>
        </small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-body">
                    <a href="#" id="bt-upload" >
                        <div id="target_img" class="text-center">
                            <?php if (is_null($supply->file['hash'])): ?>
                                <?=$this->Html->image('no-image.jpg', ['alt' => __('Image not available.'), 'class' => 'profile-user-img img-responsive', 'id' => 'preview_image']); ?>
                            <?php else : ?>
                                <?=$this->Html->image($supply->file['path'] . DS . $supply->file['hash'] . '.media.' . $supply->file['extension'], ['pathPrefix' => '', 'class' => 'profile-user-img img-responsive', 'id' => 'preview_image']);  ?>
                            <?php endif ?>
                        </div>
                    </a>

                    <h3 class="profile-username text-center">&nbsp;</h3>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b><?=__('products')?></b> <a class="pull-right">322</a>
                        </li>
                        <li class="list-group-item">
                            <b><?=__('lines')?></b> <a class="pull-right">8</a>
                        </li>
                        <li class="list-group-item">
			    <?=$this->Html->link('enviar tabela', ['controller' => 'supplies', 'action' => 'upload_table', $supply->id]);?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /.col-md -->
        <div class="col-md-9">
            <!-- general form elements -->
            <?php echo $this->Form->create($supply, ['enctype' => 'multipart/form-data', 'role' => 'form']); ?>
            <div class="nav-tabs-custom">
	        <ul class="nav nav-tabs">
	            <li class="active">
                        <a href="#identity" data-toggle="tab" aria-expanded="true"><?=__('Identification')?></a>
                    </li>
	            <li class="">
                        <a href="#address" data-toggle="tab" aria-expanded="false"><?=__('Address')?></a>
                    </li>
	            <li class="">
                        <a href="#contact" data-toggle="tab" aria-expanded="false"><?=__('Contact')?></a>
                    </li>
	            <li class="">
                        <a href="#properties" data-toggle="tab" aria-expanded="false"><?=__('Properties')?></a>
                    </li>
                    <?php if ($this->template == 'edit'): ?>
	                <li class="">
                            <a href="#prices" data-toggle="tab" aria-expanded="false"><?=__('Prices')?></a>
                        </li>
                    <?php endif; ?>
	        </ul>
	        <div class="tab-content">
	            <div class="tab-pane active" id="identity">
		        <!-- identity content -->
                        <!-- form start -->
                        <div class="row">
                            <div class="col-md-9">
                                <?php
                                echo $this->Form->control('name', ['onblur' => 'buildSku(this)']);
                                ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $this->Form->control('code'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <?=$this->Form->control('cnpj', [
	                            'label' => __('CNPJ'),
	                            'maxlength' => '18',
	                            'templateVars' => [
		                        'otherClass' => 'cnpj',
	                            ],
	                        ])?>
                            </div>
                        </div>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="address">
                        <!-- address content -->
                        <?=$this->Element('Address');?>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="contact">
                        <div class="row">
                            <div class="col-md-3">
                                <?=$this->Form->control('phone', [
                                    'templateVars' => [
                                        'otherClass' => 'phone',
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-3">
                                <?=$this->Form->control('phone2', [
                                    'templateVars' => [
                                        'otherClass' => 'phone',
                                    ],
                                ])?>
                            </div>
                            <div class="col-md-6">
                                <?php
                                echo $this->Form->control('email', [
                                    'type' => 'text',
                                    'templateVars' => [
                                        'otherClass' => 'email'
                                    ]
                                ]);

                                ?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-9">
                                <?php echo $this->Form->control('contact'); ?>
                            </div>
                            <div class="col-md-3">
                                <?php echo $this->Form->control('mobile_contact', [
                                    'templateVars' => [
                                        'otherClass' => 'mobile',
                                    ],
                                ]);?>
                            </div>
                        </div>

                        <div class="form-group input text" style="display:none">
                            <label class="control-label" for="agent-id"><?=__('Agent')?></label>
                            <?php
                            echo $this->Form->select('agent_id', $agents, [
                                'id' => 'agent_id',
                                'empty' => true,
                                'class' => 'select2',
                                'style' => 'width: 100%'
                            ]);
                            ?>
                        </div>
                    </div>
                    <div class="tab-pane" id="properties">
                        <?php
                        echo ''; // $this->Form->control('deadline');
                        echo ''; // $this->Form->control('working_days');
                        echo ''; // $this->Form->control('maximum_discount');
                        echo $this->Form->control('email_request');
                        /* echo $this->Form->control('print_request'); */
                        ?>
                    </div>

                    <?php if ($this->template == 'edit'): ?>

                        <div class="tab-pane" id="prices">
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th scope="col"><?=__('price')?></th>
                                            <th scope="col"><?=__('inherited')?></th>
                                            <th scope="col"><?=__('name')?></th>
                                            <th scope="col"><?=__('value')?></th>
                                            <th scope="col"><?=__('calculation')?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $i = 0; ?>
                                        <?php $oldPrice = '' ?>
                                        <?php foreach ($supply->supply_taxes as $tax): ?>
                                            <?php if ($tax['price_id'] != $oldPrice): ?>
                                                <tr>
                                                    <td><h5><?=$tax['price']['name']?></h5></td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td>
                                                </tr>
                                                <?php $oldPrice = $tax['price_id']; ?>
                                            <?php endif; ?>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>
                                                    <?=$this->Form->checkbox('supply_taxes.' . $i . '.inherited', [
                                                        'id' => 'supply_taxes-' . $i . '-inherited',
                                                        'checked' => $tax['inherited'] == 1 ? true : false,
                                                        'value' => $tax['inherited'],
                                                        'onclick' => 'toggleHeritage("' . $i . '")'
                                                    ]);?>
                                                </td>
                                                <td>
                                                    <?=$tax->name?>
                                                    <?=$this->Form->input('supply_taxes.' . $i . '.name', ['type' => 'hidden'])?>
                                                </td>
                                                <td><?=$this->Form->control('supply_taxes.' . $i . '.value', [
                                                    'id' => 'supply_taxes-' . $i . '-value',
                                                    'label' => false,
                                                    'readonly' => $tax['inherited'] ? true : false
                                                    ])?>
                                                    <input type="hidden" id="inherited-value-<?=$i?>" value="<?=$tax->inherited_value?>">
                                                    <?=$this->Form->input('supply_taxes.' . $i . '.price_id', ['type' => 'hidden'])?>
                                                    <?=$this->Form->input('supply_taxes.' . $i . '.id', ['type' => 'hidden'])?>
                                                    <?=$this->Form->input('supply_taxes.' . $i . '.foreign_key', ['type' => 'hidden'])?>
                                                </td>
                                                <td>
                                                    <?php
                                                    echo $this->Form->select('supply_taxes.' . $i . '.calculation', [
                                                        '%' => __('percentage'),
                                                        '$' => __('currency')
                                                    ], [
                                                        //                                                        'id' => 'calculation',
                                                        'class' => 'select2',
                                                        'style' => 'width: 100%',
                                                    ]);
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php $i++; ?>
                                        <?php endforeach ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

                <!-- /.tab-content -->
                <div style="display:none;">
                    <?php echo $this->Form->file('file.submittedfile', ['id' => 'submittedfile', 'class' => 'upload']); // Pay attention here! ?>
                </div>

		<!-- <input type="file" id="myFile" name="filename"> -->

                <div class="text-right">
                    <?php echo $this->Form->submit(__('Submit')); ?>
                </div>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col-md -->
    </div>
    <!-- /.row -->
</section>
<script>
 function buildSku(t)
 {
     if (t.value.length == 0)
         return false;

     var targeturl = '/supplies/generate-code?';
     var data = {name: t.value};
     getSku(targeturl, data, 'code');
 }

 function toggleHeritage(i)
 {
     var chkb = document.getElementById('supply_taxes-' + i + '-inherited');
     var inputValue = document.getElementById('supply_taxes-' + i + '-value');

     if (chkb.checked) {
         inputValue.setAttribute('readonly', true);
         //            inputValue.readonly = true;
         inputValue.value = document.getElementById('inherited-value-' + i).value;
         chkb.value = 1;
     } else {
         inputValue.removeAttribute('readonly');
         //            inputValue.readonly = false;
         chkb.value = 0;
         inputValue.value = '';
         inputValue.focus();
     }
 }
</script>
<?php
echo $this->Html->css([
    'AdminLTE./bower_components/select2/dist/css/select2.css'], [
        'block' => 'css'
]);
echo $this->Html->script([
    '/node_modules/blueimp-load-image/js/load-image.all.min',
    'AdminLTE./bower_components/select2/dist/js/select2.js',
    '/node_modules/inputmask/dist/jquery.inputmask.min',
    '/node_modules/inputmask/dist/bindings/inputmask.binding',
], [
    'block' => 'script'
]);
echo $this->Html->script([
    'load-image',
    'addresses',
    'apply-select2',
    'get-sku',
    'common_masks',
], [
    'block' => 'scriptBottom'
]);
?>
