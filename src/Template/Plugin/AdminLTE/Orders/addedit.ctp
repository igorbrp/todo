<?php
use Cake\Utility\Inflector;
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $order
 */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	<?=__('Order')?>
	<small>
            <?='';//Inflector::humanize($this->request->getParam('action')); ?>
            <?php if(strtolower($this->request->getParam('action')) == 'edit') : ?>
                <?=$order->number;?>
            <?php else : ?>
                <?='novo'?> <!-- TRADUZIR -->
            <?php endif; ?>
        </small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <?php echo $this->Form->create($order, ['id' => 'order-form', 'enctype' => 'multipart/form-data', 'role' => 'form', 'onsubmit' => 'validateOrder(event);']); ?>
            <div class="nav-tabs-custom">
	        <ul class="nav nav-tabs">
	            <li class="active">
                        <a href="#identity" data-toggle="tab" aria-expanded="true"><?=__('Identification')?></a>
                    </li>
	            <li class="">
                        <a href="#address" data-toggle="tab" aria-expanded="false"><?=__('Address')?></a>
                    </li>
	            <li class="">
                        <a href="#items" data-toggle="tab" aria-expanded="false"><?=__('Items')?></a>
                    </li>
	            <li class="">
                        <a href="#delivery" data-toggle="tab" aria-expanded="false"><?=__('Delivery')?></a>
                    </li>
	            <li class="">
                        <a href="#payment" data-toggle="tab" aria-expanded="false"><?=__('Payment')?></a>
                    </li>
	            <li class="">
                        <a href="#comments" data-toggle="tab" aria-expanded="false"><?=__('Comments')?></a>
                    </li>
	        </ul>

	        <div class="tab-content">
	            <div class="tab-pane active" id="identity">
		        <!-- identity content -->
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group input text">
                                    <?=$this->Form->control('number');?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group input text">
                                    <?php
                                    echo $this->Form->control('date_order', [
                                        /* 'empty' => true*/
                                        'type' => 'text',
	                                'data-inputmask-alias' => 'datetime',
	                                'data-inputmask-inputformat' => 'dd/mm/yyyy',
	                                'templateVars' => [
		                            'otherClass' => 'date',
	                                ],
                                    ]);?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group input text">
                                    <label class="control-label" for="subsidiary-id"><?=__('Subsidiary')?></label>
                                    <?=$this->Form->select('subsidiary_id', $subsidiaries, [
                                        'empty' => __('choose...'),
                                        'class' => 'select2',
                                        'style' => 'width: 100%']);?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label class="control-label" for="subsidiary-id"><?=__('Employee')?></label>
                                <div class="form-group input text">
                                    <?=$this->Form->select('employee_id', $employees, [
                                        'empty' => __('choose...'),
                                        'class' => 'select2',
                                        'style' => 'width: 100%']);?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group input text">
                                    <?=$this->Form->control('customer.cpf_cnpj', [
	                                'maxlength' => '18',
	                                'templateVars' => [
		                            'otherClass' => 'cpf_cnpj',
	                                ],
                                        'onchange' => 'checkCpfCnpj(this);',
	                            ]);?>
                                </div>
                            </div>
                            <div class="col-md-9">
                                <?=$this->Form->control('customer.name');?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group input text">
                                    <?=$this->Form->control('customer.phone', [
                                        'templateVars' => [
                                            'otherClass' => 'phone',
                                        ],
                                    ]);?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group input text">
                                    <?=$this->Form->control('customer.phone2', [
                                        'templateVars' => [
                                            'otherClass' => 'phone',
                                        ],
                                    ]);?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group input text">
                                    <?=$this->Form->control('customer.mobile', [
                                        'templateVars' => [
                                            'otherClass' => 'mobile',
                                        ],
                                    ]);?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group input text">
                                    <?=$this->Form->control('customer.mobile2', [
					'label' => 'Celular (2)',
                                        'templateVars' => [
                                            'otherClass' => 'mobile',
                                        ],
                                    ]);?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group input text">
                                    <?php
                                    echo $this->Form->control('customer.email', [
                                        'type' => 'text',
                                        'templateVars' => [
                                            'otherClass' => 'email'
                                        ]
                                    ]);?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group input text">
                                    <?php
                                    echo $this->Form->control('customer.birth_date', [
                                        /* 'empty' => true*/
                                        'type' => 'text',
                                        /* 'class' => 'date',*/
	                                'data-inputmask-alias' => 'datetime',
	                                'data-inputmask-inputformat' => 'dd/mm/yyyy',
	                                'templateVars' => [
		                            'otherClass' => 'date',
	                                ],
                                    ]);?>
                                </div>
                            </div>
                            <?=$this->Form->control('existing_customer', [
                                'label' => false,
                                'div' => false,
                                'style' => 'display:none;',
                            ]);?>
                        </div>
                    </div>
	            <div class="tab-pane" id="address">
                        <?=$this->Form->control('unnecessary_customer_address', [
                            'type' => 'checkbox',
                            'label' => '&nbsp;&nbsp<b>endereço desnecessário</b>',
                            'onchange' => 'changeUnnecessaryCustomerAddress()',
                            'escape' => false,
                        ])?>
                        <div id="divCustomerAddress">
                            <?=$this->Element('Address', ['type' => 'customer']);?>
                        </div>
                    </div>
	            <div class="tab-pane" id="items">
                        <?=$this->Element('ProductAdd');?>
                    </div>
	            <div class="tab-pane" id="delivery">
                        <?=$this->Element('Delivery');?>
                    </div>
	            <div class="tab-pane" id="payment">
                        <?=$this->Element('PaymentAdd');?>
                    </div>
	            <div class="tab-pane" id="comments">
                        <?=$this->Form->textarea('comment.body', ['required' => false]);?>
                        <br>
                        <div>
                            <?php for($i = $countComments['count'] - 1; $i >= 0; $i--): ?>
                                <?php echo '' // $order->comments[$i]['body'] . '<br>'; ?>

                                <div class="post clearfix">
                                    <div class="user-block">
                                        <?php if (empty($files) || !isset($files[$order->comments[$i]['user_id']])): ?>
                                            <?=$this->Html->image('avatar_placeholder.png', ['class' => 'img-circle img-bordered-sm']);?>
                                        <?php else: ?>
                                            <?=$this->Html->image(
                                                $files[$order->comments[$i]['user']['id']]['path'] . DS .
                                                $files[$order->comments[$i]['user']['id']]['hash'] . '.mini.' .
                                                $files[$order->comments[$i]['user']['id']]['extension'], [
                                                    'pathPrefix' => '',
                                                    'class' => 'img-circle img-bordered-sm',
                                            ]);?>
                                        <?php endif; ?>
                                        <span class="username">
                                            <a href="#"><?=$order->comments[$i]['user']['nick_name'] ?></a>
                                            <!-- <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a> -->
                                        </span>
                                        <span class="description"><?=$order->comments[$i]['created'] ?></span>
                                    </div>
                                    <!-- /.user-block -->
                                    <p style="margin-left:50px;">
                                        <?=$order->comments[$i]['body'] ?>
                                    </p>
                                </div>
                            <?php endfor; ?>
                        </div>
                    </div>
                </div>
                <!-- /.tab-content -->
                <div class="text-right">
                    <?php echo $this->Form->submit(__('Submit')); ?>
                </div>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col-md -->
    </div>
    <!-- /.row -->

</section>

<?php
echo $this->Html->css([
    'AdminLTE./bower_components/select2/dist/css/select2.css'], [
        'block' => 'css'
]);
echo $this->Html->script([
    'AdminLTE./bower_components/select2/dist/js/select2.js',
    '/node_modules/inputmask/dist/jquery.inputmask.min',
    '/node_modules/inputmask/dist/bindings/inputmask.binding',
    'AdminLTE./bower_components/jquery-sparkline/dist/jquery.sparkline.min',
], [
    'block' => 'script'
]);
echo $this->Html->script([
    'apply-select2',
    'common_masks',
    'order',
    'addresses',
    'js-base',
], [
    'block' => 'scriptBottom'
]);
?>
