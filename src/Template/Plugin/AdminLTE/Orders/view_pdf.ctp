<?php
use Cake\Core\Configure;
use Cake\I18n\Date;
use Cake\Log\Log;

$system_configuration = Configure::read('system_configurations.configuration');
$today = new Date();
//$this->layout = 'AdminLTE.print';
?>
<section>
    <table width="100%">
	<tr valign="top">
	    <td>
		<h3>
		    <?=$system_configuration['name']?>
		</h3>
		<span>CNPJ:&nbsp;<?=$system_configuration['cnpj']?></span><br>
		<?=
		$system_configuration['address']['thoroughfare']
		. ', '
		. $system_configuration['address']['number']
		. ', '
		. $system_configuration['address']['complement']
		. ' - '
		. $system_configuration['address']['state_id']
		. ' - '
		. $system_configuration['address']['city_name']
		. ' - '
		. $system_configuration['address']['neighborhood']
		. ' - '
		. $system_configuration['address']['postal_code']
		?><br>
		tel:&nbsp;<?=$system_configuration['phone']?><br>
		<?=$system_configuration['email']?>
	    </td>
	    <td>
		<span class="pull-right" style="text-align:right;">
		    <?=$today->i18nFormat('dd/MM/yyyy')?><br>
		    pedido:&nbsp;<b><?=$order->number?></b>
		</span>
	    </td>
	</tr>
    </table>
</section>

<span>
    <h5><i class="fa fa-user"></i> <?= __('cliente') ?></h5>
</span>
<section>
    <table width="100%">
	<tr valign="top">
	    <td>
		<p>
		    <small>nome</small><br>
		    <?=$order->customer->name?>
		</p>
		<p>
		    <small>endereço</small><br>
		    <?=
		    $order->delivery['address']['thoroughfare']
		    . ', '
		    . $order->delivery['address']['number']
		    . ', '
		    . $order->delivery['address']['complement']
		    . ' - '
		    . $order->delivery['address']['neighborhood']
		    . ' - '
		    . $order->delivery['address']['state_id']
		    . ' - '
		    . $cityNameOfCustomer
		    . ' - ' . $order->delivery['address']['postal_code']
		    . '<br><small>referência:</small><br>' . $order->delivery['address']['reference']
		    ?><br>
		    
		    <small>telefone(s)</small><br>
			<?php 
			$phones = '';
			if (!empty($order->customer->phone)) {
			    if (strlen($order->customer->phone) == 10) {
				$phones = '(' . substr($order->customer->phone, 0, 2) . ')' . substr($order->customer->phone, 2);
			    } elseif(strlen($order->customer->phone) == 8 || strlen($order->customer->phone) > 10) {
				$phones = $order->customer->phone;
			    } 
			}
			if (!empty($order->customer->phone2)) {
			    if ($phones != '')
				$phones .= ', ';
			    if (strlen($order->customer->phone2) == 10) {
				$phones .= '(' . substr($order->customer->phone2, 0, 2) . ')' . substr($order->customer->phone2, 2);
			    } elseif(strlen($order->customer->phone2) == 8 || strlen($order->customer->phone2) > 10) {
				$phones .= $order->customer->phone2;
			    }
			}
			if (!empty($order->customer->mobile)) {
			    if ($phones != '')
				$phones .= ', ';
			    if (strlen($order->customer->mobile) == 11) {
				$phones .= '(' . substr($order->customer->mobile, 0, 2) . ')' . substr($order->customer->mobile, 2);
			    } elseif(strlen($order->customer->mobile) == 9 || strlen($order->customer->mobile) > 11) {
				$phones .= $order->customer->mobile;
			    }
			}
			if (!empty($order->customer->mobile2)) {
			    if ($phones != '')
				$phones .= ', ';
			    if (strlen($order->customer->mobile2) == 11) {
				$phones .= '(' . substr($order->customer->mobile2, 0, 2) . ')' . substr($order->customer->mobile2, 2);
			    } elseif(strlen($order->customer->mobile2) == 9 || strlen($order->customer->mobile2) > 11) {
				$phones .= $order->customer->mobile2;
			    }
			}
			?>
		    <?=$phones?><br>
		    <small>email</small><br>
		    <?=$order->customer->email?><br>
		    <small>vendedor</small><br>
		    <?=$order->employee->nick_name?><br>
		    
		</p>
	    </td>
	</tr>
    </table>
</section>

<span>
    <h5 class="box-title"><i class="fa fa-cube"></i> <?= __('items') ?></h5>
</span>
<section>
    <table width="100%">
	<thead>
            <tr valign="top">
		<td width="10%" class="text-right"><small><b><?= __('qtd') ?></b></small></td>
		<td scope="col" width="20%"><small><b><?= __('código') ?></b></small></td>
		<td scope="col" width="60%"><small><b><?= __('produto') ?></b></small></td> <!-- TRADUZIR -->
		<!-- <td scope="col" width="10%" style="text-align:right"><small><b> --><?='';// __('preço') ?><!-- </b></small></td> -->
            </tr>
	</thead>
	<tbody>

            <?php foreach ($order->items as $item): ?>
		<tr valign="top">
		    <td valign="top"><?= h($item->qtd) ?></td>
		    <td valign="top"><?= h($item->sku) ?></td>
		    <td valign="top"><?= h($item->title) ?>
			<?=empty($item->complement) ? '' : '<br><i>' . $item->complement . '</i></br>'?>
		    </td>
		    <td valign="top" style="text-align:right"><?=h($this->Number->currency($item->price))?></td>
		</tr>
            <?php endforeach; ?>

	</tbody>
	<tfoot>
	    <tr valign="top">
		<td valign="top"><b>total:&nbsp;</b></td>
		<td valign="top"></td>
		<td valign="top"></td>
		<td  valign="top" style="text-align:right"><?= h($this->Number->currency($orderAmount->order_amount)) ?></td>
	    </tr>
	</tfoot>
    </table>
</section>

