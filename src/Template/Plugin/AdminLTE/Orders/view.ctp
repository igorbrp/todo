<section class="content-header">
  <h1>
      <?=__('Pedido')?> <!-- TRADUZIR -->
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
      <?=$this->element('MenuHeader')?>
  </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-info"></i>
                    <h3 class="box-title"><?php echo __('Information'); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt scope="row"><?= __('Number') ?></dt>
                        <dd><?= h($order->number) ?></dd>
                        <dt scope="row"><?= __('Customer') ?></dt>
                        <dd><?= $order->has('customer') ? $this->Html->link($order->customer->name, ['controller' => 'Customers', 'action' => 'view', $order->customer->id]) : '' ?></dd>
                        <dt scope="row"><?= __('Subsidiary') ?></dt>
                        <dd><?= $order->has('subsidiary') ? $this->Html->link($order->subsidiary->name, ['controller' => 'Subsidiaries', 'action' => 'view', $order->subsidiary->id]) : '' ?></dd>
                        <dt scope="row"><?= __('Employee') ?></dt>
                        <dd><?= $order->has('employee') ? $this->Html->link($order->employee->name, ['controller' => 'Employees', 'action' => 'view', $order->employee->id]) : '' ?></dd>
                        <dt scope="row"><?= __('Date Order') ?></dt>
                        <dd><?= h($order->date_order) ?></dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Endereço do cliente') ?></h3> <!-- TRADUZIR -->
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt scope="row"><?= __('CEP') ?></dt>
                        <dd><?= $order->customer['address']['postal_code'] ?></dd>
                        <dt scope="row"><?= __('Estado') ?></dt>
                        <dd><?= $order->customer['address']['state_id']?></dd>
                        <dt scope="row"><?= __('Cidade') ?></dt>
                        <dd><?= $order->customer['address']['city_id']?></dd>
                        <dt scope="row"><?= __('Bairro') ?></dt>
                        <dd><?= $order->customer['address']['neighborhood']?></dd>
                        <dt scope="row"><?= __('Logradouro') ?></dt>
                        <dd><?= $order->customer['address']['thoroughfare']?></dd>
                        <dt scope="row"><?= __('Número') ?></dt>
                        <dd><?= $order->customer['address']['number']?></dd>
                        <dt scope="row"><?= __('Complemento') ?></dt>
                        <dd><?= $order->customer['address']['complement']?></dd>
                        <dt scope="row"><?= __('Referência') ?></dt>
                        <dd><?= $order->customer['address']['reference']?></dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Items') ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php if (!empty($order->items)): ?>
                        <table class="table table-hover">
                            <tr>
                                <th scope="col"><?= __('Qtd') ?></th>
                                <th scope="col"><?= __('Sku') ?></th>
                                <th scope="col" colspan="2"><?= __('Fornecedor') ?></th> <!-- TRADUZIR -->
                                <!-- <th scope="col"> --><?='';// __('Supply Name') ?><!-- </th> -->
                                <th scope="col"><?= __('Title') ?></th>
                                <th class="text-right" scope="col"><?= __('Price') ?></th>
                            </tr>
                            <?php foreach ($order->items as $items): ?>
                                <tr>
                                    <td><?= h($items->qtd) ?></td>
                                    <td><?= h($items->sku) ?></td>
                                    <td><?= h($items->supply_code) ?></td>
                                    <td><?= h($items->supply_name) ?></td>
                                    <td><?= h($items->title) ?></td>
                                    <td class="text-right"><?= h($this->Number->currency($items->price))?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Comments') ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php if (!empty($order->comments)): ?>
                        <table class="table table-hover">
                            <tr>
                                <th scope="col"><?= __('Usuário') ?></th> <!-- TRADUZIR -->
                                <th scope="col"><?= __('Comentário') ?></th> <!-- TRADUZIR -->
                                <th class="text-right" scope="col"><?= __('Created') ?></th>
                            </tr>
                            <?php foreach ($order->comments as $comments): ?>
                                <tr>
                                    <td><?= h($comments->user['nick_name']) ?></td>
                                    <td><?= h($comments->body) ?></td>
                                    <td class="text-right"><?= h($comments->created) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Deliveries') ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php if (!empty($order->delivery)): ?>
                        <table class="table table-hover">
                            <tr>
                                <th scope="col"><?= __('Id') ?></th>
                                <th scope="col"><?= __('Order Id') ?></th>
                                <th scope="col"><?= __('Foreseen Date') ?></th>
                                <th scope="col"><?= __('Scheduled Date') ?></th>
                                <th scope="col"><?= __('Delivery Date') ?></th>
                            </tr>
                            <tr>
                                <td><?= h($order->delivery->id) ?></td>
                                <td><?= h($order->delivery->order_id) ?></td>
                                <td><?= h($order->delivery->foreseen_date) ?></td>
                                <td><?= h($order->delivery->scheduled_date) ?></td>
                                <td><?= h($order->delivery->delivery_date) ?></td>
                            </tr>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>

    <?php if(isset($order->delivery->same_customer_address)) : ?>
        <div class="row">
            <div class="col-md-12">
                <div class="box box-solid">
                    <div class="box-header with-border">
                        <i class="fa fa-share-alt"></i>
                        <h3 class="box-title"><?= __('Endereço de entrega') ?></h3> <!-- TRADUZIR -->
                    </div>
                    <?php if($order->delivery->same_customer_address == 0) : ?>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <dt scope="row"><?= __('CEP') ?></dt>
                                <dd><?= $order->delivery['address']['postal_code'] ?></dd>
                                <dt scope="row"><?= __('Estado') ?></dt>
                                <dd><?= $order->delivery['address']['state_id']?></dd>
                                <dt scope="row"><?= __('Cidade') ?></dt>
                                <dd><?= $order->delivery['address']['city_id']?></dd>
                                <dt scope="row"><?= __('Bairro') ?></dt>
                                <dd><?= $order->delivery['address']['neighborhood']?></dd>
                                <dt scope="row"><?= __('Logradouro') ?></dt>
                                <dd><?= $order->delivery['address']['thoroughfare']?></dd>
                                <dt scope="row"><?= __('Número') ?></dt>
                                <dd><?= $order->delivery['address']['number']?></dd>
                                <dt scope="row"><?= __('Complemento') ?></dt>
                                <dd><?= $order->delivery['address']['complement']?></dd>
                                <dt scope="row"><?= __('Referência') ?></dt>
                                <dd><?= $order->delivery['address']['reference']?></dd>
                            </dl>
                        </div>
                    <?php else : ?>
                        <div class="box-body">
                            <dl class="dl-horizontal">
                                <dt scope="row"><?= 'mesmo do cliente';  ?></dt>
                                <dd></dd>
                            </dl>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>

    <?php endif; ?>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-share-alt"></i>
                    <h3 class="box-title"><?= __('Payments') ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php if (!empty($order->payments)): ?>
                        <table class="table table-hover">
                            <tr>
                                <th scope="col"><?= __('Método de pagamento') ?></th> <!-- TRADUZIR -->
                                <th scope="col"><?= __('Financeira') ?></th> <!-- TRADUZIR -->
                                <th scope="col"><?= __('Value') ?></th>
                                <th scope="col"><?= __('Installments') ?></th>
                            </tr>
                            <?php foreach ($order->payments as $payments): ?>
                                <tr>
                                    <td><?= h($payments['payment_method']['name']) ?></td>
                                    <td><?= h(isset($payments['financier']['name']) ? $payments['financier']['name'] : '') ?></td>
                                    <?php
                                        $installments = $payments->value / $payments->installments;
                                    ?>
                                    <td><?= h($this->Number->currency($payments->value)) ?></td>
                                    <td><?= h($payments->installments) . ' X ' . $this->Number->currency($installments) ?></td>
                                </tr>
                            <?php endforeach; ?>
                        </table>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
