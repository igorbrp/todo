<style>
 .user-mini {
     margin:auto 10px auto auto;
     width: 25px;
     height: 25px;
 }
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Orders')?>
        <small>&nbsp;</small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true,'nodelete' => true])?>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
                        <?=$this->element('Search', ['optionSearch' => 'OrdersSearch', 'placeholder' => 'pesquise por número do pedido ou nome do cliente'])?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">

		    <div class="panel-group">
			<div class="panel">

			    <div class="row panel-heading"> <!-- cabeçalho do pedido -->
				<div class="col-md-1"></div>
				<div class="col-md-1"><b><?=$this->Paginator->sort('number', ['label' => 'pedido']) ?></b></div>
				<div class="col-md-1"><b><?=$this->Paginator->sort('date_order', ['label' => 'data']) ?></b></div>
				<div class="col-md-3"><b><?=$this->Paginator->sort('customer_id', ['label' => 'cliente']) ?></b></div>
				<div class="col-md-2"><b><?=$this->Paginator->sort('subsidiary_id', ['label' => 'filial']) ?></b></div>
				<div class="col-md-1"><b><?=$this->Paginator->sort('employee_id', ['label' => 'vendedor']) ?></b></div>
				<div class="col-md-2 text-right"><b><?=$this->Paginator->sort('order_amount', ['label' => 'valor']) ?></b></div>
				<div class="col-md-1 text-right"><b>ações</b></div>
			    </div>

			    <div class="panel-orders" id="panel-orders">
				<ul style="list-style-type:none;padding-left: 0px;">
				    <?php foreach ($orders as $order): ?>
					<li id="li-order-<?=$order->id?>">
					    <?=$this->element('ShowOrderLine', ['order' => $order]);?>
					</li>
				    <?php endforeach; ?>
				</ul>
			    </div>

			</div>
		    </div>

		</div>
	    </div>
	</div>
    </div>
    <div class="row">
	<div class="col-sm-5">
	    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
		<?php echo $this->Paginator->counter(['format' => 'range']);?>
	    </div>
	</div>
	<div class="col-sm-7">
	    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
		<!-- Paginator -->
		<?=
		$this->element('Paginator');
		?>
	    </div>
	</div>
    </div>
</section>

<div id="divModalFormInvoice" style="display:none">
    <div class="row">
	<div class="col-md-6">
	    <?php
	    echo $this->Form->control('modal_invoice', ['label' => 'nota fiscal']);
	    ?>
	</div>
    </div>
</div>

<div id="divModalFormDispatched" style="display:none">

    <div class="row">
	<div class="col-md-6">
	    <div class="form-group required">
		<label class="control-label" for="modal-datepicker">data:&nbsp;</label>
		<br>
		<div class="input-group date">
		    <div class="input-group-addon">
			<i class="fa fa-calendar">
			</i>
		    </div>
		    <input type="text" class="form-control pull-right" id="modal-datepicker">
		</div>
	    </div>
	</div>

	<div class="col-md-6">
	    <div class="form-group required">
		<label class="control-label" for="modal_scheduled_horary">turno previsto:&nbsp;</label>
		<div class="form-group input required">

		    <select class="select2-modal" name="modal_horary" id="modal-horary">
			<option value="morning">manhã</option>
			<option value="afternoon">tarde</option>
			<option value="any">horário comercial</option>
		    </select>
		</div>
	    </div>
	</div>
    </div>

    <div class="row">
	<div class="col-md-6">
	    <div class="form-group required">
		<label class="control-label" for="modal-vehicle">veículo:&nbsp;</label><br>
		<div class="form-group input required">
		    <?php echo $this->Form->select('modal_vehicle', $vehicles, [
			'id' => 'modal-vehicle',
			//			'class' => 'select2 select2-modal',
			'empty' => 'escolha', // TRADUZIR
			'width' => '100%',
		    ]);?>
		</div>
	    </div>
	</div>
    </div>

</div>
<?php
echo $this->Html->script([
    'AdminLTE./bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min',
    'AdminLTE./bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min',
    'AdminLTE./bower_components/jquery-sparkline/dist/jquery.sparkline.min',
    '/node_modules/inputmask/dist/jquery.inputmask.min',
    '/node_modules/inputmask/dist/bindings/inputmask.binding',
], [
    'block' => 'script'
]);

echo $this->Html->script([
    'js-base',
    'index',
    'order-item',
    /* 'items',
     * 'order-index', */
], [
    'block' => 'scriptBottom'
]);
?>
