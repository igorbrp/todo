<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Vehicle $vehicle
 */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	<?=__('Veículo')?>
	<small>
            <?php if(strtolower($this->request->getParam('action')) == 'edit') : ?>
                <?=$vehicle->identification;?>
            <?php else : ?>
                <?='novo'?> <!-- TRADUZIR -->
            <?php endif; ?>
        </small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <span><?=__('Identification'); ?></span>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create($vehicle, ['role' => 'form']); ?>
                <div class="box-body">
                    <?php
                    echo $this->Form->control('identification');
                    echo $this->Form->control('driver', ['label' => 'motorista']);
                    ?>
                </div>
                <!-- /.box-body -->

                <?php echo $this->Form->submit(__('Submit')); ?>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>
