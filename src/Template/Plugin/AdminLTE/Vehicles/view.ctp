<section class="content-header">
  <h1>
    Veículo
    <small><?=$vehicle->identification ?></small>
  </h1>
  <ol class="breadcrumb">
      <?=$this->element('MenuHeader')?>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Identification') ?></dt>
            <dd><?= h($vehicle->identification) ?></dd>
            <dt scope="row"><?= __('Motorista') ?></dt>
            <dd><?= h($vehicle->driver) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
