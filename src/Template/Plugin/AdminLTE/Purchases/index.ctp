<style>
 .user-mini {
     margin:auto 10px auto auto;
     width: 25px;
     height: 25px;
 }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Compras (estoque)')?> <!-- TRADUZIR -->
        <small><?=__('List'); ?></small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->

<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('List'); ?></h3>
                    <div class="box-tools">
                        <?=$this->element('Search')?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('id', ['label' => 'número']) ?></th>
                                <th scope="col"><?= $this->Paginator->sort('date_purchase', ['label' => 'compra']) ?></th>
                                <th scope="col"><?= $this->Paginator->sort('qtd', ['label' => 'qtd']) ?></th>
                                <th scope="col" colspan="2"><?= $this->Paginator->sort('supply_id') ?></th>
                                <th scope="col" colspan="2"><?= $this->Paginator->sort('product_id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('forecast_date', ['label' => 'previsão de recebimento']) ?></th>
                                <th scope="col" class="actions text-right"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($purchases as $purchase): ?>
                                <tr>
                                    <td><?= h($purchase->id) ?></td>
                                    <td><?= h($purchase->date_purchase) ?></td>
                                    <td><?= $this->Number->format($purchase->qtd) ?></td>
                                    <td><?= $purchase->has('supply_id') ? $this->Html->link($purchase->product->supply->code, ['controller' => 'Supplies', 'action' => 'view', $purchase->product->supply->id]) : '' ?></td>
                                    <td><?= $purchase->has('supply_id') ? $this->Html->link($purchase->product->supply->name, ['controller' => 'Supplies', 'action' => 'view', $purchase->product->supply->id]) : '' ?></td>
                                    <td><?= $purchase->has('product') ? $this->Html->link($purchase->product->sku, ['controller' => 'Products', 'action' => 'view', $purchase->product->id]) : '' ?></td>
                                    <td><?= $purchase->has('product') ? $this->Html->link($purchase->product->title, ['controller' => 'Products', 'action' => 'view', $purchase->product->id]) : '' ?></td>
                                    <td><?= h($purchase->forecast_date) ?></td>
                                    <td class="actions text-right">
                                        <?=$this->element('Actions', ['cod_id' => $purchase->id,])?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
	<div class="col-sm-5">
	    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
		<?php echo $this->Paginator->counter(['format' => 'range']);?>
	    </div>
	</div>
	<div class="col-sm-7">
	    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
		<!-- Paginator -->
		<?=
		$this->element('Paginator');
		?>
	    </div>
	</div>
    </div>
</section>

<?php
$this->Html->script([
    'js-base',
    'index',
], [
    'block' => 'scriptBottom'
]);
?>
