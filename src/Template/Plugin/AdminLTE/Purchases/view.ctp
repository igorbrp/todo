<section class="content-header">
  <h1>
    Compras
    <small><?php echo ''; // __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
      <?=$this->element('MenuHeader', ['noprint' => true])?>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <dl class="dl-horizontal">
		<dt scope="row"><?= __('Número da compra') ?></dt>
		<dd><?= h($purchase->id) ?></dd>
		<dt scope="row"><?= __('Supply') ?></dt>
		<dd><?= $purchase->has('supply') ? $this->Html->link($purchase->supply->name, ['controller' => 'Supplies', 'action' => 'view', $purchase->supply->id]) : '' ?></dd>
		<dt scope="row"><?= __('Product') ?></dt>
		<dd><?= $purchase->has('product') ? $this->Html->link($purchase->product->title, ['controller' => 'Products', 'action' => 'view', $purchase->product->id]) : '' ?></dd>
		<dt scope="row"><?= __('Quantidade') ?></dt>
		<dd><?= $this->Number->format($purchase->qtd) ?></dd>
		<dt scope="row"><?= __('Data da compra') ?></dt>
		<dd><?= h($purchase->date_purchase) ?></dd>
		<dt scope="row"><?= __('Previsão de entrega') ?></dt>
            <dd><?= h($purchase->forecast_date) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-share-alt"></i>
          <h3 class="box-title"><?= __('Recebimentos') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php if (!empty($purchase->receiveds)): ?>
          <table class="table table-hover">
              <tr>
                    <th scope="col"><?= __('Qtd') ?></th>
                    <th scope="col"><?= __('Cancelamento') ?></th>
                    <th scope="col"><?= __('Recebido em') ?></th>
              </tr>
              <?php foreach ($purchase->receiveds as $receiveds): ?>
              <tr>
                    <td><?= h($receiveds->qtd) ?></td>
                    <td><?= h($receiveds->canceled) ?></td>
                    <td><?= h($receiveds->created) ?></td>
              </tr>
              <?php endforeach; ?>
          </table>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
