<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Purchase $purchase
 */
use Cake\I18n\Number;
$template = $this->template == 'add' ? __('Add') : __('Edit');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	<?=__('Compras (estoque)')?>
	<small>
            <?php if(strtolower($this->request->getParam('action')) == 'edit') : ?>
                <?=$product_title;?>
            <?php else : ?>
                <?='novo'?> <!-- TRADUZIR -->
            <?php endif; ?>
        </small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <span><?=__('Identification'); ?></span>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create($purchase, ['role' => 'form']); ?>
		<!-- id hidden -->

                <div style="display:none;">
		    <?php echo $this->Form->input('id', [
			'label' => false,
		    ]);?>
                </div>
                <div class="box-body">
                    <div class="row">

			<?php if (strtolower($this->request->getParam('action') == 'edit')) : ?>

                            <div class="col-md-2">
				<div class="form-group input text">
                                    <?php
                                    echo $this->Form->control('id', [
					'label' => 'número da compra',
					'type' => 'text',
					'disabled' => true,
                                    ]);
                                    ?>
				</div>
                            </div>

			<?php endif; ?>
                        <div class="col-md-2">
                            <div class="form-group input text">
                                <?php
                                echo $this->Form->control('date_purchase', [
                                    'label' => 'data da compra',
                                    'type' => 'text',
	                            'data-inputmask-alias' => 'datetime',
	                            'data-inputmask-inputformat' => 'dd/mm/yyyy',
	                            'templateVars' => [
		                        'otherClass' => 'date',
	                            ],
                                    'disabled' => strtolower($this->request->getParam('action')) == 'edit' ? true : false,
                                ]);
                                ?>
                            </div>
                        </div>

			<div class="col-md-2">
			    <div id="divWorkingDays" class="form-group input">
				<br/>
				<?=$this->Form->control('working_days', [
				    'type' => 'checkbox',
				    'label' => '&nbsp;&nbsp;<b>dias úteis</b>',
				    /* 'div' => false, */
				    'escape' => false,
				    //                'onchange' => 'changeSameCustomerAddress();',
				]);?>
			    </div>
			</div>

			<div class="col-md-2">
			    <div id="divForeseenDays" class="form-group input text">
				<?php
				echo $this->Form->control('forecast_days', [
				    'label' => 'dias previstos',
				    'type' => 'number',
				]);?>
			    </div>
			</div>

			<div class="col-md-2">
                            <div class="form-group input text">
                                <?php
                                echo $this->Form->control('forecast_date', [
                                    'label' => 'previsão',
                                    'type' => 'text',
	                            'data-inputmask-alias' => 'datetime',
	                            'data-inputmask-inputformat' => 'dd/mm/yyyy',
	                            'templateVars' => [
		                        'otherClass' => 'date',
	                            ],
                                ]);
                                ?>
                            </div>
                        </div>
                        
		    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group input text">
                                <label class="control-label" for="sypply-id"><?=__('fornecedor')?></label>
                                <?php
                                echo $this->Form->select('supply_id', $supplies, [
                                    'id' => 'supply-id', 
                                    'empty' => __('escolha...'),
                                    'class' => 'select2',
                                    'style' => 'width: 100%',
                                    'disabled' => strtolower($this->request->getParam('action')) == 'edit' ? true : false,
                                ]);
                                ?>
                            </div>
                        </div>
                        
                        <div class="col-md-4">
                            <div class="form-group input text">
                                <label class="control-label" for="product-id"><?=__('produto')?></label>
                                <?php
                                echo $this->Form->select('product_id', $products, [
                                    'id' => 'product-id', 
                                    'empty' => __('escolha...'),
                                    'class' => 'select2',
                                    'style' => 'width: 100%',
                                    'disabled' => strtolower($this->request->getParam('action')) == 'edit' ? true : false,
                                ]);
                                ?>
                            </div>
                        </div>
			<!-- </div> -->
			<!-- /.row -->
			<!-- <div class="row"> -->
                        <div class="col-md-2">
                            <div class="form-group input text">
                                <?php echo $this->Form->control('qtd', [
                                    'label' => 'qtd',
                                    'disabled' => strtolower($this->request->getParam('action')) == 'edit' ? true : false,
                                ]); ?>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <?php echo $this->Form->control('total_price', [
				'label' => 'preço total',
                                'type' => 'text',
	                        'templateVars' => [
		                    'otherClass' => 'currency',
	                    ]]);?>
                        </div>
		    </div>
		    <!-- /.row -->
		    <div class="text-right">
			<?php echo $this->Form->submit(__('Enviar')); ?>
		    </div>
		    <?php echo $this->Form->end(); ?>
                </div>
            </div>
            <!-- /.box-body -->

        </div>
        <!-- /.box -->
    </div>
    <!-- /.row -->
    <div class="row">
	<div class="col-sm-12">
	    <!-- inicio recebimentos -->
	    <?php if($this->template == 'edit') : ?>
		<div class="box box-primary">
		    <div class="box-header">
			<?php $q = $purchase->qtd - $receivedsSum->recSum; ?>
			<h3 class="box-title">Recebimentos</h3>
			<span id="label-receiveds" class="label label-success">
			    <?=is_null($receivedsSum->recSum) ? 0 : $receivedsSum->recSum;?></span>
			&nbsp;&nbsp;
			<span id="label-to-receive" class="label label-warning"><?=$q;?></span>
			<?php $itemsRecIndex = 0; ?>

			<div class="box-tools">

			    <ul class="nav navbar-nav">
				<li id="li-qtd" style="display:<?=($q > 0) ? 'contents' : 'none' ?>">

				    
				    <div class="box-tools pull-right">
					<div class="input-group">
					    <input id="qtd-input" type="number" class="form-control pull-right" style="width:35%;" value="1" min="1" max="<?=$q?>">
					    <span class="input-group-btn">
						<button id="btrecok" type="button" class="btn btn-info btn-flat">receber</button>
					    </span>
					</div>
				    </div> 
				    


				</li>
				<li id="li-recsum" style="display:<?=($receivedsSum->recSum > 0) ? 'contents' : 'none' ?>">
				    
				    <div class="box-tools pull-right">
					<div class="input-group">
					    <input id="cancel-input" type="number" class="form-control pull-right" style="width:35%;" value="1" min="1" max="<?=$receivedsSum->recSum?>">
					    <span class="input-group-btn">
						<button id="btreccancel" type="button" class="btn btn-warning btn-flat">cancelar</button>
					    </span>
					</div>
				    </div>
				    <li>
			    </ul>
			    


			</div>


		    </div> <!-- /.box-header -->

		    <div class="box-body">
			<ul id="receiveds" class="todo-list">
			    <?php foreach($purchase->receiveds as $received) : ?>
				<?php echo ''; // if($received->canceled == 0) : ?>

				<li id="li-receiveds-<?=$itemsRecIndex;?>">
				    <!-- campos type hidden -->
				    <input id="recs-<?=$itemsRecIndex;?>-id" value="<?=$received->id;?>" type="hidden">
				    <?php echo '';
				    /* <input id="recs-<?=$itemsRecIndex;?>-product_id" name="recs[<?=$itemsRecIndex;?>][product_id]" value="<?=$received->product_id;?>" type="hidden">*/
				    ?>
				    <input id="recs-<?=$itemsRecIndex;?>-qtd" name="recs[<?=$itemsRecIndex;?>][qtd]" value="<?=$received->qtd;?>" type="hidden">
				    <input id="recs-<?=$itemsRecIndex;?>-created" name="recs[<?=$itemsRecIndex;?>][created]" value="<?=$received->created;?>" type="hidden">
				    <!-- Fim de campos type hidden -->
				    <span class="label label-<?=$received->qtd > 0 ? 'success' : 'warning'?>">
					<?=$received->qtd;?>
				    </span>
				    <span class="text-green">
					<?=$received->qtd > 0 ? '&nbsp; recebido' : '&nbsp; cancelado'?> em <?=$received->created->format('d/m/Y \à\s H:i');?>
				    </span>
				</li>

				<?php $itemsRecIndex++; ?>
				<?php echo ''; // endif; ?> <!-- /if($received->canceled == 0) -->
			    <?php endforeach; ?>
			</ul>
		    </div> <!-- /.box-body -->
		</div> <!-- /.box-primary -->
	    <?php else : ?>
		<h3>não existem recebimentos</h3>	
	    <?php endif; ?>

	    <!-- fim recebimentos -->
	</div>
    </div> <!-- /.row -->

</section>
<?php
echo $this->Html->css([
    'AdminLTE./bower_components/select2/dist/css/select2.css'], [
        'block' => 'css'
]);
echo $this->Html->script([
    'AdminLTE./bower_components/select2/dist/js/select2.js',
    '/node_modules/inputmask/dist/jquery.inputmask.min',
    '/node_modules/inputmask/dist/bindings/inputmask.binding',
], [
    'block' => 'script'
]);
echo $this->Html->script([
    'apply-select2',
    'common_masks',
    'purchases',
    'js-base',
], [
    'block' => 'scriptBottom'
]);

?>
