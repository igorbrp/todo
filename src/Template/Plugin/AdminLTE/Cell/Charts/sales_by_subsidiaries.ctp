<!-- DONUT CHART -->
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Vendas por lojas neste mês</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body">
        <canvas id="pieChart" style="height:250px"></canvas>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->
<script>
 var salesBySubsidiariesArr = <?=json_encode($sales_by_subsidiaries)?>;
</script>
