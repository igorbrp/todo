<?php
$percent = $salesMonth * 100 / $salesLastMonth;
if ($percent > 100) {
    $icon = 'fa fa-arrow-up';
} elseif ($percent < 100) {
    $icon = 'fa fa-arrow-down';
} elseif ($percent == 100) {
    $icon = 'fa fa-circle';
}
//$percent = $percent < 0 ? $percent * -1 : $percent;
?>

<div class="info-box bg-green">
    <span class="info-box-icon"><i class="fa fa-shopping-cart"></i></span>

    <div class="info-box-content">
        <span class="info-box-text">Pedidos neste mês</span>
        <span class="info-box-number"><?=$salesMonth?></span>

        <div class="progress">
            <div class="progress-bar" style="width: <?=$percent?>%;"></div>
        </div>
        <span class="progress-description">
            <span><i class="<?=$icon?>"></i></span> <?=$this->Number->precision($percent, 0)?>% do mês passado
        </span>
    </div>
    <!-- /.info-box-content -->
</div>
<!-- /.info-box -->
