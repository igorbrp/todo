<?php use Cake\Log\Log; ?>
<li>
    <?=$this->Html->link(
        '<i class="fa fa-dashboard"></i> <span>' . __('Dashboard') . '</span>',
        '/',
        ['escape' => false])?>
</li>
    <?php
    $pull_right = $this->Html->tag('i', '', ['class' => 'fa fa-angle-left pull-right']);
    $span_pull_right = $this->Html->tag('span', $pull_right, ['class' => 'pull-right-container']);

    $fullMenu = '';
    foreach($menus as $menu) {

	if(!empty($menu['menu_items'])) {

            $linkTitle = $this->Html->link(
                $menu['icon'] . $this->Html->tag('span', $menu['title']) . $span_pull_right,
                '#',
                ['escape' => false]
            );
            $itemsCount = 0;
            $arrayItems = [];

	    foreach($menu['menu_items'] as $menu_item) {
                $urlIndex = '/' . $menu_item['controller'] . '/' . $menu_item['action'];
                if ($this->AuthLink->isAuthorized($urlIndex)) {
                    $linkIndex = $this->Html->link(
                        $menu_item['icon'] . $this->Html->tag('span', $menu_item['title']),
                        $urlIndex,
                        ['escape' => false]
                    );
                    $itemAdd = $class = '';
                    //                        $urlAdd = $this->Url->build(['controller' => $menu_item['controller'], 'action' => 'add']);

                    /* $urlAdd = '/' . $menu_item['controller'] . '/add';
                     * if($this->AuthLink->isAuthorized($urlAdd)) {
                     *     $linkAdd = $this->Html->link(
                     *         '<i class="fa fa-circle-o text-success"></i>' . $this->Html->tag('span', __('new')),
                     *         $urlAdd,
                     *         ['escape' => false]
                     *     );
                     *     $class = 'treeview';
                     *     $span = $span_pull_right;
                     *     $itemAdd = $this->Html->tag('ul', $this->Html->tag('li', $linkAdd), ['class' => 'treeview-menu']);
                     * }
                     * $arrayItems[] = $this->Html->tag('li', $linkIndex . $itemAdd, ['class' => $class]);
                     */

                    $arrayItems[] = $this->Html->tag('li', $linkIndex, ['class' => $class]);
                    $itemsCount++;
                }
            }
            // endforeach
            $fullMenu .= $this->Html->tag('li', $linkTitle . $this->Html->tag('ul', $arrayItems, ['class' => 'treeview-menu', 'style' => 'display: block']), ['class' => 'treeview']);
        }
        // endif
    }
    // endforeach

    echo $fullMenu;
    ?>
