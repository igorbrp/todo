<?php
    if (empty($systemuser->file)) {
        $image = 'avatar_placeholder.png';
        $pathPrefix = DS . 'img' . DS;
    } else {
        $image = $systemuser->file['path'] . DS . $systemuser->file['hash'] . '.media.' . $systemuser->file['extension'];
        $pathPrefix = '';
    }
?>

<a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <?php echo ''; // $this->Html->image('user2-160x160.jpg', array('class' => 'user-image', 'alt' => 'User Image')); ?>
    <?php echo $this->Html->image($image, array('class' => 'user-image', 'alt' => 'User Image', 'pathPrefix' => $pathPrefix)); ?>
    <span class="hidden-xs"><?=$systemuser->nick_name;?></span>
</a>
<ul class="dropdown-menu">
    <!-- User image -->
    <li class="user-header">
        <?php echo $this->Html->image($image, array('class' => 'img-circle', 'alt' => 'User Image', 'pathPrefix' => $pathPrefix)); ?>

        <p>
            <?=$systemuser->nick_name;?> - <?=$systemuser->role?>
            <small><?=__('since') . ' ' . $systemuser->created?></small>
        </p>
    </li>
    <!-- Menu Body -->
    <li class="user-body">
        <!-- <div class="row">
             <div class="col-xs-4 text-center">
             <a href="#">Followers</a>
             </div>
             <div class="col-xs-4 text-center">
             <a href="#">Sales</a>
             </div>
             <div class="col-xs-4 text-center">
             <a href="#">Friends</a>
             </div>
             </div> -->
        <!-- /.row -->
    </li>
    <!-- Menu Footer-->
    <li class="user-footer">
        <div class="pull-left">
	    <?=$this->Html->link(__('Perfil'), ['plugin' => 'SystemUser', 'controller' => 'SystemUsers', 'action' => 'profile'], ['class' => 'btn btn-default btn-flat']); // TRADUZIR?>
                    </div>
        <div class="pull-right">
		    <?=$this->Html->link(__('Sair'), ['plugin' => 'CakeDC/Users', 'controller' => 'Users', 'action' => 'logout'], ['class' => 'btn btn-default btn-flat']); // TRADUZIR?>
        </div>
    </li>
</ul>
