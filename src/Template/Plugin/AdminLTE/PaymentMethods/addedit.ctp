<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PaymentMethod $paymentMethod
 */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	<?=__('Método de pagamento')?>
	<small>
            <?php if(strtolower($this->request->getParam('action')) == 'edit') : ?>
                <?=$paymentMethod->name;?>
            <?php else : ?>
                <?='novo'?> <!-- TRADUZIR -->
            <?php endif; ?>
        </small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <span><?=__('Identification'); ?></span>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create($paymentMethod, ['role' => 'form']); ?>
                <div class="box-body">
                    <?php
                    echo $this->Form->control('name');
		    //                    echo $this->Form->control('controller');
                    echo $this->Form->control('max_installments', ['label' => 'Parcelamento máximo']);
                    ?>
                </div>
                <!-- /.box-body -->

                <div class="text-right
"><?php echo $this->Form->submit(__('Submit')); ?></div>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>
