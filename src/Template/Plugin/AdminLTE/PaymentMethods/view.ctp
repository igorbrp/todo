<section class="content-header">
  <h1>
    Método de pagamento
    <small><?php echo $paymentMethod->name; ?></small>
  </h1>
  <ol class="breadcrumb">
      <?=$this->element('MenuHeader', ['noprint' => true])?>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Name') ?></dt>
            <dd><?= h($paymentMethod->name) ?></dd>
            <dt scope="row"><?= __('Parcelamento máximo') ?></dt>
            <dd><?= $this->Number->format($paymentMethod->max_installments) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
