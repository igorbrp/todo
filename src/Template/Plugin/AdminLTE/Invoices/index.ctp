<style>
 .user-mini {
     margin:auto 10px auto auto;
     width: 25px;
     height: 25px;
 }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Invoices')?>
        <small><?=__('List'); ?></small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"><?php echo __('List'); ?></h3>
                    <div class="box-tools">
                        <?=$this->element('Search')?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('invoice_number') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                                <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($invoices as $invoice): ?>
				<tr>
                                    <td><?= $this->Number->format($invoice->id) ?></td>
                                    <td><?= h($invoice->invoice_number) ?></td>
                                    <td><?= h($invoice->created) ?></td>
                                    <td><?= h($invoice->modified) ?></td>
                                    <td class="actions text-right">
					<?= $this->Html->link(__('View'), ['action' => 'view', $invoice->id], ['class'=>'btn btn-info btn-xs']) ?>
					<?= $this->Html->link(__('Edit'), ['action' => 'edit', $invoice->id], ['class'=>'btn btn-warning btn-xs']) ?>
					<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $invoice->id], ['confirm' => __('Are you sure you want to delete # {0}?', $invoice->id), 'class'=>'btn btn-danger btn-xs']) ?>
                                    </td>
				</tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
	</div>
    </div>
</section>
<?php
$this->Html->css(['AdminLTE./plugins/iCheck/all'],['block' => 'css']);
$this->Html->script(['AdminLTE./plugins/iCheck/icheck.min', 'js-index', 'js-base'], ['block' => 'scriptBottom']);
?>
