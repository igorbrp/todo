<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="users form large-9 medium-8 columns content">
    <h1>Dashboard - Welcome</h1>
    <h2><?=$user->role?></h2>
    <li class="treeview">
	<?=$this->Html->link(
	    '<i class="fa fa-dashboard"></i> <span>' . __('Dashboard') . '</span>',
	    ['controller' => 'dashboard', 'action' => 'welcome'],
	    ['escape' => false]
	)?>
    </li>


    <?php foreach($menus as $menu): ?>

	<?php if(!empty($menu['menu_items'])): ?>
	    <li>
		<!-- <li class="treeview"> -->
		    <a href="#">
		    <?=$menu['icon']?> <span><?=$menu['title']?></span>
		    <span class="pull-right-container">
			<i class="fa fa-angle-left pull-right"></i>
		    </span>
		    </a>
		    <ul>
			<!-- <ul class="treeview-menu"> -->
			    <?php foreach($menu['menu_items'] as $menu_item): ?>

			<li>
			    <a href="<?=$this->Url->build([ 'controller' => $menu_item['controller'], 'action' => $menu_item['action']])?>">
				<?=$menu_item['icon']?> <?=$menu_item['title']?>
			    </a>
			</li>

		    <?php  endforeach; ?>
		</ul>
	    </li>
	<?php endif; ?>

    <?php endforeach; ?>
</div>
