<?php
use Cake\I18n\Date;
$today = new Date();
$this->layout = 'AdminLTE.print';
?>
<style>
 body {
     font-size: 0.8em;
 }
 @media print{
     @page {
	 size: landscape
     }
 }
</style>
<section class="content">
    <header>
	<h3>
	    Estoque Bienestar
	    <div class="pull-right"><?=$today->i18nFormat('dd/MM/yyyy')?></div>
	</h3>
    </header>
    <table width="100%">
	<thead style="color:#808080;">
	    <th scope="col">
		fornecedor
	    </th>
	    <th scope="col">
		sku
	    </th>
	    <th scope="col">
		produto
	    </th>
	    <th scope="col" width="5%" style="text-align:center">
		qtd
	    </th>
	    <th scope="col" width="5%" style="text-align:center">
		pedido
	    </th>
	    <th scope="col" width="5%" style="text-align:center">
		pedido
	    </th>
	    <th scope="col" width="5%" style="text-align:center">
		pedido
	    </th>
	    <th scope="col" width="5%" style="text-align:center">
		pedido
	    </th>
	    <th scope="col" width="5%" style="text-align:center">
		pedido
	    </th>
	    <th scope="col" width="5%" style="text-align:center">
		pedido
	    </th>
	</thead>
	<tbody>
	    <?php foreach ($stocks as $stock) : ?>
		<tr>
		    <td valign="top">
			<?=$stock->product->supply->name?>
		    </td>
		    <td valign="top">
			<?=$stock->product->sku?>
		    </td>
		    <td valign="top">
			<?=$stock->product->title?><br>
			<i><?=$stock->product->complement?></i>
		    </td>
		    <td valign="top" style="text-align:center">
			<b><?=$stock->qtd?></b>
		    </td>
		    <td valign="top">
			&nbsp;
		    </td>
		    <td valign="top">
			&nbsp;
		    </td>
		    <td valign="top">
			&nbsp;
		    </td>
		    <td valign="top">
			&nbsp;
		    </td>
		    <td valign="top">
			&nbsp;
		    </td>
		    <td valign="top">
			&nbsp;
		    </td>
		</tr>
	    <?php endforeach; ?>
	</tbody>
	<tfoot>
	    <tr>
		<td>&nbsp</td>
		<td>&nbsp</td>
		<td>&nbsp</td>
		<td>&nbsp</td>
		<td>&nbsp</td>
		<td>&nbsp</td>
		<td>&nbsp</td>
		<td>&nbsp</td>
		<td>&nbsp</td>
		<td>&nbsp</td>
	    </tr>
	</tfoot>
    </table>
</section>
