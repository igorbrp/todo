<section class="content-header">
  <h1>
    Stock
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
      <?=$this->element('MenuHeader')?>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Product') ?></dt>
            <dd><?= $stock->has('product') ? $this->Html->link($stock->product->title, ['controller' => 'Products', 'action' => 'view', $stock->product->id]) : '' ?></dd>
            <dt scope="row"><?= __('Qtd') ?></dt>
            <dd><?= $this->Number->format($stock->qtd) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($stock->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($stock->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-share-alt"></i>
          <h3 class="box-title"><?= __('Receiveds') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php if (!empty($stock->receiveds)): ?>
          <table class="table table-hover">
              <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Product Id') ?></th>
                    <th scope="col"><?= __('Purchase Id') ?></th>
                    <th scope="col"><?= __('Qtd') ?></th>
                    <th scope="col"><?= __('Canceled') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
              <?php foreach ($stock->receiveds as $receiveds): ?>
              <tr>
                    <td><?= h($receiveds->id) ?></td>
                    <td><?= h($receiveds->product_id) ?></td>
                    <td><?= h($receiveds->purchase_id) ?></td>
                    <td><?= h($receiveds->qtd) ?></td>
                    <td><?= h($receiveds->canceled) ?></td>
                    <td><?= h($receiveds->created) ?></td>
                    <td><?= h($receiveds->modified) ?></td>
                      <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['controller' => 'Receiveds', 'action' => 'view', $receiveds->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['controller' => 'Receiveds', 'action' => 'edit', $receiveds->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['controller' => 'Receiveds', 'action' => 'delete', $receiveds->id], ['confirm' => __('Are you sure you want to delete # {0}?', $receiveds->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
              </tr>
              <?php endforeach; ?>
          </table>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-share-alt"></i>
          <h3 class="box-title"><?= __('Outputs') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php if (!empty($stock->outputs)): ?>
          <table class="table table-hover">
              <tr>
                    <th scope="col"><?= __('Id') ?></th>
                    <th scope="col"><?= __('Product Id') ?></th>
                    <th scope="col"><?= __('Item Id') ?></th>
                    <th scope="col"><?= __('Qtd') ?></th>
                    <th scope="col"><?= __('Canceled') ?></th>
                    <th scope="col"><?= __('Created') ?></th>
                    <th scope="col"><?= __('Modified') ?></th>
                    <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
              </tr>
              <?php foreach ($stock->outputs as $outputs): ?>
              <tr>
                    <td><?= h($outputs->id) ?></td>
                    <td><?= h($outputs->product_id) ?></td>
                    <td><?= h($outputs->item_id) ?></td>
                    <td><?= h($outputs->qtd) ?></td>
                    <td><?= h($outputs->canceled) ?></td>
                    <td><?= h($outputs->created) ?></td>
                    <td><?= h($outputs->modified) ?></td>
                      <td class="actions text-right">
                      <?= $this->Html->link(__('View'), ['controller' => 'Outputs', 'action' => 'view', $outputs->id], ['class'=>'btn btn-info btn-xs']) ?>
                      <?= $this->Html->link(__('Edit'), ['controller' => 'Outputs', 'action' => 'edit', $outputs->id], ['class'=>'btn btn-warning btn-xs']) ?>
                      <?= $this->Form->postLink(__('Delete'), ['controller' => 'Outputs', 'action' => 'delete', $outputs->id], ['confirm' => __('Are you sure you want to delete # {0}?', $outputs->id), 'class'=>'btn btn-danger btn-xs']) ?>
                  </td>
              </tr>
              <?php endforeach; ?>
          </table>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
