<style>
 .user-mini {
     margin:auto 10px auto auto;
     width: 25px;
     height: 25px;
 }
</style>

<?php
$sKey = $this->Paginator->sortKey();
if (!is_null($sKey)) {
    $sDir = $this->Paginator->sortDir() == 'asc' ? '<span style="color:#cccccc"><i class="fa fa-arrow-down"></i></span>' : '<span style="color:#cccccc"><i class="fa fa-arrow-up"></i></span>';
}
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Estoque'); // TRADUZIR?> 
        <small>&nbsp;</small>
    </h1>
    <?=$this->element('MenuHeader', ['nodelete' => true, 'noadd' => true])?>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
                        <?=$this->element('Search', ['optionSearch' => 'StocksSearch', 'placeholder' => 'pesquise por nome ou código do produto'])?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('Supplies.name', ('Supplies.name' == $sKey ? $sDir : '') . ' fornecedor' , ['escape' => false]) ?></th>
                                <th scope="col"><?= $this->Paginator->sort('Products.sku', ('Products.sku' == $sKey ? $sDir : '') . ' sku' , ['escape' => false]) ?></th>
                                <th scope="col"><?= $this->Paginator->sort('Products.title', ('Products.title' == $sKey ? $sDir : '') . ' produto' , ['escape' => false]) ?></th>
                                <th scope="col"><?= $this->Paginator->sort('qtd', ('qtd' == $sKey ? $sDir : '') . ' qtd' , ['escape' => false]) ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($stocks as $stock): ?>
				<tr>
                                    <td>
					<?= $stock->product->has('supply') ? $this->Html->link($stock->product->supply->name, ['controller' => 'Supplies', 'action' => 'view', $stock->product->supply->id]) : '' ?>
				    </td>
                                    <td>
					<?= $stock->product->has('sku') ? $this->Html->link($stock->product->sku, ['controller' => 'Products', 'action' => 'view', $stock->product->id]) : '' ?>
				    </td>
                                    <td>
					<?= $stock->has('product') ? $this->Html->link($stock->product->title, ['controller' => 'Products', 'action' => 'view', $stock->product->id]) : '' ?>
				    </td>
                                    <td>
					<span id="label-receiveds" class="label label-success">
					    <?= $this->Number->format($stock->qtd) ?>
					</span>
				    </td>
				</tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
	</div>
    </div>
    <div class="row">
	<div class="col-sm-5">
	    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
		<?php echo $this->Paginator->counter(['format' => 'range']);?>
	    </div>
	</div>
	<div class="col-sm-7">
	    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
		<!-- Paginator -->
		<?=
		$this->element('Paginator');
		?>
	    </div>
	</div>
    </div>
</section>
<?php
echo $this->Html->script([
    'js-base',
    'index',
    'items',
], [
    'block' => 'scriptBottom'
]);
?>
