<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SystemConfiguration $systemConfiguration
 */

$skins = [
    'black' => 'Clean',
    'black-light' => 'Clean full',
    'blue' => 'Azul (padrão)',
    'blue-light' => 'Azul (light)',
    'green' => 'Verde',
    'green-light' => 'Verde (light)',
    'purple' => 'Púrpura',
    'purple-light' => 'Púrpura (light)',
    'red' => 'Vermelho',
    'red-light' => 'Vermelho (light)',
    'yellow' => 'Amarelo',
    'yellow-light' => 'Amarelo (light)',
];

$layouts = [
    'default' => 'Padrão',
    'fixed' => 'Fixo',
    'layout-boxed' => 'Box',
];
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	<?=__('Configuração de aparência')?> <!-- TRADUZIR -->
	<small>&nbsp;</small>
    </h1>
    <p>&nbsp;</p>
</section>

<!-- Main content -->
<section class="content">

    <?php echo $this->Form->create('theme-configuration', ['role' => 'form']); ?>

    <legend><?php echo __('Tema'); ?></legend>

    <fieldset>
	<div class="row">
            <div class="col-md-6">
                <div class="form-group input text">
                    <label class="control-label" for="subsidiary-id"><?=__('Layout')?></label>
		    <?php echo $this->Form->select('layout', $layouts, [
			'class' => 'select2',
			'style' => 'width: 100%',
			'value' => $themeConfiguration['layout'],
			'label' => false]); ?>
		</div>
		<em class="help-block">
		    O layout do sistema pode ser:
		    <ul style="list-style-type:circle">
			<li><b>Padrão: </b>largura da tela e menu acompanhando o scroll da tela</li>
			<li><b>Fixo: </b>largura da tela e menu com scroll independente do conteúdo</li>
			<li><b>Box: </b>largura reduzida e menu acompanhando o scroll da tela</li>
		    </ul>
		</em>		
	    </div>
            <div class="col-md-6">
                <div class="form-group input text">
                    <label class="control-label" for="subsidiary-id"><?=__('Cor')?></label>
		    <?php echo $this->Form->select('skin', $skins, [
			'class' => 'select2',
			'style' => 'width: 100%',
			'value' => $themeConfiguration['skin'],
			'label' => false]); ?>
		</div>
		<em class="help-block">
		    Alterar a cor base do tema
		</em>		
	    </div>
	</div> <!-- /.row -->
    </fieldset>
    <div class="form-group text-right">
	<?php echo $this->Form->submit(__('Submit')); ?>
    </div>
    <?php echo $this->Form->end(); ?>
    <!-- /.box -->
</section>

<?php
echo $this->Html->css([
    'AdminLTE./bower_components/select2/dist/css/select2.css'], [
        'block' => 'css'
]);
echo $this->Html->script([
    'AdminLTE./bower_components/select2/dist/js/select2.js',
    '/node_modules/inputmask/dist/jquery.inputmask.min',
    '/node_modules/inputmask/dist/bindings/inputmask.binding',
], [
    'block' => 'script'
]);
echo $this->Html->script([
    'apply-select2',
    'common_masks',
    'addresses',
], [
    'block' => 'scriptBottom'
]);

?>
