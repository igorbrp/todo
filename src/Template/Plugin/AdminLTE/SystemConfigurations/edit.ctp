<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SystemConfiguration $systemConfiguration
 */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	<?=__('Configurações do sistema')?> <!-- TRADUZIR -->
	<small>&nbsp;<?='';// __('Edit'); ?></small>
    </h1>
    <p>&nbsp;</p>    <?='';//$this->element('MenuHeader')?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->

            <!-- /.box-header -->
            <!-- form start -->
            <?php echo $this->Form->create($systemConfiguration, ['role' => 'form', 'onsubmit' => 'cleanDivFolders();']); ?>
            <div class="nav-tabs-custom">
	        <ul class="nav nav-tabs">
	            <li class="active">
                        <a href="#identity" data-toggle="tab" aria-expanded="true"><?=__('Empresa')?></a> <!-- TRADUZIR -->
                    </li>
	            <li class="">
                        <a href="#address" data-toggle="tab" aria-expanded="false"><?=__('Address')?></a>
                    </li>
	            <li class="">
                        <a href="#properties" data-toggle="tab" aria-expanded="false"><?=__('Configurações')?></a>
                    </li>
	            <li class="">
                        <a href="#emails" data-toggle="tab" aria-expanded="false"><?=__('Emails')?></a>
                    </li>
	            <li class="">
                        <a href="#deliveries" data-toggle="tab" aria-expanded="false"><?=__('Entregas')?></a>
                    </li>
	        </ul>

	        <div class="tab-content">
	            <div class="tab-pane active" id="identity">

                        <?=$this->Form->control('configuration.name');?>
                        <div class="row">
                            <div class="col-md-3">
                                <?=$this->Form->control('configuration.cnpj', [
                                    'templateVars' => [
                                        'otherClass' => 'cnpj',
                                    ],
                                ]);?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <?=$this->Form->control('configuration.phone', [
                                    'templateVars' => [
                                        'otherClass' => 'phone',
                                    ],
                                ]);?>
                            </div>

                            <div class="col-md-3">
                                <?=$this->Form->control('configuration.phone2', [
                                    'templateVars' => [
                                        'otherClass' => 'phone',
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-3">
                                <?=$this->Form->control('configuration.email', [
                                    'type' => 'text',
                                    'templateVars' => [
                                        'otherClass' => 'email',
                                    ],
                                ]);?>
                            </div>
                        </div>
                    </div>

	            <div class="tab-pane " id="address">
                        <?=$this->Element('Address', ['type' => 'configuration']);?>
                    </div>

	            <div class="tab-pane " id="properties">
                        <div class="row">
                            <div class="col-md-12">
                                <?=$this->Form->control('configuration.deadline', ['type' => 'number']);?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <br>
                                <?=$this->Form->control('configuration.automatic_prefix_on_sku', [
                                    'type' => 'checkbox',
                                    'label' => '&nbsp;&nbsp<b>Prefixo automático ao gerar sku</b>',
                                    'escape' => false
                                ]);?>
                            </div>
                        </div>
                    </div>

	            <div class="tab-pane " id="emails">
                        <h4>Email de requisição</h4>
			<h1><?php echo $authenticators?></h1>
                        <div class="row">
                            <div class="col-md-3">
                                <?=$this->Form->control('configuration.imap_email', ['type' => 'text', 'label' => 'Email']);?>
                            </div>
                            <div class="col-md-3">
                                <?=$this->Form->control('configuration.imap_user', [
                                    'type' => 'text',
                                    'label' => 'Usuário',
                                ]);?>
                            </div>
                            <div class="col-md-3">
                                <?=$this->Form->control('configuration.imap_password', [
                                    'type' => 'password',
                                    'label' => 'Senha',
                                ]);?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <?=$this->Form->control('configuration.smtp_server', ['type' => 'text', 'label' => 'Servidor de saída']);?>
                            </div>
                            <div class="col-md-3">
                                <?=$this->Form->control('configuration.smtp_port', ['type' => 'number', 'label' => 'Porta']);?>
                            </div>
                            <div class="col-md-3">
                                <br>
                                <?=$this->Form->control('configuration.smtp_layer', [
                                    'type' => 'checkbox',
                                    'label' => '&nbsp;&nbsp<b>SSL</b> (saída)',
                                    'escape' => false
                                ]);?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <?=$this->Form->control('configuration.imap_server', ['type' => 'text', 'label' => 'Servidor de entrada']);?>
                            </div>
                            <div class="col-md-3">
                                <?=$this->Form->control('configuration.imap_port', ['type' => 'number', 'label' => 'Porta']);?>
                            </div>

                            <div class="col-md-3">
                                <br>
                                <?=$this->Form->control('configuration.imap_layer', [
                                    'type' => 'checkbox',
                                    'label' => '&nbsp;&nbsp<b>SSL</b> (entrada)',
                                    'escape' => false
                                ]);?>
                            </div>
                        </div>

			<div class="row">
                            <div class="col-md-3">
				<?=$this->Html->link('verificar configuração', '#', [
				    'onclick' => 'checkEmailConfiguration();']);
				?>
				&nbsp;
				<div id="waiting-check" class="overlay" style="display:inline">
				    &nbsp;
				</div>				
			    </div>
                            <div class="col-md-3">
			    </div>
                            <div class="col-md-3">
				<div class="test-send" style="display:none">
				    <?=$this->Html->link('testar envio', '#', [
					'onclick' => 'testShipping();']);
				    ?>
				    &nbsp;
				    <div id="waiting-shipping" class="overlay" style="display:inline">
					&nbsp;
				    </div>
				</div>
			    </div>				
			</div>
			<p></p>			    
			<div class="row">
			    <div class="col-md-3">
                                <?=$this->Form->control('configuration.sents_folder', ['type' => 'text', 'label' => 'Pasta de emails enviados', 'readonly']);?>
				<em class="help-block">
				    A pasta onde os emails enviados serão guardados. Caso este campo esteja vazio, clique em 'verificar configuração' para listar as pastas disponíveis no servidor e selecione uma delas.
				</em>
			    </div>				
			    <div id="list-sents-folder" onclick="setListSentsFolder();" class="col-md-3">
			    </div>
			    <div class="col-md-3">
				<div class="test-send" style="display:none">
                                    <?=$this->Form->control('test-target-email', [
					'type' => 'text',
					//					'value' => Configure::system
					'label' => 'Email para teste',
					'templateVars' => [
                                            'otherClass' => 'email',
					],
				    ]);?>
				</div>
			    </div>
			    <div class="col-md-3">
			    </div>
			</div>
                    </div>
	            <div class="tab-pane " id="deliveries">
			<div class="col-md-3">
			    <?=$this->Form->control('configuration.freight', [
				'type' => 'text',
				'label' => 'Valor do frete',
			    	'templateVars' => [
		                    'otherClass' => 'currency',
				]
			    ]);?>
			</div>
		    </div>
                </div>
                <div class="text-right">
                    <?php echo $this->Form->submit(__('Submit')); ?>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
        <!-- /.row -->
</section>

<?php echo $this->Form->create('check-configuration', ['id' => 'form-check-configuration', 'role' => 'form', 'style' => 'display:none', 'action' => '/check-configuration']); ?>
<?=$this->Form->control('check-imap-email');?>
<?=$this->Form->control('check-imap-user');?>
<?=$this->Form->control('check-imap-password');?>
<?=$this->Form->control('check-imap-server');?>
<?=$this->Form->control('check-imap-port');?>
<?=$this->Form->control('check-imap-layer');?>
<?='';//$this->Form->control('smtp-server');?>
<?='';//$this->Form->control('smtp-port');?>
<?='';//$this->Form->control('smtp-layer');?>
<?='';//$this->Form->control('target-email');?>
<?php echo $this->Form->end(); ?>

<?php echo $this->Form->create('test-shipping', ['id' => 'form-test-shipping', 'role' => 'form', 'style' => 'display:none', 'action' => '/test-shipping']); ?>
<?=$this->Form->control('test-imap-email');?>
<?=$this->Form->control('test-imap-user');?>
<?=$this->Form->control('test-imap-password');?>
<?=$this->Form->control('test-imap-server');?>
<?=$this->Form->control('test-imap-port');?>
<?=$this->Form->control('test-imap-layer');?>
<?=$this->Form->control('test-smtp-server');?>
<?=$this->Form->control('test-smtp-port');?>
<?=$this->Form->control('test-smtp-layer');?>
<?=$this->Form->control('test-sents-folder');?>
<?=$this->Form->control('target-email');?>
<?php echo $this->Form->end(); ?>

<?php
echo $this->Html->css([
    'AdminLTE./bower_components/select2/dist/css/select2.css'], [
        'block' => 'css'
]);
echo $this->Html->script([
    'AdminLTE./bower_components/select2/dist/js/select2.js',
    '/node_modules/inputmask/dist/jquery.inputmask.min',
    '/node_modules/inputmask/dist/bindings/inputmask.binding',
], [
    'block' => 'script'
]);
echo $this->Html->script([
    'apply-select2',
    'common_masks',
    'addresses',
    'js-base',
    'system-configurations',
], [
    'block' => 'scriptBottom'
]);
?>
