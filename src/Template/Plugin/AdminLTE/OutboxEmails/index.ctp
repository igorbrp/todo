<style>
 .user-mini {
     margin:auto 10px auto auto;
     width: 25px;
     height: 25px;
 }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Caixa de saída (emails)')?>
        <small>&nbsp;<?='';//__('List'); ?></small>
    </h1>
    <p>&nbsp;</p><?='';//$this->element('MenuHeader')?>
</section>

<!-- Main content -->
<?=''; /* $this->Html->link('test mail', [
    *  'action' => 'test-send-mail'
      ]) */?>
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
                        <?=$this->element('Search', ['optionSearch' => 'OutboxEmailsSearch', 'placeholder' => 'pesquise por número do pedido...'])?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col" class="text-center"><?= ''; // $this->Paginator->sort('sented', 'enviado') ?></th>
                                <th scope="col"><?= 'tipo' ?></th>
                                <th scope="col"><?= 'pedido' ?></th>
                                <th scope="col"><?= 'fornecedor' ?></th>
                                <th scope="col"><?= 'email' ?></th>
                                <th scope="col"><?= $this->Paginator->sort('modified', 'envio') ?></th>
                                <th scope="col">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($outboxEmails as $outboxEmail): ?>
                                <tr>
                                    <td>
					<?php if ($outboxEmail['sented']) : ?>
					    <span class="text-green" data-toggle="tooltip" data-placement="top" title="enviado"><i class="fa fa-check"></i></span>
					<?php else : ?>
					    <span class="text-red" data-toggle="tooltip" data-placement="top" title="não enviado"><i class="fa fa-ban"></i></span>
					<?php endif; ?>
				    </td>
				    
				    <td>
					<?php 
					if ($outboxEmail['controller'] == 'purchases') {
					    $icon = '<span class="text-green" data-toggle="tooltip" data-placement="top" title="" data-original-title="compra para o estoque"><i class="fa fa-bars"></i></span>';
					} else {
					    $icon = '<span class="text-yellow" data-toggle="tooltip" data-placement="top" title="" data-original-title="requisiçao de pedido"><i class="fa fa-calendar"></i></span>';
					}
					echo $icon;
					?>
				    </td>

                                    <td>
					<?php
					echo $this->Html->link($outboxEmail['refer'], [
					    'controller' => $outboxEmail['controller'],
					    'action' => 'edit',
					    $outboxEmail['refer_id'],
					]); 
					?>
				    </td>
                                    <td>
					<?=$outboxEmail['supply_name'];?>
				    </td>
                                    <td>
					<?=$outboxEmail['supply_email'];?>
				    </td>
                                    <td>
					<?=$outboxEmail['modified']->i18nFormat('dd/MM/yyyy');?>
				    </td>
                                    <td>
					<?= $this->Html->link($outboxEmail['sented'] ? '<i class="fa fa-repeat"></i>' : '<i class="fa fa-mail-forward"></i>', [
					    'action' => 'manualEmailSend',
					    $outboxEmail['id']
					], [
					    'data-toggle' => 'tooltip',
					    'data-placement' => 'top',
					    'title' => $outboxEmail['sented'] ? 'reenviar' : 'enviar',
					    'escape' => false,
					]); ?>
				    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
	<div class="col-sm-5">
	    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
		<?php echo $this->Paginator->counter(['format' => 'range']);?>
	    </div>
	</div>
	<div class="col-sm-7">
	    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
		<!-- Paginator -->
		<?=$this->element('Paginator');?>
	    </div>
	</div>
    </div>
</section>

<?php
echo $this->Html->script([
    'js-base',
    'index',
], [
    'block' => 'scriptBottom'
]);
?>
