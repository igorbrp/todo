<section class="content-header">
  <h1>
    Menu
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
      <?=$this->element('MenuHeader')?>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Name') ?></dt>
            <dd><?= h($menu->name) ?></dd>
            <dt scope="row"><?= __('Url') ?></dt>
            <dd><?= h($menu->url) ?></dd>
            <dt scope="row"><?= __('Icon') ?></dt>
            <dd><?= h($menu->icon) ?></dd>
            <dt scope="row"><?= __('Icon Type') ?></dt>
            <dd><?= h($menu->icon_type) ?></dd>
            <dt scope="row"><?= __('Title') ?></dt>
            <dd><?= h($menu->title) ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($menu->id) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($menu->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($menu->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
