<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Menus
        <small><?php echo __('View'); ?></small>
    </h1>
    <ol class="breadcrumb">
        <?=$this->element('MenuHeader', ['noprint' => true])?>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
                        <form action="<?php echo $this->Url->build(); ?>" method="POST">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="<?php echo __('Search'); ?>">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('url') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('icon') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('icon_type') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                                <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($menus as $menu): ?>
				<tr>
                                    <td><?= $this->Number->format($menu->id) ?></td>
                                    <td><?= h($menu->name) ?></td>
                                    <td><?= h($menu->url) ?></td>
                                    <td><?= h($menu->icon) ?></td>
                                    <td><?= h($menu->icon_type) ?></td>
                                    <td><?= h($menu->title) ?></td>
                                    <td><?= h($menu->created) ?></td>
                                    <td><?= h($menu->modified) ?></td>
                                    <td class="actions text-right">
					<?= $this->Html->link(__('View'), ['action' => 'view', $menu->id], ['class'=>'btn btn-info btn-xs']) ?>
					<?= $this->Html->link(__('Edit'), ['action' => 'edit', $menu->id], ['class'=>'btn btn-warning btn-xs']) ?>
					<?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $menu->id], ['confirm' => __('Are you sure you want to delete # {0}?', $menu->id), 'class'=>'btn btn-danger btn-xs']) ?>
                                    </td>
				</tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
	</div>
  </div>
</section>
