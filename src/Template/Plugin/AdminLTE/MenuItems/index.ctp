<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Menu Items
        <small><?php echo __('View'); ?></small>
    </h1>
    <ol class="breadcrumb">
        <?=$this->element('MenuHeader')?>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
                        <form action="<?php echo $this->Url->build(); ?>" method="POST">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="<?php echo __('Search'); ?>">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('menu_id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('title') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('controller') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('action') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('icon') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('icon_type') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                                <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($menuItems as $menuItem): ?>
                                <tr>
                                    <td><?= $this->Number->format($menuItem->id) ?></td>
                                    <td><?= $this->Number->format($menuItem->menu_id) ?></td>
                                    <td><?= h($menuItem->title) ?></td>
                                    <td><?= h($menuItem->controller) ?></td>
                                    <td><?= h($menuItem->action) ?></td>
                                    <td><?= h($menuItem->icon) ?></td>
                                    <td><?= h($menuItem->icon_type) ?></td>
                                    <td><?= h($menuItem->created) ?></td>
                                    <td><?= h($menuItem->modified) ?></td>
                                    <td class="actions text-right">
                                        <?=$this->element('Actions', ['cod_id' => $menuItem->id,])?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
