<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $product
 */
$template = $this->template == 'add' ? __('Add') : __('Edit');
if($this->template == 'edit') {
    $readonly = $product->is_stock ? 'readonly' : ''; 
} else {
    $readonly = '';
}
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	<?=__('Product')?>
	<small>
            <?php if(strtolower($this->request->getParam('action')) == 'edit') : ?>
                <?=$product->title;?>
            <?php else : ?>
                <?='novo'?> <!-- TRADUZIR -->
            <?php endif; ?>
        </small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-body">
                    <a href="#" id="bt-upload" >
                        <div id="target_img" class="text-center">
                            <?php if (is_null($product->file['hash'])): ?>
                                <?=$this->Html->image('no-image.jpg', ['alt' => __('Image not available.'), 'class' => 'profile-user-img img-responsive', 'id' => 'preview_image']); ?>
                            <?php else : ?>
                                <?=$this->Html->image($product->file['path'] . DS . $product->file['hash'] . '.media.' . $product->file['extension'], ['pathPrefix' => '', 'class' => 'profile-user-img img-responsive', 'id' => 'preview_image']);  ?>
                            <?php endif ?>
                        </div>
                    </a>

                    <h3 class="profile-username text-center">&nbsp;</h3>
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <b><?=__('products')?></b> <a class="pull-right">322</a>
                        </li>
                        <li class="list-group-item">
                            <b><?=__('lines')?></b> <a class="pull-right">8</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- /.col-md -->
        <div class="col-md-9">
            <!-- general form elements -->
            <?php echo $this->Form->create($product, ['enctype' => 'multipart/form-data', 'role' => 'form']); ?>
            <div class="nav-tabs-custom">

	        <ul class="nav nav-tabs">
	            <li class="active">
                        <a href="#identity" data-toggle="tab" aria-expanded="true"><?=__('Identification')?></a>
                    </li>
	            <li class="">
                        <a href="#properties" data-toggle="tab" aria-expanded="false"><?=__('Properties')?></a>
                    </li>
	        </ul>

	        <div class="tab-content">
	            <div class="tab-pane active" id="identity">
		        <!-- identity content -->
                        <div class="row">
                            <div class="col-md-9">
                                <?php echo $this->Form->control('title', ['onblur' => 'buildSku(this)']); ?>
                            </div>
                            <div class="col-md-3">
                                <?php
                                echo $this->Form->control('sku');
                                ?>
                            </div>
                        </div>
                        <?php
			if($readonly) {
			    echo $this->Form->control('derived_from', ['style' => 'display:none']);
			    echo $this->Form->textarea('complement', ['placeholder' => 'complemento']);
			}
                        echo $this->Form->control('description');
                        echo $this->Form->control('long_description');
                        ?>
                        <!-- <div class="form-group input text">
                             <label class="control-label" for="agent-id">Type</label> -->
                        <?php
                        echo ''; /*$this->Form->select('product_type_id', $productTypes, [
                                    'id' => 'product_type_id',
                                    'empty' => true,
                                    'class' => 'select2',
                                    'style' => 'width:100%'
                                    ]);*/
                        ?>
                        <!-- </div> -->
                        <div class="form-group input text">
                            <label class="control-label" for="supply-id"><?=__('Supply')?></label>
                            <?php
                            echo $this->Form->select('supply_id', $supplies, [
                                'id' => 'supply_id',
                                'empty' => true,
                                'class' => 'select2',
                                'style' => 'width:100%',
				$readonly,
                            ]);
                            // echo $this->Form->control('supply_id', ['options' => $supplies]);
                            ?>
                        </div>
                        <div class="form-group input text" style="display:none">
                            <label class="control-label" for="agent-id"><?=__('Line')?></label>
                            <?php
                            echo $this->Form->select('product_line_id', $productLines, [
                                'id' => 'product_line_id',
                                'empty' => true,
                                'allowClear' => true,
                                'class' => 'select2',
                                'style' => 'width:100%',
				$readonly,
                            ]);
                            ?>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <?php echo $this->Form->control('root_price', [
                                    'onblur' => 'changeRootPrice()',
                                    'type' => 'text',
	                            'templateVars' => [
		                        'otherClass' => 'currency',
	                        ]
]);?>
                            </div>
                            <div class="col-md-4 text-center">
                                <br>
                                <?php echo $this->Form->control('price_calculated', [
                                    'onclick' => 'toggleCalculated()',
                                ]); ?>
                            </div>
                            <div class="col-md-4">
                                <?php echo $this->Form->control('price', [
                                    'readonly' => $product->price_calculated ? true : false,
                                    'type' => 'text',
				    $readonly,
				    // tirei abaixo pq estava dando erro, multiplicando por 100000
	                            'templateVars' => [
					'otherClass' => 'currency',
				    ]
				]); ?>
                            </div>
                        </div>
			<div class="row">
			    <div class="col-md-12">
				<span style="display:none"><?php echo $this->Form->control('is_stock') ?></span>
				<?php if ($product->is_stock) : ?>
				    <span class="text-green"><b>PRODUTO DE ESTOQUE DERIVADO DE <?=$originalProduct->sku?> - <?=$originalProduct->title?></b></span>
				<?php endif; ?>
			    </div>
			</div>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="properties">
                        <?php
                        echo ''; //  $this->Form->control('deadline');
                        echo ''; //  $this->Form->control('working_days');
                        echo ''; //  $this->Form->control('deadline_inherited');
                        echo ''; //  $this->Form->control('maximum_discount');
                        echo ''; //  $this->Form->control('maximum_discount_inherited');
                        ?>
                    </div>
                </div>
                <!-- /.tab-content -->
                <div style="display:none;">
                    <?php echo $this->Form->file('file.submittedfile', ['id' => 'submittedfile', 'class' => 'upload']); // Pay attention here! ?>
                </div>

                <div class="text-right">
                    <?php echo $this->Form->submit(__('Submit')); ?>
                </div>
                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col-md -->
    </div>
    <!-- /.row -->
</section>
<?php
echo $this->Html->css([
    'AdminLTE./bower_components/select2/dist/css/select2.css'], [
        'block' => 'css'
]);
echo $this->Html->script([
    '/node_modules/blueimp-load-image/js/load-image.all.min',
    'AdminLTE./bower_components/select2/dist/js/select2.js',
    '/node_modules/inputmask/dist/jquery.inputmask.min',
    '/node_modules/inputmask/dist/bindings/inputmask.binding',
], [
    'block' => 'script'
]);
echo $this->Html->script([
    'load-image',
    'addresses',
    'apply-select2',
    'get-sku',
    'get-price',
    'common_masks',
], [
    'block' => 'scriptBottom'
]);
?>
<script>
 function buildSku(t)
 {
     if('<?=$readonly?>' == 'readonly')
	 return false;

     if (t.value.length == 0)
         return false;

     var targeturl = '/products/generate-sku?';
     var data = {title: t.value};
     getSku(targeturl, data, 'sku');
 }

 function toggleCalculated()
 {
     var chkb = document.getElementById('price-calculated');
     var priceValue = document.getElementById('price');
     var rootPrice = document.getElementById('root-price');
     var supplyId = document.getElementById('supply_id');
     var productLineId = document.getElementById('product_line_id');
     console.log('root price: ' + rootPrice.inputmask.unmaskedvalue().toString().replace(/\$|\,/g, '.'));
     if (chkb.checked) {
         if (supplyId.value == '')
             return;
         priceValue.setAttribute('readonly', true);
         var targeturl = '/products/get-ajax-price?';
         var data = {
             /* root_price: rootPrice.value, */
             root_price: rootPrice.inputmask.unmaskedvalue().toString().replace(/\$|\,/g, '.'),
             supply_id: supplyId.value,
             product_line_id: productLineId.value
         };
         getPrice(targeturl, data, 'price');
     } else {
         priceValue.removeAttribute('readonly');
         priceValue.value = '';
         priceValue.focus();
     }
 }

 function changeRootPrice()
 {
     var chkb = document.getElementById('price-calculated');
     var rootPrice = document.getElementById('root-price');


     //        if (chkb.checked && rootPrice.value.length > 0)
     if (chkb.checked && rootPrice.inputmask.unmaskedvalue().length > 0)
         toggleCalculated();
 }
</script>
