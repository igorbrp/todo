<style>
 .user-mini {
     margin:auto 10px auto auto;
     width: 25px;
     height: 25px;
 }
</style>

<?php
$sKey = $this->Paginator->sortKey();
if (!is_null($sKey)) {
    $sDir = $this->Paginator->sortDir() == 'asc' ? '<span style="color:#cccccc"><i class="fa fa-arrow-down"></i></span>' : '<span style="color:#cccccc"><i class="fa fa-arrow-up"></i></span>';
}
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Products')?>
        <small>&nbsp;</small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
                        <?=$this->element('Search', ['optionSearch' => 'ProductsSearch', 'placeholder' => 'pesquise por sku ou nome do produto'])?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col" width="5%"><label><input id="chk-all" type="checkbox" class="minimal" onchange="chkAllChange();"></label></th>
                                <th scope="col" width="5%">&nbsp;</th>
                                <th scope="col" width="10%"><?= $this->Paginator->sort('sku', ('sku' == $sKey ? $sDir : '') . ' sku' , ['escape' => false]) ?></th>
                                <th scope="col"><?= $this->Paginator->sort('title', ('title' == $sKey ? $sDir : '') . ' produto' , ['escape' => false]) ?></th>
				<th scope="col" width="10%" class="text-center"><?= $this->Paginator->sort('Stocks.qtd', ('Stocks.qtd' == $sKey ? $sDir : '') . ' estoque' , ['escape' => false])?></th>
                                <th scope="col"><?= $this->Paginator->sort('Supplies.name', ('Supplies.name' == $sKey ? $sDir : '') . ' fornecedor' , ['escape' => false]) ?></th>
                                <th scope="col" class="actions text-right"><?= $this->Paginator->sort('price', ('price' == $sKey ? $sDir : '') . ' preço' , ['escape' => false]) ?></th>
				<th scope="col" width="5%"></th>
                                <th scope="col" width="10%" class="actions text-right"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($products as $product): ?>
                                <tr>
                                    <td>
                                        <?=$this->Form->checkbox('id', [
                                            'id' => 'chk-b',
                                            'name' => 'chkb',
                                            'class' => 'chk-box',
                                            'value' => $product->id,
                                            'onclick' => 'checkOrUncheck(this)'
                                        ]);?>
                                    </td>
                                    <td>
                                        <span class="image" style="display:inline-block">
                                            <?php if (is_null($product->file['hash'])): ?>
                                                <?=$this->Html->image('no-image.jpg', ['class' => 'user-mini img-circle', 'style' => 'width:25px;height:25px']);?>
                                            <?php else: ?>
                                                <?=$this->Html->image($product->file['path'] . DS . $product->file['hash'] . '.mini.' . $product->file['extension'], ['pathPrefix' => '', 'class' => 'user-mini img-circle', 'style' => 'width:25px;height:25px;margin: auto 10px auto auto;']);  ?>
                                            <?php endif; ?>
                                        </span>
                                    </td>
				    <td><?= h($product->sku) ?></td>
                                    <td><?= h($product->title) ?><p><i><?=$product->complement?></i></p></td>
				    <?php
				    $stock = '';
				    if($product->is_stock) {
					if($product->has('stock')) {
					    $stock = '<span class="label bg-green">' . $product->stock->qtd . '</span>';
					} else {
					    $stock = '<span class="label bg-gray">0</span>';
					}
				    } 
				    ?>
				    
                                    <td class="text-center"><?=$stock?></td>

                                    <td><?= $product->has('supply') ? $this->Html->link($product->supply->name, ['controller' => 'Supplies', 'action' => 'view', $product->supply->id]) : '' ?></td>
                                    <!-- <td> --><?= ''; //$product->has('product_line') ? $this->Html->link($product->product_line->name, ['controller' => 'ProductLines', 'action' => 'view', $product->product_line->id]) : '' ?><!-- </td> -->
                                    <td class="actions text-right"><?= $this->Number->currency($product->price) ?></td>
				    <td class="text-right">
					
					<?php
					if ($this->AuthLink->isAuthorized($this->Url->build(['controller' => 'Products', 'action' => 'build-stock']))) {
					    if(!$product->is_stock) {
						echo $this->Html->link('<i class="fa fa-copy fa-lg text-info"></i>', [
						    'controller' => 'Products',
						    'action' => 'build-stock',
						    $product->id,
						], [
						    'data-toggle' => 'tooltip',
						    'data-placement' => 'top',
						    'data-original-title' => 'clonar para o estoque',
						    'escape' => false,
						]);
					    }
					}
					?>
				    </td>
                                    <td class="actions text-right">
                                        <?=$this->element('Actions', ['cod_id' => $product->id,])?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
	<div class="col-sm-5">
	    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
		<?php echo $this->Paginator->counter(['format' => 'range']);?>
	    </div>
	</div>
	<div class="col-sm-7">
	    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
		<!-- Paginator -->
		<?=
		$this->element('Paginator');
		?>
	    </div>
	</div>
    </div>
</section>
<?php
echo $this->Html->script([
    'js-base',
    'index',
], [
    'block' => 'scriptBottom'
]);
?>
