<section class="content-header">
    <h1>
	Produto
	<small><?php echo $product->title; ?></small>
    </h1>
    <ol class="breadcrumb">
	<?=$this->element('MenuHeader', ['noprint' => true])?>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
	<div class="col-md-12">
	    <div class="box box-solid">
		<div class="box-header with-border">
		    <i class="fa fa-info"></i>
		    <h3 class="box-title"><?php echo __('Information'); ?></h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		    <dl class="dl-horizontal">
			<dt scope="row"><?= __('Sku') ?></dt>
			<dd><?= h($product->sku) ?></dd>
			<dt scope="row"><?= __('Supply Sku') ?></dt>
			<dd><?= h($product->supply_sku) ?></dd>
			<dt scope="row"><?= __('Supply') ?></dt>
			<dd><?= $product->has('supply') ? $this->Html->link($product->supply->name, ['controller' => 'Supplies', 'action' => 'view', $product->supply->id]) : '' ?></dd>
			<dt scope="row"><?= __('Title') ?></dt>
			<dd><?= h($product->title) ?></dd>
			<dt scope="row"><?= __('Description') ?></dt>
			<dd><?= h($product->description) ?></dd>
			<dt scope="row"><?= __('Price') ?></dt>
			<dd><?= $this->Number->currency($product->price) ?></dd>
			<dt scope="row"><?= __('Deadline') ?></dt>
			<dd><?= $this->Number->format($product->deadline) ?></dd>
			<dt scope="row"><?= __('Maximum Discount') ?></dt>
			<dd><?= $this->Number->format($product->maximum_discount) ?></dd>
			<dt scope="row"><?= __('Price Calculated') ?></dt>
			<dd><?= $product->price_calculated ? __('Yes') : __('No'); ?></dd>
		    </dl>
		</div>
	    </div>
	</div>
    </div>

    <div class="row">
	<div class="col-md-12">
	    <div class="box box-solid">
		<div class="box-header with-border">
		    <i class="fa fa-text-width"></i>
		    <h3 class="box-title"><?= __('Long Description') ?></h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		    <?= $this->Text->autoParagraph($product->long_description); ?>
		</div>
	    </div>
	</div>
    </div>
</section>
