<?php
use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\Log\Log;
$colors = [
    'uncovered' => '#ff0000',
    'required' => '#f4c113',
    'received' => '#008000',
    'dispatched' => '#ff6680',
    'completed' => '#3333ff',
];

$lastHistory = $item_histories->toArray()[0];

$max = $lastHistory['item']['qtd'];

$actualizedTotals = $actualItem['total_uncovered'] . ',' . $actualItem['total_required'] . ',' . $actualItem['total_received'] . ',' . $actualItem['total_dispatched'] . ',' . $actualItem['total_completed'];

$options = $this->HtmlElement->getOptions($lastHistory['status']);
?>

<span id="actualized-totals-<?=$actualItem['id']?>" style="display:none"><?=$actualizedTotals?></span>
<div class="row panel-heading"> <!-- cabeçalho do histórico do item -->
    <div class="col-md-3"></div>
    <div class="col-md-2"><span id="graphic-big-<?=$actualItem['id']?>" class="piesparkline" style="font-size:80px;"></span></div>
    <div class="col-md-7">
        <?php
	/* troquei != 0 por > 0 para dar a conta certa nos selects */
	if($item_histories_status['total_uncovered'] > 0)
	    echo $this->HtmlElement->mk_dropdown_status('uncovered', $item_histories_status, $colors, $actualItem['origin']);
	if($item_histories_status['total_required'] > 0)
	    echo $this->HtmlElement->mk_dropdown_status('required', $item_histories_status, $colors, $actualItem['origin']);
	if($item_histories_status['total_received'] > 0)
	    echo $this->HtmlElement->mk_dropdown_status('received', $item_histories_status, $colors, $actualItem['origin']);
	if($item_histories_status['total_dispatched'] > 0)
	    echo $this->HtmlElement->mk_dropdown_status('dispatched', $item_histories_status, $colors, $actualItem['origin']);
	if($item_histories_status['total_completed'] > 0)
	    echo $this->HtmlElement->mk_dropdown_status('completed', $item_histories_status, $colors, $actualItem['origin']);
	?>
        <?php foreach ($item_histories as $itemHistory) : ?>
            <?php $timeAgo = new Time($itemHistory->created);?>
            <?php
            if($itemHistory->qtd < 0) {
                $tmpStatus = $this->HtmlElement->statusCancel($itemHistory->status);
            } else {
                $tmpStatus = $itemHistory->status;
            }
            ?>

	    <div class="row panel-heading">
                <label class="label" style="background-color:<?=$colors[$tmpStatus]?>"><?=$itemHistory->qtd?></label>&nbsp;
		<span style="display:inline-block">

		    &nbsp;
		    <?php
		    if (!isset($avatars[$itemHistory->user['id']])) {
			echo $this->Html->image('avatar_placeholder.png', ['class' => 'user-mini img-circle']);

		    } else {
			echo $this->Html->image($avatars[$itemHistory->user['id']], ['pathPrefix' => '', 'class' => 'user-mini img-circle']);
		    }
		    $dateCreated = $itemHistory->created->i18nFormat('dd/MM/yyyy');
		    $timeCreated = $itemHistory->created->i18nFormat('HH:mm');
		    ?>
		    &nbsp;<?=$itemHistory->user['nick_name']?>
		    &nbsp;<?=$itemHistory->qtd < 0 ? 'CANCELOU' : $this->HtmlElement->getTextStatus($tmpStatus);?>
		    &nbsp;em
		    &nbsp;<?=$dateCreated?>
		    &nbsp;às
		    &nbsp;<?=$timeCreated?>
		</span>
		<?php
		if ($tmpStatus == 'received' && $itemHistory->qtd > 0) {
		    if (is_null($itemHistory->invoice_id)) {
			$txtInvoice = 'sem nota';
		    } else {
			$txtInvoice = $itemHistory['invoice']['invoice_number'];
		    }
		    echo $this->Html->link($txtInvoice, '#', ['onclick' => 'insertInvoice(' . $itemHistory->id . ', ' . $itemHistory->item_id . ', $(this));', 'class' => 'pull-right']);
		}
		?>
        <?php endforeach; ?>
	</div>
    </div>
</div>

