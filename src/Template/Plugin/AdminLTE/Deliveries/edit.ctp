<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Delivery $delivery
 */
$template = $this->template == 'add' ? __('Add') : __('Edit');
$horary = ['morning' => 'manhã', 'afternoon' => 'tarde', 'any' => 'comercial'];
?>
<!-- Content Header (Page header) -->

<section class="content-header">
    <h1>
	<?=__('Delivery')?>
	<small>
	    pedido <?=$delivery->order->number?> em <?=h($delivery->order->date_order->i18nFormat());?> 
	</small>
    </h1>
    <ol class="list-inline text-right">
	<li>
	    <?=$this->Html->link('<i class="fa fa-mail-reply"></i>', [
                'controller' => 'deliveries',
                'action' => 'index'], [
                    'style' => 'color:#fff',
                    'class' => 'btn btn-primary btn-sm',
                    'data-toggle' => 'tooltip',
                    'data-placement' => 'top',
                    'title' => 'voltar à lista',
                    'escape' => false
            ])?>
	</li>
	<li>
	    <?=$this->Html->link('<i class="fa fa-print"></i>', [
		'action' => 'romance',
		'controller' => 'deliveries',
		$delivery->order->id
	    ], [
		'style' => 'color:#0000ff',
                'class' => 'btn btn-default btn-sm',
		'data-toggle' => 'tooltip',
		'data-original-title' => __('romaneio'),
		'target' => '_blank',
		'escape' => false,
	    ]);?>
	</li>
    </ol>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <span><?=__('Identification'); ?></span>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create($delivery, ['role' => 'form']); ?>
                <div class="box-body">

		    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group input text">
				<br>
				<?php if ($delivery->working_days) : ?>
				    <b>dias úteis</b>
				<?php else : ?>
				    <b>dias corridos</b>
				<?php endif ?>
			    </div>
			</div>

                        <div class="col-md-3">
                            <div class="form-group input text">
				<?=$this->Form->control('foreseen_days', ['label' => 'dias previstos', 'readonly']);?>
			    </div>
			</div>

                        <div class="col-md-3">
                            <div class="form-group input text">
				<?=$this->Form->control('foreseen_date', [
                                    'type' => 'text',
				    'label' => 'data prevista',
	                            /* 'data-inputmask-alias' => 'datetime',
	                               'data-inputmask-inputformat' => 'dd/mm/yyyy',
	                               'templateVars' => [
		                       'otherClass' => 'datepicker date',
	                               ], */
				    'readonly'
                                ]);?>
			    </div>
			</div>

                        <div class="col-md-3">
                            <div class="form-group input text">
				<?=$this->Form->control('foreseen_horary', ['label' => 'turno previsto', 'value' => $horary[$delivery->foreseen_horary], 'readonly']);?>
			    </div>
			</div>
		    </div>

		    <div class="row">
                        <div class="col-md-12">
			    <div class="table-responsive no-padding">
				<table class="table table-hover">
				    <thead>
					<tr>
					    <th scope="col">&nbsp;</th>
					    <th scope="col"><?= 'qtd' ?></th>
					    <th scope="col"><?= 'data agendada' ?></th>
					    <th scope="col"><?= 'turno agendado' ?></th>
					    <th scope="col"><?= 'data da entrega' ?></th>
					    <!-- <th scope="col"><?= 'turno da entrega' ?></th> -->
					    <th scope="col"><?= 'veículo' ?></th>
					</tr>
				    </thead>
				    <tbody>
					<?php foreach ($delivery->delivery_mades as $deliveryMade): ?>
					    <!-- <tr id="tr- --><?='';//$deliveryMade->scheduled_date->i18nFormat('yyy-MM-dd') . '-' . $delivery->id . '-' . $deliveryMade->scheduled_horary . '-' . $deliveryMade->vehicle_id?><!-- "> -->
					    <tr>
						<td>
						    <?php
 						    if (isset($deliveryMade->delivery_date)) {
							$color = '#3875d7';
						    } else {
							$color = '#ff6680';
						    }
						    ?>
						    
						    <?=
						    $this->Html->link('<i class="fa fa-truck"></i>', [
							'controller' => 'orders',
							'action' => 'index',
							'?' => ['q' => $delivery->order->number]
						    ], [
							'data-toggle' => 'tooltip',
							'data-placement' => 'top',
							'title' => 'gerenciar itens',
							'style' => 'color:' . $color,
							'target' =>'_blank',
							'escape' => false,
						    ]);
						    ?>
						    
						</td>
						<td><?= h($deliveryMade->sum_qtd) ?></td>
						<td>

						    <?php if (isset($deliveryMade->scheduled_date)) {
							echo $this->Form->input('deliveryMade.scheduled_date', [
							    'type' => 'text',
							    'label' => false,
							    'data-inputmask-alias' => 'datetime',
							    'data-inputmask-inputformat' => 'dd/mm/yyyy',
							    'value' => $deliveryMade->scheduled_date,
							    'data-original-value' => $deliveryMade->scheduled_date,
							    'data-delivery-id' => $delivery->id,
							    'templateVars' => [
								'otherClass' => 'datepicker date pull-right scheduled-date',
							    ],
							    /* 'onchange' => 'changeScheduledDate(' . $deliveryMade->id . ', this.value)', */
							]);
						    }?>


						</td>
						<td>
						    <?=$this->Form->select('deliveryMade.scheduled_horary', [
							'morning' => 'manhã',
							'afternoon' => 'tarde',
							'any' => 'comercial',
						    ], [
							'class' => 'select2 scheduled-horary',
							'data-original-value' => $deliveryMade->scheduled_horary,
							'data-delivery-id' => $delivery->id,
							'value' => $deliveryMade->scheduled_horary,
							'style' => 'width: 100%']);?></td>
						<td><?= h($deliveryMade->delivery_date) ?></td>
						<!-- <td> --><?='';// h($deliveryMade->delivery_horary) ?><!-- </td> -->
						<td>

						    <?=$this->Form->select('deliveryMade.vehicle_id', $vehicles, [
							'empty' => 'escolha um veículo',
							'class' => 'select2 vehicle',
							'required' => false,
							'style' => 'width: 100%',
							'value' => $deliveryMade->vehicle_id,
							'data-original-value' => $deliveryMade->vehicle_id,
						    ]);
						    ?>

						</td>
					    </tr>
					<?php endforeach; ?>
				    </tbody>
				</table>
			    </div>
			</div>
		    </div>

		</div>
	    </div>
	</div>
	<!-- /.box-body -->
	<div class="text-right">
	    <?php echo $this->Form->submit(__('Submit')); ?>
	</div>
	
	<?php echo $this->Form->end(); ?>

    </div>
    <!-- /.box -->
    </div>
    </div>
    <!-- /.row -->
</section>
<?php
echo $this->Html->css([
    'AdminLTE./bower_components/select2/dist/css/select2.css',
    'AdminLTE./bower_components/bootstrap-daterangepicker/daterangepicker.css',
    'AdminLTE./bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
], [
    'block' => 'css'
]);
echo $this->Html->script([
    'AdminLTE./bower_components/select2/dist/js/select2.js',
    'AdminLTE./bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min',
    'AdminLTE./bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min',
    /* '/node_modules/inputmask/dist/jquery.inputmask.min',
     * '/node_modules/inputmask/dist/bindings/inputmask.binding', */
], [
    'block' => 'script'
]);
echo $this->Html->script([
    'js-base',
    'deliveries',
    // 'addresses',
    'apply-select2',
], [
    'block' => 'scriptBottom'
]);
?>
