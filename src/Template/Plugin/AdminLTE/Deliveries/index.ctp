<?php
$colors = [
    'uncovered' => '#ff0000',
    'required' => '#f4c113',
    'received' => '#008000',
    'dispatched' => '#ff6680',
    'completed' => '#3333ff',
];

$sKey = $this->Paginator->sortKey();
if (!is_null($sKey)) {
    $sDir = $this->Paginator->sortDir() == 'asc' ? '<span style="color:#cccccc"><i class="fa fa-arrow-down"></i></span>' : '<span style="color:#cccccc"><i class="fa fa-arrow-up"></i></span>';
}
?>

<style>
 .user-mini {
     margin:auto 10px auto auto;
     width: 25px;
     height: 25px;
 }
</style>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Deliveries')?>
        <small>&nbsp;</small>
    </h1>
    <?=$this->element('MenuHeader', ['nodelete' => true, 'noadd' => true])?>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <?=$this->element('Search', ['optionSearch' => 'DeliveriesSearch', 'placeholder' => 'pesquise por número do pedido, nome do cliente ou bairro'])?>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col" width="10%"><?= 'pedido' ?></th>
                                <th scope="col"><?= $this->Paginator->sort('Customers.name', ('Customers.name' == $sKey ? $sDir : '') . ' cliente' , ['escape' => false])?></th>
                                <th scope="col"><?= $this->Paginator->sort('Addresses.neighborhood', ('Addresses.neighborhood' == $sKey ? $sDir : '') . ' bairro' , ['escape' => false]) ?></th>
                                <th scope="col" width="10%"><?= $this->Paginator->sort('foreseen_date', ('foreseen_date' == $sKey ? $sDir : '') . ' data prevista' , ['escape' => false]) ?></th>
                                <th scope="col" width="10%"><?= $this->Paginator->sort('DeliveryMades.scheduled_date', ('DeliveryMades.scheduled_date' == $sKey ? $sDir : '') . ' data agendada' , ['escape' => false]) ?></th>
                                <th scope="col" width="10%"><?= $this->Paginator->sort('delivery_date', ('delivery_date' == $sKey ? $sDir : '') . ' data da entrega' , ['escape' => false]) ?></th>
                                <th scope="col"><?= $this->Paginator->sort('Vehicles.identification', ('Vehicles.identification' == $sKey ? $sDir : '') . ' veículo' , ['escape' => false]) ?></th>
                                <th scope="col">&nbsp;</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php
                            $turnos = [
                                'morning' => __('manhã'),
                                'afternoon' => __('tarde'),
                                'any' => __('horário comercial'),
                            ];
                            ?>
                            <?php foreach ($deliveries as $delivery): ?>
                                <tr>
                                    <td><?= $delivery->has('order') ? $this->Html->link($delivery->order->number, ['controller' => 'Orders', 'action' => 'view', $delivery->order->id]) : '' ?></td>
                                    <td><?= h($delivery->order->customer->name) ?></td>
                                    <td><?= h($delivery->order->customer->address->neighborhood) ?></td>
                                    <td><?= h($delivery->foreseen_date) ?></td>
                                    <td>&nbsp;</td> <!-- scheduled_date -->
                                    <td>&nbsp;</td> <!-- delivery_date -->
                                    <td>&nbsp;</td> <!-- vehicle -->
				    <td>
					<ol class="list-inline text-right">
					    <li>
						<?=$this->Html->link('<i class="fa fa-edit fa-lg text-yellow"></i>', [
						    'controller' => 'deliveries',
						    'action' => 'edit',
						    $delivery->id
						], [
						    'escape' => false,
						    /* 'class'=>'btn btn-warning btn-xs', */
						    'data-toggle' => 'tooltip',
						    'data-original-title' => __('edit')
						]);?>
					    </li>
					</ol>
				    </td>
                                </tr>
                                <?php if (!empty($delivery->delivery_mades)) : ?>
                                    <?php foreach ($delivery->delivery_mades as $delivery_made) : ?>
                                        <tr>
                                            <td>&nbsp;</td> <!-- order number -->
                                            <td>&nbsp;</td> <!-- customer name -->
                                            <td>&nbsp;</td> <!-- neighborhood -->
					    <td class="text-center">
						<?php $truckIconColor = isset($delivery_made->delivery_date) ? '#3875d7' : '#ff6680'; ?>
						<span style="color:<?=$truckIconColor?>;"><i class="fa fa-truck"></i></span>
					    </td> <!-- foreseen_date -->
						<td>
						    <?=h($delivery_made->scheduled_date)?>
						    <?php
						    /* if (isset($delivery_made->scheduled_date)) {
						       echo $this->Form->input('changescheduled-' . $delivery_made->id, [
						       'label' => false,
						       'data-inputmask-alias' => 'datetime',
						       'data-inputmask-inputformat' => 'dd/mm/yyyy',
						       'value' => $delivery_made->scheduled_date,
						       'data-original-value' => $delivery_made->scheduled_date,
						       'templateVars' => [
						       'otherClass' => 'datepicker date pull-right',
						       ],
						       'onchange' => 'changeScheduledDate(' . $delivery_made->id . ', this.value)',
						       ]);
						       } */
						    echo ''; //$this->Form->end();
						    ?>
						
					    </td>
                                            <td><?= h(isset($delivery_made->delivery_date) ? $delivery_made->delivery_date : '') ?></td>
                                            <td>
						<?=h($vehicles->toArray()[$delivery_made->vehicle_id])?>
						<?php
						/* echo $this->Form->select('select_vehicle', $vehicles, [
						   'id' => 'select-vehicle',
						   // 'empty' => 'escolha um veículo',
						   'class' => 'select2',
						   'required' => false,
						   'style' => 'width: 100%',
						   //						      'label' => 'veículo',
						   'value' => $delivery_made->vehicle_id,
						   'data-original-value' => $delivery_made->vehicle_id,
						   'onchange' => 'changeVehicle(' . $delivery_made->id . ', this.value)',
						   ]); */
						?>
						
					    </td>
                                        </tr>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </tbody>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
	<div class="col-sm-5">
	    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
		<?php echo $this->Paginator->counter(['format' => 'range']);?>
	    </div>
	</div>
	<div class="col-sm-7">
	    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
		<!-- Paginator -->
		<?=
		$this->element('Paginator');
		?>
	    </div>
	</div>
    </div>
</section>

<?php
echo $this->Html->script([
    'deliveries',
    'index',
    'js-base',
], [
    'block' => 'scriptBottom'
]);
?>

<?php
function getTotalStatusItem($items)
{
    $total_item = [
        'uncovered' => 0,
        'required' => 0,
        'received' => 0,
        'dispatched' => 0,
        'completed' => 0,
    ];

    foreach($items as $item) {
        $total_item['uncovered'] += $item['total_uncovered'];
        $total_item['required'] += $item['total_required'];
        $total_item['received'] += $item['total_received'];
        $total_item['dispatched'] += $item['total_dispatched'];
        $total_item['completed'] += $item['total_completed'];
    }

    return $total_item;
}
?>
