<section class="content-header">
  <h1>
    Delivery
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
      <?=$this->element('MenuHeader')?>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Order') ?></dt>
            <dd><?= $delivery->has('order') ? $this->Html->link($delivery->order->id, ['controller' => 'Orders', 'action' => 'view', $delivery->order->id]) : '' ?></dd>
            <dt scope="row"><?= __('Foreseen Horary') ?></dt>
            <dd><?= h($delivery->foreseen_horary) ?></dd>
            <dt scope="row"><?= __('Scheduled Horary') ?></dt>
            <dd><?= h($delivery->scheduled_horary) ?></dd>
            <dt scope="row"><?= __('Delivery Horary') ?></dt>
            <dd><?= h($delivery->delivery_horary) ?></dd>
            <dt scope="row"><?= __('Address') ?></dt>
            <dd><?= $delivery->has('address') ? $this->Html->link($delivery->address->id, ['controller' => 'Addresses', 'action' => 'view', $delivery->address->id]) : '' ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($delivery->id) ?></dd>
            <dt scope="row"><?= __('Foreseen Date') ?></dt>
            <dd><?= h($delivery->foreseen_date) ?></dd>
            <dt scope="row"><?= __('Scheduled Date') ?></dt>
            <dd><?= h($delivery->scheduled_date) ?></dd>
            <dt scope="row"><?= __('Delivery Date') ?></dt>
            <dd><?= h($delivery->delivery_date) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($delivery->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($delivery->modified) ?></dd>
            <dt scope="row"><?= __('Same Customer Address') ?></dt>
            <dd><?= $delivery->same_customer_address ? __('Yes') : __('No'); ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
