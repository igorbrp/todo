<?php
use Cake\I18n\Date;
$today = new Date();
$this->layout = 'AdminLTE.print';
?>

<section class="content">
    <div>
	<h3>
	    Roteiro
	    <div class="pull-right"><?=$today->i18nFormat('dd/MM/yyyy')?></div>
	</h3>
    </div>
    <table width="100%">
	<thead style="color:#808080;">
	    <th scope="col" width=10%">
		pedido
	    </th>
	    <th scope="col">
		cliente
	    </th>
	    <th scope="col">
		endereço
	    </th>
	    <th scope="col">
		bairro
	    </th>
	</thead>
	<tfoot>
	    <tr>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	    </tr>
	</tfoot>
	<tbody>
	    <?php foreach ($deliveries as $delivery) : ?>
		<tr>
		    <td valign="top">
			<?=$delivery->order->number?>
		    </td>
		    <td valign="top">
			<?=$delivery->order->customer->name?>
		    </td>
		    <td valign="top">
			<?=$delivery->address->thoroughfare?>, <?=$delivery->address->number?>
		    </td>
		    <td valign="top">
			<?=$delivery->address->neighborhood?>
		    </td>
		</tr>
	    <?php endforeach; ?>
	</tbody>
    </table>
</section>
