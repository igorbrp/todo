<?php
if (isset($type)) {
    $cakeType = $type . '.';
    $jsType = $type . '-';
} else {
    $jsType = $cakeType = '';
}
?>
<div class="row">
    <div class="col-md-2">
        <?php
        echo $this->Form->control($cakeType . 'address.postal_code', [
	    'maxlength' => '9',
	    'templateVars' => [
		'otherClass' => 'cep check_address',
	    ],
        ]);?>
    </div>
    <div class="col-md-2">
        <br>
        <?php
        echo $this->Html->link(__('find address'), '#', [
            'class' => 'btn btn-default btn-sm lnk-find-address',
            'param_type' => $jsType,
        ]);
        ?>
    </div>
    <div class="col-md-4 text-right">
        <br>
        <span id="<?=$jsType?>msg-find-address">
        </span>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group input text">
            <label class="control-label" for="address-state-id"><?=__('State')?></label>
            <?php
            echo $this->Form->select($cakeType . 'address.state_id', $states, [
                'id' => $jsType . 'address-state_id', // importante: jsType pq o id nao e gerado pelo cake
                'empty' => __('choose...'),
                'class' => 'select2 address_state_id',
		'required' => false,
                'style' => 'width: 100%',
                'param_type' => $jsType
            ]);
            ?>
        </div>
    </div>

    <div class="col-md-6">
        <div class="form-group input text">
            <label class="control-label" for="address-city-id"><?=__('City')?></label>
            <?php
            echo $this->Form->select($cakeType . 'address.city_id', $cities, [
                'id' => $jsType . 'address-city_id', // importante: jsType pq o id nao e gerado pelo cake
                'empty' => __('choose...'),
                'class' => 'select2',
                'style' => 'width: 100%',
		'required' => false,
                'param_type' => $jsType
            ]);
            ?>
        </div>
    </div>

</div>
<div class="row">
    <div class="col-md-6">
        <?php echo $this->Form->control($cakeType . 'address.neighborhood', ['class' => 'form-control check_address', 'required' => false]); ?>
    </div>
    <div class="col-md-6">
        <?php echo $this->Form->control($cakeType . 'address.thoroughfare', ['class' => 'form-control check_address', 'required' => false]); ?>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <?php echo $this->Form->control($cakeType . 'address.number'); ?>
    </div>
    <div class="col-md-8">
        <?php echo $this->Form->control($cakeType . 'address.complement');?>
    </div>
</div>
<?=$this->Form->control($cakeType . 'address.reference');?>
<!-- <div style="display:block;"> -->
<?=''; //$this->Form->control($cakeType . 'address.valid_address', [/*'id' => 'address-valid_address', */'type' => 'text']);?>
<!-- </div> -->

<?=$this->Html->script(['addresses'], ['block' => 'scriptBottom']);?>
