<?php
use Cake\Utility\Inflector;
$q = isset($q) ? $q : '';
$url = '/' . Inflector::dasherize($this->request->getParam('controller'));
?>
<div class="row">
    <div class="col-md-12">
        <ol class="list-inline text-right">
            <li id="totalOrder"></li>
        </ol>
    </div>
</div>
<div class="box-body table-responsive no-padding">
    <table class="table table-hover">
        <thead>
            <tr>
                <th scope="col" width="5%">&nbsp;</th>
                <th scope="col" width="10%"><?=__('qtd')?></th>
                <th scope="col"><?=__('product')?></th>
                <th scope="col" width="20%"><?=__('value')?></th>
                <th scope="col" class="text-right" width="15%"><?=__('actions')?></th>
            </tr>
            <tr>
                <!-- hidden inputs -->
                <?=''; //$this->Form->control('inputPriceUnmasked', ['id' => 'inputPriceUnmasked', /*'type' => 'hidden'*/])?>
                <?=''; //$this->Form->control('inputUnitPriceUnmasked', ['id' => 'inputUnitPriceUnmasked', /*'type' => 'hidden'*/])?>
                <!-- end hidden inputs -->
		<td></td>
                <td>
                    <?=$this->Form->control('inputQtd', [
                        'id' => 'inputQtd',
                        'type' => 'number',
                        'class' => 'form-control',
                        'value' => '1',
                        'min' => '1',
                        'label' => false,
                        'onchange' => 'changedQtd()'
                    ])?>
                </td>
                <td>
                    <?=$this->Form->control('inputTitle', [
                        'id' => 'inputTitle',
                        'name' => 'q',
                        'class' => 'js-data-example-ajax form-control',
                        'label' => false,
                        'style' => 'width:100%'
                    ])?>
		    <?=$this->Form->textarea('inputComplement', [
			'id' => 'inputComplement',
			'class' => 'form-control',
			'label' => false,
			'rows' => 3,
			'maxlength' => 1024,
		    ]);?>
                </td>
                <td>
                    <?=$this->Form->control('inputPrice', [
                        'id' => 'inputPrice',
                        'class' => 'form-control',
                        'label' => false,
                        'type' => 'text',
                        /* 'onblur' => 'changedPrice()', */
                        'onchange' => 'changedPrice()',
	                'templateVars' => [
		            'otherClass' => 'currency',
	            ]])?>
                </td>
                <td class="text-right">
                    <div class="form-group">
                        <?=$this->Html->link('<i class="fa fa-plus"></i>', '#', ['id' => 'bt-add', 'class' => 'btn btn-info', 'onclick' => 'addItem();', 'escape' => false])?>
                        <?=$this->Html->link('<i class="fa fa-ban"></i>', '#', ['id' => 'bt-cancel', 'class' => 'btn btn-danger', 'onclick' => 'cancelEditItem();'/*, 'disabled' => true*/, 'escape' => false])?>
                    </div>
                </td>
            </tr>
	    <!-- 
		 <tr>
		 <td>&nbsp;</td> -->
	    <!-- <td> --><?='';//$this->Form->textarea('inputComplement', [
			 /* 'id' => 'inputComplement',
			    'class' => 'form-control',
			    'label' => false,
			    'rows' => 3,
			    'maxlength' => 1024,
			    ]); */?><!-- </td> -->
	    <!-- <td></td>
		 <td></td>
		 </tr> -->
	    
        </thead>
        <tbody id="tbody_items">
            <?php $i = 0; ?>
            <?php if (!empty($order->items)) : ?>
                <?php foreach ($order->items as $items) : ?>
                    <tr id="items-<?=$i?>" style="">
                        <div style="display:none"><!-- hidden files -->
                            <?=$this->Form->control('items.' . $i . '.id', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.product_id', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.sku', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.supply_name', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.supply_id', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.supply_code', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.supply_line_id', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.status', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.total_uncovered', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.total_required', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.total_received', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.total_dispatched', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.total_completed', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.deadline', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.working_days', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.unit_original_price', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.minimum_unitary_price', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.qtd', ['required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.title', ['required' => false, 'label' => false])?>
			    <?=$this->Form->textarea('items.' . $i . '.complement', ['id' => 'items-' . $i . '-complement', 'required' => false])?>
                            <?=$this->Form->control('items.' . $i . '.price', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('items.' . $i . '.origin', ['type' => 'text', 'required' => false, 'label' => false])?>
                        </div><!-- end hidden files -->
			<td> <!-- sparkline -->
			    <p>
				<span id="graphic-<?=$i?>" class="piesparkline">
				    ...
				</span>
			    </p>
			    <span id="totals-<?=$i?>" class="totals-item" style="display:none"><?=$order->items[$i]->total_uncovered?>,<?=$order->items[$i]->total_required?>,<?=$order->items[$i]->total_received?>,<?=$order->items[$i]->total_dispatched?>,<?=$order->items[$i]->total_completed?></span>
			</td>
                        <td> <!-- qtd show -->
			    <p>
				<span id="qtd-<?=$i?>-show">
				    <?=$order->items[$i]['qtd']?>
				</span>
			    </p>
			</td>
                        <td> <!-- title & complement show -->
			    <p>
				<span id="title-<?=$i?>-show">
				    <?=$order->items[$i]['title']?>
				</span>
			    </p>
			    <p>
				<i>
				    <span id="complement-<?=$i?>-show">
					<?=$order->items[$i]['complement']?>
				    </span>
				</i>
			    </p>
			</td>
                        <td class="text-right"> <!-- price show -->
			    <p>
				<span id="price-<?=$i?>-show">
				    <?=$this->Number->currency($order->items[$i]['price'])?>
				</span>
			    </p>
			</td>
                        <td class="text-right"> <!-- buttons -->
                            <div class="form-group">
                                <?=$this->Html->link('<i class="fa fa-edit"></i>', '#', ['id' => 'bt-edit-' . $i, 'class' => 'btn btn-warning', 'onclick' => 'editItem(' . $i . ');', 'escape' => false])?>
                                <?=$this->Html->link('<i class="fa fa-trash"></i>', '#', ['id' => 'bt-delete-' . $i,'class' => 'btn btn-danger', 'onclick' => 'deleteItem(' . $i . ');', 'escape' => false])?>
                            </div>
                        </td>
                    </tr>
		    <?php $i++; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php for (; $i < 20; $i++) : ?>
                <tr id="items-<?=$i?>" style="display:none">
                    <div style="display:none"><!-- hidden files -->
                        <?=$this->Form->control('items.' . $i . '.id', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.product_id', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.sku', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.supply_name', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.supply_id', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.supply_code', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.supply_line_id', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.status', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.total_uncovered', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.total_required', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.total_received', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.total_dispatched', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.total_completed', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.deadline', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.working_days', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.unit_original_price', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.minimum_unitary_price', ['type' => 'text', 'required' => false, 'label' => false])?>
			<?=$this->Form->control('items.' . $i . '.qtd', ['required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.title', ['required' => false, 'label' => false])?>
			<?=$this->Form->textarea('items.' . $i . '.complement', ['id' => 'items-' . $i . '-complement', 'required' => false])?>
                        <?=$this->Form->control('items.' . $i . '.price', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('items.' . $i . '.origin', ['type' => 'text', 'required' => false, 'label' => false])?>
                    </div><!-- end hidden files -->
		    <td> <!-- sparkline -->
			<p>
			    <span id="graphic-<?=$i?>" class="piesparkline">
				...
			    </span>
			</p>
			<span id="totals-<?=$i?>" class="totals-item" style="display:none"></span>
		    </td>
                    <td> <!-- qtd show -->
			<p>
			    <span id="qtd-<?=$i?>-show">
				
			    </span>
			</p>
		    </td>
                    <td> <!-- title & complement show -->
			<p>
			    <span id="title-<?=$i?>-show">
				<?=empty($order->items[$i]['title']) ? '' : $order->items[$i]['title']?>
			    </span>
			</p>
			<p>
			    <i>
				<span id="complement-<?=$i?>-show">
				    <?=empty($order->items[$i]['complement']) ? '' : $order->items[$i]['complement']?>
				</span>
			    </i>
			</p>
		    </td>
                    <td class="text-right"> <!-- price show -->
			<p>
			    <span id="price-<?=$i?>-show">

			    </span>
			</p>
		    </td>
                    <td class="text-right"> <!-- buttons -->
                        <div class="form-group">
                            <?=$this->Html->link('<i class="fa fa-edit"></i>', '#', ['id' => 'bt-edit-' . $i, 'class' => 'btn btn-warning', 'onclick' => 'editItem(' . $i . ');', 'escape' => false])?>
                            <?=$this->Html->link('<i class="fa fa-trash"></i>', '#', ['id' => 'bt-delete-' . $i,'class' => 'btn btn-danger', 'onclick' => 'deleteItem(' . $i . ');', 'escape' => false])?>
                        </div>
                    </td>
                </tr>
            <?php endfor; ?>
        </tbody>
    </table>
    <p><span id="span_product_id"></span></p>
    <p><span id="span_sku"></span></p>
    <p><span id="span_title"></span></p>
    <p><span id="span_supply_name"></span></p>
    <p><span id="span_supply_code"></span></p>
    <p><span id="span_supply_id"></span></p>
    <p><span id="span_supply_line_id"></span></p>

    <p><span id="span_status"></span></p>
    <p><span id="span_total_uncovered"></span></p>
    <p><span id="span_total_required"></span></p>
    <p><span id="span_total_received"></span></p>
    <p><span id="span_total_dispatched"></span></p>
    <p><span id="span_total_completed"></span></p>

    <p><span id="span_price"></span></p>
    <p><span id="span_unitary_price_unmasked"></span></p>
    <p><span id="span_price_calculated"></span></p>
    <p><span id="span_deadline"></span></p>
    <p><span id="span_working_days"></span></p>
    <p><span id="span_maximum_discount"></span></p>
    <p><span id="span_minimum_unitary_price"></span></p>
    <p><span id="span_product_type_id"></span></p>
</div>
<?php
echo $this->Html->css([
    '/third-party/JavaScript-autoComplete-master/auto-complete',
    'autocomplete',
], [
    'block' => 'css'
]);
echo $this->Html->script([
    '/third-party/JavaScript-autoComplete-master/auto-complete',
], [
    'block' => 'script'
]);
?>
<style>
 .autocomplete-suggestion.selected {
     background: #3875d7;
     color: #fff
 }
</style>
