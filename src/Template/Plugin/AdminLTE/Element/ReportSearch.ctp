<?php
use Cake\Utility\Inflector;
$q = null !== $this->request->getQuery('q') ? $this->request->getQuery('q') : '';;
$url = '/' . Inflector::dasherize($this->request->getParam('controller'));
$options = $this->request->getQuery();
?>
<?php echo $this->Form->create(null, ['id' => 'frm-search', 'type' => 'post', 'onsubmit' => 'validateForm(event);']); ?>
<span style="display:none"><?=$this->Form->control('print', ['label' => false, 'value' => false])?></span>
<section id="header-search" class="content-header">
    <div class="row">
    <br>
    <div class="col-md-3">
        <div class="form-group input text">
            <?=$this->Form->select('select_report', $reports, [
                'id' => 'select-report',
                'value' => empty($options['select_report']) ? '' : $options['select_report'],
                'empty' => __('escolha um relatório'),
                'class' => 'select2',
                'style' => 'width: 100%',
                'onchange' => 'changeVisibleSelect();',
            ]);?>
        </div>
    </div>
    <div class="col-md-3">
        <div id="div-select-subsidiary" class="form-group input text selects-reports" style="display:none">
            <?=$this->Form->select('select_subsidiary', $selectSubsidiaries, [
                'id' => 'select-subsidiary',
                'value' => empty($options['select_subsidiary']) ? '' : $options['select_subsidiary'],
                'empty' => __('todas'),
                'class' => 'select2 select-entity',
                'style' => 'width: 100%'
            ]);?>
        </div>
        <div id="div-select-employee" class="form-group input text selects-reports" style="display:none">
            <?=$this->Form->select('select_employee', $selectEmployees, [
                'id' => 'select-employee',
                'value' => empty($options['select_employee']) ? '' : $options['select_employee'],
                'empty' => __('todos'),
                'class' => 'select2 select-entity',
                'style' => 'width: 100%'
            ]);?>
        </div>
        <div id="div-select-supply" class="form-group input text selects-reports" style="display:none">
            <?=$this->Form->select('select_supply', $selectSupplies, [
                'id' => 'select-supply',
                'value' => empty($options['select_supply']) ? '' : $options['select_supply'],
                'empty' => __('todos'),
                'class' => 'select2 select-entity',
                'style' => 'width: 100%'
            ]);?>
        </div>
    </div>
    <div class="col-md-3">
        <div id="div-graphics" class="form-group input text options-hidden" style="display:none">
            <?=$this->Form->control('checkbox_graphics', [
                'id' => 'checkbox-graphics',
                'type' => 'checkbox',
                'checked' => empty($options['checkbox_graphics']) ? '' : $options['checkbox_graphics'],
                'class' => 'checkbox-search',
                'label' => '&nbsp;&nbsp<b>gráficos</b>',
                'escape' => false,
            ])?>
        </div>
    </div>
    <div class="col-md-3">
        <div id="div-details" class="form-group input text options-hidden" style="display:none">
            <?=$this->Form->control('checkbox_details', [
                'id' => 'checkbox-details',
                'type' => 'checkbox',
                'checked' => empty($options['checkbox_details']) ? '' : $options['checkbox_details'],
                'class' => 'checkbox-search',
                'label' => '&nbsp;&nbsp<b>detalhes</b>',
                'escape' => false,
            ])?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div id="div-past-month" class="form-group input text options-hidden" style="display:none">
            <br>
            <?=$this->Form->control('checkbox_past_month', [
                'id' => 'checkbox-past-month',
                'type' => 'checkbox',
                'checked' => empty($options['checkbox_past_month']) ? '' : $options['checkbox_past_month'],
                'class' => 'checkbox-search',
                'label' => '&nbsp;&nbsp<b>mês passado</b>',
                'escape' => false,
                'onchange' => 'changeVisibleDates();',
            ])?>
        </div>
    </div>
    <div class="col-md-3">
        <div id="div-past-month" class="form-group input text options-hidden date-range" style="display:none">
	    <?=$this->Form->input('since_date', [
                'id' => 'since-date',
	        'label' => 'desde',
	        'data-inputmask-alias' => 'datetime',
	        'data-inputmask-inputformat' => 'dd/mm/yyyy',
	        'value' => empty($options['since_date']) ? '' : $options['since_date'],
	        'templateVars' => [
		    'otherClass' => 'datepicker date pull-right',
	        ],
	    ]);?>
        </div>
        </div>
    <div class="col-md-3">
        <div id="div-past-month" class="form-group input text options-hidden date-range" style="display:none">
	    <?=$this->Form->input('until_date', [
                'id' => 'until-date',
	        'label' => 'até',
	        'data-inputmask-alias' => 'datetime',
	        'data-inputmask-inputformat' => 'dd/mm/yyyy',
	        'value' => empty($options['until_date']) ? '' : $options['until_date'],
	        'templateVars' => [
		    'otherClass' => 'datepicker date pull-right',
	        ],
	    ]);?>
        </div>
    </div>

    <div class="col-md-3 text-right">
        <br/>
        <?php echo $this->Form->submit(__('Submit')); ?>
        <!-- <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button> -->
    </div>

</div>
</section>
<?php echo $this->Form->end(); ?>
<script>
 function changeVisibleSelect() {
     var selectReport = document.getElementById('select-report').value;
     var elements = document.getElementsByClassName('options-hidden');
     var divSelects = document.getElementsByClassName('selects-reports');
     var selects = document.getElementsByClassName('select-entity');
     for (var i = 0; i < elements.length; i++) {elements[i].style.setProperty('display', 'none');}
     for (var i = 0; i < divSelects.length; i++) {divSelects[i].style.setProperty('display', 'none');}
     for (var i = 0; i < selects.length; i++) {
         if (selects[i].id != 'select-' + selectReport) {selects[i].value = '';}
     }
     if (selectReport == '') {
         document.getElementById('checkbox-past-month').checked = document.getElementById('checkbox-graphics').checked = document.getElementById('checkbox-details').checked = false;
         return;
     }
     document.getElementById('div-select-' + selectReport).style.removeProperty('display');
     for (var i = 0; i < elements.length; i++) {elements[i].style.removeProperty('display');}
     changeVisibleDates();
 }
 function changeVisibleDates() {
     var dates = document.getElementsByClassName('date-range');
     if (document.getElementById('checkbox-past-month').checked) {
         for (var i = 0; i < dates.length; i++) {dates[i].style.setProperty('display', 'none');}
         document.getElementById('since-date').value = document.getElementById('until-date').value = '';
     } else {
         for (var i = 0; i < dates.length; i++) {dates[i].style.removeProperty('display');}
     }
 }
 function validateForm(e) {
     var selectReport = document.getElementById('select-report');
     var checkboxPastMonth = document.getElementById('checkbox-past-month');
     var sinceDate = document.getElementById('since-date');
     var untilDate = document.getElementById('until-date');

     if (selectReport.value == '') {
         alert('Você deve escolher um relatório');
         selectReport.focus();
         e.preventDefault();
         return;
     }
     if (checkboxPastMonth.checked == false && (sinceDate.value == '' || untilDate.value == '')) {
         alert('Você deve definir um período para o relatório');
         e.preventDefault();
         return;
     }
 }
 changeVisibleSelect();
</script>
