<?php
    use Cake\Utility\Inflector;
    $q = isset($q) ? $q : '';
    $url = '/' . Inflector::dasherize($this->request->getParam('controller'));
?>
<div class="row">
    <div class="col-sm-2">
	    <!-- amount -->
	    <input id="inputQtd"  onchange="changedQtd()" type="number" class="form-control" value="1" min="1">
    </div>
    <div class="col-sm-7">
	    <!-- <input id="inputProduct" name="q" class="form-control" style="width:100%"> -->
	    <select id="product-add-ajax" class="product-add-ajax select2" style="width:100%""></select>
	    <div id="product-type-details" class="box box-solid"></div>
    </div>
    <div class="col-sm-3">
	    <input id="unmaskedPrice" type="hidden">
	    <div class="input-group">
	        <!-- <input id="inputPrice" class="form-control" data-inputmask="'alias': 'numeric', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, 'prefix': 'R$ ', 'placeholder': '0'"> -->
	        <span class="input-group-addon">R$</span>
	        <!-- <input id="inputPrice" class="form-control currency"> -->
	        <input id="inputPrice" class="form-control currency">
	        <input id="inputOriginalPriceUnmasked" type="hidden">
	        <input id="inputActualUnitPriceUnmasked" type="hidden">
	        <span class="input-group-btn">
		        <button id="btok" type="button" onclick="addItem($(this));return false;" class="btn btn-default btn-flat">
		            <i class="fa fa-check"></i>
		        </button>
		        <button id="btclear" type="reset" class="btn btn-default btn-flat">
		            <i class="fa fa-eraser"></i>
		        </button>
	        </span>
	    </div>
    </div>
</div> <!-- /.row -->
<?php
    echo $this->Html->css([
        'AdminLTE./bower_components/select2/dist/css/select2.css'], [
            'block' => 'css'
        ]);
    echo $this->Html->script([
        'AdminLTE./bower_components/select2/dist/js/select2.js',
        'apply-select2',
        'product-add',
    ], [
        'block' => 'scriptBottom'
    ]);
?>
