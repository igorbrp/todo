<?php $controller = $this->request->getParam('controller');?>
<div id="line-item-<?=$item->id?>" data-item-id="<?=$item->id?>" class="row panel-heading line-item"> <!-- linha do item -->
    <?php if ($controller == 'Orders') : ?>
	<div class="col-md-2"></div>
    <?php endif; ?>
    <div class="col-md-1 text-center">
	<a data-toggle="collapse" href="#group-item-history-<?=$item->id?>">
            <span id="graphic-<?=$item->id?>" class="piesparkline">
                ...
            </span>
	</a>
        <span id="totals-<?=$item->id?>" class="totals-item" style="display:none"><?=$item['total_uncovered']?>,<?=$item['total_required']?>,<?=$item['total_received']?>,<?=$item['total_dispatched']?>,<?=$item['total_completed']?></span>
    </div>
    <div class="col-md-1"><?= $this->Number->format($item->qtd);?></div>

    <?php if ($controller == 'Items') : ?>
	<div class="col-md-1"><?=$item->order->number?></div>
    <?php endif; ?>

    <div class="col-md-3"><?= $this->Html->link($item->title, ['controller' => 'Products', 'action' => 'view', $item->product_id], ['target' => '_blank']);?><?=!empty($item->complement) ? '<br><span style="font-size:0.8em"><i>' . $item->complement . '</i></span>' : '';?></div>
    <div class="col-md-<?=$controller == 'Items' ? '3' : '2';?>"><?= $this->Html->link($item->supply_name, ['controller' => 'Supplies', 'action' => 'view', $item->product['supply_id']], ['target' => '_blank']) ?></div>
    <div class="col-md-2 text-right"><?=$this->Number->currency($item->price);?></div>
    <div class="col-md-1 text-right">
	<?=$item->origin == 'stock' ? '<small class="label text-green" data-toggle="tooltip" data-placement="top" title="estoque"><i class="fa fa-bars"></i></small>' : '<small class="label text-yellow" data-toggle="tooltip" data-placement="top" title="requisição"><i class="fa fa-calendar"></i></small>'?>
    </div>
</div>

<div id="group-item-history-<?=$item->id?>" data-item-id="<?=$item->id?>" class="panel-collapse collapse group-item-history">
    &nbsp;
</div>
