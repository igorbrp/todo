<?php
$selectSupply = null !== $this->request->getQuery('selectsupply') ? $this->request->getQuery('selectsupply') : '';
?>
<div class="row">
    <div class="col-md-3">
        <div class="form-group input text">
            <?=$this->Form->select('selectsupply', $supplies, [
                'id' => 'selectsupply',
                'value' => $selectSupply == '' ? '' : $selectSupply,
                'empty' => __('escolha um fornecedor'),
                'class' => 'select2',
                'style' => 'width: 100%'
            ]);?>
        </div>
    </div>
</div>

<?php
echo $this->Html->css([
    'AdminLTE./bower_components/select2/dist/css/select2.css'], [
        'block' => 'css'
]);
echo $this->Html->script([
    'AdminLTE./bower_components/select2/dist/js/select2.js',
], [
    'block' => 'script'
]);
echo $this->Html->script([
    'apply-select2',
    /* 'product-search', */
], [
    'block' => 'scriptBottom'
]);
?>
