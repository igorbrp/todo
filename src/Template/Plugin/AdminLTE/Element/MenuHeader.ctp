<?php
use Cake\Utility\Inflector;
use Cake\Core\Configure;
use Cake\Log\Log;
$controller = $this->request->getParam('controller');

if(count($this->request->getParam('pass')) > 0) {
    $param = $this->request->getParam('pass')[0];
} else {
    $param = '';
}

$noedit = isset($noedit) ? $noedit : false;
$noadd = isset($noadd) ? $noadd : false;
$noindex = isset($noindex) ? $noindex : false;
$nodelete = isset($nodelete) ? $nodelete : false;
$noprint = isset($noprint) ? $noprint : false;
?>

<ol class="list-inline text-right">
    <?php if($this->template != "index" && !$noindex):?>
        <?php if($this->AuthLink->isAuthorized($this->Url->build(['controller' => $controller, 'action' => 'index']))): ?>
	    <li>
	        <?=$this->Html->link('<i class="fa fa-mail-reply"></i>', [
                    'controller' => $controller,
                    'action' => 'index'], [
                        'style' => 'color:#fff',
                        'class' => 'btn btn-primary btn-sm',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'voltar à lista',
                        'escape' => false
                ])?>
	    </li>
        <?php endif; ?>
    <?php endif; ?>

    <?php if($this->template == "view" && !$noedit):?>
        <?php if($this->AuthLink->isAuthorized($this->Url->build(['controller' => $controller, 'action' => 'edit']))): ?>
	    <li>
	        <?=$this->Html->link('<i class="fa fa-edit"></i>', [
                    'controller' => $controller,
                    'action' => 'edit', $param], [
                        'style' => 'color:#fff',
                        'class' => 'btn btn-warning btn-sm',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'editar',
                        'escape' => false
                ])?>
	    </li>
        <?php endif; ?>
    <?php endif; ?>

    <?php if($this->template != "add" && !$noadd):?>
        <?php if($this->AuthLink->isAuthorized($this->Url->build(['controller' => $controller, 'action' => 'add']))): ?>
	    <li>
	        <?=$this->Html->link('<i class="fa fa-square-o"></i>', [
                    'controller' => $controller,
                    'action' => 'add'], [
                        'style' => 'color:#fff',
                        'class' => 'btn btn-success btn-sm',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'novo',
                        'escape' => false
                ])?>
	    </li>
        <?php endif; ?>
    <?php endif; ?>

    <?php if($this->template != "add" && !$noprint):?>
    <?php
    $params = $this->request->getQuery();
    $action = $this->request->getParam('action');
    if ($action == 'index') {
	$action = $action . '-print';
	$arrPrintAction = ['#', '_full' => false];
    } elseif ($action == 'view') {
	$arrPrintAction = [
	    'controller' => $controller,
	    'action' => $action,
	    $param,
	    '?' => ['print' => '1']
	];
    } 

    if($this->AuthLink->isAuthorized($this->Url->build(['controller' => $controller, 'action' => $action]))): ?>
	<?php if ($action != 'profile'): ?>
	    <li>
		<?=$this->Html->link('<i class="fa fa-print"></i>', $arrPrintAction
		    /* [
		       'controller' => $controller,
		       'action' => $action,
		       '?' => $params,
		       //		    '_full' => false,
		       ] */, [
			   'style' => 'color:#0000ff',
			   'class' => 'btn btn-default btn-sm',
			   'data-toggle' => 'tooltip',
			   'data-placement' => 'top',
			   'title' => 'imprimir',
			   'escape' => false,
			   'target' => '_blank',
			   'onclick' => 'printPage(event);',
		])?>
	    </li>
	<?php endif; ?>
	
    <?php endif; ?>
    <?php endif; ?>

    <?php if($this->template != "add" && $this->template != "view" && !$nodelete):?>
        <?php if($this->AuthLink->isAuthorized($this->Url->build(['controller' => $controller, 'action' => 'delete']))): ?>
            <?php if($this->template == 'edit'): ?>
	        <li>
	            <?=$this->Form->postLink('<i class="fa fa-trash"></i>', [
                        'controller' => $controller,
                        'action' => 'delete',
                        $param
                    ], [
                        'id' => 'btdelete',
                        'style' => 'color:#fff',
                        'class' => 'btn btn-danger btn-sm',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'excluir',
                        'escape' => false,
                        'confirm' => __('Are you sure you wish to delete this record?')
                    ])?>
	        </li>
            <?php elseif($this->template == 'index'): ?>
	        <li>
                    <?=$this->Form->create('', [
                        'id' => 'delete_some_records',
                        'action' => 'delete-some-records',
                        'onsubmit' => 'delSomeRecs(event);',
                    ]);?>
                    <span style="display:none;">
                        <?=$this->Form->control('ids', ['id' => 'ids']);?>
                    </span>
                    <?=$this->Form->button('<i class="fa fa-trash"></i>', [
                        'id' => 'btdelete',
                        'type' => 'submit',
                        'style' => 'color:#fff',
                        'data-toggle' => 'tooltip',
                        'data-placement' => 'top',
                        'title' => 'excluir',
                        'escape' => false,
                        'class' => 'btn btn-danger btn-sm',
                        'disabled' => true
                    ]);?>
                    <?=$this->Form->end();?>
	        </li>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>
</ol>

<script>
 function printPage(e)
 {
     document.getElementById("print").value = true;
     document.getElementById("frm-search").target = '_blank';
     document.getElementById("frm-search").submit();
     e.preventDefault();
 }
</script>
