<ul class="pagination">
    <?php
        if($this->Paginator->hasPrev()) {
	        echo $this->Paginator->first("&laquo;", ['escape' => false]) ;
	        echo $this->Paginator->prev("&lsaquo;", ['escape' => false]) ;
        }
        echo $this->Paginator->numbers();
        if($this->Paginator->hasNext()) {
	        echo $this->Paginator->next("&rsaquo;", ['escape' => false]) ;
	        echo $this->Paginator->last("&raquo;", ['escape' => false]) ;
        }
    ?>
</ul>
