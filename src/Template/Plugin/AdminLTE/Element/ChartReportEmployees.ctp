<!-- DONUT CHART -->
<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">Vendas por vendedores</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-6">
                <canvas id="pieChart" style="height:250px"></canvas>
            </div>
            <div class="col-md-6">
                <ul style="list-style-type: none;">
                    <?php foreach ($chartArray as $chart) : ?>
                        <li>
                            <span style="color:<?= $chart['color'] ?>;"><i class="fa fa-square"></i></span>
                            <span><?='&nbsp;&nbsp;&nbsp;' . $chart['label']?></span>
                            <span class="pull-right"><?=$this->Number->currency($chart['value'])?></span>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

<?php echo $this->Html->script('chart-sales-by-employees', ['block' => 'script']);?>
<script>
 var chartArray = <?=json_encode($chartArray)?>;
</script>
