<?php
    use Cake\Utility\Inflector;
    $q = isset($q) ? $q : '';
    $url = '/' . Inflector::dasherize($this->request->getParam('controller'));
?>
<div class="row">
    <div class="col-md-12">
        <ol class="list-inline text-right">
            <li id="totalPaid"></li>
        </ol>
    </div>
</div>
<div class="box-body table-responsive no-padding">
    <table class="table table-hover">
        <thead>
            <tr>
                <th width="30%" scope="col"><?=__('payment')?></th>
                <th width="20%" scope="col" class="text-right"><?=__('value')?></th>
                <th width="10%" scope="col"><?=__('installments')?></th>
                <th scope="col"><?=__('financier')?></th>
                <th  width="15%" scope="col" class="text-right"><?=__('actions')?></th>
            </tr>
            <tr>
                <td>
                    <?=$this->Form->select('inputPaymentMethod', $paymentMethods, [
                        'id' => 'inputPaymentMethod',
                        'empty' => __('choose...'),
                        'class' => 'select2',
                        'onchange' => 'buildPayment(this)',
                        'style' => 'width: 100%',
                        'label' => false])?>
                </td>
                <td>
                    <?=$this->Form->control('inputValue', [
                        'id' => 'inputValue',
                        'label' => false,
                        'templateVars' => [
		                    'otherClass' => 'currency',
	                    ]])?>
                </td>
                <td>
                    <?=$this->Form->control('inputInstallments', ['id' => 'inputInstallments', 'type' => 'number', 'label' => false,])?>
                </td>
                <td>
                    <div id="divFinanciers">
                        <span id="credcard" class="financier" style="display:none">
                            <?=$this->Form->select('inputCredcard', $credCards, [
                                'id' => 'inputCredcard',
                                'empty' => __('choose...'),
                                'class' => 'select2',
                                'style' => 'width: 100%',
                                'label' => false])?>
                        </span>
                        <span id="debitcard" class="financier" style="display:none">
                            <?=$this->Form->select('inputDebitcard', $debitCards, [
                                'id' => 'inputDebitcard',
                                'empty' => __('choose...'),
                                /* 'class' => 'select2',*/
                                'style' => 'width: 100%',
                                'label' => false])?>
                        </span>
                        <span id="checkbook" class="financier" style="display:none">
                            <?=$this->Form->select('inputCheckbook', $banks, [
                                'id' => 'inputCheckbook',
                                'empty' => __('choose...'),
                                /* 'class' => 'select2',*/
                                'style' => 'width: 100%',
                                'label' => false])?>
                        </span>
                    </div>
                </td>
                <td class="text-right">
                    <div class="form-group">
                        <?=$this->Html->link('<i class="fa fa-plus"></i>', '#', ['id' => 'bt-add-payment', 'class' => 'btn btn-info', 'onclick' => 'addPayment();', 'escape' => false])?>
                        <?=$this->Html->link('<i class="fa fa-ban"></i>', '#', ['id' => 'bt-cancel-payment-payment', 'class' => 'btn btn-danger', 'onclick' => 'cancelEditPayment();'/*, 'disabled' => true*/, 'escape' => false])?>
                    </div>
                </td>
            </tr>
            <div style="display:none"><?=$this->Form->control('inputDetails', ['id' => 'inputDetails', 'label' => false])?></div>
        </thead>
        <tbody id="tbody_payments">
            <?php $i = 0; ?>
            <?php if (!empty($order->payments)) : ?>
                <?php foreach ($order->payments as $payments) : ?>
                    <tr id="payments-<?=$i?>">
                        <!-- hidden files -->
                        <div style="display:none">
                            <?=$this->Form->textarea('payments.' . $i . '.details', ['id' => 'payments-' . $i . '-details', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('payments.' . $i . '.financier_id', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('payments.' . $i . '.payment_methods_id', ['type' => 'text', 'required' => false, 'label' => false])?>
                            <?=$this->Form->control('payments.' . $i . '.payment_methods_text', ['type' => 'text', 'required' => false, 'label' => false, 'value' => $paymentMethods->toArray()[$order->payments[$i]['payment_methods_id']]])?>
                            <?=$this->Form->control('payments.' . $i . '.value', ['required' => false, 'label' => false])?>
                            <?=$this->Form->control('payments.' . $i . '.installments', ['required' => false, 'label' => false])?>
                            <?=$this->Form->control('payments.' . $i . '.financier_text', ['type' => 'text', 'required' => false, 'label' => false, 'value' => empty($payments['financier_id']) ? '' : $allFinanciers->toArray()[$order->payments[$i]['financier_id']]])?>
                        </div>
                        <!-- end hidden files -->
                        <td>
                            <span id="payment-methods-text-<?=$i?>-show"><?=$paymentMethods->toArray()[$order->payments[$i]['payment_methods_id']]?></span>
                        </td>
                        <td>
                            <span id="value-<?=$i?>-show"><?=$this->Number->currency($payments['value'])?></span>
                        </td>
                        <td width="10%">
                            <span id="installments-<?=$i?>-show"><?=$payments['installments']?></span>
                        </td>
                        <td>
                            <span id="financier-text-<?=$i?>-show"><?=empty($payments['financier_id']) ? '' : $allFinanciers->toArray()[$order->payments[$i]['financier_id']]?></span>
                        </td>
                        <td class="text-right">
                            <div class="form-group">
                                <?=$this->Html->link('<i class="fa fa-edit"></i>', '#', ['id' => 'bt-edit-payment-' . $i, 'class' => 'btn btn-warning', 'onclick' => 'editPayment(' . $i . ');', 'escape' => false])?>
                                <?=$this->Html->link('<i class="fa fa-trash"></i>', '#', ['id' => 'bt-delete-payment-' . $i,'class' => 'btn btn-danger', 'onclick' => 'deletePayment(' . $i . ');', 'escape' => false])?>
                            </div>
                        </td>
                    </tr>
                    <?php $i++; ?>
                <?php endforeach; ?>
            <?php endif; ?>
            <?php for (; $i < 20; $i++) : ?>
                <tr id="payments-<?=$i?>" style="display:none">
                    <!-- hidden files -->
                    <div style="display:none">
                        <?=$this->Form->textarea('payments.' . $i . '.details', ['id' => 'payments-' . $i . '-details', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('payments.' . $i . '.financier_id', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('payments.' . $i . '.payment_methods_id', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('payments.' . $i . '.payment_methods_text', ['type' => 'text', 'required' => false, 'label' => false])?>
                        <?=$this->Form->control('payments.' . $i . '.value', ['required' => false, 'label' => false])?>
                        <?=$this->Form->control('payments.' . $i . '.installments', ['required' => false, 'label' => false])?>
                        <?=$this->Form->control('payments.' . $i . '.financier_text', ['type' => 'text', 'required' => false, 'label' => false])?>
                    </div>
                    <!-- end hidden files -->
                    <td><span id="payment-methods-text-<?=$i?>-show"><?=isset($order->payments[$i]['payment_methods_text']) ? $order->payments[$i]['payment_methods_text'] : ''?></span>
                    </td>
                    <td>
                        <span id="value-<?=$i?>-show"><?=$payments['value']?></span>
                    </td>
                    <td width="10%">
                        <span id="installments-<?=$i?>-show"><?=$payments['installments']?></span>
                    </td>
                    <td>
                        <?php
                            if(isset($order->payments[$i]['financier_id'])) {
                                $tmp_financier_id = $allFinanciers->toArray()[$order->payments[$i]['financier_id']];
                            } else {
                                $tmp_financier_id = '';
                            }
                        ?>
                        <span id="financier-text-<?=$i?>-show"><?=empty($payments['financier_id']) ? '' : $tmp_financier_id?></span>
                    </td>
                    <td class="text-right">
                        <div class="form-group">
                            <?=$this->Html->link('<i class="fa fa-edit"></i>', '#', ['id' => 'bt-edit-payment-' . $i, 'class' => 'btn btn-warning', 'onclick' => 'editPayment(' . $i . ');', 'escape' => false])?>
                            <?=$this->Html->link('<i class="fa fa-trash"></i>', '#', ['id' => 'bt-delete-payment-' . $i,'class' => 'btn btn-danger', 'onclick' => 'deletePayment(' . $i . ');', 'escape' => false])?>

                        </div>
                    </td>
                </tr>
            <?php endfor; ?>
        </tbody>
    </table>
</div>
