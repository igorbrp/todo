<?php
use Cake\Utility\Inflector;
$q = null !== $this->request->getQuery('q') ? $this->request->getQuery('q') : '';;
//    $url = '/' . $this->templatePath .'/index';
// $buttonReset = "<button onclick=\"location.href='".$url."'\" class='btn btn-warning' type='reset'>reset</button>";
// $controller = $this->request->getParam('controller');
//    $q = isset($q) ? $q : '';
//    $q = null !== $this->request->getQuery('q') ? $this->request->getQuery('q') : '';;
$url = '/' . Inflector::dasherize($this->request->getParam('controller'));
?>
<?php echo $this->Form->create(null, ['id' => 'frm-search', 'type' => 'post']); ?>
<span style="display:none"><?=$this->Form->control('print', ['label' => false, 'value' => false])?></span>
<!-- <section id="header-search" class="content-header"> -->
<div class="input-group">
    <?=$this->Form->control('q', [
        'label' => false,
        'div' => false,
        'type' => 'text',
        'class' => 'form-control',
        'value' => $q,
        'placeholder' => isset($placeholder) ? $placeholder : __('search...'),
    ]);?>
    <span class="input-group-btn">
        <button type="submit" class="btn btn-default btn-flat"><i class="fa fa-search"></i></button>
        <button type="button" class="btn btn-default btn-flat" onclick="location.href='<?=$url?>'"><i class="fa fa-eraser"></i></button>
	<button type="button" class="btn btn-default btn-flat" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-cog"></i></button>
    </span>
</div>
<div class="collapse" id="collapseExample">
    <div class="card-body">
        <?php if(isset($optionSearch)) : ?>
            <br>
            <?=$this->element($optionSearch)?>
        <?php endif; ?>
    </div>
</div>
<!-- </section> -->
<?php echo $this->Form->end(); ?>
