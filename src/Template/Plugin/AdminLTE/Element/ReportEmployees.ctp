<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h2>
                    <?=__('Vendas por vendedores')?>
                    <small class="pull-right"><b><?=$period?></b></small>
                </h2>
                <div class="text-right">
                    <h4><?=$this->Number->currency($totalPeriod)?></h4>
                </div>
            </div>

            <!-- /.box-header -->
            <div class="box-body no-padding">

                <div class="panel-group">
                    <?php if ($data['checkbox_graphics']) : ?>
                        <div class="panel">
                            <div class="panel-body">
                                <?=$this->element('ChartReportEmployees')?>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="panel">

                        <?php foreach ($query as $employee): ?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-8"><h3><?=$employee->name?></h3></div>
                                    <div class="col-md-4 text-right"><h4><?= h($this->Number->currency($employee->order_amount));?></h4></div>
                                </div>
                                <!-- </div> -->
                                <?php if ($data['checkbox_details']) :?>
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-1"><b><?=$this->Paginator->sort('number', ['label' => 'pedido']) ?></b></div>
                                        <div class="col-md-1"><b><?=$this->Paginator->sort('date_order', ['label' => 'data']) ?></b></div>
                                        <div class="col-md-4"><b><?=$this->Paginator->sort('customer_id', ['label' => 'cliente']) ?></b></div>
                                        <div class="col-md-2"><b><?=$this->Paginator->sort('subsidiary_id', ['label' => 'filial']) ?></b></div>
                                        <div class="col-md-2 text-right"><b><?=$this->Paginator->sort('order_amount', ['label' => 'valor']) ?></b></div>
                                    </div>
                                    <?php foreach ($employee->orders as $order) :?>
                                        <div class="row">
                                            <div class="col-md-2"></div>
                                            <div class="col-md-1"><?= $this->Html->link($order->number, ['controller' => 'Orders', 'action' => 'view', $order->id]);?></div>
                                            <div class="col-md-1"><?= h($order->date_order->i18nFormat());?></div>
                                            <div class="col-md-4"><?= h($order->customer->name)?></div>
                                            <div class="col-md-2"><?= h($order->subsidiary->name)?></div>
                                            <!-- <div class="col-md-2 text-right"><?='';// h($this->Number->currency($order->order_amount));?></div> -->
                                            <div class="col-md-2 text-right"><?= h($this->Number->currency($ordersList[$order->id]));?></div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
