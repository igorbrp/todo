<?php foreach ($items as $item): ?>
    <tr id="tr-<?=$item->id?>" data-id="<?=$item->id?>" data-order-id="<?=$item->order->id?>">
        <td width="5%">&nbsp;&nbsp;
            <a href="#" onclick="loadInfo('<?=$item->id?>')">
                <span id="graphic-<?=$item->id?>" class="piesparkline">
                    ...
                </span>
            </a>
            <span id="totals-<?=$item->id?>" class="totals-item" style="display:none"><?=$item['total_uncovered']?>,<?=$item['total_required']?>,<?=$item['total_received']?>,<?=$item['total_dispatched']?>,<?=$item['total_completed']?></span>
        </td>
        <td width="5%">
	    <?= $this->Number->format($item->qtd) ?>
	</td>
        <td width="10%">
	    <?= $this->Html->link($item->order['number'], ['controller' => 'Orders', 'action' => 'view', $item->order['id']], ['target' => '_blank'])?>
	</td>
        <td width="10%">
	    <?= $this->Html->link($item->sku, ['controller' => 'Products', 'action' => 'view', $item->product_id], ['target' => '_blank']) ?>
	</td>
        <td width="30%">
	    <?= $this->Html->link($item->title, ['controller' => 'Products', 'action' => 'view', $item->product_id], ['target' => '_blank']) ?>
	</td>
        <td width="5%">
	    <?= $this->Html->link($item->supply_code, ['controller' => 'Supplies', 'action' => 'view', $item->product['supply_id']], ['target' => '_blank']) ?>
	</td>
        <td width="20%">
	    <?= $this->Html->link($item->supply_name, ['controller' => 'Supplies', 'action' => 'view', $item->product['supply_id']], ['target' => '_blank']) ?>
	</td>
	<td width="5%" class="text-center">
	    <?=
	    $item->origin == 'stock' ? '<small class="label text-green" data-toggle="tooltip" data-placement="top" title="estoque"><i class="fa fa-bars"></i></small>' : '<small class="label text-yellow" data-toggle="tooltip" data-placement="top" title="requisição"><i class="fa fa-calendar"></i></small>'?>
	</td>
        <td width="10%" class="text-right">
	    <?= $this->Number->currency($item->price) ?>
	</td>
    </tr>
    <tr id="tr-<?=$item->id?>-info" style="display:none">
        <td>&nbsp;</td>
        <td id="td-<?=$item->id?>-info" colspan="8">&nbsp;</td>
    </tr>
<?php endforeach; ?>
