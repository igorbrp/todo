<?php
$nodelete = isset($nodelete) ? $nodelete : false;
?>
<ol class="list-inline">
    <?php
        use Cake\Utility\Inflector;
        $controller = empty($controller) ? $this->request->getParam('controller') : $controller;

        if($this->AuthLink->isAuthorized($this->Url->build(Inflector::dasherize($controller) . '/view'))) {
            $button =  $this->Html->link('<i class="fa fa-eye fa-lg"></i>', [
                'controller' => $controller,
                'action' => 'view',
                $cod_id
            ], [
                'escape' => false,
                /* 'class' => 'btn btn-info btn-xs', */
                'data-toggle' => 'tooltip',
                'data-original-title' => __('view')
            ]);
            echo $this->Html->tag('li', $button);
        }

        if($this->AuthLink->isAuthorized($this->Url->build(Inflector::dasherize($controller) . '/edit'))) {
            $button = $this->Html->link('<i class="fa fa-edit fa-lg text-yellow"></i>', [
                'controller' => $controller,
                'action' => 'edit',
                $cod_id
            ], [
                'escape' => false,
                /* 'class'=>'btn btn-warning btn-xs', */
                'data-toggle' => 'tooltip',
                'data-original-title' => __('edit')
            ]);
            echo $this->Html->tag('li', $button);
        }

        if($this->AuthLink->isAuthorized($this->Url->build(Inflector::dasherize($controller) . '/delete')) && !$nodelete) {
            $button = $this->Form->postLink('<i class="fa fa-trash fa-lg text-red"></i>', [
                'controller' => $controller,
                'action' => 'delete',
                $cod_id
            ], [
                'escape' => false,
                /* 'class'=>'btn btn-danger btn-xs', */
                'confirm' => __('Are you sure you want to delete {0}?', $cod_id),
                'data-toggle' => 'tooltip',
                'data-original-title' => __('delete')
            ]);
            echo $this->Html->tag('li', $button);
        }
    ?>
</ol>
