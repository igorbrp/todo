<?php
    $items_tots = $this->request->getQueryParams();
    $selectSupply = null !== $this->request->getQuery('select_supply') ? $this->request->getQuery('select_supply') : '';
?>
<div class="row">
    <div class="col-md-2">
        <?=$this->Form->control('items_tot_uncovered', [
            'id' => 'items-tot-uncovered',
            'type' => 'checkbox',
            'checked' => isset($items_tots['items_tot_uncovered']) ? $items_tots['items_tot_uncovered'] : '',
            'class' => 'checkbox-search',
            'label' => '&nbsp;&nbsp<b>pendentes</b>',
            'onchange' => 'changeItemsCheckbox(this)',
            'escape' => false,
            ])?>
    </div>
    <div class="col-md-2">
        <?=$this->Form->control('items_tot_required', [
            'id' => 'items-tot-required',
            'type' => 'checkbox',
            'checked' => isset($items_tots['items_tot_required']) ? $items_tots['items_tot_required'] : '',
            'class' => 'checkbox-search',
            'label' => '&nbsp;&nbsp<b>requisitados</b>',
            'onchange' => 'changeItemsCheckbox(this)',
            'escape' => false,
            ])?>
    </div>
    <div class="col-md-2">
        <?=$this->Form->control('items_tot_received', [
            'id' => 'items-tot-received',
            'type' => 'checkbox',
            'checked' => isset($items_tots['items_tot_received']) ? $items_tots['items_tot_received'] : '',
            'class' => 'checkbox-search',
            'label' => '&nbsp;&nbsp<b>recebidos</b>',
            'onchange' => 'changeItemsCheckbox(this)',
            'escape' => false,
            ])?>
    </div>
    <div class="col-md-2">
        <?=$this->Form->control('items_tot_dispatched', [
            'id' => 'items-tot-dispatched',
            'type' => 'checkbox',
            'checked' => isset($items_tots['items_tot_dispatched']) ? $items_tots['items_tot_dispatched'] : '',
            'class' => 'checkbox-search',
            'label' => '&nbsp;&nbsp<b>despachados</b>',
            'onchange' => 'changeItemsCheckbox(this)',
            'escape' => false,
            ])?>
    </div>
    <div class="col-md-2">
        <?=$this->Form->control('items_partially', [
            'items-partially',
            'type' => 'checkbox',
            //            'class' => 'checkbox-search',
            'checked' => isset($items_tots['items_partially']) ? $items_tots['items_partially'] : '',
            'label' => '&nbsp;&nbsp<b>parcialmente</b>',
            'onchange' => 'changeItemsCheckbox(this)',
            'escape' => false,
            ])?>
    </div>
    <div class="col-md-2">
        <?=$this->Form->control('items_completed', [
            'items-completed',
            'type' => 'checkbox',
            'checked' => isset($items_tots['items_completed']) ? $items_tots['items_completed'] : '',
            'class' => 'checkbox-search',
            'label' => '&nbsp;&nbsp<b>inclui concluídos</b>',
            'onchange' => 'changeItemsCheckbox(this)',
            'escape' => false,
            ])?>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="form-group input text">
            <?=$this->Form->select('select_supply', $supplies, [
                'id' => 'select-supply',
                'value' => $selectSupply == '' ? '' : $selectSupply,
                'empty' => __('escolha um fornecedor'),
                'class' => 'select2',
                'style' => 'width: 100%'
                ]);?>
        </div>
    </div>
</div>

<script>
    var itemsTotUncovered = document.getElementById('items-tot-uncovered');
    var itemsTotRequired = document.getElementById('items-tot-required');
    var itemsTotReceived = document.getElementById('items-tot-received');
    var itemsTotDispatched = document.getElementById('items-tot-dispatched');
    var itemsCompleted = document.getElementById('items-completed');
    var itemsPartially = document.getElementById('items-partially');

    if (itemsTotUncovered.checked || itemsTotRequired.checked || itemsTotReceived.checked || itemsTotDispatched.checked) {
        itemsPartially.removeAttribute('disabled');
    } else {
        itemsPartially.setAttribute('disabled', true);
    }

    function changeItemsCheckbox(t)
    {
        switch(t.id) {
            case 'items-completed':
                if(t.checked) {
                    clearAll(t);
                    itemsPartially.checked = false;
                    itemsPartially.setAttribute('disabled', true);
                }
                break;
            case 'items-partially':
                if(t.checked) {
                    itemsCompleted.checked = false;
                    itemsCompleted.setAttribute('disabled', true);
                } else {
                    itemsCompleted.removeAttribute('disabled');
                }
                break;
            default:
                if(t.checked) {
                    clearAll(t);
                    itemsPartially.removeAttribute('disabled');
                }
                break;
                if (!itemsTotUncovered.checked && !itemsTotRequired.checked && !itemsTotReceived.checked && !itemsTotDispatched.checked) {
                    itemsPartially.setAttribute('disabled', true);
                } else {
                    itemsPartially.removeAttribute('disabled');
                }
        }
    }

    function clearAll(t)
    {
        var listCheckbox = document.getElementsByClassName('checkbox-search');
        for (i = 0; i < listCheckbox.length; i++) {
            if (listCheckbox.item(i).id != t.id){
                listCheckbox.item(i).checked = false;
            }
        }
    }
</script>
<?php
    echo $this->Html->css([
        'AdminLTE./bower_components/select2/dist/css/select2.css'], [
            'block' => 'css'
    ]);
    echo $this->Html->script([
        'AdminLTE./bower_components/select2/dist/js/select2.js',
    ], [
        'block' => 'script'
    ]);
    echo $this->Html->script([
        'apply-select2',
        /* 'product-search', */
    ], [
        'block' => 'scriptBottom'
    ]);
?>
