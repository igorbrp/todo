<?php
    function searchMsgInArray($item, $key)
    {
        if(is_string($item))
            echo "<li>$item</li>";
    }

?>
<?php if (trim($message) != 'You are not authorized to access that location.') : ?>
    <section class="content-header">
        <div class="alert alert-danger alert-dismissible">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-ban"></i> <?= __('Alerta') ?>!</h4>
            <h5><?= h($message) ?></h5>
            <?php if (!empty($params)): ?>
                <ul>
                    <?php array_walk_recursive($params, 'searchMsgInArray') ?>
                </ul>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>
