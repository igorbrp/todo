<div id="line-order-<?=$order->id?>" data-order-id="<?=$order->id?>" class="row panel-heading line-order"> <!-- linha do pedido -->
    <span id="totals-order-<?=$order->id?>" class="totals-order" style="display:none">
	<?=$order['order_total_uncovered']?>,<?=$order['order_total_required']?>,<?=$order['order_total_received']?>,<?=$order['order_total_dispatched']?>,<?=$order['order_total_completed']?>
    </span>
    <div class="col-md-1 text-left">
	<?php if ($this->request->session()->read('Auth.User.role') != 'vendor') :?>
	    <a data-toggle="collapse" href="#group-item-<?=$order->id?>">
	<?php endif; ?>
	<span id="graphic-order-<?=$order->id?>" class="piesparkline">
	    ...
	</span>
	<?php if ($this->request->session()->read('Auth.User.role') != 'vendor') :?>
	    </a>
	<?php endif; ?>
    </div>
    <div class="col-md-1"><?= $this->Html->link($order->number, ['controller' => 'Orders', 'action' => 'view', $order->id]);?></div>
    <div class="col-md-1"><?= h($order->date_order->i18nFormat());?></div>
    <div class="col-md-3"><?= h($order->customer->name)?></div>
    <div class="col-md-2"><?= $order->has('subsidiary') ? $this->Html->link($order->subsidiary->name, ['controller' => 'Subsidiaries', 'action' => 'view', $order->subsidiary->id]) : '';?></div>
    <div class="col-md-1"><?= $order->has('employee') ? $this->Html->link($order->employee->name, ['controller' => 'Employees', 'action' => 'view', $order->employee->id]) : '';?></div>
    <div class="col-md-2 text-right"><?= h($this->Number->currency($order->order_amount));?></div>
    <div class="col-md-1 text-right"><small><?=$this->element('Actions', ['cod_id' => $order->id, 'nodelete' => true])?></small></div>
</div>

<div id="group-item-<?=$order->id?>" data-order-id="<?=$order->id?>" class="panel-collapse collapse group-item">

    <div class="row panel-heading"> <!-- cabeçalho do item -->
	<div class="col-md-1"></div>
	<div class="col-md-1">

	    <?php $orderTotalQtd = $order['order_total_uncovered'] + $order['order_total_required'] + $order['order_total_received'] + $order['order_total_dispatched'] + $order['order_total_completed'];?>
	    <small>
		<?php if ($order['order_total_completed'] < $orderTotalQtd) : ?>
		    <?php echo $this->HtmlElement->mk_dropdown_bulk($order); ?>
		    <!-- <span style="display:inline;margin-left:10px;"><a href="#" data-toggle="popover" title="Atribuições em massa" data-content="Muda o status apenas dos itens desse pedido que puderem ser mudados. Ex.: Escolhendo 'requisitar', apenas os itens pendentes serão requisitados; 'receber', apenas os itens requisitados serão recebidos."><i class="fa fa-question-circle"></i></a></span> -->
		<?php endif; ?>
	    </small>

	</div>
	<div class="col-md-1"></div>
	<div class="col-md-1"><small class="text-light-blue">qtd</small></div>
	<div class="col-md-3"><small class="text-light-blue">produto</small></div>
	<div class="col-md-2"><small class="text-light-blue">fornecedor</small></div>
	<div class="col-md-2 text-right"><small class="text-light-blue">valor</small></div>
	<div class="col-md-1 text-right"><small class="text-light-blue">origem</small></div>
    </div>

    <div id="line-items-<?=$order->id?>">
	<?=$this->element('ShowItemLine', ['items' => $order->items])?>
    </div>

</div>
