<?php
    $order_totals = $this->request->getQueryParams();
    $selectEmployee = null !== $this->request->getQuery('select_employee') ? $this->request->getQuery('select_employee') : '';
    $selectSubsidiary = null !== $this->request->getQuery('select_subsidiary') ? $this->request->getQuery('select_subsidiary') : '';
?>
<div class="row">
    <div class="col-md-2">
        <?=$this->Form->control('order_total_uncovered', [
            'id' => 'order-total-uncovered',
            'type' => 'checkbox',
            'checked' => isset($order_totals['order_total_uncovered']) ? $order_totals['order_total_uncovered'] : '',
            'class' => 'checkbox-search',
            'label' => '&nbsp;&nbsp<b>pendentes</b>',
            'onchange' => 'changeOrderCheckbox(this)',
            'escape' => false,
            ])?>
    </div>
    <div class="col-md-2">
        <?=$this->Form->control('order_total_required', [
            'id' => 'order-total-required',
            'type' => 'checkbox',
            'checked' => isset($order_totals['order_total_required']) ? $order_totals['order_total_required'] : '',
            'class' => 'checkbox-search',
            'label' => '&nbsp;&nbsp<b>requisitados</b>',
            'onchange' => 'changeOrderCheckbox(this)',
            'escape' => false,
            ])?>
    </div>
    <div class="col-md-2">
        <?=$this->Form->control('order_total_received', [
            'id' => 'order-total-received',
            'type' => 'checkbox',
            'checked' => isset($order_totals['order_total_received']) ? $order_totals['order_total_received'] : '',
            'class' => 'checkbox-search',
            'label' => '&nbsp;&nbsp<b>recebidos</b>',
            'onchange' => 'changeOrderCheckbox(this)',
            'escape' => false,
            ])?>
    </div>
    <div class="col-md-2">
        <?=$this->Form->control('order_total_dispatched', [
            'id' => 'order-total-dispatched',
            'type' => 'checkbox',
            'checked' => isset($order_totals['order_total_dispatched']) ? $order_totals['order_total_dispatched'] : '',
            'class' => 'checkbox-search',
            'label' => '&nbsp;&nbsp<b>despachados</b>',
            'onchange' => 'changeOrderCheckbox(this)',
            'escape' => false,
            ])?>
    </div>
    <div class="col-md-2">
        <?=$this->Form->control('order_partially', [
            'order-partially',
            'type' => 'checkbox',
            //            'class' => 'checkbox-search',
            'checked' => isset($order_totals['order_partially']) ? $order_totals['order_partially'] : '',
            'label' => '&nbsp;&nbsp<b>parcialmente</b>',
            'onchange' => 'changeOrderCheckbox(this)',
            'escape' => false,
            ])?>
    </div>
    <div class="col-md-2">
        <?=$this->Form->control('order_completed', [
            'order-completed',
            'type' => 'checkbox',
            'checked' => isset($order_totals['order_completed']) ? $order_totals['order_completed'] : '',
            'class' => 'checkbox-search',
            'label' => '&nbsp;&nbsp<b>inclui concluídos</b>',
            'onchange' => 'changeOrderCheckbox(this)',
            'escape' => false,
            ])?>
    </div>
</div>

<div class="row">
    <div class="col-md-3">
        <div class="form-group input text">
            <?=$this->Form->select('select_employee', $employees, [
                'id' => 'select-employee',
                'value' => $selectEmployee == '' ? '' : $selectEmployee,
                'empty' => __('escolha um vendedor'),
                'class' => 'select2',
                'style' => 'width: 100%'
                ]);?>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group input text">
            <?=$this->Form->select('select_subsidiary', $subsidiaries, [
                'id' => 'select-subsidiary',
                'value' => $selectSubsidiary == '' ? '' : $selectSubsidiary,
                'empty' => __('escolha uma loja'),
                'class' => 'select2',
                'style' => 'width: 100%'
                ]);?>
        </div>
    </div>
</div>

<script>
    var orderTotUncovered = document.getElementById('order-total-uncovered');
    var orderTotRequired = document.getElementById('order-total-required');
    var orderTotReceived = document.getElementById('order-total-received');
    var orderTotDispatched = document.getElementById('order-total-dispatched');
    var orderCompleted = document.getElementById('order-completed');
    var orderPartially = document.getElementById('order-partially');

    if (orderTotUncovered.checked || orderTotRequired.checked || orderTotReceived.checked || orderTotDispatched.checked) {
        orderPartially.removeAttribute('disabled');
    } else {
        orderPartially.setAttribute('disabled', true);
    }

    function changeOrderCheckbox(t)
    {
        switch(t.id) {
            case 'order-completed':
                if(t.checked) {
                    clearAll(t);
                    orderPartially.checked = false;
                    orderPartially.setAttribute('disabled', true);
                }
                break;
            case 'order-partially':
                if(t.checked) {
                    orderCompleted.checked = false;
                    orderCompleted.setAttribute('disabled', true);
                } else {
                    orderCompleted.removeAttribute('disabled');
                }
                break;
            default:
                if(t.checked) {
                    clearAll(t);
                    orderPartially.removeAttribute('disabled');
                }
                break;
                if (!orderTotUncovered.checked && !orderTotRequired.checked && !orderTotReceived.checked && !orderTotDispatched.checked) {
                    orderPartially.setAttribute('disabled', true);
                } else {
                    orderPartially.removeAttribute('disabled');
                }
        }
    }

    function clearAll(t)
    {
        var listCheckbox = document.getElementsByClassName('checkbox-search');
        for (i = 0; i < listCheckbox.length; i++) {
            if (listCheckbox.item(i).id != t.id){
                listCheckbox.item(i).checked = false;
            }
        }
    }
</script>
<?php
    echo $this->Html->css([
        'AdminLTE./bower_components/select2/dist/css/select2.css'], [
            'block' => 'css'
    ]);
    echo $this->Html->script([
        'AdminLTE./bower_components/select2/dist/js/select2.js',
    ], [
        'block' => 'script'
    ]);
    echo $this->Html->script([
        'apply-select2',
        /* 'product-search', */
    ], [
        'block' => 'scriptBottom'
    ]);
?>
