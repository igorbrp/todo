<?php
use Cake\Core\Configure;

$freight = Configure::read('system_configurations.configuration.freight');
?>
<div class="row">
    <div class="col-md-2">
        <div class="form-group input">
            <br/>
            <?=$this->Form->control('delivery_less', [
                'type' => 'checkbox',
                /* 'style' => 'display:inline', */
                'label' => '&nbsp;&nbsp;<b>sem entrega</b>',
                //                                            'div' => false,
                'escape' => false,
                'onchange' => 'changeDeliveryLess();',
            ]);?>

        </div>
    </div>
    <!-- </div>
         <div class="row"> -->
    <div class="col-md-3">
        <div id="divSameCustomerAddress" class="form-group input">
            <br/>
            <?=$this->Form->control('delivery.same_customer_address', [
                'type' => 'checkbox',
                'label' => '&nbsp;&nbsp;<b>mesmo endereço do cliente</b>',
                /* 'div' => false, */
                'escape' => false,
                'onchange' => 'changeSameCustomerAddress();',
            ]);?>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-2">
        <div id="divWorkingDays" class="form-group input">
            <br/>
            <?=$this->Form->control('delivery.working_days', [
                'type' => 'checkbox',
                'label' => '&nbsp;&nbsp;<b>dias úteis</b>',
                /* 'div' => false, */
                'escape' => false,
		//                'onchange' => 'changeSameCustomerAddress();',
            ]);?>
        </div>
    </div>
    <div class="col-md-2">
        <div id="divForeseenDays" class="form-group input text">
            <?php
            echo $this->Form->control('delivery.foreseen_days', [
		'label' => 'dias previstos',
                'type' => 'number',
            ]);?>
        </div>
    </div>
    <div class="col-md-3">
        <div id="divForeseenDate" class="form-group input text">
            <?php
            echo $this->Form->control('delivery.foreseen_date', [
                /* 'empty' => true*/
                'type' => 'text',
                /* 'class' => 'date',*/
	        'data-inputmask-alias' => 'datetime',
	        'data-inputmask-inputformat' => 'dd/mm/yyyy',
	        'templateVars' => [
		    'otherClass' => 'date',
	        ],
                //                                            'div' => false,
                /* 'label' => false */
            ]);?>
        </div>
    </div>
    <div class="col-md-3">
        <div id="divForeseenHorary" class="form-group input text">
            <label class="control-label" for="subsidiary-id"><?=__('turno previsto')?></label>
            <div class="form-group input text">
                <?php
                echo $this->Form->select('delivery.foreseen_horary', [
                    'morning' => __('manhã'),
                    'afternoon' => __('tarde'),
                    'any' => __('horário comercial'),
                ],  [
                    'value' => is_null($order->delivery['foreseen_horary']) ? 'any' : $order->delivery->foreseen_horary,
                    'class' => 'select2',
                    'style' => 'width: 100%'
                ]);?>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group input">
            <?=$this->Form->control('delivery.freight', [
		'label' => 'frete',
		'type' => 'text',
		'value' => strtolower($this->request->getParam('action')) == 'add' ? $freight : $order->delivery['freight'],
	        'templateVars' => [
		    'otherClass' => 'currency',
	    ]]);?>
        </div>
    </div>
</div>
<div>
    <div id="divDeliveryAddress">
        <?=$this->Element('Address', ['type' => 'delivery']);?>
    </div>
</div>
<?php debug($order->delivery); ?>
