<?php
$datePickerForeseen = null !== $this->request->getQuery('foreseen') ? $this->request->getQuery('foreseen') : '';
$datePickerScheduled = null !== $this->request->getQuery('scheduled') ? $this->request->getQuery('scheduled') : '';
$datePickerDelivery  = null !== $this->request->getQuery('delivery') ? $this->request->getQuery('delivery') : '';
$deliveryVehicle =  null !== $this->request->getQuery('vehicle') ? $this->request->getQuery('vehicle') : '';
$deliveriesCompleted  =  null !== $this->request->getQuery('completeds') ? $this->request->getQuery('completeds') : '';
?>

<div class="row">
    <div class="col-md-3">
	<?=$this->Form->input('foreseen', [
//	    'id' => 'foreseen',
	    'label' => 'data prevista',
	    'data-inputmask-alias' => 'datetime',
	    'data-inputmask-inputformat' => 'dd/mm/yyyy',
	    'value' => $datePickerForeseen,
	    'templateVars' => [
		'otherClass' => 'datepicker date pull-right',
	    ],
	]);?>
    </div>

    <div class="col-md-3">
	<?=$this->Form->input('scheduled', [
//	    'id' => 'datepicker-scheduled',
	    'label' => 'data agendada',
	    'data-inputmask-alias' => 'datetime',
	    'data-inputmask-inputformat' => 'dd/mm/yyyy',
	    'value' => $datePickerScheduled,
	    'templateVars' => [
		'otherClass' => 'datepicker date pull-right',
	    ],
	]);?>
    </div>

    <div class="col-md-3">
	<?=$this->Form->input('delivery', [
//	    'id' => 'datepicker-delivery',
	    'label' => 'data de entrega',
	    'data-inputmask-alias' => 'datetime',
	    'data-inputmask-inputformat' => 'dd/mm/yyyy',
	    'value' => $datePickerDelivery,
	    'templateVars' => [
		'otherClass' => 'datepicker date pull-right',
	    ],
	]);?>
    </div>

    <div class="col-md-3">
        <div class="form-group input text">
	    <label class="control-label" for="delivery-vehicle">veículo</label>
	    <br>
            <?=$this->Form->select('vehicle', $vehicles, [
                'id' => 'vehicle',
                'empty' => 'escolha um veículo',
                'class' => 'select2',
		'required' => false,
                'style' => 'width: 100%',
		'label' => 'veículo',
		'value' => $deliveryVehicle,
            ]);?>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-3">
        <?=$this->Form->control('completeds', [
//            'id' => 'deliveries-completed',
            'type' => 'checkbox',
            'checked' => isset($deliveriesCompleted) ? $deliveriesCompleted : '',
            'class' => 'checkbox-search',
            'label' => '&nbsp;&nbsp;<b>incluir concluídas</b>',
            'escape' => false,
        ])?>
    </div>
</div>
<?php
echo $this->Html->css([
    'AdminLTE./bower_components/select2/dist/css/select2.css',
    'AdminLTE./bower_components/bootstrap-daterangepicker/daterangepicker.css',
    'AdminLTE./bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
], [
    'block' => 'css'
]);
echo $this->Html->script([
    'AdminLTE./bower_components/select2/dist/js/select2.js',
    'AdminLTE./bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min',
    'AdminLTE./bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min',
    '/node_modules/inputmask/dist/jquery.inputmask.min',
    '/node_modules/inputmask/dist/bindings/inputmask.binding',
], [
    'block' => 'script'
]);
echo $this->Html->script([
    'apply-select2',
    'common_masks',
], [
    'block' => 'scriptBottom'
]);
?>
