<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <h3>
                    <?=__('Vendas por fornecedores')?>
                    <small class="pull-right">&nbsp;período 10/10/2020 a 10/12/2020</small>
                </h3>
            </div>

            <!-- /.box-header -->
            <div class="box-body no-padding">

                <div class="panel-group">
                    <div class="panel">

                        <div class="panel-header"> <!-- cabeçalho do pedido -->
                            <div class="col-md-8"><b><?=$this->Paginator->sort('supply_id', ['label' => 'fornecedor']) ?></b></div>
                            <div class="col-md-4 text-right"><b><?=$this->Paginator->sort('order_amount', ['label' => 'valor']) ?></b></div>
                        </div>
                        <?php foreach ($query as $supply): ?>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-md-8"><?=$supply->name?></div>
                                    <div class="col-md-4 text-right"><?= h($this->Number->currency($supply->item_amount));?></div>
                                </div>
                                <!-- </div> -->
                                <?php if ($data['checkbox_details']) :?>
                                    <div class="row">
                                        <div class="col-md-3"></div>
                                        <div class="col-md-1"><b><?=$this->Paginator->sort('number', ['label' => 'pedido']) ?></b></div>
                                        <div class="col-md-1"><b><?=$this->Paginator->sort('date_order', ['label' => 'data']) ?></b></div>
                                        <div class="col-md-5"><b><?=$this->Paginator->sort('title', ['label' => 'produto']) ?></b></div>
                                        <div class="col-md-2 text-right"><b><?=$this->Paginator->sort('price', ['label' => 'valor']) ?></b></div>
                                    </div>
                                    <?php echo '';  //debug($supply->_matchingData); ?>
                                    <?php foreach ($supply->_matchingData as $item) :?>
                                        <div class="row">
                                            <div class="col-md-3"></div>
                                            <div class="col-md-1">9999<?='';// $this->Html->link($item->order->number, ['controller' => 'Orders', 'action' => 'view', $order->id]);?></div>
                                            <div class="col-md-1">10/10/2020<?='';// h($item->order->date_order->i18nFormat());?></div>
                                            <div class="col-md-5"><?= h($item->title)?></div>
                                            <div class="col-md-2 text-right"><?=h($this->Number->currency($item->price));?></div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
