<div class="row">
    <div class="col-md-9">
        <?=$this->Form->control('name')?>
    </div>
    <div class="col-md-3">
        <?=$this->Form->control('cnpj', [
	        'label' => __('CNPJ'),
	        'maxlength' => '18',
	        'templateVars' => [
		        'otherClass' => 'cnpj',
	        ],
	        ])?>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <?=$this->Form->control('phone', [
            'templateVars' => [
                'otherClass' => 'phone',
            ],
            ]);?>
    </div>
    <div class="col-md-3">
        <?=$this->Form->control('phone2', [
            'templateVars' => [
                'otherClass' => 'phone',
            ],
            ])?>
    </div>
    <div class="col-md-3">
        <?=$this->Form->control('mobile', [
            'templateVars' => [
                'otherClass' => 'mobile',
            ],
            ])?>
    </div>
    <div class="col-md-3">
        <?=$this->Form->control('mobile2', [
            'templateVars' => [
                'otherClass' => 'mobile',
            ],
            ])?>
    </div>
</div>
<div class="form-group input text">
    <?php
        echo $this->Form->checkbox('active', ['class' => 'minimal']);
    ?>
    <label class="control-label" for="address-state-id">&nbsp;<?=__('Active')?></label>
</div>
