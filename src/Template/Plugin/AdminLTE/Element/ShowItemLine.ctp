<ul style="list-style-type:none;padding-left: 0px;">
    <?php foreach ($items as $item): ?>
	<li id="li-item-<?=$item->id?>">
	    <?=$this->element('ShowItemDetails', ['item' => $item]);?>
	</li>
    <?php endforeach; ?>
</ul>
