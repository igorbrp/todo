<!-- Content Header (Page header) -->
<!-- <section class="content-header">
    <!-- <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li class="active">Dashboard</li>
         </ol> -->
<!-- </section> -->
<?php if ($_SESSION['Auth']['User']['is_superuser'] || $_SESSION['Auth']['User']['role'] == 'board'): ?>
    <section class="content">
	<div class="row">
            <div class="col-lg-3 col-xs-6">
		<?=$this->cell('Charts::ordersThisMonth');?>
	    </div>
            <div class="col-lg-3 col-xs-6">
		<?=$this->cell('Charts::todayScheduled');?>
	    </div>
            <div class="col-lg-3 col-xs-6">
		<?=$this->cell('Charts::todayForeseen');?>
	    </div>
            <div class="col-lg-3 col-xs-6">
		<?=$this->cell('Charts::uncovereds');?>
	    </div>
	</div>
	<!-- ./row -->
	<div class="row">
            <div class="col-lg-6 col-xs-6">
		<?=$this->cell('Charts::salesThisYear');?>
	    </div>
            <div class="col-lg-6 col-xs-6">
		<?=$this->cell('Charts::salesBySubsidiaries');?>
	    </div>
	</div>
    </section>
<?php endif; ?>

<!-- Morris chart -->
<?php echo ''; // $this->Html->css('AdminLTE./bower_components/morris.js/morris', ['block' => 'css']); ?>
<!-- jvectormap -->
<?php echo ''; // $this->Html->css('AdminLTE./bower_components/jvectormap/jquery-jvectormap', ['block' => 'css']); ?>
<!-- Date Picker -->
<?php echo ''; // $this->Html->css('AdminLTE./bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min', ['block' => 'css']); ?>
<!-- Daterange picker -->
<?php echo ''; // $this->Html->css('AdminLTE./bower_components/bootstrap-daterangepicker/daterangepicker', ['block' => 'css']); ?>
<!-- bootstrap wysihtml5 - text editor -->
<?php echo ''; // $this->Html->css('AdminLTE./plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min', ['block' => 'css']); ?>

<!-- jQuery UI 1.11.4 -->
<?php echo $this->Html->script('AdminLTE./bower_components/jquery-ui/jquery-ui.min', ['block' => 'script']); ?>
<!-- Morris.js charts -->
<?php echo $this->Html->script('AdminLTE./bower_components/raphael/raphael.min', ['block' => 'script']); ?>
<?php echo $this->Html->script('AdminLTE./bower_components/morris.js/morris.min', ['block' => 'script']); ?>
<!-- Sparkline -->
<?php echo $this->Html->script('AdminLTE./bower_components/jquery-sparkline/dist/jquery.sparkline.min', ['block' => 'script']); ?>
<!-- jvectormap -->
<?php echo $this->Html->script('AdminLTE./plugins/jvectormap/jquery-jvectormap-1.2.2.min', ['block' => 'script']); ?>
<?php echo $this->Html->script('AdminLTE./plugins/jvectormap/jquery-jvectormap-world-mill-en', ['block' => 'script']); ?>
<!-- jQuery Knob Chart -->
<?php echo $this->Html->script('AdminLTE./bower_components/jquery-knob/dist/jquery.knob.min', ['block' => 'script']); ?>
<!-- daterangepicker -->
<?php echo $this->Html->script('AdminLTE./bower_components/moment/min/moment.min', ['block' => 'script']); ?>
<?php echo $this->Html->script('AdminLTE./bower_components/bootstrap-daterangepicker/daterangepicker', ['block' => 'script']); ?>
<!-- datepicker -->
<?php echo $this->Html->script('AdminLTE./bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min', ['block' => 'script']); ?>
<!-- ChartJS -->
<?php $this->Html->script('AdminLTE./bower_components/chart.js/Chart', ['block' => 'script']); ?>
<!-- Bootstrap WYSIHTML5 -->
<?php echo $this->Html->script('AdminLTE./plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min', ['block' => 'script']); ?>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<?php echo $this->Html->script('AdminLTE.pages/dashboard', ['block' => 'script']); ?>
<!-- AdminLTE for demo purposes -->
<?php echo ''; // $this->Html->script('AdminLTE.demo', ['block' => 'script']); ?>

<?php echo $this->Html->script('chart-sales-this-year', ['block' => 'script']);?>
<?php echo ''; //$this->Html->script('chart-orders-this-year', ['block' => 'script']);?>
<?php echo $this->Html->script('chart-sales-by-subsidiaries', ['block' => 'script']);?>

<?php $this->start('scriptBottom'); ?>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
 $.widget.bridge('uibutton', $.ui.button);
</script>
<?php  $this->end(); ?>
