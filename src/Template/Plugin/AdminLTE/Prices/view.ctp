<section class="content-header">
  <h1>
    Price
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
      <?=$this->element('MenuHeader')?>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Name') ?></dt>
            <dd><?= h($price->name) ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($price->id) ?></dd>
            <dt scope="row"><?= __('Sequence') ?></dt>
            <dd><?= $this->Number->format($price->sequence) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($price->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($price->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
