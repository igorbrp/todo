<section class="content-header">
    <h1>
        <?=__('Ajuste de estoque')?>
	<small><?php echo $stockAdjustment->product->title; ?></small>
    </h1>
    <ol class="breadcrumb">
	<?=$this->element('MenuHeader', ['noprint' => true])?>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
	<div class="col-md-12">
	    <div class="box box-solid">
		<div class="box-header with-border">
		    <i class="fa fa-info"></i>
		    <h3 class="box-title"><?php echo __('Information'); ?></h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		    <dl class="dl-horizontal">
			<dt scope="row"><?= __('Supply') ?></dt>
			<dd><?= $stockAdjustment->has('supply') ? $this->Html->link($stockAdjustment->supply->name, ['controller' => 'Supplies', 'action' => 'view', $stockAdjustment->supply->id]) : '' ?></dd>
			<dt scope="row"><?= __('Product') ?></dt>
			<dd><?= $stockAdjustment->has('product') ? $this->Html->link($stockAdjustment->product->title, ['controller' => 'Products', 'action' => 'view', $stockAdjustment->product->id]) : '' ?></dd>
			<?php $color = $stockAdjustment->qtd > 0 ? 'green' : 'red'; ?>
			<dt scope="row"><?= __('Qtd') ?></dt>
			<dd style="color:<?=$color?>;font-weight:bold;?>"><?= $this->Number->format($stockAdjustment->qtd) ?></dd>
		    </dl>
		</div>
	    </div>
	</div>
    </div>

    <div class="row">
	<div class="col-md-12">
	    <div class="box box-solid">
		<div class="box-header with-border">
		    <i class="fa fa-text-width"></i>
		    <h3 class="box-title"><?= __('Comentário') ?></h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		    <?= $this->Text->autoParagraph($stockAdjustment->comment); ?>
		</div>
	    </div>
	</div>
    </div>
</section>
