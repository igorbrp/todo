<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\StockAdjustment $stockAdjustment
 */

$template = $this->template == 'add' ? __('Add') : __('Edit');
?>

<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	<?=__('Ajuste de estoque')?>
	<small>
            <?php if(strtolower($this->request->getParam('action')) == 'edit') : ?>
                <?=$product_title;?>
            <?php else : ?>
                <?='novo'?> <!-- TRADUZIR -->
            <?php endif; ?>
        </small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <span><?=__('Identification'); ?></span>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create($stockAdjustment, ['role' => 'form']); ?>
                <div class="box-body">
		    <div class="row">
			<div class="col-md-5">
			    <?php
			    if (!empty($supplies)) {
				$suppliesCount = count($supplies->toArray());
			    } else {
				$suppliesCount = '0';
			    }
			    ?>
			    <div class="form-group input text">
				<label class="control-label" for="sypply-id"><?=__('fornecedor') . ' <span style="color:gray">(' . $suppliesCount . ')</span>'?></label>
				<?php
				echo $this->Form->select('supply_id', $supplies, [
				    'id' => 'supply-id', 
				    'empty' => __('escolha...'),
				    'class' => 'select2',
				    'style' => 'width: 100%',
				    'disabled' => strtolower($this->request->getParam('action')) == 'edit' ? true : false,
				]);
				?>
			    </div>
			</div>
			<div class="col-md-5">
			    <?php
			    if (!empty($products)) {
				$productsCount = $products->count();
			    } else {
				$productsCount = '0';
			    }
			    ?>
			    <div class="form-group input text">
				<label class="control-label" for="product-id">
				    <?=__('produto') . ' <span id="span-products-count" style="color:gray">(' . $productsCount . ')</span>'?>
				</label>
				<?php
				echo $this->Form->select('product_id', $products, [
				    'id' => 'product-id', 
				    'empty' => __('escolha...'),
				    'class' => 'select2',
				    'style' => 'width: 100%',
				    'disabled' => strtolower($this->request->getParam('action')) == 'edit' ? true : false,
				]);
				?>
			    </div>
			</div>
			<div class="col-md-2">
			    <div class="form-group input text">
				<?php
				echo $this->Form->control('current_stock', [
				    'id' => 'current-stock',
				    'readonly',
				    'label' => 'estoque atual'
				]);
				?>
			    </div>
			</div>
		    </div>
		    <div class="row">
			<div class="col-md-2">
			    <div class="form-group input text">
				<?php
				echo $this->Form->control('qtd', ['label' => 'qtd']);
				?>
			    </div>
			</div>
		    </div>
		    <div class="row">
			<div class="col-md-12">
			    <div class="form-group input text">
				<?php 
				echo $this->Form->control('comment', ['label' => 'Comentário']);
				?>
			    </div>
			</div>
		    </div>
		</div>
                <!-- /.box-body -->

		<div class="text-right">
                    <?php echo $this->Form->submit(__('Submit')); ?>
		</div>
                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>
<?php
echo $this->Html->css([
    'AdminLTE./bower_components/select2/dist/css/select2.css'], [
        'block' => 'css'
]);
echo $this->Html->script([
    'AdminLTE./bower_components/select2/dist/js/select2.js',
], [
    'block' => 'script'
]);
echo $this->Html->script([
    'apply-select2',
    'stock-adjustments',
    'js-base',
], [
    'block' => 'scriptBottom'
]);

?>
