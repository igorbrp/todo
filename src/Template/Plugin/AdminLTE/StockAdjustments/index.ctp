<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Ajuste de estoque')?>
        <small><?=''; //__('List'); ?></small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true, 'nodelete' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
                        <?=$this->element('Search')?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <!-- <th scope="col"> --><?=''; // $this->Paginator->sort('id') ?><!-- </th> -->
                                <th scope="col"><?= $this->Paginator->sort('qtd', ['label' => 'qtd']) ?></th>
                                <th scope="col"><?= $this->Paginator->sort('supply_id', ['label' => 'fornecedor']) ?></th>
                                <th scope="col"><?= $this->Paginator->sort('product_id', ['label' => 'produto']) ?></th>
                                <th scope="col" class="actions text-right"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($stockAdjustments as $stockAdjustment): ?>
				<tr>
                                    <!-- <td> --><?=''; // $this->Number->format($stockAdjustment->id) ?><!-- </td> -->
				    <?php $color = $stockAdjustment->qtd > 0 ? 'green' : 'red'; ?>
                                    <td style="color:<?=$color?>;font-weight:bold;?>"><?= $this->Number->format($stockAdjustment->qtd) ?></td>
                                    <td><?= $stockAdjustment->has('supply') ? $this->Html->link($stockAdjustment->supply->name, ['controller' => 'Supplies', 'action' => 'view', $stockAdjustment->supply->id]) : '' ?></td>
                                    <td><?= $stockAdjustment->has('product') ? $this->Html->link($stockAdjustment->product->title, ['controller' => 'Products', 'action' => 'view', $stockAdjustment->product->id]) : '' ?></td>
                                    <td class="actions text-right">
                                        <?=$this->element('Actions', ['cod_id' => $stockAdjustment->id,])?>
                                    </td>
				</tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
	</div>
    </div>
</section>
<?php
$this->Html->script(['index', 'js-base'], ['block' => 'scriptBottom']);
?>
