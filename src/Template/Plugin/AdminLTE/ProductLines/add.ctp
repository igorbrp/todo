<?php
    /**
     * @var \App\View\AppView $this
     * @var \Cake\Datasource\EntityInterface $productLine
     */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	    <?=__('Product Line')?>
	    <small><?= __('Add'); ?></small>
    </h1>
    <?=$this->element('MenuHeader')?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <?php echo $this->Form->create($productLine, ['role' => 'form']); ?>
            <div class="nav-tabs-custom">
	            <ul class="nav nav-tabs">
	                <li class="active">
                        <a href="#identity" data-toggle="tab" aria-expanded="true"><?=__('Identification')?></a>
                    </li>
	                <li class="">
                        <a href="#properties" data-toggle="tab" aria-expanded="false"><?=__('Properties')?></a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="identity">
		                <!-- identity content -->
                        <?php
                            echo $this->Form->select('supply_id', $supplies, [
                                'class' => 'select2',
                                'style' => 'width: 100%',
                            ]);
                            echo $this->Form->control('name');
                        ?>
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="properties">
		                <!-- properties content -->
                        <?php
                            echo $this->Form->control('deadline');
                            echo $this->Form->control('working_days');
                            echo $this->Form->control('deadline_inherited');
                            echo $this->Form->control('maximum_discount');
                            echo $this->Form->control('maximum_discount_inherited');
                        ?>
                    </div>
                </div>
                <!-- /.tab-content -->
                <?php echo $this->Form->submit(__('Submit')); ?>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>
<?php
    echo $this->Html->css([
        'AdminLTE./bower_components/select2/dist/css/select2.css'], [
            'block' => 'css'
        ]);
    echo $this->Html->script([
        'AdminLTE./bower_components/select2/dist/js/select2.js',
        'apply-select2',
    ], [
        'block' => 'script'
    ]);
?>
