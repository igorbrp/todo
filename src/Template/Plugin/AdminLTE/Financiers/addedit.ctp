<?php
use Cake\Utility\Inflector;
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Financier $financier
 */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	<?=__('Instituições financeiras')?>
	<small>
            <?php if(strtolower($this->request->getParam('action')) == 'edit') : ?>
                <?=$financier->name;?>
            <?php else : ?>
                <?='novo'?> <!-- TRADUZIR -->
            <?php endif; ?>
        </small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header with-border">
                    <span><?=__('Identification'); ?></span>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php echo $this->Form->create($financier, ['role' => 'form']); ?>
                <div class="box-body">
                    <?php
                    echo $this->Form->control('code');
                    echo $this->Form->control('name');
                    //                        echo $this->Form->control('payment_method_id');
                    ?>
                    <div>
                        <label class="control-label" for="subsidiary-id"><?=__('Método de pagamento')?></label>
                        <div class="form-group input text">
                            <?php
                            echo $this->Form->select('payment_method_id', $paymentMethods, [
                                'empty' => __('choose...'),
                                'class' => 'select2',
                                'style' => 'width: 100%']);
                            ?>
                        </div>
                    </div>
                    <?php

                    echo $this->Form->control('url');
                    echo $this->Form->control('active');
                    ?>
                </div>
                <!-- /.box-body -->

                <?php echo $this->Form->submit(__('Submit')); ?>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>
<?php
echo $this->Html->css([
    'AdminLTE./bower_components/select2/dist/css/select2.css'], [
        'block' => 'css'
]);
echo $this->Html->script([
    'AdminLTE./bower_components/select2/dist/js/select2.js',
], [
    'block' => 'script'
]);
echo $this->Html->script([
    'apply-select2',
], [
    'block' => 'scriptBottom'
]);

?>
