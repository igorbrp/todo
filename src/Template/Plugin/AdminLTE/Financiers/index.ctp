<style>
 .user-mini {
     margin:auto 10px auto auto;
     width: 25px;
     height: 25px;
 }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Instituições financeiras')?>
        <small>&nbsp;</small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
                        <?=$this->element('Search')?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col"><label><input id="chk-all" type="checkbox" class="minimal" onchange="chkAllChange();"></label></th>
                                <th scope="col" width="10%"><?= $this->Paginator->sort('code', 'código') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('payment_method_id', 'método de pagamento') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('active', 'ativa') ?></th>
                                <th scope="col" class="actions text-right"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($financiers as $financier): ?>
                                <tr>
                                    <td>
                                        <?=$this->Form->checkbox('id', [
                                            'id' => 'chk-b',
                                            'name' => 'chkb',
                                            'class' => 'chk-box',
                                            'value' => $financier->id,
                                            'onclick' => 'checkOrUncheck(this)'
                                        ]);?>
                                    </td>
                                    <td><?= h($financier->code) ?></td>
                                    <td><?= h($financier->name) ?></td>
                                    <td><?= $this->Html->link($financier->payment_method->name, ['controller' => 'PaymentMethods', 'action' => 'view', $financier->payment_method->id])?></td>
                                    <td><?= h(h($financier->active == 1 ? 'SIM' : 'NÃO')) ?></td>
                                    <td class="actions text-right">
                                        <?=$this->element('Actions', ['cod_id' => $financier->id,])?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
	<div class="col-sm-5">
	    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
		<?php echo $this->Paginator->counter(['format' => 'range']);?>
	    </div>
	</div>
	<div class="col-sm-7">
	    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
		<!-- Paginator -->
		<?=
		$this->element('Paginator');
		?>
	    </div>
	</div>
    </div>
</section>
<?php
echo $this->Html->script([
    'js-base',
    'index',
], [
    'block' => 'scriptBottom'
]);
?>
