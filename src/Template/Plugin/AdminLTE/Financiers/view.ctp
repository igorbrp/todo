<section class="content-header">
    <h1>
	Financeira
	<small><?php echo $financier->name; ?></small>
    </h1>
    <ol class="breadcrumb">
	<?=$this->element('MenuHeader', ['noprint' => true])?>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
	<div class="col-md-12">
	    <div class="box box-solid">
		<div class="box-header with-border">
		    <i class="fa fa-info"></i>
		    <h3 class="box-title"><?php echo __('Information'); ?></h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		    <dl class="dl-horizontal">
			<dt scope="row"><?= __('Code') ?></dt>
			<dd><?= h($financier->code) ?></dd>
			<dt scope="row"><?= __('Name') ?></dt>
			<dd><?= h($financier->name) ?></dd>
			<dt scope="row"><?= __('Url') ?></dt>
			<dd><?= h($financier->url) ?></dd>
			<dt scope="row"><?= __('Método de pagamento') ?></dt>
			<dd><?= h($financier->payment_method->name) ?></dd>
			<dt scope="row"><?= __('Active') ?></dt>
			<dd><?= $financier->active ? __('Yes') : __('No'); ?></dd>
		    </dl>
		</div>
	    </div>
	</div>
    </div>

</section>
