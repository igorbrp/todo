<section class="content-header">
    <h1>
        <?=__('Subsidiary')?>
        <small><?php echo $subsidiary->name; ?></small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Name') ?></dt>
            <dd><?= h($subsidiary->name) ?></dd>
            <dt scope="row"><?= __('Cnpj') ?></dt>
            <dd><?= h($subsidiary->cnpj) ?></dd>
            <dt scope="row"><?= __('Phone') ?></dt>
            <dd><?= h($subsidiary->phone) ?></dd>
            <dt scope="row"><?= __('Mobile') ?></dt>
            <dd><?= h($subsidiary->mobile) ?></dd>
            <dt scope="row"><?= __('Fax') ?></dt>
            <dd><?= h($subsidiary->fax) ?></dd>
            <dt scope="row"><?= __('Ativa') ?></dt>
            <dd><?= $subsidiary->active ? __('Yes') : __('No'); ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-share-alt"></i>
          <h3 class="box-title"><?= __('Endereço') ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <?php if (!empty($subsidiary->address)): ?>
          <table class="table table-hover">
              <tr>
                    <th scope="col"><?= __('CEP') ?></th>
                    <th scope="col"><?= __('estado') ?></th>
                    <th scope="col"><?= __('cidade') ?></th>
                    <th scope="col"><?= __('Neighborhood') ?></th>
                    <th scope="col"><?= __('Thoroughfare') ?></th>
                    <th scope="col"><?= __('Number') ?></th>
                    <th scope="col"><?= __('Complement') ?></th>
                    <th scope="col"><?= __('Reference') ?></th>
              </tr>
              <?php echo ''; // foreach ($subsidiary->address as $subsidiaryAddresses): ?>
              <tr>
                    <td><?= h($subsidiary->address->postal_code) ?></td>
                    <td><?= h($subsidiary->address->state_id) ?></td>
                    <td><?= h($subsidiary->address->city) ?></td>
                    <td><?= h($subsidiary->address->neighborhood) ?></td>
                    <td><?= h($subsidiary->address->thoroughfare) ?></td>
                    <td><?= h($subsidiary->address->number) ?></td>
                    <td><?= h($subsidiary->address->complement) ?></td>
                    <td><?= h($subsidiary->address->reference) ?></td>
              </tr>
              <?php echo ''; // endforeach; ?>
          </table>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</section>
