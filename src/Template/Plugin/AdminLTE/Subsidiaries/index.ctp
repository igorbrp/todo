<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Subsidiaries')?>
        <small>&nbsp;<?php echo ''; // __('List'); ?></small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
                        <?=$this->element('Search')?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col"><label><input id="chk-all" type="checkbox" class="minimal" onchange="chkAllChange();"></label></th>
                                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('cnpj') ?></th>

				<?php if ($this->request->session()->read('Auth.User.role') == 'board') :?>
                                    <th scope="col" class="text-center"><?= $this->Paginator->sort('active', 'ativa') ?></th>
				<?php endif; ?>

                                <th scope="col" class="actions text-right"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody id="tbody">
                            <?php foreach ($subsidiaries as $subsidiary): ?>
                                <tr>
                                    <td>
                                        <?=$this->Form->checkbox('id', [
                                            'id' => 'chk-b',
                                            'name' => 'chkb',
                                            'class' => 'chk-box',
                                            'value' => $subsidiary->id,
                                            'onclick' => 'checkOrUncheck(this)'
                                        ]);?>
                                    </td>
                                    <td>
                                        <span class="image" style="display:inline-block">
                                            <?php if (empty($subsidiary->file)): ?>
                                                <?=$this->Html->image('no-image.jpg', ['class' => 'user-mini img-circle', 'style' => 'width:25px;height:25px']);  ?>
                                            <?php else: ?>
                                                <?=$this->Html->image($subsidiary->file['path'] . DS . $subsidiary->file['hash'] . '.mini.' . $subsidiary->file['extension'], ['pathPrefix' => '', 'class' => 'user-mini img-circle', 'style' => 'width:25px;height:25px;margin: auto 10px auto auto;']);  ?>
                                            <?php endif; ?>
                                            <?= h($subsidiary->name) ?>
                                        </span>
                                    </td>
                                    <td><?= h(empty($subsidiary->cnpj) ? '' :  $this->Format->mask($subsidiary->cnpj, 'cnpj')) ?></td>

				    <?php if ($this->request->session()->read('Auth.User.role') == 'board') :?>
					<td class="text-center">
                                            <?=$this->Form->create('', [
						'id' => 'change-active-' . $subsidiary->id,
						'action' => 'change-activity',
                                            ]);?>
                                            <span style="display:none;"><?=$this->Form->input('change-id', ['value' => $subsidiary->id]);?></span>
                                            <?=$this->Form->checkbox('id', [
						'id' => 'chk-activity',
						'name' => 'chk_activity',
						'class' => 'chk-box',
						'checked' => $subsidiary->active == 1 ? true : false,
						'value' => $subsidiary->id,
						'onclick' => 'document.getElementById("change-active-' . $subsidiary->id . '").submit()'
                                            ]);?>
                                            <?=$this->Form->end(); ?>
					</td>
				    <?php endif; ?>

				    
                                    <td class="actions text-right">
                                        <?=$this->element('Actions', ['cod_id' => $subsidiary->id,])?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
	<div class="col-sm-5">
	    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
		<?php echo $this->Paginator->counter(['format' => 'range']);?>
	    </div>
	</div>
	<div class="col-sm-7">
	    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
		<!-- Paginator -->
		<?=
		$this->element('Paginator');
		?>
	    </div>
	</div>
    </div>
</section>
<?php
echo $this->Html->script([
    'js-base',
    'index',
], [
    'block' => 'scriptBottom'
]);
?>
