<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $subsidiary
 */
$template = $this->template == 'add' ? __('Add') : __('Edit');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	<?=__('Subsidiary')?>
	<small>
            <?php if(strtolower($this->request->getParam('action')) == 'edit') : ?>
                <?=$subsidiary->name;?>
            <?php else : ?>
                <?='novo'?> <!-- TRADUZIR -->
            <?php endif; ?>
        </small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <?php echo $this->Form->create($subsidiary, ['role' => 'form']); ?>
            <div class="nav-tabs-custom">
	        <ul class="nav nav-tabs">
	            <li class="active">
                        <a href="#identity" data-toggle="tab" aria-expanded="true"><?=__('Identification')?></a>
                    </li>
	            <li class="">
                        <a href="#address" data-toggle="tab" aria-expanded="false"><?=__('Address')?></a>
                    </li>
	        </ul>

	        <div class="tab-content">
	            <div class="tab-pane active" id="identity">
		        <!-- identity content -->
                        <!-- form start -->
                        <div class="row">
                            <div class="col-md-9">
                                <?=$this->Form->control('name')?>
                            </div>
                            <div class="col-md-3">
                                <?=$this->Form->control('cnpj', [
	                            'label' => __('CNPJ'),
	                            'maxlength' => '18',
	                            'templateVars' => [
		                        'otherClass' => 'cnpj',
	                            ],
	                        ])?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <?=$this->Form->control('phone', [
                                    'templateVars' => [
                                        'otherClass' => 'phone',
                                    ],
                                ]);?>
                            </div>
                            <div class="col-md-3">
                                <?=$this->Form->control('phone2', [
                                    'templateVars' => [
                                        'otherClass' => 'phone',
                                    ],
                                ])?>
                            </div>
                        </div>
                        <div class="form-group input text">
                            <?php
                            echo $this->Form->checkbox('active', ['class' => 'minimal']);
                            ?>
                            <label class="control-label" for="address-state-id">&nbsp;<?=__('Active')?></label>
                        </div>
                        <?='';//$this->Element('BranchIdentity');?>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="address">
                        <!-- address content -->
                        <?=$this->Element('Address');?>
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->

                <div class="text-right">
                    <?php echo $this->Form->submit(__('Submit')); ?>
                </div>
                <?php echo $this->Form->end(); ?>

            </div>
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col-md -->
    </div>
    <!-- /.row -->
</section>
<?php
echo $this->Html->css([
    'AdminLTE./bower_components/select2/dist/css/select2.css'], [
        'block' => 'css'
]);
echo $this->Html->script([
    'AdminLTE./bower_components/select2/dist/js/select2.js',
    '/node_modules/inputmask/dist/jquery.inputmask.min',
    '/node_modules/inputmask/dist/bindings/inputmask.binding',
], [
    'block' => 'script'
]);
echo $this->Html->script([
    'addresses',
    'apply-select2',
    'common_masks',
], [
    'block' => 'scriptBottom'
]);
?>
