<style>
 .user-mini {
     margin:auto 10px auto auto;
     width: 25px;
     height: 25px;
 }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Items')?>
        <small>&nbsp;</small>
    </h1>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
                        <?=$this->element('Search', ['optionSearch' => 'ItemsSearch', 'placeholder' => 'pesquise por produto, código do produto ou número do pedido'])?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body no-padding">

                    <div class="panel-group">
			<div class="panel">
			    
			    <div class="row panel-heading">
				<div class="col-md-1"></div>
				<div class="col-md-1"><b><?= $this->Paginator->sort('qtd', ['label' => 'qtd']);?></b></div>
				<div class="col-md-1"><b><?= $this->Paginator->sort('order_id', ['label' => 'pedido'])?></b></div>
				<div class="col-md-3"><b><?= $this->Paginator->sort('title', ['label' => 'produto']);?></b></div>
				<div class="col-md-3"><b><?= $this->Paginator->sort('supply_name', ['label' => 'fornecedor']) ?></b></div>
                                <div class="col-md-2 text-right"><b><?= $this->Paginator->sort('price', ['label' => 'preço']) ?></b></div>
				<div class="col-md-1 text-right"><b>origem</b></div>
			    </div>
			</div>

		    </div>

		    <div class="panel-items" id="panel-items">
			<ul style="list-style-type:none;padding-left: 0px;">
			    <?php foreach ($items as $item): ?>
				<li id="li-item-<?=$item->id?>">
				    <?=$this->element('ShowItemDetails', ['item' => $item]);?>
				</li>
			    <?php endforeach; ?>
			</ul>
		    </div>

                </div>
                <!-- /.box-body -->
	    </div>
	    <!-- /.box -->
        </div>
	<!-- /.col-xs-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
	<div class="col-sm-5">
	    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
		<?php echo $this->Paginator->counter(['format' => 'range']);?>
	    </div>
	</div>
	<div class="col-sm-7">
	    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
		<!-- Paginator -->
		<?=
		$this->element('Paginator');
		?>
	    </div>
	</div>
    </div>
</section>

<div id="divModalFormInvoice" style="display:none">
    <div class="row">
	<div class="col-md-6">
	    <?php
	    echo $this->Form->control('modal_invoice', ['label' => 'nota fiscal']);
	    ?>
	</div>
    </div>
</div>
<div id="divModalFormDispatched" style="display:none">
    <div class="row">
	<div class="col-md-6">
	    <div class="form-group required">
		<label class="control-label" for="modal-datepicker">data:&nbsp;</label>
		<br>
		<div class="input-group date">
		    <div class="input-group-addon">
			<i class="fa fa-calendar">
			</i>
		    </div>
		    <input type="text" class="form-control pull-right" id="modal-datepicker">
		</div>
	    </div>
	</div>
	<div class="col-md-6">
	    <div class="form-group required">
		<label class="control-label" for="modal_scheduled_horary">turno previsto:&nbsp;</label>
		<div class="form-group input required">
		    <?php
		    echo $this->Form->select('modal_horary', [
			'morning' => __('manhã'),
			'afternoon' => __('tarde'),
			'any' => __('horário comercial'),
		    ],  [
			'id' => 'modal-horary',
			//			    'value' => 'any',
			'class' => 'select2 select2-modal',
			'style' => 'width: 100%'
		    ]);?>
		</div>
	    </div>
	</div>
    </div>
    <div class="row">
	<div class="col-md-6">
	    <div class="form-group required">
		<label class="control-label" for="modal-vehicle">veículo:&nbsp;</label><br>
		<div class="form-group input required">
		    <?php echo $this->Form->select('modal_vehicle', $vehicles, [
			'id' => 'modal-vehicle',
			//			'class' => 'select2 select2-modal',
			'empty' => 'escolha', // TRADUZIR
			'width' => '100%',
		    ]);?>
		</div>
	    </div>
	</div>
    </div>
</div>

<?php
echo $this->Html->css([
    'AdminLTE./bower_components/bootstrap-daterangepicker/daterangepicker.css',
    'AdminLTE./bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
], [
    'block' => 'css'
]);

echo $this->Html->script([
    'AdminLTE./bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min',
    'AdminLTE./bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.pt-BR.min',
    'AdminLTE./bower_components/jquery-sparkline/dist/jquery.sparkline.min',
    '/node_modules/inputmask/dist/jquery.inputmask.min',
    '/node_modules/inputmask/dist/bindings/inputmask.binding',
], [
    'block' => 'script'
]);

echo $this->Html->script([
    'js-base',
    'index',
    'order-item',
], [
    'block' => 'scriptBottom'
]);
?>
