<?php
    /**
     * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
     *
     * Licensed under The MIT License
     * Redistributions of files must retain the above copyright notice.
     *
     * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
     * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
     */
?>
<h1>src/Template/Plugin/AdminLTE/Users/add.ctp</h1>
<section class="content-header">
    <h1>
	    User
	    <small><?php echo __('Add'); ?></small>
    </h1>
    <?=$this->element('MenuHeader')?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-body">
                    <div id="target_img" class="text-center">
                        <?php echo $this->Html->image('no-image.jpg', ['alt' => __('Image not available.'), 'class' => 'img-thumbnail', 'id' => 'preview_image']); ?>
                    </div>
                    <div class="text-right" style="margin-top:5px">
                        <button class="btn btn-primary btn-xs" id="bt-upload"><?=__('change image')?></button>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.col-md -->
	    <div class="col-md-9">
            <!-- general form elements -->
            <div class="box box-primary">
		        <div class="box-header with-border">
		            <h3 class="box-title"><?php echo __('Form'); ?></h3>
		        </div>

		        <!-- /.box-header -->
		        <!-- form start -->
                <?= $this->Form->create(${$tableAlias}); ?>
		        <div class="box-body">
		            <?php
                        echo $this->Form->control('username', ['label' => __('username')]);
                        echo $this->Form->control('email', ['label' => __('email')]);
                        echo $this->Form->control('password', ['label' => __('password')]);
		                echo $this->Form->control('confirm_password', ['label' => __('confirm password'), 'type' => 'password']);
                        echo $this->Form->control('nick_name', ['label' => __('name')]);
                        echo $this->Form->control('role', ['label' => __('role')]);
                        echo $this->Form->control('is_active', [
                            'type' => 'checkbox',
                            'label' => __('Active')
                        ]);
		            ?>
		        </div>
		        <!-- /.box-body -->
                <div style="display:none;">
                    <?php echo ''; //$this->Form->file('file.submittedfile', ['id' => 'submittedfile', 'class' => 'upload']); // Pay attention here! ?>
                </div>

                <?= $this->Form->button(__d('CakeDC/Users', 'Submit')) ?>
		        <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
	    </div>
    </div>
    <!-- /.row -->
</section>
<?php echo $this->Html->script('JavaScript-Load-Image-master/js/load-image.all.min'); ?>
<?php echo $this->Html->script('load-image'); ?>
