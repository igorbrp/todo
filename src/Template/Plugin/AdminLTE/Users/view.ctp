<?php
    /**
     * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
     *
     * Licensed under The MIT License
     * Redistributions of files must retain the above copyright notice.
     *
     * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
     * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
     */

    $user = ${$tableAlias};
?>
<section class="content-header">
    <h1>
        User
        <small><?php echo __('View'); ?></small>
    </h1>
    <?=$this->element('MenuHeader')?>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-info"></i>
                    <h3 class="box-title"><?php echo __('Information'); ?></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <dl class="dl-horizontal">
                        <dt scope="row"><?= __('username') ?></dt>
                        <dd><?= h($user->username) ?></dd>
                        <dt scope="row"><?= __('email') ?></dt>
                        <dd><?= h($user->email) ?></dd>
                        <dt scope="row"><?= __('name') ?></dt>
                        <dd><?= h($user->first_name) ?></dd>
                        <dt scope="row"><?= __('role') ?></dt>
                        <dd><?= h($user->role) ?></dd>
                        <dt scope="row"><?= __('Created') ?></dt>
                        <dd><?= h($user->created) ?></dd>
                        <dt scope="row"><?= __('Modified') ?></dt>
                        <dd><?= h($user->modified) ?></dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>

</section>
