<?php
/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
?>

<section class="content-header">
    <h1>
	Users
	<small><?php echo __('Index'); ?></small>
    </h1>
    <?=$this->element('MenuHeader', ['nodelete' => true])?>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
	<div class="col-xs-12">
	    <div class="box">
		<div class="box-header">
		    <div>
			<form action="<?php echo $this->Url->build(); ?>" method="POST">
			    <div class="input-group input-group-sm" style="width: 150px;">
				<input type="text" name="table_search" class="form-control pull-right" placeholder="<?php echo __('Search'); ?>">

				<div class="input-group-btn">
				    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
				</div>
			    </div>
			</form>
		    </div>
		</div>
		<!-- /.box-header -->

		<div class="box-body table-responsive no-padding">
		    <table class="table table-hover">
			<thead>
			    <tr>
				<th scope="col"><?= $this->Paginator->sort('username') ?></th>
				<th scope="col"><?= $this->Paginator->sort('email', ['label' => __('email')]) ?></th>
                                <th scope="col"><?= $this->Paginator->sort('first_name', [__('First name')]) ?></th>
				<th scope="col"><?= $this->Paginator->sort('role') ?></th>
				<th scope="col" class="actions text-right"><?= __('Actions') ?></th>
			    </tr>
			</thead>
			<tbody>
                            <?php foreach (${$tableAlias} as $user) : ?>
				<tr>
				    <td><?= h($user->username) ?></td>
				    <td><?= h($user->email) ?></td>
				    <td><?= h($user->first_name) ?></td>
				    <td><?= h($user->role) ?></td>
				    <td class="actions text-right">
                                        <?=$this->element('Actions', ['cod_id' => $user->id,])?>
				    </td>
				</tr>
			    <?php endforeach; ?>
			</tbody>
		    </table>
		</div>
		<!-- /.box-body -->
	    </div>
	    <!-- /.box -->
	</div>
    </div>
</section>
