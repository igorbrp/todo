<?php
    /**
     * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
     *
     * Licensed under The MIT License
     * Redistributions of files must retain the above copyright notice.
     *
     * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
     * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
     */

//    use Cake\Core\Configure;
    $this->layout = 'AdminLTE.login';
    $this->Form->setTemplates([
        'inputContainer' => '<div class="form-group input input-group {{type}}{{required}}">{{content}}</div>',
        'formGroup' => '<span class="input-group-addon"><i class="fa fa-{{icon}}"></i></span>{{input}}',
        'input' => '<input type="{{type}}" name="{{name}}"{{attrs}}/>',
    ]);
?>

<?= $this->Flash->render('auth') ?>
<?= $this->Form->create() ?>
<div class="form-group has-feedback">
    <?= $this->Form->control('username', [
	    'placeholder' => __('username'),
	    'templateVars' => [
	        'icon' => 'user'
	    ],
        'required' => true
        ]);?>
    <?= $this->Form->control('password', [
	    'placeholder' => __('password'),
	    'templateVars' => [
	        'icon' => 'lock'
	    ],
        'required' => true
        ]);?>
</div>
<div class="row">
    <div class="col-xs-8">
	    <!-- <div class="checkbox icheck">
	         <label>
	         <input type="checkbox"> Remember Me
	         </label>
	         </div> -->
    </div>
    <!-- /.col -->
    <div class="col-xs-4">
	    <?= $this->Form->button(__('Login'), ['btn btn-primary btn-block btn-flat']);?>
    </div>
    <!-- /.col -->
</div>
<?= $this->Form->end() ?>
