<?php
    /**
     * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
     *
     * Licensed under The MIT License
     * Redistributions of files must retain the above copyright notice.
     *
     * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
     * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
     */
    use Cake\Core\Configure;

    $Users = ${$tableAlias};
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        User
	    <small><?php echo __('Edit'); ?></small>
    </h1>
    <?=$this->element('MenuHeader')?>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
	    <div class="col-md-12">
            <!-- general form elements -->
            <div class="box box-primary">
		        <div class="box-header with-border">
		            <h3 class="box-title"><?php echo __('Form'); ?></h3>
		        </div>
		        <!-- /.box-header -->
		        <!-- form start -->
                <?= $this->Form->create($Users); ?>
		        <div class="box-body">
		            <?php
                        echo $this->Form->control('username', ['label' => __('username')]);
                        echo $this->Form->control('email', ['label' => __('email')]);
                        echo $this->Form->control('password', ['label' => __('password')]);
		                echo $this->Form->control('confirm_password', ['label' => __('confirm password'), 'type' => 'password']);
                        echo $this->Form->control('name', ['label' => __('name')]);
                        echo $this->Form->control('role', ['label' => __('role')]);
                        echo $this->Form->control('is_active', [
                            'type' => 'checkbox',
                            'label' => __('Active')
                        ]);
		            ?>
                </div>
                <!-- /.box-body -->

                <?php echo $this->Form->submit(__('Submit')); ?>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
        </div>
    </div>
    <!-- /.row -->
</section>
