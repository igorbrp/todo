<style>
 .user-mini {
     margin:auto 10px auto auto;
     width: 25px;
     height: 25px;
 }
</style>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        <?=__('Vendedores')?> <!-- TRADUZIR -->
        <small>&nbsp;</small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
                        <?=$this->element('Search');/*, ['q' => $q])*/?>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col"><label><input id="chk-all" type="checkbox" class="minimal" onchange="chkAllChange();"></label></th>
                                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('nick_name') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
				<?php if ($this->request->session()->read('Auth.User.role') == 'board') :?>
                                    <th scope="col" class="text-center"><?= $this->Paginator->sort('active', 'ativo') ?></th>
				<?php endif; ?>
                                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                                <th scope="col" class="actions text-right"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($employees as $employee): ?>
                                <tr>
                                    <td>
                                        <?=$this->Form->checkbox('id', [
                                            'id' => 'chk-b',
                                            'name' => 'chkb',
                                            'class' => 'chk-box',
                                            'value' => $employee->id,
                                            'onclick' => 'checkOrUncheck(this)'
                                        ]);?>
                                    </td>
                                    <td><?= h($employee->name) ?></td>
                                    <td><?= h($employee->nick_name) ?></td>
                                    <td><?= $employee->has('user') ? $this->Html->link($employee->user->username, ['controller' => 'Users', 'action' => 'view', $employee->user->id]) : '' ?></td>

                                    <!-- <td> --><?='';// h($employee->active == 1 ? 'SIM' : 'NÃO') ?><!-- </td> -->

				    <?php if ($this->request->session()->read('Auth.User.role') == 'board') :?>
					<td class="text-center">
                                            <?=$this->Form->create('', [
						'id' => 'change-active-' . $employee->id,
						'action' => 'change-activity',
                                            ]);?>
                                            <span style="display:none;"><?=$this->Form->input('change-id', ['value' => $employee->id]);?></span>
                                            <?=$this->Form->checkbox('id', [
						'id' => 'chk-activity',
						'name' => 'chk_activity',
						'class' => 'chk-box',
						'checked' => $employee->active == 1 ? true : false,
						'value' => $employee->id,
						'onclick' => 'document.getElementById("change-active-' . $employee->id . '").submit()'
                                            ]);?>
                                            <?=$this->Form->end(); ?>
					</td>
				    <?php endif; ?>

                                    <td><?= h($employee->created) ?></td>
                                    <td class="actions text-right">
                                        <?=$this->element('Actions', ['cod_id' => $employee->id,])?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
    <div class="row">
	<div class="col-sm-5">
	    <div class="dataTables_info" id="example2_info" role="status" aria-live="polite">
		<?php echo $this->Paginator->counter(['format' => 'range']);?>
	    </div>
	</div>
	<div class="col-sm-7">
	    <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
		<!-- Paginator -->
		<?=
		$this->element('Paginator');
		?>
	    </div>
	</div>
    </div>
</section>
<?php
echo $this->Html->script([
    'js-base',
    'index',
], [
    'block' => 'scriptBottom'
]);
?>
