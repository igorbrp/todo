<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $employee
 */
$template = $this->template == 'add' ? __('Add') : __('Edit');
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	<?=__('Vendedor')?> <!-- TRADUZIR -->
	<small>
            <?php if(strtolower($this->request->getParam('action')) == 'edit') : ?>
                <?=$employee->name;?>
            <?php else : ?>
                <?='novo'?> <!-- TRADUZIR -->
            <?php endif; ?>
        </small>
    </h1>
    <?=$this->element('MenuHeader', ['noprint' => true])?>
</section>
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <!-- general form elements -->
            <?php echo $this->Form->create($employee, ['role' => 'form']); ?>
            <div class="nav-tabs-custom">
	        <ul class="nav nav-tabs">
	            <li class="active">
                        <a href="#identity" data-toggle="tab" aria-expanded="true"><?=__('Identification')?></a>
                    </li>
	            <li class="">
                        <a href="#address" data-toggle="tab" aria-expanded="false"><?=__('Address')?></a>
                    </li>
	        </ul>

	        <div class="tab-content">
	            <div class="tab-pane active" id="identity">
		        <!-- identity content -->
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group input text">
                                    <?php echo $this->Form->control('name');?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group input text">
                                    <?php echo $this->Form->control('nick_name'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group input text">
                                    <?php echo $this->Form->control('cpf', [
                                        'maxlength' => 14,
                                        'templateVars' => [
                                            'otherClass' => 'cpf',
                                        ],
                                    ]);
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group input text">
                                    <?php echo $this->Form->control('birth_date', [
                                        'type' => 'text',
	                                'data-inputmask-alias' => 'datetime',
	                                'data-inputmask-inputformat' => 'dd/mm/yyyy',
	                                'templateVars' => [
		                            'otherClass' => 'date',
	                                ],
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group input text">
                                    <?php echo $this->Form->control('phone', [
                                        'templateVars' => [
                                            'otherClass' => 'phone',
                                        ],
                                    ]);
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group input text">
                                    <?php echo $this->Form->control('fax', [
                                        'templateVars' => [
                                            'otherClass' => 'phone',
                                        ],
                                    ]); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group input text">
                                    <?php echo $this->Form->control('mobile', [
                                        'templateVars' => [
                                            'otherClass' => 'mobile',
                                        ],
                                    ]); ?>
                                </div>
                            </div>
                        </div>


                        <div class="form-group input text">
                            <label class="control-label" for="user-id"><?=__('user')?></label>
                            <?php echo $this->Form->select('user_id', $users, [
                                'empty' => true
                            ]);?>
                        </div>
                        <?=$this->Form->control('active', ['label' => 'ativo']);?>
                    </div>
                    <!-- /.tab-pane -->

                    <div class="tab-pane" id="address">
                        <!-- address content -->
                        <?=$this->Element('Address');?>
                    </div>

                </div>
                <!-- /.tab-content -->
                <div class="text-right">
                    <?php echo $this->Form->submit(__('Submit')); ?>
                </div>

                <?php echo $this->Form->end(); ?>
            </div>
            <!-- /.box -->
            <!-- /.nav-tabs-custom -->
        </div>
        <!-- /.col-md -->
    </div>
    <!-- /.row -->
</section>

<?php
echo $this->Html->css([
    'AdminLTE./bower_components/select2/dist/css/select2.css'], [
        'block' => 'css'
]);
echo $this->Html->script([
    'AdminLTE./bower_components/select2/dist/js/select2.js',
    '/node_modules/inputmask/dist/jquery.inputmask.min',
    '/node_modules/inputmask/dist/bindings/inputmask.binding',
], [
    'block' => 'script'
]);
echo $this->Html->script([
    'addresses',
    'apply-select2',
    'common_masks',
], [
    'block' => 'scriptBottom'
]);
?>
