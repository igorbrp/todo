<section class="content-header">
    <h1>
	Vendedor
	<small><?php echo $employee->name; ?></small>
    </h1>
    <ol class="breadcrumb">
	<?=$this->element('MenuHeader', ['noprint' => true])?>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
	<div class="col-md-12">
	    <div class="box box-solid">
		<div class="box-header with-border">
		    <i class="fa fa-info"></i>
		    <h3 class="box-title"><?php echo __('Information'); ?></h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		    <dl class="dl-horizontal">
			<dt scope="row"><?= __('Name') ?></dt>
			<dd><?= h($employee->name) ?></dd>
			<dt scope="row"><?= __('Nick Name') ?></dt>
			<dd><?= h($employee->nick_name) ?></dd>
			<dt scope="row"><?= __('Phone') ?></dt>
			<dd><?= h($employee->phone) ?></dd>
			<dt scope="row"><?= __('Mobile') ?></dt>
			<dd><?= h($employee->mobile) ?></dd>
			<dt scope="row"><?= __('Fax') ?></dt>
			<dd><?= h($employee->fax) ?></dd>
			<dt scope="row"><?= __('Address') ?></dt>
			<dd><?= $employee->has('address') ? $this->Html->link($employee->address->id, ['controller' => 'Addresses', 'action' => 'view', $employee->address->id]) : '' ?></dd>
			<dt scope="row"><?= __('Birth Date') ?></dt>
			<dd><?= h($employee->birth_date) ?></dd>
			<dt scope="row"><?= __('Active') ?></dt>
			<dd><?= $employee->active ? __('Yes') : __('No'); ?></dd>
		    </dl>
		</div>
	    </div>
	</div>
    </div>


    <div class="row">
	<div class="col-md-12">
	    <div class="box box-solid">
		<div class="box-header with-border">
		    <i class="fa fa-share-alt"></i>
		    <h3 class="box-title"><?= __('Endereço') ?></h3>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
		    <?php if (!empty($employee->address)): ?>
			<table class="table table-hover">
			    <tr>
				<th scope="col"><?= __('CEP') ?></th>
				<th scope="col"><?= __('estado') ?></th>
				<th scope="col"><?= __('cidade') ?></th>
				<th scope="col"><?= __('Neighborhood') ?></th>
				<th scope="col"><?= __('Thoroughfare') ?></th>
				<th scope="col"><?= __('Number') ?></th>
				<th scope="col"><?= __('Complement') ?></th>
				<th scope="col"><?= __('Reference') ?></th>
			    </tr>
			    <?php echo ''; // foreach ($employee->address as $employeeAddresses): ?>
			    <tr>
				<td><?= h($employee->address->postal_code) ?></td>
				<td><?= h($employee->address->state_id) ?></td>
				<td><?= h($employee->address->city) ?></td>
				<td><?= h($employee->address->neighborhood) ?></td>
				<td><?= h($employee->address->thoroughfare) ?></td>
				<td><?= h($employee->address->number) ?></td>
				<td><?= h($employee->address->complement) ?></td>
				<td><?= h($employee->address->reference) ?></td>
			    </tr>
			    <?php echo ''; // endforeach; ?>
			</table>
		    <?php endif; ?>
		</div>
	    </div>
	</div>
    </div>



</section>
