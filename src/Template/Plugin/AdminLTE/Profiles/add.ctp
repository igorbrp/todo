<?php
/**
 * @var \App\View\AppView $this
 * @var \Cake\Datasource\EntityInterface $profile
 */
?>
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
	Profile
	<small><?php echo __('Add'); ?></small>
    </h1>
    <ol class="breadcrumb">
        <?=$this->element('MenuHeader')?>
    </ol>
</section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <!-- general form elements -->
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title"><?php echo __('Form'); ?></h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <?php echo $this->Form->create($profile, ['role' => 'form']); ?>
            <div class="box-body">
              <?php
                echo $this->Form->control('user_id');
                echo $this->Form->control('name');
                echo $this->Form->control('nickname');
                echo $this->Form->control('email');
                echo $this->Form->control('bio');
                echo $this->Form->control('photo');
              ?>
            </div>
            <!-- /.box-body -->

          <?php echo $this->Form->submit(__('Submit')); ?>

          <?php echo $this->Form->end(); ?>
        </div>
        <!-- /.box -->
      </div>
  </div>
  <!-- /.row -->
</section>
