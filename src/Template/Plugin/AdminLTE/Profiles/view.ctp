<section class="content-header">
  <h1>
    Profile
    <small><?php echo __('View'); ?></small>
  </h1>
  <ol class="breadcrumb">
      <?=$this->element('MenuHeader')?>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box box-solid">
        <div class="box-header with-border">
          <i class="fa fa-info"></i>
          <h3 class="box-title"><?php echo __('Information'); ?></h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <dl class="dl-horizontal">
            <dt scope="row"><?= __('Name') ?></dt>
            <dd><?= h($profile->name) ?></dd>
            <dt scope="row"><?= __('Nickname') ?></dt>
            <dd><?= h($profile->nickname) ?></dd>
            <dt scope="row"><?= __('Email') ?></dt>
            <dd><?= h($profile->email) ?></dd>
            <dt scope="row"><?= __('Bio') ?></dt>
            <dd><?= h($profile->bio) ?></dd>
            <dt scope="row"><?= __('Photo') ?></dt>
            <dd><?= h($profile->photo) ?></dd>
            <dt scope="row"><?= __('Id') ?></dt>
            <dd><?= $this->Number->format($profile->id) ?></dd>
            <dt scope="row"><?= __('User Id') ?></dt>
            <dd><?= $this->Number->format($profile->user_id) ?></dd>
            <dt scope="row"><?= __('Created') ?></dt>
            <dd><?= h($profile->created) ?></dd>
            <dt scope="row"><?= __('Modified') ?></dt>
            <dd><?= h($profile->modified) ?></dd>
          </dl>
        </div>
      </div>
    </div>
  </div>

</section>
