<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Profiles
        <small><?php echo __('View'); ?></small>
    </h1>
    <ol class="breadcrumb">
        <?=$this->element('MenuHeader', ['noprint' => true])?>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div>
                        <form action="<?php echo $this->Url->build(); ?>" method="POST">
                            <div class="input-group input-group-sm" style="width: 150px;">
                                <input type="text" name="table_search" class="form-control pull-right" placeholder="<?php echo __('Search'); ?>">

                                <div class="input-group-btn">
                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('user_id') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('nickname') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('bio') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('photo') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                                <th scope="col" class="actions text-center"><?= __('Actions') ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($profiles as $profile): ?>
                                <tr>
                                    <td><?= $this->Number->format($profile->id) ?></td>
                                    <td><?= $this->Number->format($profile->user_id) ?></td>
                                    <td><?= h($profile->name) ?></td>
                                    <td><?= h($profile->nickname) ?></td>
                                    <td><?= h($profile->email) ?></td>
                                    <td><?= h($profile->bio) ?></td>
                                    <td><?= h($profile->photo) ?></td>
                                    <td><?= h($profile->created) ?></td>
                                    <td><?= h($profile->modified) ?></td>
                                    <td class="actions text-right">
                                        <?=$this->element('Actions', ['cod_id' => $profile->id,])?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>
</section>
