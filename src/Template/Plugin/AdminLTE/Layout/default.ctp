<?php use Cake\Core\Configure; ?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $this->fetch('title'); ?> | <?php echo Configure::read('Theme.title'); ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <?php echo $this->Html->css('AdminLTE./bower_components/bootstrap/dist/css/bootstrap.min'); ?>
        <!-- Font Awesome -->
        <?php echo $this->Html->css('AdminLTE./bower_components/font-awesome/css/font-awesome.min'); ?>
        <!-- Ionicons -->
        <?php echo $this->Html->css('AdminLTE./bower_components/Ionicons/css/ionicons.min'); ?>
        <!-- Theme style -->
        <?php echo $this->Html->css('AdminLTE.AdminLTE.min'); ?>
        <!-- AdminLTE Skins. Choose a skin from the css/skins
             folder instead of downloading all of them to reduce the load. -->
        <?php echo $this->Html->css('AdminLTE.skins/skin-'. Configure::read('Theme.skin') .'.min'); ?>

        <!-- AdminLTE Pace -->
        <?php echo ''; //$this->Html->css('AdminLTE./plugins/pace/pace.min'); ?>
        <?php echo $this->Html->css('pace-theme') ?>
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- AdminLTE Pace -->
        <?php echo $this->Html->script('AdminLTE./plugins/pace/pace.min'); ?>

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

        <!-- fetch css -->
        <?php echo $this->fetch('css'); ?>
        <!-- end fetch css -->

    </head>
    <body class="hold-transition skin-<?php echo Configure::read('Theme.skin'); ?> <?php echo Configure::read('Theme.layout'); ?> <?php  echo Configure::read('Preferences.collapsedBar'); ?> sidebar-mini">
        <!-- Modal -->
        <!-- <div class="example-modal"> -->
        <div id="main_modal" class="modal fade modal-info" tabindex="-1">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h3 class="modal-title">
                            <span id="modal_icon" style="display:inline-block;margin-right:7px;"><i class="fa fa-info"></i></span>
                            <span id="modal_title" style="display:inline-block;">Success Modal</span>
                        </h3>
                    </div>
                    <div id="modal_body" class="modal-body">
                        <h4 id="modal_msg_body"></h4>
                    </div>
                    <div class="modal-footer">
                        <button id="bt_modal_close" type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                        <button id="bt_modal_confirm" type="button" class="btn btn-outline">Save changes</button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- </div> -->
        <!-- /.example-modal -->

        <div class="wrapper">

            <header class="main-header">
                <!-- Logo -->
                <a href="<?php echo $this->Url->build('/'); ?>" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><?php echo Configure::read('Theme.logo.mini'); ?></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><?php echo Configure::read('Theme.logo.large'); ?></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <?php echo $this->element('nav-top'); ?>
            </header>

            <?php echo $this->element('aside-main-sidebar'); ?>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <?php echo $this->Flash->render(); ?>
                <?php echo $this->Flash->render('auth'); ?>
                <?php echo $this->fetch('content'); ?>

            </div>
            <!-- /.content-wrapper -->

            <?php echo $this->element('footer'); ?>

            <!-- Control Sidebar -->
            <?php echo $this->element('aside-control-sidebar'); ?>
            <!-- /.control-sidebar -->

            <!-- Add the sidebar's background. This div must be placed
                 immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->

        <!-- jQuery 3 -->
        <?php echo $this->Html->script('AdminLTE./bower_components/jquery/dist/jquery.min'); ?>
        <!-- Bootstrap 3.3.7 -->
        <?php echo $this->Html->script('AdminLTE./bower_components/bootstrap/dist/js/bootstrap.min'); ?>
        <!-- AdminLTE App -->
        <?php echo $this->Html->script('AdminLTE.adminlte.min'); ?>
        <!-- Slimscroll -->
        <?php echo $this->Html->script('AdminLTE./bower_components/jquery-slimscroll/jquery.slimscroll.min'); ?>
        <!-- FastClick -->
        <?php echo $this->Html->script('AdminLTE./bower_components/fastclick/lib/fastclick'); ?>

	<!-- default todo -->
        <?php echo $this->Html->script('default.js'); ?>

        <!-- fetch scripts -->
        <?php echo $this->fetch('script'); ?>
        <!-- end fetch scripts -->

        <!-- fetch scriptBottom -->
        <?php echo $this->fetch('scriptBottom'); ?>
        <!-- end fetch scriptBottom -->

        <script type="text/javascript">
            $(document).ready(function(){
                $(".navbar .menu").slimscroll({
                    height: "200px",
                    alwaysVisible: false,
                    size: "3px"
                }).css("width", "100%");

                var a = $('a[href="<?php echo $this->Url->build() ?>"]');
                if (!a.parent().hasClass('treeview') && !a.parent().parent().hasClass('pagination')) {
                    a.parent().addClass('active').parents('.treeview').addClass('active');
                }

            });
            // csrf
            var csrfToken = <?=json_encode($this->request->getParam('_csrfToken'))?>;
        </script>

</body>
</html>
