<script>
    var models = ['products'];

    //     var models = ['employees', 'subsidiaries', 'supplies', 'products', 'orders'];
    //    var models = ['orders'];
    var index = 0;

    function migra()
    {
        console.log(models[index]);
        if(index < models.length) {
	        createProgressBar();
	        migration(0);
        }
    }

    function migration(key)
    {
        if(key == null)
	        key = 0;
        data = {id: key};
        // console.log('/migrate/' + models[index]);
        //        ajax_get('/migrate/' + models[index], data, 'json', r_migration, '');
        console.log('aqui');
        ajax_get('/migrate/' + models[index], data, '', r_migration, '');
    }

    function r_migration(response)
    {
        console.log('r_aqui');
        console.log(typeof(response));
        console.log(response);
        if (typeof(response) == 'string')
            response = JSON.parse(response);

        //        console.log(response);
        // console.log('response.count: ' + response.count);
        // console.log('response.total: ' + response.total);
        $("#total-" + models[index]).text('Total: ' + response.total);
        $("#registros-" + models[index]).text('Registros: ' + response.count);
        $("#bar-recs-" + models[index]).attr("aria-valuenow", response.percount);
        $("#bar-recs-" + models[index]).attr("style", 'width: ' + response.width_percount + '%');
        $("#bar-recs-" + models[index]).text(response.percount + '%');
        $("#erros-" + models[index]).text('Erros: ' + response.errors);
        $("#bar-errors-" + models[index]).attr("aria-valuenow", response.pererrors);
        $("#bar-errors-" + models[index]).attr("style", 'width: ' + response.width_pererrors + '%');
        $("#bar-errors-" + models[index]).text(response.pererrors + '%');

        if(response.continue == 1) {
	        migration(response.last_key);
        } else {
	        index++;
	        migra();
        }
    }

    function createProgressBar()
    {
        var prgb = ' <div class="col-sm-12"> ' +
		           '   <div class="box"> ' +
		           '     <div class="box-header"> ' +
		           '       <h3>' + models[index] + '</h3> ' +
		           '     </div> ' +
		           '     <div class="box-body"> ' +
		           '       <p id="total-' + models[index] + '">Total:</p> ' +
		           '       <p id="registros-' + models[index] + '">Registros:</p> ' +
		           '       <div class="progress"> ' +
		           '         <div id="bar-recs-' + models[index] + '" class="progress-bar progress-bar-green" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div> ' +
		           '       </div> ' +
		           '       <p id="erros-' + models[index] + '">Erros:</p> ' +
		           '       <div class="progress"> ' +
		           '         <div id="bar-errors-' + models[index] + '" class="progress-bar progress-bar-red" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div> ' +
		           '       </div> ' +
		           '     </div> ' +
		           '   </div> ' +
		           ' </div> ';

        $("#div-container").prepend(prgb);
    }

    /**********************************************************************************
     * ajax_request, ajax_post, ajax_get
     * envia requisições ajax post
     * targeturl: url
     * data: array de dados
     * func: funcao de retorno
     * param: parametros da funcao de retorno
     **********************************************************************************/
    // inseri variavel enctype para upload de arquivo
    function ajax_request(targeturl, data, ext, func, param, type, enctype)
    {
        // console.log(targeturl);
        // console.log(data);

        enctype = (enctype == '') ?  'application/x-www-form-urlencoded' : enctype;
        $.ajax({
     	    type: type,
            //	_ext: 'json',
            //	_ext: ext,
	        url: targeturl,
	        // inseri linha abaixo para upload de arquivos
            //	enctype: (enctype == '') ? 'application/x-www-form-urlencoded' : enctype,
	        enctype: enctype,
	        // data: (type == 'post') ? data : '',
	        data: data,
     	    beforeSend: function(xhr) {
	            // coloquei enctype para upload de arquivo
	            // xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
     	        xhr.setRequestHeader('Content-type', enctype);
	            // alert(enctype);
     	    },
     	    success: function(response) {
    	        if(response.error) {
    	    	    modal_err_msg(response.error);
    	        } else {
		            func(response, param);
    	        }
     	    },
     	    error: function(e) {
     	        alert("An error occurred: " + e.responseText.message);
     	        console.log('error: ' + e);
     	    }
        });
    }
    function ajax_get(targeturl, data, ext, func, param)
    {
        if(ext == 'json')
	        targeturl = targeturl.concat('.json?');
        else
	        targeturl = targeturl.concat('?');
        targeturl = targeturl.concat($.param(data));

        // console.log(targeturl);
        ajax_request(targeturl, data, ext, func, param, 'get', '');
    }
    function ajax_post(targeturl, data, ext, func, param)
    {
        if(ext == 'json')
	        targeturl = targeturl.concat('.json');
        //    alert(targeturl);
        ajax_request(targeturl, data, ext, func, param, 'post', '');
    }
    /* final de ajax_post
     **********************************************************************************/
</script>

<div class="col-xs-12">
    <div class="box">
	    <div class="box-body">
	        <div class="box-header">
		        <h3>Migrate</h3>
	        </div>
	        <div>
		        <a href="#" onclick="migra(); return false;">migra</a>
	        </div>

	        <div id="div-container" class="col-xs-12">

	        </div>
	    </div>
    </div>
</div>
