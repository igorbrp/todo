<?php
use Cake\I18n\I18n ;
use Cake\I18n\Time ;
use Cake\I18n\Date ;
use Cake\I18n\Number ;

/* Time::setDefaultLocale ( 'pt-BR' ); // For any mutable DateTime
 * //FrozenTime::setDefaultLocale ( 'pt-BR' ); // For any immutable DateTime
 * Date::setDefaultLocale ( 'pt-BR' ); // For any mutable Date
 * //FrozenDate::setDefaultLocale ( 'pt-BR' ); // For any immutable Date */
// debug($salesMonth);
/* echo "primeiro dia mes passad<br>";
 * debug($firstDayOnLastMonth);
 * echo "primeiro dia deste mes<br>";
 * debug($firstDay);
 * echo "ultimo dia do mesmo periodo do mes passado<br>";
 * debug($onLastMonth);
 * echo "este mes<br>";
 * debug($salesMonth);
 * echo "mes passado<br>";
 * debug($salesLastMonth); */
/* $dt = new Date();
 * $dt->setDate(20020, 10, 01);
 * Date::setDefaultLocale ( 'pt-BR' ); // For any mutable Date
 * //echo $dt->format('F');
 * setlocale(LC_ALL, NULL);
 * setlocale(LC_ALL, 'pt');    */
//debug(ucfirst(gmstrftime('%A')));
//echo strftime('%B', 2);
//echo $dt->i18nFormat(\IntlDateFormatter::FULL, 'Europe/Paris')->month;

/* for ($i = 0; $i < count($totalOrdersThisMonth); $i++) {
 *     $totalOrdersThisMonth[$i]['mes'] = $meses[$totalOrdersThisMonth[$i]['month']];
 * } */

debug($order_amount);
//debug($subsidiaries);
?>
<!-- DONUT CHART -->
<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">Donut Chart</h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
    <div class="box-body">
        <canvas id="pieChart" style="height:250px"></canvas>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

<?php echo $this->Html->script('AdminLTE./bower_components/chart.js/Chart', ['block' => 'script']); ?>

<?php $this->start('scriptBottom'); ?>
<script>
 $(function () {

     //-------------
     //- PIE CHART -
     //-------------
     // Get context with jQuery - using jQuery's .get() method.
     var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
     var pieChart       = new Chart(pieChartCanvas);
     var PieData        = <?=json_encode($order_amount)?>;
	 var pieOptions     = {
	     //Boolean - Whether we should show a stroke on each segment
	     segmentShowStroke    : true,
	     //String - The colour of each segment stroke
	     segmentStrokeColor   : '#fff',
	     //Number - The width of each segment stroke
	     segmentStrokeWidth   : 2,
	     //Number - The percentage of the chart that we cut out of the middle
	     percentageInnerCutout: 50, // This is 0 for Pie charts
	     //Number - Amount of animation steps
	     animationSteps       : 100,
	     //String - Animation easing effect
	     animationEasing      : 'easeOutBounce',
	     //Boolean - Whether we animate the rotation of the Doughnut
	     animateRotate        : true,
	     //Boolean - Whether we animate scaling the Doughnut from the centre
	     animateScale         : false,
	     //Boolean - whether to make the chart responsive to window resizing
	     responsive           : true,
	     // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
	     maintainAspectRatio  : true,
	     //String - A legend template
	     legendTemplate       : '<ul class="<%=name.toLowerCase()%>-legend"><% for (var i=0; i<segments.length; i++){%><li><span style="background-color:<%=segments[i].fillColor%>"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>'
	 }
     //Create pie or douhnut chart
     // You can switch between pie and douhnut using the method below.
     pieChart.Doughnut(PieData, pieOptions)
 })
</script>
<?php  $this->end(); ?>
