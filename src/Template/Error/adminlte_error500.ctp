<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Erro 500
    </h1>
    <!-- <ol class="breadcrumb">
         <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
         <li><a href="#">Examples</a></li>
         <li class="active">500 error</li>
         </ol> -->
</section>

<!-- Main content -->
<section class="content">

    <div class="error-page">
        <h2 class="headline text-red">500</h2>

        <div class="error-content">
            <br>
            <h3><i class="fa fa-warning text-red"></i> Opa! Algo deu errado.</h3>
            <p>
                Vamos trabalhar para consertar isso imediatamente.<br>
                Retorne ao <a href="<?php echo $this->Url->build('pages/home'); ?>">painel de controle</a>.
            </p>

            <!-- <form class="search-form">
                 <div class="input-group">
                 <input type="text" name="search" class="form-control" placeholder="Search">

                 <div class="input-group-btn">
                 <button type="submit" name="submit" class="btn btn-danger btn-flat"><i class="fa fa-search"></i>
                 </button>
                 </div>
                 </div> -->
            <!-- /.input-group -->
            <!-- </form> -->
        </div>
      </div>
      <!-- /.error-page -->

    </section>
    <!-- /.content -->
