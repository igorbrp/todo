<body>
    <p>Olá, <?=$contact?></p>

    <p>Estamos requisitando os seguintes produtos abaixo</p>

    <table id="requisition">
        <!-- <thead> -->
            <tr>
                <th>Quantidade</th>
                <th>Descrição do produto</th>
            </tr>
            <!-- </thead> -->

            <!-- <tbody> -->
            <?php foreach ($items as $item): ?>
                <tr>
                    <td><?=$item['qtd']?></td>
                    <td><?=$item['title']?><?= !empty($item['complement']) ? '<br><i>' . $item['complement'] . '</i>' : ''; ?></td>
                </tr>
            <?php endforeach; ?>
            <!-- </tbody> -->
    </table>
    <br>
    <p>Qualquer dúvida, por favor entre em contato conosco</p>
    <p>Atenciosamente,<br><?=$nick_name?></p>
    <p>
	<?=$company?><br>
	<?=$thoroughfare?>, <?=$number?>&nbsp;&nbsp;<?=$complement?><br>
	<?=$neighborhood?> - <?=$city_name?> - <?=$state_id?><br>
	<?=$postal_code?>
    </p>
</body>
