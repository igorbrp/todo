<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\ORM\Query;

/**
 * StockAdjustments Controller
 *
 * @property \App\Model\Table\StockAdjustmentsTable $StockAdjustments
 *
 * @method \App\Model\Entity\StockAdjustment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StockAdjustmentsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Supplies', 'Products', 'Users'],
        ];
        $stockAdjustments = $this->paginate($this->StockAdjustments);

        $this->set(compact('stockAdjustments'));
    }

    /**
     * View method
     *
     * @param string|null $id Stock Adjustment id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $stockAdjustment = $this->StockAdjustments->get($id, [
            'contain' => ['Supplies', 'Products', 'Users'],
        ]);

        $this->set('stockAdjustment', $stockAdjustment);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $stockAdjustment = $this->StockAdjustments->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['user_id'] = $_SESSION['Auth']['User']['id'];
            $stockAdjustment = $this->StockAdjustments->patchEntity($stockAdjustment, $data);
            if ($this->StockAdjustments->save($stockAdjustment)) {
                $this->Flash->success(__('O ajuste de estoque foi salvo.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O ajuste de estoque não pode ser salvo. Por favor, verifique os erros e tente novamente.'));
        }

        $supplies = $this->StockAdjustments->Supplies
                  //                  ->find('list', ['limit' => 200]);
                  ->find('list');
        $supplies->contain(['Products']);
        $supplies->select(function (Query $q) {
            return [
                'Supplies.id',
                'Supplies.name'
            ];
        });
        $supplies->innerJoinWith('Products');
        $supplies->where(['Products.is_stock' => 1]);
        $supplies->order(['Supplies.name']);

        if (!empty($stockAdjustment['supply_id'])) {
            $products = $this->StockAdjustments->Products
                      ->find('list')
                      ->where(['supply_id' => $stockAdjustment['supply_id']]);
        } else {
            $products = [];
        }
        // $products = $this->StockAdjustments->Products
        //           ->find('list', ['limit' => 200])
        //           ->where(['is_stock' => '1']);

        $this->set(compact('stockAdjustment', 'supplies', 'products'));
        $this->render('addedit');
    }

    /**
     * Edit method
     *
     * @param string|null $id Stock Adjustment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $stockAdjustment = $this->StockAdjustments->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $data['user_id'] = $_SESSION['Auth']['User']['id'];
            $stockAdjustment = $this->StockAdjustments->patchEntity($stockAdjustment, $data);
            if ($this->StockAdjustments->save($stockAdjustment)) {
                $this->Flash->success(__('O ajuste de estoque foi salvo.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O ajuste de estoque não pode ser salvo. Por favor, verifique os erros e tente novamente.'));
        }

        $supplies = $this->StockAdjustments->Supplies
                  ->find('list', ['limit' => 200])
                  ->where(['Products.is_stock' => '1'])
                  ->innerJoinWith('Products');

        if (!empty($stockAdjustment['supply_id'])) {
            $products = $this->StockAdjustments->Products
                      ->find('list', ['limit' => 200])
                      ->where(['supply_id' => $stockAdjustment['supply_id']]);
        } else {
            $products = [];
        }
        $product_title = $this->StockAdjustments->Products->get($stockAdjustment->product_id)->title;
        $this->set(compact('stockAdjustment', 'supplies', 'products', 'product_title'));

        $this->render('addedit');
    }

    /**
     * Delete method
     *
     * @param string|null $id Stock Adjustment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $stockAdjustment = $this->StockAdjustments->get($id);
        if ($this->StockAdjustments->delete($stockAdjustment)) {
            $this->Flash->success(__('The stock adjustment has been deleted.'));
        } else {
            $this->Flash->error(__('The stock adjustment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
