<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\Collection\Collection;
use Cake\Database\FunctionsBuilder;
use Cake\Database\Expression\QueryExpression;
use Cake\ORM\Query;

use Cake\Log\Log;

/**
 * OutboxEmails Controller
 *
 * @property \App\Model\Table\OutboxEmailsTable $OutboxEmails
 *
 * @method \App\Model\Entity\OutboxEmail[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OutboxEmailsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $data = $this->request->getQuery();
        $q = isset($data['q']) ? $data['q'] : '';
        $selectsupply = isset($data['selectsupply']) ? $data['selectsupply'] : '';
        Log::debug($data);
        $this->loadModel('Supplies');
        
        // ==========================================================================================
        // purchase purchases
        $purchaseQuery = $this->getPurchaseQuery();

        if (!isset($data['sented']) || $data['sented'] != 1) {
            $purchaseQuery->where(['sented' => 0]);
        } 

        if (isset($data['selectsupply'])) {
            $purchaseQuery->andWhere(['supply_id' => $data['selectsupply']]);
        }

        if (isset($data['q'])) {
            $purchaseQuery->andWhere(['Purchases.id' => $data['q']]);
        }

        // ==========================================================================================
        // order items
        $itemsQuery = $this->getItemsQuery();

        if (!isset($data['sented']) || $data['sented'] != 1) {
            $itemsQuery->where(['sented' => 0]);
        }

        if (isset($data['selectsupply'])) {
            $itemsQuery->andWhere(['supply_id' => $data['selectsupply']]);
        }

        if (isset($data['q'])) {
            $itemsQuery->andWhere(['Orders.number' => $data['q']]);
        }

        // ==========================================================================================
        // union
        $itemsQuery->union($purchaseQuery);


        // $outboxEmails = $itemsQuery->epilog('ORDER BY sented_hour DESC LIMIT 100 ');
        $outboxEmails = $itemsQuery->epilog('LIMIT 50');

        $this->set('outboxEmails', $this->paginate($outboxEmails));

        $supplies = $this->OutboxEmails->Requisitions->Items->Supplies->find('list', ['limit' => 200]);
        $this->set(compact('supplies'));
    }

    private function getPurchaseQuery()
    {
        $query = $this->OutboxEmails
                       ->find()
                       ->contain([
                           'Requisitions.Purchases.Supplies',
                       ]);
        $controller = $query->func()->concat(['purchases']);
        //        $refer = $query->func()->concat(['estoque']);
        $query->select([
            'id',
            'sented',
            //            'refer' => $refer,
            'refer' => 'OutboxEmails.id',
            'refer_id' => 'Purchases.id',
            'controller' => $controller,
            'supply_id' => 'Supplies.id',
            'supply_name' => 'Supplies.name',
            'supply_email' => 'Supplies.email',
            'modified',
        ])
              ->innerJoinWith('Requisitions.Purchases.Supplies')
              ->order(['OutboxEmails.modified' => 'DESC']);
        return $query;
    }
    
    private function getItemsQuery()
    {
        $query = $this->OutboxEmails
                      ->find()
                      ->contain([
                          'Requisitions.Items.Orders',
                          'Requisitions.Items.Supplies',
                      ]);

        $controller = $query->func()->concat(['orders']);
        $query->select([
            'id',
            'sented',
            'refer' => 'Orders.number',
            'refer_id' => 'Orders.id',
            'controller' => $controller,
            'supply_id' => 'Items.supply_id',
            'supply_name' => 'Items.supply_name',
            'supply_email' => 'Supplies.email',
            'modified',
        ])
                   ->innerJoinWith('Requisitions.Items.Orders')
                   ->innerJoinWith('Requisitions.Items.Supplies')
                   ->order(['OutboxEmails.modified' => 'DESC']);
        return $query;
    }
    
    public function manualEmailSend($id = null)
    {
        Log::debug('entrou em manualEmailSend  ============================================================');
        $outboxEmail = $this->OutboxEmails
                      ->find()
                      ->contain([
                          'Requisitions',
                          'Requisitions.Items' => [
                              'strategy' => 'subquery',
                          ],
                          'Requisitions.Items.Supplies',
                          'Requisitions.Purchases' => [
                              'strategy' => 'subquery',
                          ],
                          'Requisitions.Purchases.Supplies',
                          'Requisitions.Purchases.Products',
                      ])
                      ->where(['OutboxEmails.id' => $id])
                      ->first();

        if(!empty($outboxEmail['requisition']['items'])) { // items
            $tmpRecipient = $outboxEmail->requisition->items[0]->supply->email;
        } else { // purchases
            $tmpRecipient = $outboxEmail->requisition->purchases[0]->supply->email;
        }
        
        if (empty($outboxEmail->recipient) || trim($outboxEmail->recipient) != $tmpRecipient) {
            if (!empty($tmpRecipient)) {
                $outboxEmail->recipient = $tmpRecipient;
                $this->OutboxEmails->save($outboxEmail);
            } else {
                $this->Flash->error(__('O fornecedor não tem email cadastrado. Atualize o endereço de email do fornecedor e tente novamente.'));
                return $this->redirect(['action' => 'index']);
            }
        }            

        Log::debug('$outboxEmail ============================================================');
        $this->OutboxEmails->sendMail($outboxEmail);
        // $outboxEmail->sented = '1';
        // $this->OutboxEmails->save($outboxEmail);

        return $this->redirect(['action' => 'index']);
    }

    public function testSendMail()
    {
        $this->OutboxEmails->sentEmailsOnOutbox();
        return $this->redirect(['action' => 'index']);
    }
}
