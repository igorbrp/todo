<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Core\Configure;
// use Cake\Core\Configure\Engine\PhpConfig;

use Cake\Collection\Collection;

use Cake\Log\Log;

//=================================================================
// // teste CSV
// use Goodby\CSV\Import\Standard\Lexer;
// use Goodby\CSV\Import\Standard\Interpreter;
// use Goodby\CSV\Import\Standard\LexerConfig;
//=================================================================
/**
 * Employees Controller
 *
 * @property \App\Model\Table\EmployeesTable $Employees
 *
 * @method \App\Model\Entity\Employee[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class EmployeesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
//=================================================================
// teste CSV
// $lexer = new Lexer(new LexerConfig());
// $interpreter = new Interpreter();
//=================================================================

        $query = $this->Employees
               //->find('search', ['search' => $this->request->getData()]);
               ->find('search', ['search' => $this->request->getQuery()])
               ->contain(['Users']);
               //            ->where(['name IS NOT' => null]);
        $this->set('employees', $this->paginate($query));

        //        $this->set(compact('employees'));
        //        $this->set('q', isset($data['q']) ? $data['q'] : '');
        $this->set('_serialize', ['employees']);
    }

    /**
     * View method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $employee = $this->Employees->get($id, [
            'contain' => ['Users', 'Addresses', 'Orders']
        ]);

        $this->set('employee', $employee);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
		$userId = $this->Auth->user('id');

        $employee = $this->Employees->newEntity();

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if (!empty(implode("", array_values($data['address'])))) {
                $data['address']['model'] = $this->Employees->registryAlias();
            } else {
                $data['address'] = null;
            }

            $employee = $this->Employees->patchEntity($employee, $data, [
                'associated' => [
                    'Addresses',
                    // 'Files',
                ]
            ]);
            //            debug($employee);die();
            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('O vendedor foi salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O vendedor não pode ser salvo. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $employee->errors(),
                ]]);
        }

        $modelStates = $this->loadModel('States');
        $states = $modelStates->find('list')->toArray();
        $this->set(compact('states'));

        $modelCities = $this->loadModel('Cities');
        if (!empty($order['customer']['address']['state_id'])) {
            $cities = $modelCities->find('list')->toArray();
        } else {
            $cities = [];
        }
        $this->set(compact('cities'));

        $usersFull = $this->Employees->Users
                   ->find()
                   ->select(['id', 'nick_name']);

        $collection = (new Collection($usersFull))->combine('id', 'nick_name');
        $users = $collection->toArray();

        $this->set(compact('employee', 'users'));

        $this->render('addedit');
    }

    /**
     * Edit method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
		$userId = $this->Auth->user('id');

        $employee = $this->Employees->get($id, [
            'contain' => [
                'Addresses',
                // 'Files',
            ]
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            if (!empty(implode("", array_values($data['address'])))) {
                $data['address']['model'] = $this->Employees->registryAlias();
            } else {
                $data['address'] = null;
            }

            // if (!empty($data['file']['submittedfile']) && $data['file']['submittedfile']['error'] == 0) {
            //     $data['file']['user_id'] = $userId; // Optional
            //     $data['file']['model'] = $this->Employees->registryAlias();
            // }

            $employee = $this->Employees->patchEntity($employee, $data, [
                'associated' => [
                    'Addresses',
                    //                    'Files',
                ]
            ]);

            if ($this->Employees->save($employee)) {
                $this->Flash->success(__('O vendedor foi salvo com sucesso.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O vendedor não pode ser salvo. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $employee->errors(),
                ]]);
        }

        $modelStates = $this->loadModel('States');
        $states = $modelStates->find('list')->toArray();
        $this->set(compact('states'));

        $modelCities = $this->loadModel('Cities');
        if (!empty($employee['address']['state_id'])) {
            $cities = $modelCities->find('list')->toArray();
        } else {
            $cities = [];
        }
        $this->set(compact('cities'));

        $usersFull = $this->Employees->Users
                   ->find()
                   ->select(['id', 'nick_name']);

        $collection = (new Collection($usersFull))->combine('id', 'nick_name');
        $users = $collection->toArray();

        $this->set(compact('employee', 'users'));

        $this->render('addedit');
    }

    /**
     * Delete method
     *
     * @param string|null $id Employee id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $employee = $this->Employees->get($id);
        if ($this->Employees->delete($employee)) {
            $this->Flash->success(__('O vendedor foi excluído com sucesso.'));
        } else {
            $this->Flash->error(__('O vendedor não pode ser excluído. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $employee->errors(),
                ]]);
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deleteSomeRecords($result = null)
    {
        if ($this->request->is('post')) {
            $ids = explode(",", $this->request->getData('ids'));

            $query = $this->Employees->query();
            $query->delete();

            foreach($ids as $id)
                $query->orWhere(['id' => $id]);

            if($query->execute()) {
                $this->Flash->success(__('Os vendedores foram excluídos com sucesso.'));
            } else {
                $this->Flash->error(__('Os vendedores não puderam ser excluídos. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $employee->errors(),
                ]]);
            }
        } else {
            $data = '';
        }

        return $this->redirect(['action' => 'index']);
    }

    public function changeActivity()
    {
        $this->request->allowMethod(['post']);
        $id = $this->request->getData('change-id');
        $employee = $this->Employees->get($id);
        $employee->active = ($employee->active == 1 ? 0 : 1);
        if ($this->Employees->save($employee)) {
            $this->Flash->success(__('O status do vendedor foi atualizado com sucesso.'));
        } else {
            $this->Flash->error(__('O status do vendedor não pode ser atualizado. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $employee->errors(),
                ]]);
        }

        return $this->redirect(['action' => 'index']);
    }
}
