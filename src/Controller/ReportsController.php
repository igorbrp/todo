<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Collection\Collection;
use Cake\I18n\Time;

use Cake\Log\Log;
/**
 * Reports Controller
 *
 *
 * @method \App\Model\Entity\Report[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReportsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('CorrecoesDeEmergencia');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $where = [];
        $data = $this->request->getQuery();

        if (!empty($data['select_report'])) {
            // verify dates
            if ($data['checkbox_past_month']) {
                $now = Time::now();
                $sinceDate = $now->subMonth(1)->day(1)->format("Y-m-d");
                $untilDate = date("Y-m-t", strtotime($sinceDate));
                $period = $now->subMonth(1)->i18nFormat("MMMM");
            } else {
                $sinceDate = $this->CorrecoesDeEmergencia->BrDateToUtcFormat($data['since_date']);
                $untilDate = $this->CorrecoesDeEmergencia->BrDateToUtcFormat($data['until_date']);
                $period = $data['since_date'] . ' a ' . $data['until_date'];
            }
            $whereDates = ['Orders.date_order >=' => $sinceDate, 'Orders.date_order <=' => $untilDate];

            // define report
            switch ($data['select_report']) {
            case 'employee':
                $whereOrders = !empty($data['select_employee']) ? ['Orders.employee_id' => $data['select_employee']] : null;
                $whereEmployees = !empty($data['select_employee']) ? ['employee_id' => $data['select_employee']] : null;
                $employees = $this->employeeReport($whereEmployees, $whereDates);
                //                $this->set('query', $this->paginate($employees));
                $this->set('query', $employees);

                if ($data['checkbox_details']) {
                    $ordersList = $this->buildOrderList($whereDates, $whereOrders);
                    $this->set(compact('ordersList'));
                }
                $chartArray = $this->buildArrayForChart($employees->toArray(), 'order_amount', 'name');
                $this->set('chartArray', $chartArray);
            break;
            case 'subsidiary':
                $whereOrders = !empty($data['select_subsidiary']) ? ['Orders.subsidiary_id' => $data['select_subsidiary']] : null;
                $whereSubsidiaries = !empty($data['select_subsidiary']) ? ['subsidiary_id' => $data['select_subsidiary']] : null;
                $subsidiaries = $this->subsidiaryReport($whereSubsidiaries, $whereDates);
                //                $this->set('query', $this->paginate($subsidiaries));
                $this->set('query', $subsidiaries);

                if ($data['checkbox_details']) {
                    $ordersList = $this->buildOrderList($whereDates, $whereOrders);
                    $this->set(compact('ordersList'));
                }
                $chartArray = $this->buildArrayForChart($subsidiaries->toArray(), 'order_amount', 'name');
                $this->set('chartArray', $chartArray);
                break;
            case 'supply':
                $whereOrders = !empty($data['select_supply']) ? ['Orders.supply_id' => $data['select_supply']] : null;
                $whereSupplies = !empty($data['select_supply']) ? ['supply_id' => $data['select_supply']] : null;
                $supplies = $this->supplyReport($whereSupplies, $whereDates);
                //                $this->set('query', $this->paginate($supplies));
                $this->set('query', $supplies);
                break;
            default:
                break;
            }

            $totalPeriod = $this->totalPeriod($whereDates, $whereOrders);
            $this->set('totalPeriod', $totalPeriod[0]['total_amount']);

            $this->set(compact('sinceDate'));
            $this->set(compact('untilDate'));

            $this->set(compact('whereDates'));
            //            $combined = (new Collection($subsidiaries->toList()))->combine('name', 'order_amount');
            $this->set('period', $period);
        }

        $modelSelectEmployees = $this->loadModel('Employees');
        $selectEmployees = $modelSelectEmployees->find('list', ['limit' => 200]);
        $this->set(compact('selectEmployees'));

        $modelSelectSubsidiaries = $this->loadModel('Subsidiaries');
        $selectSubsidiaries = $modelSelectSubsidiaries->find('list', ['limit' => 200]);
        $this->set(compact('selectSubsidiaries'));

        $modelSelectSupplies = $this->loadModel('Supplies');
        $selectSupplies = $modelSelectSupplies->find('list', ['limit' => 200]);
        $this->set(compact('selectSupplies'));

        $reports = ['subsidiary' => 'lojas', 'employee' => 'vendedores'// , 'supply' => 'fornecedores'
        ];
        $this->set(compact('reports'));


        $this->set('data', $data);

    }

    public function buildArrayForChart($array, $value, $label)
    {
        $colors = [
            0 => [
                'color'    => '#f56954',
                'highlight'=> '#f56954',
            ],
            1 => [
                'color'    => '#00a65a',
                'highlight'=> '#00a65a',
            ],
            2 => [
                'color'    => '#f39c12',
                'highlight'=> '#f39c12',
            ],
            3 => [
                'color'    => '#00c0ef',
                'highlight'=> '#00c0ef',
            ],
            4 => [
                'color'    => '#3c8dbc',
                'highlight'=> '#3c8dbc',
            ],
            5 => [
                'color'    => '#b33c00',
                'highlight'=> '#b33c00',
            ],
            6 => [
                'color'    => '#990000',
                'highlight'=> '#990000',
            ],
            7 => [
                'color'    => '#24478f',
                'highlight'=> '#24478f',
            ],
            8 => [
                'color'    => '#99b3e6',
                'highlight'=> '#99b3e6',
            ],
            9 => [
                'color'    => '#cc33ff',
                'highlight'=> '#cc33ff',
            ],
            10 => [
                'color'    => '#006600',
                'highlight'=> '#006600',
            ],
            11 => [
                'color'    => '#66ff66',
                'highlight'=> '#66ff66',
            ],
            12 => [
                'color'    => '#c6ff1a',
                'highlight'=> '#c6ff1a',
            ],
        ];

        $chartArray = [];
        $maxArraySize = count($array) > 12 ? 12 : count($array);
        for ($i = 0; $i < $maxArraySize; $i++) {
            $chartArray[$i]['value'] = $array[$i][$value];
            $chartArray[$i]['label'] = $array[$i][$label];
            $chartArray[$i]['color'] = $colors[$i]['color'];
            $chartArray[$i]['highlight'] = $colors[$i]['highlight'];
        }
        Log::debug($chartArray);
        $combine = new Collection($chartArray);
        $combine->sortBy('value', SORT_DESC, SORT_NUMERIC);
        Log::debug($combine->toArray());
        Log::debug('=========================================');
        return $combine->toArray();
    }

    public function totalPeriod($whereDates, $whereOrders) {
        $modelOrders = $this->loadModel('Orders');
        $totalOrders = $modelOrders->find();
        $totalOrders->select(function (\Cake\ORM\Query $subQuery) {
            return [
                'total_amount' => $subQuery->func()->sum(
                    $subQuery->identifier('Items.price')
                ),
            ];
        })
                    ->where($whereDates)
                    ->where($whereOrders)
                    ->innerJoinWith('Items');

        return $totalOrders->toList();
    }

    public function buildOrderList($whereDates, $whereOrders) {
        // orders
        $modelOrders = $this->loadModel('Orders');
        $orders = $modelOrders->find();
        $orders->select(function (\Cake\ORM\Query $subQuery) {
            return [
                'Orders.id',
                'order_amount' => $subQuery->func()->sum(
                    $subQuery->identifier('Items.price')
                ),
            ];
        })
               ->where($whereDates)
               ->where($whereOrders)
               ->innerJoinWith('Items')
               ->group('Orders.id');

        return (new Collection($orders))->combine('id', 'order_amount')->toArray();
    }

    public function subsidiaryReport($whereSubsidiaries, $whereDates) {
        $contain = [
            'Orders' =>
                function ($q) use ($whereDates) {
                    return $q
                        ->where($whereDates)
                        ->order(['date_order' => 'ASC']);
                },
            'Orders.Customers' => ['fields' => ['name']],
            'Orders.Employees' => ['fields' => ['name']],
            'Orders.Items',
        ];

        $modelSubsidiaries = $this->loadModel('Subsidiaries');
        $subsidiaries = $modelSubsidiaries->find();
        $subsidiaries->contain($contain);
        $subsidiaries->select(function (\Cake\ORM\Query $query) {
            return [
                'Subsidiaries.id',
                'order_amount' => $query->func()->sum(
                    $query->identifier('Items.price')
                ),
            ];
        })
                  ->innerJoinWith('Orders')
                  ->innerJoinWith('Orders.Items')
                  ->group('Subsidiaries.id')
                  ->order(['Subsidiaries.name' => 'ASC']);

        if (!is_null($whereDates)) {
            $subsidiaries->where($whereDates);
        }
        if (!is_null($whereSubsidiaries)) {
            $subsidiaries->where($whereSubsidiaries);
        }

        $subsidiaries->select($modelSubsidiaries);
        return $subsidiaries;
    }

    public function employeeReport($whereEmployees, $whereDates) {
        $contain = [
            'Orders' =>
                function ($q) use ($whereDates) {
                    return $q
                        ->where($whereDates)
                        ->order(['date_order' => 'ASC']);
                },
            'Orders.Customers' => ['fields' => ['name']],
            'Orders.Subsidiaries' => ['fields' => ['name']],
            'Orders.Items',
        ];

        $modelEmployees = $this->loadModel('Employees');
        $employees = $modelEmployees->find();
        $employees->contain($contain);
        $employees->select(function (\Cake\ORM\Query $query) {
            return [
                'Employees.id',
                'order_amount' => $query->func()->sum(
                    $query->identifier('Items.price')
                ),
            ];
        })
                  ->innerJoinWith('Orders')
                  ->innerJoinWith('Orders.Items')
                  ->group('Employees.id')
                  ->order(['Employees.name' => 'ASC']);

        if (!is_null($whereDates)) {
            $employees->where($whereDates);
        }
        if (!is_null($whereEmployees)) {
            $employees->where($whereEmployees);
        }

        $employees->select($modelEmployees);
        return $employees;
    }

    // public function supplyReport($whereSupplies, $whereDates) {
    //     $contain = [
    //         'Items',
    //         // 'Items.Orders' =>
    //         //     function ($q) use ($whereDates) {
    //         //         return $q
    //         //             ->where($whereDates)
    //         //             ->order(['date_order' => 'ASC']);
    //         //     },
    //     ];

    //     $modelSupplies = $this->loadModel('Supplies');
    //     $supplies = $modelSupplies->find();
    //     $supplies->select(function (\Cake\ORM\Query $query) {
    //         return [
    //             'Items.id',
    //             'Items.supply_id',
    //             'Items.title',
    //             'Items.price',
    //             'item_amount' => $query->func()->sum(
    //                 $query->identifier('price')
    //             ),
    //         ];
    //     })
    //              ->innerJoinWith('Items')
    //              ->innerJoinWith('Items.Orders')
    //              ->group('Supplies.id')
    //              ->order(['Supplies.name' => 'ASC']);

    //     if (!is_null($whereDates)) {
    //         $supplies->where($whereDates);
    //     }
    //     if (!is_null($whereSupplies)) {
    //         $supplies->where($whereSupplies);
    //     }

    //     // $supplies->select($modelSupplies);
    //     return $supplies;
    // }

    public function supplyReport($whereSupplies, $whereDates) {

    }
}
