<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Collection\Collection;

use Cake\Log\Log;

use Box\Spout\Reader\Common\Creator\ReaderEntityFactory;
// use Box\Spout\Writer\Common\Creator\WriterEntityFactory;

// teste ------------------
use Cake\Datasource\ConnectionManager;
use Cake\Database\Schema\TableSchema;

use Cake\I18n\I18n;
use Cake\I18n\Time;
use Cake\I18n\Number;
use Cake\Database\Type;
// ------------------------

use Cake\Core\Configure;

/**
 * Supplies Controller
 *
 * @property \App\Model\Table\SuppliesTable $Supplies
 *
 * @method \App\Model\Entity\Supply[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SuppliesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('HandlerSku');
    }

    public function mergeWithGlobalTaxes($childTaxes)
    {
        $taxes = [];

        $globalModel = $this->loadModel('Price.Taxes');

        $globalTaxes = $globalModel
                     ->find()
                     ->contain(['Prices' => ['fields' => ['id', 'name', 'sequence', 'active']]])
                     ->order(['Prices.sequence'])
                     ->all();

        foreach ($globalTaxes as $tax) {
            $tax['inherited'] = 1;
            $tax['inherited_value'] = $tax['value'];
            $tax['cost_name'] = $tax['price']['name'];
            $taxes[] = $tax;
        }

        $names = array_column($taxes, 'name');

        foreach ($childTaxes as $childTax) {
            $key = array_search(trim($childTax['name']), $names);
            if ($key !== false) {
                //                $taxes[$key]['price_id'] = $childTax['price_id'];
                //                $taxes[$key]['model'] = $childTax['model'];
                $taxes[$key]['name'] = $childTax['name'];
                $taxes[$key]['value'] = $childTax['value'];
                $taxes[$key]['calculation'] = $childTax['calculation'];
                $taxes[$key]['inherited'] = 0;
            }
        }

        return $taxes;
    }

    public function teste()
    {
        // $taxes = [];

        // $globalModel = $this->loadModel('Price.Taxes');

        // $globalTaxes = $globalModel
        //              ->find()
        //              ->contain(['Prices' => ['fields' => ['id', 'name', 'sequence', 'active']]])
        //              ->order(['Prices.sequence'])
        //              ->all();

        // foreach ($globalTaxes as $tax) {
        //     $tax['inherited'] = 1;
        //     $tax['inherited'] = $tax['value'];
        //     $tax['cost_name'] = $tax['price']['name'];
        //     $taxes[] = $tax;
        // }

        // $names = array_column($taxes, 'name');

        // $this->set(compact('globalPrices'));

        // Prior to 3.5 use I18n::locale()
        //        I18n::setLocale('pt_BR');

        $date = new Time('2015-10-31 23:00:00');

        echo $date . "<br>"; // Displays 05/04/2015 23:00

        echo Number::format(524.23) . "<br>"; // Displays 524,23

        $this->createTemporaryProductsTable();
        die();
    }

    protected function createTemporaryProductsTable($tableName)
    {
        $db = ConnectionManager::get('default');
        $collection = $db->getSchemaCollection();

        // $tableSchema = $collection->describe('products');
        // $columns = $tableSchema->columns();
        // $indexes = $tableSchema->indexes();
        // $options = $tableSchema->options();

        // $constraints = $tableSchema->constraints();
        // $newSchema = new TableSchema($tableName);
        // foreach($columns as $column){
        //     $newSchema->addColumn($column, $tableSchema->getColumn($column));
        // }
        // foreach($indexes as $index){
        //     $newSchema->addIndex($index, $tableSchema->getIndex($index));
        // }
        // foreach($constraints as $constraint) {
        //     $newSchema->addConstraint($constraint,$tableSchema->getConstraint($constraint));
        // }
        // foreach($options as $option) {
        //     $newSchema->options($options);
        // }

        $newSchema = new TableSchema($tableName);

        for ($i = 0; $i < 22; $i++) {
            $newSchema->addColumn('field_' . $i,[
                'type' => 'text',
                'length' => 1024,
                'null' => true,
                'default' => null,
            ]);
        }

        $db->execute('DROP TABLE IF EXISTS ' . $tableName);

        $queries = $newSchema->createSql($db);
        foreach($queries as $sql) {
            $db->execute($sql);
        }
    }
    
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        // $this->paginate = [
        //     'contain' => ['Agents', 'Files']
        // ];
        // $supplies = $this->paginate($this->Supplies);

        // $this->set(compact('supplies'));
        $query = $this->Supplies
               ->find('search', ['search' => $this->request->getQuery()]);
               //            ->where(['name IS NOT' => null]);
        $this->set('supplies', $this->paginate($query));

        //        $this->set(compact('supplies'));
        $this->set('q', isset($data['q']) ? $data['q'] : '');
        $this->set('_serialize', ['supplies']);
    }

    /**
     * View method
     *
     * @param string|null $id Supply id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $supply = $this->Supplies->get($id, [
            'contain' => ['Agents', 'Files', 'Addresses']
        ]);

        $this->set('supply', $supply);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
		$userId = $this->Auth->user('id');

        $supply = $this->Supplies->newEntity();

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if (!empty(implode("", array_values($data['address'])))) {
                $data['address']['model'] = $this->Supplies->registryAlias();
            } else {
                $data['address'] = null;
            }

            if (!empty($data['file']['submittedfile']) && $data['file']['submittedfile']['error'] == 0) {
                $data['file']['user_id'] = $userId; // Optional
                $data['file']['model'] = $this->Supplies->registryAlias();
            }

            $supply = $this->Supplies->patchEntity($supply, $data, [
                'associated' => [
                    'Addresses',
                    'Files'
                ]
            ]);
            if ($this->Supplies->save($supply)) {
                $this->Flash->success(__('O fornecedor foi salvo com sucesso.')); // TRADUZIR

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O fornecedor não pode ser salvo. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $supply->errors(),
                ]]);
        }

        $agents = $this->Supplies->Agents->find('list', ['limit' => 200]);

        $modelStates = $this->loadModel('States');
        $states = $modelStates->find('list')->toArray();
        $this->set(compact('states'));

        $modelCities = $this->loadModel('Cities');
        if (!empty($supply['address']['state_id'])) {
            $cities = $modelCities->find('list')->toArray();
        } else {
            $cities = [];
        }
        $this->set(compact('cities'));

        $this->set(compact('supply', 'agents'));

        $this->render('addedit');
    }

    /**
     * Edit method
     *
     * @param string|null $id Supply id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
		$userId = $this->Auth->user('id');

        $supply = $this->Supplies->get($id, [
            'contain' => [
                    'Addresses',
                    'Files',
                    'SupplyTaxes'
            ]
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $data = $this->request->getData();
            if (!empty(implode("", array_values($data['address'])))) {
                $data['address']['model'] = $this->Supplies->registryAlias();
            } else {
                $data['address'] = null;
            }

            if (!empty($data['supply_taxes'])) {
                $collection = new Collection($data['supply_taxes']);
                $valids = $collection->reject(function ($t, $k) {
                    return $t['inherited'] == '1';
                });

                $data['supply_taxes'] = $valids->toList();
            }

            if (!empty($data['file']['submittedfile']) && $data['file']['submittedfile']['error'] == 0) {
                $data['file']['user_id'] = $userId; // Optional
                $data['file']['model'] = $this->Supplies->registryAlias();
            }

            $supply = $this->Supplies->patchEntity($supply, $data, [
                'associated' => [
                    'Addresses',
                    'Files',
                    'SupplyTaxes'
                ]
            ]);
            if ($this->Supplies->save($supply)) {
                $this->Flash->success(__('O fornecedor foi salvo com sucesso.')); // TRADUZIR

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O fornecedor não pode ser salvo. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $supply->errors(),
                ]]);
        }

        $supply->supply_taxes = $this->mergeWithGlobalTaxes($supply->supply_taxes);

        $agents = $this->Supplies->Agents->find('list', ['limit' => 200]);

        $modelStates = $this->loadModel('States');
        $states = $modelStates->find('list')->toArray();
        $this->set(compact('states'));

        $modelCities = $this->loadModel('Cities');
        if (!empty($supply['address']['state_id'])) {
            $cities = $modelCities->find('list')->toArray();
        } else {
            $cities = [];
        }
        $this->set(compact('cities'));

        $this->set(compact('supply', 'agents'));

        $this->render('addedit');
    }

    /**
     * Delete method
     *
     * @param string|null $id Supply id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $supply = $this->Supplies->get($id);
        if ($this->Supplies->delete($supply)) {
            $this->Flash->success(__('O fornecedor foi excluído.'));
        } else {
            $this->Flash->error(__('O fornecedor não pode ser excluído. Por favor verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $supply->errors(),
                ]]);
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deleteSomeRecords($result = null)
    {
        if ($this->request->is('post')) {
            $ids = explode(",", $this->request->getData('ids'));

            $query = $this->Supplies->query();
            $query->delete();

            foreach($ids as $id)
                $query->orWhere(['id' => $id]);

            if($query->execute()) {
                $this->Flash->success(__('Os fornecedores foram excluídos.'));
            } else {
                $this->Flash->error(__('Os fornecedores não puderam ser excluídos. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $supply->errors(),
                ]]);
            }
        } else {
            $data = '';
        }

        return $this->redirect(['action' => 'index']);
    }
    public function generateCode()
    {
        $this->request->allowMethod('ajax');
        $name = $this->request->query('name');
        $sku_list = $this->HandlerSku->buildSkuList($name);

        foreach($sku_list as $sl) {
            $s = $this->Supplies->find()->where([
                'code' => $sl,
            ])->first();
            if(empty($s)) {
                $sku = $sl;
                break;
            }
        }

        if(empty($sku)) {
            $data['error'] = "Não foi possivel gerar automaticamente o código.";
            $data['code'] = '';
        } else {
            $data['code'] = $sku;
        }

        /*-------------------------------------------------------------
         * Depois de longo e tenebroso inverno, esse trecho funcionou.
         * Depende do layout ajax.ctp e do render json
         */
        $this->set('response', json_encode($data));
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/
    }

    public function uploadTable($id = null)
    {
        $supply = $this->Supplies->get($id);
        $this->set(compact('supply'));
    }

    public function validateTable($id = null) {
        $fields = [
            'sku' => ['description' => __('código'), 'required' => false],
            'title' => ['description' => __('título'), 'required' => true],
            'description' => ['description' => __('descrição'), 'required' => false],
            'long_description' => ['description' => __('descrição longa'), 'required' => false],
            'root_price' => ['description' => __('preço de custo'), 'required' => false],
            'price' => ['description' => __('preço de venda'), 'required' => false],
            'deadline' => ['description' => __('prazo de entrega'), 'required' => false],
            'working_days' => ['description' => __('entrega em dias úteis'), 'required' => false],
        ];

        $theTable = array();
        $supply = $this->Supplies->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {

            $uploadedFile = $this->request->data['submittedfile'];
            //            debug($uploadedFile);
            $withHeader = $this->request->data['withheader'];

            $this->set(compact('uploadedFile'));

            if($uploadedFile['type'] == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                $reader = ReaderEntityFactory::createXLSXReader();
            } elseif ($uploadedFile['type'] == 'application/vnd.oasis.opendocument.spreadsheet') {
                $reader = ReaderEntityFactory::createODSReader();
            }

            $reader->open($uploadedFile['tmp_name']);
            $header = [];

            /*
              types
              0=int, float
              1=string 
              3='' (vazio)
             */

            $tableName = 'tmp_products';
            $this->createTemporaryProductsTable($tableName);
            $db = ConnectionManager::get('default');            
            foreach ($reader->getSheetIterator() as $sheet) {
                // only read data from 1rd sheet
                if ($sheet->getIndex() === 0) { // index is 0-based
                    $numRow = 0;
                    foreach ($sheet->getRowIterator() as $row) {
                        if ($numRow == 0 && $withHeader == 1) {
                            $header = $this->readHeader($row);
                        } else {
                            $cells = $row->getCells();
                            $newRow = [];
                            $i = 0;
                            foreach ($cells as $cell) {
                                // $newRow[$i++] = $cell->getValue();
                                $newRow['field_' . $i++] = $cell->getValue();
                            }
                            $db->insert($tableName, $newRow);
                            $theTable[] = $newRow;
                        }
                        $numRow++;
                    }
                    break; // no need to read more sheets
                }
            }

            // if ((count($this->currentlyProcessedRowData) + $actualNumColumnsRepeated) !== self::MAX_COLUMNS_EXCEL) { 
            $reader->close();
            $this->set(compact('cells'));
            $this->set(compact('theTable'));
            $this->set(compact('header'));
            $this->set(compact('withHeader'));
            $this->set(compact('uploadedFile'));
            $this->set(compact('fields'));
            $this->set(compact('supply'));
        }
    }

    public function insertTable() {
        $system_configuration = Configure::read('system_configurations.configuration');
        $data = $this->request->getData();

        $withHeader = $data['withheader'];
        $products = $this->loadModel('Products');

        $flagError = false;
        
        if ($data['title'] == '') {
            $errors[] = 'A tabela deve conter os títulos dos produtos';
            $flagError = true;
        }

        if ($data['price'] == '' && $data['root_price'] == '') {
            $errors[] = 'A tabela dev conter ou o preço de custo ou o preço de venda.';
            $flagError = true;
        }

        if ($flagError) {
            $this->Flash->error(__('A tabela não pôde ser inserida. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $errors,
                ]]);
        }  else {
            // In a controller.
            $db = ConnectionManager::get('default');            

            $flagError = false;
            //            $products->getConnection()->transactional(function () use ($data, $db) {
            $products->getConnection()->begin();
            $tableName = 'tmp_products';
            $priceCalculated = $data['root_price'] != '' ? 1 : 0;
            //            $priceCalculated = $data['root_price'] == '' ? 0 : 0
            $automaticSku = $data['sku'] != '' ? 0 : 1;
            $supplyId = $data['supply_id'];
            $isStock = 0;
            $supply = $this->Supplies->get($supplyId);
            $supplySku = $supply->code;
            
            $products = $this->loadModel('Products');
            
            $deletados = $products->deleteAll(['supply_id' => $supplyId]);
            
            $results = $db->execute('SELECT * FROM ' . $tableName)->fetchAll('assoc');
            $insertedLines = 0;

            $prefix = $system_configuration['automatic_prefix_on_sku'] == '1' ? $supplySku : '';
            foreach ($results as $row) {
                if (!$this->lineIsEmpty($row)) {
                    $newLine = [];
                    //    $newLine['sku'] =
                    $newLine['supply_sku'] = $supply->code;
                    $newLine['title'] = $row['field_' . $data['title']];
                    if ($automaticSku) {
                        $sku_list = $this->HandlerSku->buildSkuList($newLine['title'], $prefix);
                        foreach($sku_list as $sl) {
                            $s = $products->find()->where([
                                'sku' => $sl,
                            ])->first();
                            if(empty($s)) {
                                $sku = $sl;
                                break;
                            }
                        }
                        if (empty($sku)) {
                            echo 'erro no sku';
                            die();
                        }

                        $newLine['automatic_sku'] = '1';
                        $newLine['sku'] = $sku;
                    } else {
                        $newLine['automatic_sku'] = '0';
                        $newLine['sku'] = $row['field_' . $data['sku']];
                    }
                    $newLine['derived_from'] = null;
                    $newLine['description'] = $data['description'] == '' ? '' : $data['description'];
                    $newLine['long_description'] = $data['long_description'] == '' ? '' : $data['long_description'];
                    if ($priceCalculated) {
                        $newLine['price'] = $data['price'] == '' ? 0 : $row['field_' . $data['price']];
                        $newLine['price_calculated'] = '1';
                        $newLine['root_price'] = $row['field_' . $data['root_price']];
                    } else {
                        $newLine['root_price'] = $data['root_price'] == '' ? 0 : $row['field_' . $data['root_price']];
                        $newLine['price_calculated'] = '0';
                        $newLine['price'] = $row['field_' . $data['price']];
                    }
                    $newLine['deadline'] = $data['deadline'] == '' ? null : $data['deadline'];
                    $newLine['working_days'] = $data['working_days'] == '' ? '0' : $data['working_days'];
                    $newLine['supply_id'] = $supplyId;
                    
                    $product = $products->newEntity($newLine);
                    if(!$products->save($product, ['atomic' => false])) {
                        $flagError = true;
                        if ($product->hasErrors()) {
                            break;
                        }
                    } else {
                        $insertedLines++;
                    }
                }
            }
        } // end foreach
        if ($flagError) {
            $products->getConnection()->rollback();
            //            $lineError = $withHeader == 1 ? $insertedLines + 1 : $insertedLines + 2;
            $lineError = $insertedLines;
            $this->Flash->error('A tabela não pôde ser incluída. Por favor, verifique o erro na linha ' . ($lineError + 1) . ' e tente novamente.', [
                'clear' => true,
                'params' => [
                    'messages' => $product->errors(),
                ]]);
        } else {
            $products->getConnection()->commit();
            $this->Flash->success($insertedLines . ' produtos foram inseridos para o fornecedor \'' . $supply->name . '\' e ' . $deletados . ' produtos que estavam no banco de dados foram excluídos.');
        }
        $this->set(compact('supply'));
    }

    protected function lineIsEmpty($row) {
        foreach ($row as $r) {
            if (trim($r) != '' && !is_null($r)) {
                return false;
            }
        }
        return true;
    }

    protected function readHeader($row) {
        $hd = [];
        $cells = $row->getCells();
        $i = 0;
        foreach ($cells as $cell) {
            $hd[$i++] = $cell->getValue();
        }
        return $hd;
    }        
}
