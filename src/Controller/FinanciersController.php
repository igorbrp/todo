<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Financiers Controller
 *
 * @property \App\Model\Table\FinanciersTable $Financiers
 *
 * @method \App\Model\Entity\Financier[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FinanciersController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        // $financiers = $this->paginate($this->Financiers->find()->contain(['PaymentMethods']));
        // $this->set(compact('financiers'));

        $query = $this->Financiers
               ->find('search', ['search' => $this->request->getQuery()])
               ->contain(['PaymentMethods']);

        $this->set('financiers', $this->paginate($query));
        $this->set('q', isset($data['q']) ? $data['q'] : '');
        $this->set('_serialize', ['financiers']);
    }

    /**
     * View method
     *
     * @param string|null $id Financier id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $financier = $this->Financiers->get($id, [
            'contain' => ['PaymentMethods'],
        ]);

        $this->set('financier', $financier);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $financier = $this->Financiers->newEntity();
        if ($this->request->is('post')) {
            $financier = $this->Financiers->patchEntity($financier, $this->request->getData());
            if ($this->Financiers->save($financier)) {
                $this->Flash->success(__('The financier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The financier could not be saved. Please, try again.'));
        }
        $paymentMethods = $this->Financiers->PaymentMethods->find('list', ['limit' => 200])->where(['id !=' => 1]);

        $this->set(compact('financier', 'paymentMethods'));

        $this->render('addedit');
    }

    /**
     * Edit method
     *
     * @param string|null $id Financier id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $financier = $this->Financiers->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $financier = $this->Financiers->patchEntity($financier, $this->request->getData());
            if ($this->Financiers->save($financier)) {
                $this->Flash->success(__('The financier has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The financier could not be saved. Please, try again.'));
        }
        $paymentMethods = $this->Financiers->PaymentMethods->find('list', ['limit' => 200])->where(['id !=' => 1]);

        $this->set(compact('financier', 'paymentMethods'));

        $this->render('addedit');
    }

    /**
     * Delete method
     *
     * @param string|null $id Financier id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $financier = $this->Financiers->get($id);
        if ($this->Financiers->delete($financier)) {
            $this->Flash->success(__('The financier has been deleted.'));
        } else {
            $this->Flash->error(__('The financier could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
