<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Collection\Collection;
use Cake\I18n\Date;
use Cake\I18n\Time;

use Cake\Database\Expression\IdentifierExpression;
use Cake\ORM\Query;
use Cake\Database\Expression\QueryExpression;
use Cake\Log\Log;

use CakePdf\Pdf\CakePdf;
/**
 * Orders Controller
 *
 * @property \App\Model\Table\OrdersTable $Orders
 *
 * @method \App\Model\Entity\Order[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class OrdersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('CorrecoesDeEmergencia');
    }

    public function buildOrderLine($q)
    {
        $q->contain(['Customers', 'Subsidiaries', 'Employees', 'Users', 'Items', 'Items.Products', 'Items.Supplies', 'Items.Orders']);
        $q->select(function (\Cake\ORM\Query $query) {
            return [
                'Orders.id',
                'order_total_uncovered' => $query->func()->sum(
                    $query->identifier('Items.total_uncovered')
                ),
                'order_total_required' => $query->func()->sum(
                    $query->identifier('Items.total_required')
                ),
                'order_total_received' => $query->func()->sum(
                    $query->identifier('Items.total_received')
                ),
                'order_total_dispatched' => $query->func()->sum(
                    $query->identifier('Items.total_dispatched')
                ),
                'order_total_completed' => $query->func()->sum(
                    $query->identifier('Items.total_completed')
                ),
                'order_amount' => $query->func()->sum(
                    $query->identifier('Items.price')
                ),
                'order_total_qtd' => $query->func()->sum(
                    $query->identifier('Items.qtd')
                ),
            ];
        })
          ->innerJoinWith('Customers')
          ->innerJoinWith('Subsidiaries')
          ->innerJoinWith('Employees')
          ->innerJoinWith('Users')
          ->innerJoinWith('Items')
          ->group('Orders.id');

        return $q;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $data = $this->request->getQuery();

        $orders = $this->Orders->find('search', ['search' => $data]);
        $orders = $this->buildOrderLine($orders);

        $orders->having($this->buildHaving($data));
        $orders->select($this->Orders);

        $employees = $this->Orders->Employees->find('list', ['limit' => 200]);
        $this->set(compact('employees'));

        $subsidiaries = $this->Orders->Subsidiaries->find('list', ['limit' => 200]);
        $this->set(compact('subsidiaries'));

        $modelVehicles = $this->loadModel('Vehicles');
        $vehicles = $modelVehicles->find('list');
        $this->set(compact('vehicles'));

        $this->set('q', isset($data['q']) ? $data['q'] : '');
        $this->set('orders', $this->paginate($orders));
        //        $this->set('_serialize', ['orders']);
    }

    public function buildHaving($data)
    {
        $having = ['1']; // importante

        if(!isset($data['order_total_uncovered']) && // pesquisa padrão
           !isset($data['order_total_required']) &&
           !isset($data['order_total_received']) &&
           !isset($data['order_total_dispatched']) &&
           !isset($data['order_partially']) &&
           !isset($data['order_completed'])) {

            $data['order_total_uncovered'] = $data['order_total_required'] = $data['order_total_received'] = $data['order_total_dispatched'] = $data['order_partially'] = $data['order_completed'] = 0;

        } // else {
        if($data['order_total_uncovered']) { // pendentes
            if($data['order_partially']) {
                $having = ['order_total_uncovered > ' => 0];
            } else {
                //                $having = ['qtd' => new \Cake\Database\Expression\IdentifierExpression('order_total_uncovered')];
                $having = ['order_total_qtd = order_total_uncovered'];
            }
        } else {
            if($data['order_total_required']) { // requisitados
                if($data['order_partially']) {
                    $having = ['order_total_required > ' => 0];
                } else {
                    //                    $having = ['order_total_qtd' => new \Cake\Database\Expression\IdentifierExpression('order_total_required')];
                    $having = ['order_total_qtd = order_total_required'];
                }
            } else {
                if($data['order_total_received']) { // recebidos
                    if($data['order_partially']) {
                        $having = ['order_total_received > ' => 0];
                    } else {
                        //                        $having = ['order_total_qtd' => new \Cake\Database\Expression\IdentifierExpression('order_total_received')];
                        $having = ['order_total_qtd = order_total_received'];
                    }
                } else {
                    if($data['order_total_dispatched']) { // despachados
                        if($data['order_partially']) {
                            $having = ['order_total_dispatched > ' => 0];
                        } else {
                            //                            $having = ['order_total_qtd' => new \Cake\Database\Expression\IdentifierExpression('order_total_dispatched')];
                            $having = ['order_total_qtd = order_total_dispatched'];
                        }
                    } else {
                        if($data['order_completed']) { // inclusive concluidos
                        } else {
                            // créditos a esse link: https://stackoverflow.com/questions/43725445/how-to-compare-two-fields-columns-in-a-condition/43726213#43726213
                            //                            $having = ['order_total_qtd != ' => new \Cake\Database\Expression\IdentifierExpression('order_total_completed')];
                            $having = ['order_total_qtd != order_total_completed'];
                        }
                    }
                }
            }
        }
        // Log::debug(' >>>>>>>>>>> having');
        // Log::debug($having);
        return $having;
    }

    /**
     * View method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $data = $this->request->getQuery();

        $order = $this->Orders->get($id, [
            'contain' => [
                'Customers',
                'Customers.Addresses',
                'Customers.Addresses.Cities',
                'Subsidiaries',
                'Employees',
                'Users',
                'Comments',
                'Comments.Users',
                'Deliveries',
                'Deliveries.Addresses',
                'Items',
                'Payments',
                'Payments.PaymentMethods',
                'Payments.Financiers',
            ]
        ]);

        $orderAmount = $this->Orders
                     ->find()
                     ->select(function (\Cake\ORM\Query $query) {
                         return [
                             'order_amount' => $query->func()->sum(
                                 $query->identifier('Items.price')
                             ),
                         ];
                     })
                     ->innerJoinWith('Items')
                     ->where(['Orders.id' => $id])
                     ->first();

        if ($order->delivery['address']['city_id'] != '') {
            $cities = $this->loadModel('Cities');
            $city = $cities->get($order->delivery['address']['city_id']);
            $cityNameOfCustomer = $city['name'];
        }

        $this->set('cityNameOfCustomer', $cityNameOfCustomer);
        $this->set('orderAmount', $orderAmount);
        $this->set('order', $order);

        if (isset($data['print']) && $data['print']) {
            $this->set('data', $data);
            $this->render('view-print');
        } 

        // if (isset($data['print']) && $data['print']) {

        // $this->viewBuilder()->options([
        //     'pdfConfig' => [
        //         'orientation' => 'portrait',
        //         'filename' => 'Order_' . $id . '.pdf'
        //     ]
        // ]);
        // $this->render('view-pdf');
        
        //        $this->viewBuilder()->setLayout('pdf');
            
        //            $this->set('data', $data);
            //            $this->render('view-pdf');
        // }
    }

    public function romance($id = null)
    {
        $order = $this->Orders
               ->find()
               ->contain([
                   'Customers',
                   'Customers.Addresses',
                   'Customers.Addresses.Cities',
                   'Subsidiaries',
                   'Employees',
                   'Users',
                   'Comments',
                   'Comments.Users',
                   'Deliveries',
                   'Deliveries.Addresses',
                   'Deliveries.DeliveryMades', // function (Query $q) {
//                        return $q
//                            //                           ->select(['body', 'author_id'])
//                            ->where(['Comments.approved' => true]);
// }

                   'Items',
                   'Payments',
                   'Payments.PaymentMethods',
                   'Payments.Financiers',
               ])
               ->select(function (\Cake\ORM\Query $query) {
                   return [
                       'order_amount' => $query->func()->sum(
                           $query->identifier('Items.price')
                       ),
                   ];
               })
               ->innerJoinWith('Customers')
               ->innerJoinWith('Customers.Addresses')
               ->innerJoinWith('Customers.Addresses.Cities')
               ->innerJoinWith('DeliveryMades')
               ->innerJoinWith('Addresses')
               ->innerJoinWith('Subsidiaries')
               ->innerJoinWith('Employees')
               ->innerJoinWith('Users')
               ->innerJoinWith('Items')
               ->where(['Orders.id' => $id])
               //               ->andWhere(['Orders.Deliveries.delivery_mades IS NOT' => null])
               ->matching('Deliveries.DeliveryMades', function ($q) {
                   return $q->where([
                       'DeliveryMades.scheduled_date IS NOT' => null
                   ]);
               })
               ->select($this->Orders)
               ->first();

        if ($order->delivery['address']['city_id'] != '') {
                $cities = $this->loadModel('Cities');
                $city = $cities->get($order->delivery['address']['city_id']);
                $cityNameOfCustomer = $city['name'];
            }

        $this->set('cityNameOfCustomer', $cityNameOfCustomer);
        $this->set('order', $order);

        $this->render('romance_print');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $associated = [
            'Customers',
            'Customers.Addresses',
            'Items',
            'Items.ItemHistories',
            'Items.Outputs',
            'Deliveries',
            'Deliveries.Addresses',
            'Payments',
            'Payments.PaymentMethods',
            'Comments',
        ];

		$userId = $this->Auth->user('id');

        $order = $this->Orders->newEntity();

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            // verifica endereço do cliente
            if($data['unnecessary_customer_address']) {
                $data['customer']['address'] = null;
            } else {
                $data['customer']['address']['model'] = 'Customers';
            }

            // if (!empty($data['customer']['address'])) {
            //     $data['customer']['address']['model'] = 'Customers';
            // }

            // arruma itens
            if (!empty($data['items'])) {
                $collection = new Collection($data['items']);
                $valids = $collection->reject(function ($t, $k) {
                    return trim($t['qtd'] . $t['title']) === '';
                });

                $data['items'] = $valids->toList();

                $deadline = 45; // temporario para testes. deve ser lido da tabela de produtos

                for($i = 0; $i < sizeof($data['items']); $i++) {
                    if($data['items'][$i]['origin'] == 'stock' && empty($data['items'][$i]['id'])) { // item novo para o estoque gera saida
                        // Log::debug('>>>>>>>>>>>>>>> entrou no if do estoque');                        
                        $data['items'][$i]['output'] = [ 'product_id' => $data['items'][$i]['product_id'], 'qtd' => $data['items'][$i]['qtd'] ];
                        $data['items'][$i]['total_received'] = $data['items'][$i]['qtd']; // como e add e stock, o numero de receiveds e igual ao qtd
                        $data['items'][$i]['item_histories'][0] = [
                            'qtd' => $data['items'][$i]['qtd'],
                            'status' => 'received',
                            'deadline' => $deadline,
                            'user_id' => $userId,
                        ];
                    } elseif($data['items'][$i]['origin'] == 'request' && empty($data['items'][$i]['id'])) { //
                        $data['items'][$i]['total_uncovered'] = $data['items'][$i]['qtd']; // como e add e require, o numero de uncovereds e igual ao qtd
                        $data['items'][$i]['item_histories'][0] = [
                            'qtd' => $data['items'][$i]['qtd'],
                            'status' => 'uncovered',
                            'deadline' => $deadline,
                            'user_id' => $userId,
                        ];
                    }
                }
            }

            // verifica entrega
            if($data['delivery_less']) {
                $data['delivery'] = '';
            } else {
                if ($data['delivery']['same_customer_address']) {
                    $data['delivery']['address'] = $data['customer']['address'];
                }
                $data['delivery']['address']['model'] = 'Deliveries';
            }

            if (!empty($data['payments'])) {
                $collection = new Collection($data['payments']);
                $valids = $collection->reject(function ($t, $k) {
                    return trim($t['value']) === '';
                });

                $data['payments'] = $valids->toList();
            }

            if (!empty($data['comment']['body'])) {
                $data['comments'] = null;
                $data['comments'][0]['user_id'] = $userId;
                $data['comments'][0]['model'] = 'Orders';
                $data['comments'][0]['body'] = $data['comment']['body'];
            } else {
                $data['comments'] = null;
            }

            $data['user_id'] = $userId;

            $order = $this->Orders->patchEntity($order, $data, [
                'associated' => $associated,
            ]);

            // se o cliente já existir, pego o id dele
            if($data['existing_customer'] != '') {
                $order->customer['id'] = $data['existing_customer'];
            }

            if ($this->Orders->save($order)) {
                $this->Flash->success(__('O pedido foi salvo com sucesso.')); // TRADUZIR
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('O pedido não pode ser salvo. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $order->errors(),
                ]]);
        }

        $modelStates = $this->loadModel('States');
        $states = $modelStates->find('list')->toArray();
        $this->set(compact('states'));

        $modelCities = $this->loadModel('Cities');
        if (!empty($order['customer']['address']['state_id'])) {
            $cities = $modelCities->find('list')->toArray();
        } else {
            $cities = [];
        }
        $this->set(compact('cities'));

        $customers = $this->Orders->Customers->find('list', ['limit' => 200]);
        $subsidiaries = $this->Orders->Subsidiaries->find('list', ['limit' => 200])->where(['active' => true]);
        $employees = $this->Orders->Employees->find('list', ['limit' => 200])->where(['active' => true]);
        $users = $this->Orders->Users->find('list', ['limit' => 200]);

        $paymentMethods = $this->Orders->Payments->PaymentMethods->find('list', ['limit' => 200]);
        $maxInstallments = $this->Orders->Payments->PaymentMethods->find()->combine('id', 'max_installments')->toArray();

        $modelFinanciers = $this->loadModel('Financiers');
        $allFinanciers = $modelFinanciers->find('list'); // todas as financeiras: credito, debito e cheque
        $credCards = $modelFinanciers->find('list')->where(['payment_method_id' => 2])->where(['active' => true]); // 2 = cartão de crédito
        $debitCards = $modelFinanciers->find('list')->where(['payment_method_id' => 3])->where(['active' => true]); // 3 = cartão de débito
        $banks = $modelFinanciers->find('list')->where(['payment_method_id' => 4])->where(['active' => true]); // 4 = cheques (bancos)

        $queryCountComments = $this->Orders->Comments->find();
        $countComments = $queryCountComments
                       ->select(['count' => $queryCountComments->func()->count('*')])
                       ->where(['foreign_key' => $order->id])
                       ->andWhere(['model' => 'Orders'])
                       ->first();

        $modelFiles = $this->loadModel('Files');
        $rawFiles = $modelFiles->find()->where(['model' => 'SystemUser.SystemUsers'])->toArray();

        $files = array();
        foreach($rawFiles as $f) {
            $files[$f['foreign_key']] = array(
                'path' => $f['path'],
                'hash' => $f['hash'],
                'extension' => $f['extension'],
            );
        }

        $this->set(compact('order', 'customers', 'subsidiaries', 'employees', 'users', 'paymentMethods', 'maxInstallments', 'credCards', 'debitCards', 'banks', 'allFinanciers', 'countComments', 'files'));

        $this->render('addedit');
    }

    /**
     * Edit method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $associated = [
            'Customers',
            'Customers.Addresses',
            'Items',
            'Items.ItemHistories',
            'Items.Outputs',
            'Deliveries',
            'Deliveries.Addresses',
            'Payments',
            'Payments.PaymentMethods',
            'Comments',
            'Comments.Users',
        ];

		$userId = $this->Auth->user('id');

        $order = $this->Orders->get($id, [
            'contain' => $associated,
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            $data['delivery']['freight'] = $this->CorrecoesDeEmergencia->BrCurrencyToValue($data['delivery']['freight']);

            if($data['unnecessary_customer_address']) {
                $data['customer']['address'] = '';
            } else {
                $data['customer']['address']['model'] = 'Customers';
            }

            // if (!empty($data['customer']['address'])) {
            //     $data['customer']['address']['model'] = 'Customers';
            // }

            if (!empty($data['items'])) {
                $collection = new Collection($data['items']);
                $valids = $collection->reject(function ($t, $k) {
                    return trim($t['qtd'] . $t['title']) === '';
                });

                $data['items'] = $valids->toList();

                $deadline = 45; // temporario para testes. deve ser lido da tabela de produtos

                for($i = 0; $i < sizeof($data['items']); $i++) {
                    if($data['items'][$i]['id'] == '') {
                        if($data['items'][$i]['origin'] == 'stock' && empty($data['items'][$i]['id'])) { // item novo para o estoque gera saida
                        // Log::debug('>>>>>>>>>>>>>>> entrou no if do estoque');                        
                            $data['items'][$i]['output'] = [ 'product_id' => $data['items'][$i]['product_id'], 'qtd' => $data['items'][$i]['qtd'] ];
                            $data['items'][$i]['total_received'] = $data['items'][$i]['qtd']; // como e add e stock, o numero de receiveds e igual ao qtd
                            $data['items'][$i]['item_histories'][0] = [
                                'qtd' => $data['items'][$i]['qtd'],
                                'status' => 'received',
                                'deadline' => $deadline,
                                'user_id' => $userId,
                            ];
                        } elseif($data['items'][$i]['origin'] == 'request' && empty($data['items'][$i]['id'])) { //
                            $data['items'][$i]['total_uncovered'] = $data['items'][$i]['qtd']; // como e add e require, o numero de uncovereds e igual ao qtd
                            $data['items'][$i]['output'] = null;
                            $data['items'][$i]['item_histories'][0] = [
                                'qtd' => $data['items'][$i]['qtd'],
                                'status' => 'uncovered',
                                'deadline' => $deadline,
                                'user_id' => $userId,
                            ];
                        }
                    }
                }
            }
            //            debug($data['items']);
            if($data['delivery_less']) {
                $data['delivery'] = '';
            } else {
                if ($data['delivery']['same_customer_address']) {
                    $data['delivery']['address'] = $data['customer']['address'];
                }
                $data['delivery']['address']['model'] = 'Deliveries';
            }

            if (!empty($data['payments'])) {
                $collection = new Collection($data['payments']);
                $valids = $collection->reject(function ($t, $k) {
                    return trim($t['value']) === '';
                });

                $data['payments'] = $valids->toList();
            }

            if (!empty($data['comment']['body'])) {
                $data['comments'] = null;
                $data['comments'][0]['user_id'] = $userId;
                $data['comments'][0]['model'] = 'Orders';
                $data['comments'][0]['body'] = $data['comment']['body'];
            } else {
                $data['comments'] = null;
            }

            $data['user_id'] = $userId;

            $order = $this->Orders->patchEntity($order, $data, [
                'associated' => $associated,
            ]);

            // debug($order['delivery']);
            // die();
            if ($this->Orders->save($order)) {
                $this->Flash->success(__('O pedido foi salvo com sucesso.')); // TRADUZIR
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('O pedido não pode ser salvo. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $order->errors(),
                ]]);
        }

        $order->delivery['freight'] = $this->CorrecoesDeEmergencia->ValueToBrCurrency($order->delivery['freight']);

        $modelStates = $this->loadModel('States');
        $states = $modelStates->find('list'); //->toArray();
        $this->set(compact('states'));

        $modelCities = $this->loadModel('Cities');
        if (!empty($order['customer']['address']['state_id'])) {
            $cities = $modelCities->find('list'); //->toArray();
        } else {
            $cities = [];
        }
        $this->set(compact('cities'));

        $customers = $this->Orders->Customers->find('list', ['limit' => 200]);
        $subsidiaries = $this->Orders->Subsidiaries->find('list', ['limit' => 200])->where(['active' => true]);
        $employees = $this->Orders->Employees->find('list', ['limit' => 200])->where(['active' => true]);
        $users = $this->Orders->Users->find('list', ['limit' => 200]);

        $paymentMethods = $this->Orders->Payments->PaymentMethods->find('list', ['limit' => 200]);
        $maxInstallments = $this->Orders->Payments->PaymentMethods->find()->combine('id', 'max_installments')->toArray();

        $modelFinanciers = $this->loadModel('Financiers');
        $allFinanciers = $modelFinanciers->find('list'); // todas as financeiras: credito, debito e cheque
        $credCards = $modelFinanciers->find('list')->where(['payment_method_id' => 2])->where(['active' => true]); // 2 = cartão de crédito
        $debitCards = $modelFinanciers->find('list')->where(['payment_method_id' => 3])->where(['active' => true]); // 3 = cartão de débito
        $banks = $modelFinanciers->find('list')->where(['payment_method_id' => 4])->where(['active' => true]); // 4 = cheques (bancos)

        $queryCountComments = $this->Orders->Comments->find();
        $countComments = $queryCountComments
                       ->select(['count' => $queryCountComments->func()->count('*')])
                       ->where(['foreign_key' => $order->id])
                       ->andWhere(['model' => 'Orders'])
                       ->first();

        $modelFiles = $this->loadModel('Files');
        $rawFiles = $modelFiles->find()->where(['model' => 'SystemUser.SystemUsers'])->toArray();

        $files = array();
        foreach($rawFiles as $f) {
            $files[$f['foreign_key']] = array(
                'path' => $f['path'],
                'hash' => $f['hash'],
                'extension' => $f['extension'],
            );
        }

        $this->set(compact('order', 'customers', 'subsidiaries', 'employees', 'users', 'paymentMethods', 'maxInstallments', 'credCards', 'debitCards', 'banks', 'allFinanciers', 'countComments', 'files'));

        $this->render('addedit');
    }

    /**
     * Delete method
     *
     * @param string|null $id Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $order = $this->Orders->get($id);
        if ($this->Orders->delete($order)) {
            $this->Flash->success(__('O pedido foi excluído.'));
        } else {
            $this->Flash->error(__('Não foi possível excluir o pedido, por favor verifique os erros e tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deleteSomeRecords($result = null)
    {
        if ($this->request->is('post')) {
            $ids = explode(",", $this->request->getData('ids'));

            $query = $this->Orders->query();
            $query->delete();

            foreach($ids as $id)
                $query->orWhere(['id' => $id]);

            if($query->execute()) {
                $this->Flash->success(__('Os pedidos foram excluídos.'));
            } else {
                $this->Flash->error(__('Não foi possível excluir os pedidos, por favor verifique os erros e tente novamente..'));
            }
        } else {
            $data = '';
        }

        return $this->redirect(['action' => 'index']);
    }

    public function changeStatusBulk()
    {
        $this->request->allowMethod('ajax');

        $data = $this->request->getQueryParams();

        $query = $this->Orders
               ->find()
               ->select(['Orders.id', 'Orders.number'])
               ->contain([
                   'Items',
                   'Items.ItemHistories',
                   'Items.ItemHistories.Invoices',
                   'Items.ItemHistories.DeliveryMades',
                   'Items.Requisitions',
                   'Items.Requisitions.OutboxEmails',
                   'Items.Supplies' => [
                     'fields' => [
                         'email',
                     ],
                   ],
                   'Deliveries' => [
                       'fields' => [
                           'id',
                       ],
                   ],
               ])
               ->where(['Orders.id' => $data['order_id']]);
        $order = $query->first();

        switch ($data['to']) {
        case 'required':
            $this->makeRequisition($order, $data);
            break;
        case 'received':
            $this->makeReception($order, $data);
            break;
        case 'dispatched':
            $this->makeDispatch($order, $data);
            break;
        case 'completed':
            $this->makeConclusion($order, $data);
            break;
        }

        //        die();

        unset($order);
        $queryOrder = $this->Orders->find();
        $queryOrder = $this->buildOrderLine($queryOrder);
        $queryOrder->where(['Orders.id' => $data['order_id']]);
        $queryOrder->select($this->Orders)->first();
        // Log::debug('OrdersController saindo de changeStatusBulk');
        $order = $queryOrder->toArray()[0];
        // Log::debug($order);
        //        die();
        $this->set(compact('order'));
        $this->viewBuilder()->setLayout('ajax');
        $this->render('orders_details');
    }

    public function makeRequisition($order, $data)
    {
        $collection = new Collection($order['items']);
        $uncoveredItems = $collection->filter(function ($item, $key) {
            return $item->total_uncovered > 0;
        })->toArray();

        $collection = new Collection($uncoveredItems);
        $uncoveredGoupedItems = $collection->groupBy('supply_id')->toArray();

        foreach ($uncoveredGoupedItems as $itemsBySupply) {
            $tmpRequisition = [];
            $itemsRequisition = [];
            $tmpRequisition['required'] = Time::now();

            $message = 'Olá, ' . $itemsBySupply[0]['contact'] . '\r\n\r\n';
            $message .= 'Estamos requisitando o(s) produto(s) descrito(s) abaixo:\r\n';
            $message .= "pedido " . $order->number . '\r\n';
            foreach ($itemsBySupply as $item) {
                $message .= 'quantidade: ' . $item['qtd'] . '\r\n';
                $message .= 'produto: ' . $item['title'] . '\r\n';
                if (!empty($item['complement'])) {
                    $message .= 'complemento: ' . $item['complement'] . '\r\n';
                }
            }

            // Log::debug('ordercontroller->makerequisition o email da porra do fornecedor');
            // Log::debug($itemsBySupply[0]['email']);

            $tmpRequisition['outbox_email'] = [
                'model' => 'Requisitions',
                //                'subject' => 'Requisição de material a ' . $itemsBySupply[0]['supply_name'],
                'subject' => 'Requisição para pedido: ' . $order->number . ' - ' . $itemsBySupply[0]['supply_name'],
                'recipient' => $itemsBySupply[0]['supply']['email'],
                'message' => $message,
                'sented' => 0,
                'fails' => 0,
                'last_message' => '',
            ];

            $requisition = $this->Orders->Items->Requisitions->newEntity($tmpRequisition, ['associated' => ['OutboxEmails']]);
     
            $this->Orders
                ->getConnection()
                ->transactional(function() use ($requisition, $order, $itemsBySupply){
                    $this->Orders->Items->Requisitions->save($requisition);
                    foreach ($itemsBySupply as &$tmpItem) {
                        $newItem = [];
                        $newItem['requisitions'] = [
                            '_ids' => [$requisition->id],
                        ];
                        
                        $item_histories_status = $this->Orders->Items->ItemHistories->getItemHistoryStatus($tmpItem['id']);
                        $newItem['total_uncovered'] = $item_histories_status['total_uncovered'] - $tmpItem['qtd'];
                        $newItem['total_required'] = $item_histories_status['total_required'] + $tmpItem['qtd'];
                        $newItem['total_received'] = $item_histories_status['total_received'];
                        $newItem['total_dispatched'] = $item_histories_status['total_dispatched'];
                        $newItem['total_completed'] = $item_histories_status['total_completed'];

                        $newItem['item_histories'][] = [
                            'qtd' => $tmpItem['qtd'],
                            'status' => 'required',
                            'deadline' => 0,
                            'user_id' => $this->request->session()->read('Auth.User.id'),
                        ];

                        $item = $this->Orders->Items->get($tmpItem['id']);
                        $item = $this->Orders->Items->patchEntity($item, $newItem, [
                            'associated' => [
                                'ItemHistories',
                                'Requisitions',
                                'Requisitions.OutboxEmails'
                            ],
                            'matching' => [
                                'Requisitions',
                            ],
                        ]);
                        // Log::debug('OrdersController->makeRequisition $item');
                        // Log::debug($item);
                        $this->Orders->Items->save($item);
                    }
                });
        }
    }

    public function makeReception($order, $data)
    {
        $collection = new Collection($order['items']);
        $requiredItems = $collection->filter(function ($item, $key) {
            return $item->total_required > 0;
        })->toArray();

        // Log::debug('$data[\'invoice\'] ================================================');
        // Log::debug($data['invoice']);

        $this->Orders
            ->getConnection()
            ->transactional(function() use ($data, $requiredItems){

                if (isset($data['invoice'])) {
                    $invoice['invoice_number'] = $data['invoice'];
                    $invoice = $this->Orders->Items->ItemHistories->Invoices->newEntity($invoice);
                    $this->Orders->Items->ItemHistories->Invoices->save($invoice);
                    $invoiceId = $invoice->id;
                } else {
                    $invoiceId = null;
                }
    
                foreach ($requiredItems as &$tmpItem) {
                    
                    // $tmpItem['item_histories'][] = [
                    //     'qtd' => $tmpItem['qtd'],
                    //     'status' => 'received',
                    //     'deadline' => 0,
                    //     'invoice_id' => $invoiceId,
                    //     'user_id' => $this->request->session()->read('Auth.User.id'),
                    // ];
                    $newItem = [];
                    $newItem['item_histories'][] = [
                        'qtd' => $tmpItem['qtd'],
                        'status' => 'received',
                        'deadline' => 0,
                        'invoice_id' => $invoiceId,
                        'user_id' => $this->request->session()->read('Auth.User.id'),
                    ];

                    $item_histories_status = $this->Orders->Items->ItemHistories->getItemHistoryStatus($tmpItem['id']);
                    $newItem['total_uncovered'] = $item_histories_status['total_uncovered'];
                    $newItem['total_required'] = $item_histories_status['total_required'] - $tmpItem['qtd'];
                    $newItem['total_received'] = $item_histories_status['total_received'] + $tmpItem['qtd'];
                    $newItem['total_dispatched'] = $item_histories_status['total_dispatched'];
                    $newItem['total_completed'] = $item_histories_status['total_completed'];
            
                    $item = $this->Orders->Items->get($tmpItem['id']);
                    $item = $this->Orders->Items->patchEntity($item, $newItem, [
                        'associated' => [
                            'ItemHistories',
                            'ItemHistories.Invoices',
                        ],
                    ]);

                    $this->Orders->Items->save($item);
                }
            });
    }

    public function makeDispatch($order, $data)
    {
        $collection = new Collection($order['items']);
        $receivedItems = $collection->filter(function ($item, $key) {
            return $item->total_received > 0;
        })->toArray();
        
        $dispatch_date = isset($data['dispatch_date']) ? $data['dispatch_date'] : null;
        $vehicle = isset($data['vehicle']) ? $data['vehicle'] : null;

        $deliveryMade['delivery_id'] = $order->delivery->id;
        $deliveryMade['vehicle_id'] = $vehicle;
        $deliveryMade['scheduled_date'] = $dispatch_date;
        $deliveryMade['scheduled_horary'] = 'any';

        foreach ($receivedItems as $tmpItem) {
            $newItem = [];
            $newItem['item_histories'][] = [
                'qtd' => $tmpItem['qtd'],
                'status' => 'dispatched',
                'deadline' => 0,
                'delivery_made' => $deliveryMade,
                'user_id' => $this->request->session()->read('Auth.User.id'),
            ];
            $item_histories_status = $this->Orders->Items->ItemHistories->getItemHistoryStatus($tmpItem['id']);
            $newItem['total_uncovered'] = $item_histories_status['total_uncovered'];
            $newItem['total_required'] = $item_histories_status['total_required'];
            $newItem['total_received'] = $item_histories_status['total_received'] - $tmpItem['qtd'];
            $newItem['total_dispatched'] = $item_histories_status['total_dispatched'] + $tmpItem['qtd'];
            $newItem['total_completed'] = $item_histories_status['total_completed'];
            
            $item = $this->Orders->Items->get($tmpItem['id']);
            $item = $this->Orders->Items->patchEntity($item, $newItem, [
                'associated' => [
                    'ItemHistories',
                    'ItemHistories.DeliveryMades',
                ],
            ]);
            $this->Orders->Items->save($item);
        }
    }

    public function makeConclusion($order, $data)
    {
        $collection = new Collection($order['items']);
        $dispatchedItems = $collection->filter(function ($item, $key) {
            return $item->total_dispatched > 0;
        })->toArray();

        foreach ($dispatchedItems as $tmpItem) {
            $collection = new Collection($tmpItem['item_histories']);
            $itemHistory = $collection->filter(function ($itemHistory, $key) {
                return $itemHistory->status == 'dispatched' && $itemHistory->qtd > 0;
            })->sortBy('id', SORT_DESC)->first()->toArray();

            // $itemHistory = $this->Orders->Items->ItemHistories->get($dispatchedItemHistories['id'], [
            //     'contain' => [
            //         'DeliveryMades',
            //     ]
            // ]);

            //            $itemHistory = $dispatchedItemHistories->toArray();

            // Log::debug('$itemHistory 1 -----------------------------------------------------------------------------------------');
            // Log::debug($itemHistory);

            $itemHistory['status'] = 'completed';
            $dt = new Date();
            $itemHistory['delivery_made']['delivery_date'] = $dt;
            // $itemHistory->setDirty('status', true);
            // $itemHistory->delivery_made->setDirty('delivery_date', true);

            // Log::debug('$itemHistory 2 -----------------------------------------------------------------------------------------');
            // Log::debug($itemHistory);

            $item_histories_status = $this->Orders->Items->ItemHistories->getItemHistoryStatus($itemHistory['item_id']);
            $newItem = [];
            $newItem['total_uncovered'] = $item_histories_status['total_uncovered'];
            $newItem['total_required'] = $item_histories_status['total_required'];
            $newItem['total_received'] = $item_histories_status['total_received'];
            $newItem['total_dispatched'] = $item_histories_status['total_dispatched'] - $tmpItem['qtd'];
            $newItem['total_completed'] = $item_histories_status['total_completed'] + $tmpItem['qtd'];

            $newItem['item_histories'][] = $itemHistory;

            // Log::debug('$newItem[\'item_histories\'] -----------------------------------------------------------------------------------------');
            // Log::debug($newItem['item_histories']);
            // die();

            // Log::debug('$newItem ------------------------------');
            // Log::debug($newItem);

            $item = $this->Orders->Items->get($itemHistory['item_id'], [
                'contain' => [
                    'ItemHistories',
                    'ItemHistories.DeliveryMades',
                ],
            ]);
            
            $item = $this->Orders->Items->patchEntity($item, $newItem, [
                'associated' => [
                    'ItemHistories',
                    'ItemHistories.DeliveryMades',
                ],
            ]);
            
            // $item->item_histories->setDirty('delivery_made', true);
            // $item->setDirty('item_histories', true);
            // Log::debug('$item ---------------------------------');
            // Log::debug($item);
            $this->Orders->Items->save($item);
        }

        // foreach ($receivedItems as $tmpItem) {
        //     $newItem = [];
        //     $newItem['item_histories'][] = [
        //         'qtd' => $tmpItem['qtd'],
        //         'status' => 'completed',
        //         'deadline' => 0,
        //         'user_id' => $this->request->session()->read('Auth.User.id'),
        //     ];
        //     $item_histories_status = $this->Orders->Items->ItemHistories->getItemHistoryStatus($tmpItem['id']);
        //     $newItem['total_uncovered'] = $item_histories_status['total_uncovered'];
        //     $newItem['total_required'] = $item_histories_status['total_required'];
        //     $newItem['total_received'] = $item_histories_status['total_received'];
        //     $newItem['total_dispatched'] = $item_histories_status['total_dispatched'] - $tmpItem['qtd'];
        //     $newItem['total_completed'] = $item_histories_status['total_completed'] + $tmpItem['qtd'];
            
        //     $item = $this->Orders->Items->get($tmpItem['id']);
        //     $item = $this->Orders->Items->patchEntity($item, $newItem, [
        //         'associated' => [
        //             'ItemHistories',
        //             'ItemHistories.DeliveryMades',
        //         ],
        //     ]);
        //     $this->Orders->Items->save($item);
        // }
    }

    public function teste()
    {
        $reader = ReaderEntityFactory::createReaderFromFile('file.ods');

        // debug($reader); 

        $reader->open('planilha.ods');

        // debug($reader);
        // debug($reader->getSheetIterator());
        // die();

        $result = [];

        foreach ($reader->getSheetIterator() as $sheet) {
            $result = $sheet->getRowIterator();
            //            debug($sheet->getIndex());
            foreach ($sheet->getRowIterator() as $row) {
                // do stuff with the row
                debug($row);
                $cells[] = $row->getCells();
            }
        }

        $reader->close();
        die();
        //        $this->set('cells', $result);
        $this->set('cells', $cells);


    //     $supplies = $this->Orders->Items
    //            ->find()
    //            ->select(['supply_id'])
    //            ->where(['order_id' => 4])
    //            ->distinct(['supply_id']);

    //     $supplies->enableHydration(false);

    //     foreach ($supplies as $supply) {
    //         echo "<br><h1> items do " . $supply['supply_id'] . "</h1><br>";
    //         $items = $this->Orders->Items
    //                ->find()
    //                ->where(['order_id' => 4])
    //                ->andWhere(['supply_id' => $supply['supply_id']]);
    //         $items->enableHydration(false);
    //         debug($items->toList());
    //     }

    }
}
