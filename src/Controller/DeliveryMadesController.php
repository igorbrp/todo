<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Log\Log;

/**
 * DeliveryMades Controller
 *
 * @property \App\Model\Table\DeliveryMadesTable $DeliveryMades
 *
 * @method \App\Model\Entity\DeliveryMade[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DeliveryMadesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('CorrecoesDeEmergencia');
    }

    public function updateScheduledDate()
    {
        $this->request->allowMethod('ajax');
        $deliveryId = $this->request->query('delivery_id');
        $scheduledDate = $this->CorrecoesDeEmergencia->BrDateToUtcFormat($this->request->query('scheduled_date'));
        $scheduledNewDate = $this->CorrecoesDeEmergencia->BrDateToUtcFormat($this->request->query('scheduled_new_date'));
        $scheduledHorary = $this->request->query('scheduled_horary');
        $vehicleId = $this->request->query('vehicle_id');

        $query = $this->DeliveryMades->query();
        $data = $query->update()
                         ->set(['scheduled_date' => $scheduledNewDate])
                         ->where(['delivery_id' => $deliveryId])
                         ->andWhere(['scheduled_date' => $scheduledDate])
                         ->andWhere(['scheduled_horary' => $scheduledHorary])
                         ->andWhere(['vehicle_id' => $vehicleId])
                         ->execute();

        /*-------------------------------------------------------------
         * Depois de longo e tenebroso inverno, esse trecho funcionou.
         * Depende do layout ajax.ctp e do render json
         */
        $this->set('response', json_encode($data));
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/
    }

    public function updateScheduledHorary()
    {
        $this->request->allowMethod('ajax');
        $deliveryId = $this->request->query('delivery_id');
        $scheduledDate = $this->CorrecoesDeEmergencia->BrDateToUtcFormat($this->request->query('scheduled_date'));
        $scheduledHorary = $this->request->getQuery('scheduled_horary');
        $scheduledNewHorary = $this->request->getQuery('scheduled_new_horary');
        $vehicleId = $this->request->query('vehicle_id');

        Log::debug($deliveryId);
        Log::debug($scheduledHorary);
        Log::debug($scheduledNewHorary);
        Log::debug($vehicleId);
        Log::debug($scheduledDate);
        
        $query = $this->DeliveryMades->query();
        $data = $query->update()
                         ->set(['scheduled_horary' => $scheduledNewHorary])
                         ->where(['delivery_id' => $deliveryId])
                         ->andWhere(['scheduled_date' => $scheduledDate])
                         ->andWhere(['scheduled_horary' => $scheduledHorary])
                         ->andWhere(['vehicle_id' => $vehicleId])
                         ->execute();

        /*-------------------------------------------------------------
         * Depois de longo e tenebroso inverno, esse trecho funcionou.
         * Depende do layout ajax.ctp e do render json
         */
        $this->set('response', json_encode($data));
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/
    }

    public function updateVehicle()
    {
        $this->request->allowMethod('ajax');
        $deliveryId = $this->request->query('vehicle_id');
        $scheduledDate = $this->CorrecoesDeEmergencia->BrDateToUtcFormat($this->request->query('scheduled_date'));
        $scheduledHorary = $this->request->query('scheduled_horary');
        $vehicleId = $this->request->query('vehicle_id');
        $vehicleNewId = $this->request->query('vehicle_new_id');
        $query = $this->DeliveryMades->query();
        $data = $query->update()
                         ->set(['vehicle_id' => $vehicleNewId])
                         ->where(['delivery_id' => $deliveryId])
                         ->andWhere(['scheduled_date' => $scheduledDate])
                         ->andWhere(['scheduled_horary' => $scheduledHorary])
                         ->andWhere(['vehicle_id' => $vehicleId])
                         ->execute();

        /*-------------------------------------------------------------
         * Depois de longo e tenebroso inverno, esse trecho funcionou.
         * Depende do layout ajax.ctp e do render json
         */
        $this->set('response', json_encode($data));
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/
    }
}
