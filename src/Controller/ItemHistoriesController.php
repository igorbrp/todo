<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\ORM\TableRegistry;

use Cake\Collection\Collection;

use Cake\Log\Log;

/**
 * ItemHistories Controller
 *
 * @property \App\Model\Table\ItemHistoriesTable $ItemHistories
 *
 * @method \App\Model\Entity\ItemHistory[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ItemHistoriesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('ItemHistories');
        //        $this->loadModel('Items');
        $this->loadModel('Requisitions');
    }

    public function getInfo($item_id = null)
    {
        // $this->loadModel('ItemHistories');
        // $this->loadModel('Items');
        $filesModel = $this->loadModel('Files');
        $files = $filesModel->find()
                      ->formatResults(
                          function (\Cake\Collection\CollectionInterface $results) {
                              return $results->map(function ($row) {
                                  $row['avatar'] = $row['path'] . DS . $row['hash'] . '.mini.' . $row['extension'];
                                  return $row;
                              });
                          })
                      ->where(['model' => 'SystemUser.SystemUsers'])
                      ->all();

        $avatars = (new Collection($files->toArray()))->combine('user_id', 'avatar')->toArray();

        $item_histories = $this->ItemHistories->getItemHistory($item_id);
        $item_histories_status = $this->ItemHistories->getItemHistoryStatus($item_id);
        //        $itemModel = $this->loadModel('Items');
        //        $actualItem = $this->Items->get($item_id, [
        $actualItem = $this->ItemHistories->Items->get($item_id, [
            'fields' => [
                'id',
                'total_uncovered',
                'total_required',
                'total_received',
                'total_dispatched',
                'total_completed',
                'origin',
            ]]);


        $this->set(compact('avatars'));
        $this->set(compact('actualItem'));
        $this->set(compact('item_histories'));
        $this->set(compact('item_histories_status'));

        $this->viewBuilder()->setLayout('ajax');
        $this->render('item_histories_details');
    }

    public function changeStatus()
    {
        $this->request->allowMethod('ajax');

        $data = $this->request->getQueryParams();

        $item_id = isset($data['item_id']) ? $data['item_id'] : null;
        $from = isset($data['from']) ? $data['from'] : null;

        $item = $this->ItemHistories->Items
              ->get($item_id, [
                  'contain' => [
                      'Orders' => [
                          'fields' => [
                              'number',
                          ],
                      ],
                      'Orders.Deliveries',
                      'Supplies' => [
                          'fields' => [
                              'contact',
                              'email',
                              'name',
                          ],
                      ],
                      'Outputs',
                      'ItemHistories',
                      'ItemHistories.Invoices',
                      'ItemHistories.DeliveryMades',
                      'Requisitions',
                      'Requisitions.OutboxEmails',
                  ],
                  'matching' => [
                      'Requisitions',
                  ],
              ]);
        
        if(!isset($item)) {
            $message = "erro ao LER item";
        }

        switch ($from) {
        case 'uncovered': // VOU REQUISITAR
            $this->makeRequisition($item, $data);
            break;

        case 'required': // VOU RECEBER OU CANCELAR REQUISIÇÃO
            if ($data['to'] == 'uncovered') { // cancelamento de requisição
                $this->cancelRequisition($item, $data);
            } else { // recebimento
                $this->makeReception($item, $data);
            }
            break;

        case 'received': // VOU DESPACHAR OU CANCELAR RECEBIMENTO
            if ($data['to'] == 'required') { // cancelamento de recebimento
                $this->cancelReception($item, $data);
            } else { //
                $this->makeDispatch($item, $data);
            }
            break;

        case 'dispatched': // VOU CONCLUIR OU CANCELAR DESPACHO
            if ($data['to'] == 'received') { // cancelamento de despacho
                $this->cancelDispatch($item, $data);
            } else {
                $this->makeConclusion($item, $data);
            }
            break;

        case 'completed': // AQUI EU SÓ POSSO CANCELAR A CONCLUSÃO
            if ($data['to'] == 'dispatched') { // cancelamento de completado
                $this->cancelConclusion($item, $data);
            }
            break;
        }

        $this->getInfo($item_id);
    }

    public function makeRequisition($item, $data) // está uncovered e será requisitado
    {
        $from = isset($data['from']) ? $data['from'] : null;
        $to = 'required'; // isset($data['to']) ? $data['to'] : null;
        $qtd = $data['qtd'];
        $arrItem['item_histories'][] = [
            'qtd' => isset($data['qtd']) ? $data['qtd'] : null,
            'status' => isset($data['to']) ? $data['to'] : null,
            'deadline' => 0,
            'user_id' => $this->request->session()->read('Auth.User.id'),
        ];

        $ref = "pedido " . $item->order->number . '\r\n';

        $message = 'Olá, ' . $item->supply->contact . '.\r\n\r\n'
                 . 'Estamos requisitando o(s) produto(s) descrito(s) abaixo:\r\n'
                 . $ref . '\r\n'
                 . 'quantidade: ' . $item->qtd . '\r\n'
                 . 'produto: ' . $item->title . '\r\n';

        if (!empty($item->complement)) {
            $message .= 'complemento: ' . $item->complement . '\r\n';
        }

        $arrItem['requisitions'][] = [
            'required' => Time::now(),
            'outbox_email' => [
                'model' => 'Requisitions',
                //                'subject' => 'Requisição de material a '. $item->supply_name,
                'subject' => 'Requisição para pedido: ' . $item->order->number . ' - ' . $item->supply->name,
                'recipient' => $item->supply->email,
                'message' => $message,
                'sented' => 0,
                'fails' => 0,
                'last_message' => '',
            ],
        ];

        $item_histories_status = $this->ItemHistories->getItemHistoryStatus($item['id']);
        $arrItem['total_uncovered'] = $item_histories_status['total_uncovered'] - $qtd;
        $arrItem['total_required'] = $item_histories_status['total_required'] + $qtd;
        $arrItem['total_received'] = $item_histories_status['total_received'];
        $arrItem['total_dispatched'] = $item_histories_status['total_dispatched'];
        $arrItem['total_completed'] = $item_histories_status['total_completed'];

        $item = $this->ItemHistories->Items->patchEntity($item, $arrItem, [
            'associated' => [
                'Outputs',
                'ItemHistories',
                'Requisitions',
                'Requisitions.OutboxEmails',
            ],
            'matching' => [
                'Requisitions',
            ],
        ]);

        // Log::debug('ItemHistoriesController->makeRequisition $item');
        // Log::debug($item);
        $this->ItemHistories->Items->save($item);
    }

    public function cancelRequisition($item, $data)
    {
        $from = $data['from'];
        $to = $data['to'];
        $qtd = $data['qtd'];

        $oldRequisition = null;
        
        $arrItem['item_histories'][] = [
            'qtd' => $qtd * -1,
            'status' => 'required',
            'deadline' => 0,
            'user_id' => $this->request->session()->read('Auth.User.id'),
        ];

        $item_histories_status = $this->ItemHistories->getItemHistoryStatus($item['id']);
        $arrItem['total_uncovered'] = $item_histories_status['total_uncovered'] + $qtd;
        $arrItem['total_required'] = $item_histories_status['total_required'] - $qtd;
        $arrItem['total_received'] = $item_histories_status['total_received'];
        $arrItem['total_dispatched'] = $item_histories_status['total_dispatched'];
        $arrItem['total_completed'] = $item_histories_status['total_completed'];

        if (isset($item->requisitions[0])) {
            $oldRequisition = $this->Requisitions->get($item->requisitions[0]['id']);
        }

        $arrItem['requisitions'] = [];

        $item = $this->ItemHistories->Items->patchEntity($item, $arrItem, [
            'associated' => [
                'ItemHistories',
            ],
        ]);

        $this->ItemHistories->getConnection()
                            ->transactional(function() use ($item, $oldRequisition){
                                $result = $this->ItemHistories->Items->save($item);
                                if (!is_null($oldRequisition)) {
                                    $this->Requisitions->delete($oldRequisition);
                                }
                            });
    }

    public function makeReception($item, $data)
    {
        $from = isset($data['from']) ? $data['from'] : null;
        $to = isset($data['to']) ? $data['to'] : null;
        $qtd = isset($data['qtd']) ? $data['qtd'] : null;

        if (isset($data['invoice'])) {
            $invoice['invoice_number'] = $data['invoice'];
            $invoice = $this->ItemHistories->Invoices->newEntity($invoice);
        } else {
            $invoice = '';
            // Log::debug('data invoice NÃO ESTÁ setado');
        }

        $arrItem['item_histories'][] = [
            'qtd' => $qtd,
            'status' => $to,
            'deadline' => 0,
            'user_id' => $this->request->session()->read('Auth.User.id'),
            'invoice_id' => null,
        ];

        $item_histories_status = $this->ItemHistories->getItemHistoryStatus($item['id']);
        $arrItem['total_uncovered'] = $item_histories_status['total_uncovered'];
        $arrItem['total_required'] = $item_histories_status['total_required'] - $qtd;
        $arrItem['total_received'] = $item_histories_status['total_received'] + $qtd;
        $arrItem['total_dispatched'] = $item_histories_status['total_dispatched'];
        $arrItem['total_completed'] = $item_histories_status['total_completed'];

        $item = $this->ItemHistories->Items->patchEntity($item, $arrItem, [
            'associated' => [
                'ItemHistories',
                'ItemHistories.Invoices',
            ],
        ]);

        $this->ItemHistories
            ->getConnection()
            ->transactional(function() use ($item, $invoice){
                if ($invoice !== '') {
                    $this->ItemHistories->Invoices->save($invoice);
                    $item->item_histories[0]['invoice_id'] = $invoice->id;
                }
                $this->ItemHistories->Items->save($item);
            });
    }

    public function cancelReception($item, $data)
    {
        $from = $data['from'];
        $to = $data['to'];
        $qtd = $data['qtd'];

        $invoiceId = null;
        foreach ($item['item_histories'] as $ih) {
            if (!is_null($ih['invoice'])) {
                $invoiceId = $ih['invoice']['id'];
            }
        }

        $arrItem['item_histories'][] = [
            'qtd' => $qtd * -1,
            'status' => 'received',
            'deadline' => 0,
            'user_id' => $this->request->session()->read('Auth.User.id'),
        ];

        $item_histories_status = $this->ItemHistories->getItemHistoryStatus($item['id']);
        $arrItem['total_uncovered'] = $item_histories_status['total_uncovered'];
        $arrItem['total_required'] = $item_histories_status['total_required'] + $qtd;
        $arrItem['total_received'] = $item_histories_status['total_received'] - $qtd;
        $arrItem['total_dispatched'] = $item_histories_status['total_dispatched'];
        $arrItem['total_completed'] = $item_histories_status['total_completed'];

        $item = $this->ItemHistories->Items->patchEntity($item, $arrItem, [
            'associated' => [
                'ItemHistories',
            ],
        ]);

        $this->ItemHistories->getConnection()
                            ->transactional(function() use ($item, $invoiceId){
                                $this->ItemHistories->Items->save($item);
                                if (!is_null($invoiceId)) {
                                    $invoice = $this->ItemHistories->Invoices->get($invoiceId);
                                    $this->ItemHistories->Invoices->delete($invoice);
                                }
                            });
    }

    public function makeDispatch($item, $data) // deapacha ou retorna a required
    {
        $from = isset($data['from']) ? $data['from'] : null;
        $to = 'dispatched'; // isset($data['to']) ? $data['to'] : null;
        $qtd = $data['qtd'];

        $dispatch_date = isset($data['dispatch_date']) ? $data['dispatch_date'] : null;
        $vehicle = isset($data['vehicle']) ? $data['vehicle'] : null;

        $deliveryMade['delivery_id'] = $item->order->delivery->id;
        $deliveryMade['vehicle_id'] = $vehicle;
        $deliveryMade['scheduled_date'] = $dispatch_date;
        $deliveryMade['scheduled_horary'] = 'any';

        $arrItem['item_histories'][] = [
            'qtd' => $qtd, // isset($data['qtd']) ? $data['qtd'] : null,
            'status' => $to, // isset($data['to']) ? $data['to'] : null,
            'deadline' => 0,
            'delivery_made' => $deliveryMade,
            'user_id' => $this->request->session()->read('Auth.User.id'),
        ];

        $item_histories_status = $this->ItemHistories->getItemHistoryStatus($item['id']);
        $arrItem['total_uncovered'] = $item_histories_status['total_uncovered'];
        $arrItem['total_required'] = $item_histories_status['total_required'];
        $arrItem['total_received'] = $item_histories_status['total_received'] - $qtd;
        $arrItem['total_dispatched'] = $item_histories_status['total_dispatched'] + $qtd;
        $arrItem['total_completed'] = $item_histories_status['total_completed'];

        $item = $this->ItemHistories->Items->patchEntity($item, $arrItem, [
            'associated' => [
                'ItemHistories',
                'ItemHistories.DeliveryMades',
            ],
        ]);

        // Log::debug('$arrItem');
        // Log::debug($arrItem);
        // Log::debug($item);
        // Log::debug('$item');

        if ($this->ItemHistories->Items->save($item)) {
            Log::debug('ItemHistoriesController->makeDispatch GRAVOU!!!');
        } else {
            Log::debug('ItemHistoriesController->makeDispatch NÃO GRAVOU!!!');
        }

        // $items = $this->ItemHistories->Items
        //        ->find('all')
        //        ->contain([
        //            'Orders',
        //            'Orders.Deliveries',
        //        ])->where(['Items.id' => $item_id]);
        
        // $item = $items->first();


        // Log::debug('$item[\'origin\']= ' . $item['origin']);
        // Log::debug('$entity[\'status\']= ' . $entity['status']);
        // Log::debug('$entity[\'qtd\']= ' . $entity['qtd']);

        // if($entity['status'] == 'dispatched' && $entity['qtd'] > 0) {
        //     $entity['delivery_made']['delivery_id'] = $item->order->delivery->id;
        //     $entity['delivery_made']['vehicle_id'] = $vehicle;
        //     //            $entity['delivery_made']['scheduled_date'] = date('Y-m-d', strtotime(str_replace('/', '-', $dispatch_date)));
        //     $entity['delivery_made']['scheduled_date'] = $dispatch_date;
        //     $entity['delivery_made']['scheduled_horary'] = 'any';

        //     // if($this->ItemHistories->DeliveryMades->save($deliveryMade)) {
        //     //     $entity['delivery_made_id'] = $deliveryMade->id;
        //     // } else {
        //     //     Log::debug('Deu erro na gravação do delivery made');
        //     // }
        // } elseif ($item['origin'] == 'stock') {
        //     if ($entity['status'] == 'required' && $entity['qtd'] < 0) {
        //         // retorna para o estoque, deleta em output
        //         $output = $this->ItemHistories->Items->Outputs
        //                 ->find()
        //                 ->where([
        //                     'item_id' => $item_id,
        //                     'qtd' => $qtd,
        //                 ])
        //                 ->first();
                
        //         $this->ItemHistories->Items->Outputs->delete($output);
        //     }
        // } elseif ($entity['status'] == 'received' && $entity['qtd'] < 0) {
        //     // deleta invoice
        //     $itemHistory = $this->ItemHistories
        //                  ->find()
        //                  ->where(['item_id' => $item_id])
        //                  ->where(['qtd' => $qtd])
        //                  ->andWhere(['status' => 'received'])
        //                  ->order(['created' => 'DESC'])
        //                  ->first();
        //     Log::debug('$itemHistory[\'invoice_id\']= ' . $itemHistory['invoice_id']);

        //     if (!is_null($itemHistory['invoice_id'])) {

        //         $invoice = $this->ItemHistories->Invoices
        //                  ->find()
        //                  ->where(['id' => $itemHistory['invoice_id']])
        //                  ->first();

        //         // $invoice = $this->ItemHistories->Invoices
        //         //          ->get($itemHistory['invoice_id']);
        //         if(!is_null($invoice)) {
        //             $this->ItemHistories->Invoices->delete($invoice);
        //         }

        //         $itemHistory->invoice_id = null;
        //         $this->ItemHistories->save($itemHistory);

        //     }
        // }
    
        
        // return $entity;
    }

    public function cancelDispatch($item, $data)
    {
        $from = $data['from'];
        $to = $data['to'];
        $qtd = $data['qtd'];

        $deliveryMadeId = null;
        foreach ($item['item_histories'] as $ih) {
            if (!is_null($ih['delivery_made'])) {
                $deliveryMadeId = $ih['delivery_made']['id'];
            }
        }

        $arrItem['item_histories'][] = [
            'qtd' => $qtd * -1,
            'status' => 'dispatched',
            'deadline' => 0,
            'user_id' => $this->request->session()->read('Auth.User.id'),
        ];

        $item_histories_status = $this->ItemHistories->getItemHistoryStatus($item['id']);
        $arrItem['total_uncovered'] = $item_histories_status['total_uncovered'];
        $arrItem['total_required'] = $item_histories_status['total_required'];
        $arrItem['total_received'] = $item_histories_status['total_received'] + $qtd;
        $arrItem['total_dispatched'] = $item_histories_status['total_dispatched'] - $qtd;
        $arrItem['total_completed'] = $item_histories_status['total_completed'];

        $item = $this->ItemHistories->Items->patchEntity($item, $arrItem, [
            'associated' => [
                'ItemHistories',
            ],
        ]);

        $this->ItemHistories->getConnection()
                            ->transactional(function() use ($item, $deliveryMadeId){
                                $this->ItemHistories->Items->save($item);
                                if (!is_null($deliveryMadeId)) {
                                    $deliveryMade = $this->ItemHistories->DeliveryMades->get($deliveryMadeId);
                                    $this->ItemHistories->DeliveryMades->delete($deliveryMade);
                                }
                            });
    }

    public function makeConclusion($item, $data)
    {
        $from = $data['from'];
        $to = $data['to'];
        $qtd = $data['qtd'];

        $deliveryMadeId = null;
        foreach ($item['item_histories'] as $ih) {
            if (!is_null($ih['delivery_made'])) {
                $ih['delivery_made']['delivery_date'] = new Date();
                $updatedItemHistory = $ih;
            }
        }

        $arrItem['item_histories'][] = [
            'qtd' => $qtd,
            'status' => 'completed',
            'deadline' => 0,
            'user_id' => $this->request->session()->read('Auth.User.id'),
        ];

        $arrItem['item_histories'][] = $updatedItemHistory->toArray();

        $item_histories_status = $this->ItemHistories->getItemHistoryStatus($item['id']);
        $arrItem['total_uncovered'] = $item_histories_status['total_uncovered'];
        $arrItem['total_required'] = $item_histories_status['total_required'];
        $arrItem['total_received'] = $item_histories_status['total_received'];
        $arrItem['total_dispatched'] = $item_histories_status['total_dispatched'] - $qtd;
        $arrItem['total_completed'] = $item_histories_status['total_completed'] + $qtd;

        // Log::debug('$arrItem');
        // Log::debug($arrItem);

        $item = $this->ItemHistories->Items->patchEntity($item, $arrItem, [
            'associated' => [
                'ItemHistories',
                'ItemHistories.DeliveryMades',
            ],
        ]);

        // Log::debug($item);
        // Log::debug('$item');
        $this->ItemHistories->Items->save($item);
    }

    public function cancelConclusion($item, $data)
    {
        $from = $data['from'];
        $to = $data['to'];
        $qtd = $data['qtd'];

        $deliveryMadeId = null;
        foreach ($item['item_histories'] as $ih) {
            if (!is_null($ih['delivery_made'])) {
                $ih['delivery_made']['delivery_date'] = null;
                $updatedItemHistory = $ih;
            }
        }

        $arrItem['item_histories'][] = [
            'qtd' => $qtd * -1,
            'status' => 'completed',
            'deadline' => 0,
            'user_id' => $this->request->session()->read('Auth.User.id'),
        ];

        $arrItem['item_histories'][] = $updatedItemHistory->toArray();

        $item_histories_status = $this->ItemHistories->getItemHistoryStatus($item['id']);
        $arrItem['total_uncovered'] = $item_histories_status['total_uncovered'];
        $arrItem['total_required'] = $item_histories_status['total_required'];
        $arrItem['total_received'] = $item_histories_status['total_received'];
        $arrItem['total_dispatched'] = $item_histories_status['total_dispatched'] + $qtd;
        $arrItem['total_completed'] = $item_histories_status['total_completed'] - $qtd;

        // Log::debug('$arrItem');
        // Log::debug($arrItem);

        $item = $this->ItemHistories->Items->patchEntity($item, $arrItem, [
            'associated' => [
                'ItemHistories',
                'ItemHistories.DeliveryMades',
            ],
        ]);

        // Log::debug($item);
        // Log::debug('$item');
        $this->ItemHistories->Items->save($item);
    }

    public function updateInvoice()
    {
        $this->request->allowMethod('ajax');
        $data = $this->request->getQueryParams();
        // Log::debug('$data');
        // Log::debug($data);
        $item_id = $data['item_id'];
        $itemHistoryId = $data['item_history_id'];
        $invoiceNumber = $data['invoice_number'];
        $itemHistory = $this->ItemHistories
                     ->find()
                     ->contain(['Invoices'])
                     ->where(['ItemHistories.id' => $itemHistoryId])
                     ->first();

        $invoice = $this->ItemHistories->Invoices
                 ->find()
                 ->where(['id' => $itemHistory['invoice_id']])
                 ->first();

        if (empty($invoice)) {
            // Log::debug('invoice is empty');

            $invoice = $this->ItemHistories->Invoices->newEntity();
            $invoice->invoice_number = $invoiceNumber;
            if ($this->ItemHistories->Invoices->save($invoice)) {
                Log::debug('salvou invoice');
            } else {
                Log::debug('NÃO salvou  invoice');
            }
            $itemHistory->invoice_id = $invoice->id;
            if ($this->ItemHistories->save($itemHistory)) {
                Log::debug('salvou itemHistory');
            } else {
                Log::debug('NÃO salvou itemHistory');
            }
        } else {
            $invoice->invoice_number = $invoiceNumber;
            if ($this->ItemHistories->Invoices->save($invoice)) {
                Log::debug('salvou invoice');
            } else {
                Log::debug('NÃO salvou  invoice');
            }
        }


        $this->getInfo($item_id);
    }

}
