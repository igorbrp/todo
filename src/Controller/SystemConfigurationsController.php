<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Validation\Validator;

use Cake\Localized\Validation\BrValidation;

use Cake\Core\Configure;

use Cake\Filesystem\Folder;
use Cake\Filesystem\File;

use Cake\Log\Log;

/**
 * SystemConfigurations Controller
 *
 * @property \App\Model\Table\SystemConfigurationsTable $SystemConfigurations
 *
 * @method \App\Model\Entity\SystemConfiguration[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SystemConfigurationsController extends AppController
{
    /**
     * Edit method
     *
     * @param string|null $id System Configuration id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit()
    {
        //        $file = new File(ROOT . DS . 'config' . DS . 'system_configurations.php', true, 0644);

        $systemConfiguration = $this->SystemConfigurations
                             ->find()
                             ->where(['model' => $this->SystemConfigurations->registryAlias()])
                             ->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if ($data['configuration']['address']['city_id'] != '') {
                $cities = $this->loadModel('Cities');
                $city = $cities->get($data['configuration']['address']['city_id']);
                $data['configuration']['address']['city_name'] = $city['name'];
            }

            $errors = $this->validateData($data['configuration']);
            if(empty($errors[0])) { // fiz pq estava devolvendo um array[0] vazio
                $systemConfiguration = $this->SystemConfigurations->patchEntity($systemConfiguration, $data);

                if ($this->SystemConfigurations->save($systemConfiguration)) {

                    // if(Configure::check('system_configurations')) {
                    Configure::write('system_configurations', $systemConfiguration->toArray());
                    Configure::dump('system_configurations', 'default', ['system_configurations']);
                        // $file->open('w', true);

                        // $str = preg_replace('#,(\s+|)\)#', '$1)', var_export($systemConfiguration->toArray(), true));
                        // $str = '<?php' . PHP_EOL . 'return ' . $str . ';' . PHP_EOL;

                        // $file->write($str, 'w');
                        $this->Flash->success(__('A configuração foi salva corretamente.'));

                        return $this->redirect(['action' => 'edit']);
                    // } 

                    // $this->Flash->success(__('A configuração foi salva corretamente.'));
                    // return $this->redirect(['action' => 'edit']);
                }
            }

            $errors[] = $systemConfiguration->errors();

            $this->Flash->error(__('A configuração não pode ser salva. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $errors,
                ]]);
        }

        $modelStates = $this->loadModel('States');
        $states = $modelStates->find('list')->toArray();
        $this->set(compact('states'));

        $modelCities = $this->loadModel('Cities');
        if (!empty($systemConfiguration['configuration']['address']['state_id'])) {
            $cities = $modelCities->find('list')->toArray();
        } else {
            $cities = [];
        }

        $authenticators = $this->request->getSession()->read('Auth.User');

        Log::debug('-------------------------------------------------------------');
        Log::debug($authenticators);
        Log::debug('-------------------------------------------------------------');

        $this->set('authenticators');

        $this->set(compact('cities'));

        $this->set(compact('systemConfiguration'));
    }
 
    public function aparence()
    {
        $themeConfiguration = Configure::read('Theme');
        $preferences = Configure::read('Preferences');

        if ($this->request->is(['patch', 'post', 'put'])) {

            $themeConfiguration = $this->request->getData();

            $fileName = 'user_preferences' . DS . $this->request->session()->read('Auth.User.id');
            $file = new File(ROOT . DS . 'config' . DS . $fileName . '.php', false, 0644);
            if (!$file->exists()) {
                $file->create();
            }

            Configure::write('Theme', $themeConfiguration);
            Configure::dump($fileName, 'default', ['Theme', 'Preferences']);
            $this->Flash->success(__('A configuração do tema foi salva corretamente.'));
        }

        $this->set(compact('themeConfiguration'));
    }

    public function collapseToggle($status)
    {
        $this->request->allowMethod('ajax');
        $preferences = Configure::read('Preferences');
        $theme = Configure::read('Theme');
        
        $fileName = 'user_preferences' . DS . $this->request->session()->read('Auth.User.id');
        $file = new File(ROOT . DS . 'config' . DS . $fileName . '.php', false, 0644);
        if (!$file->exists()) {
            $file->create();
        }

        Log::debug($preferences);
        $preferences['collapsedBar'] = !$status ? 'sidebar-collapse' : '';
        Log::debug($preferences);

        Configure::write('Preferences', $preferences);
        //        Configure::write('Theme', $theme);
        Configure::dump($fileName, 'default', ['Preferences', 'Theme']);

        /*-------------------------------------------------------------
         * aqui pra pegar o cep nao usa nem compact nem serialize
         */
        //$this->set('response', json_encode($status));
        $this->set('response', json_encode('1'));
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*-----------------------------------------------------------*/
    }

    public function checkConfiguration()
    {
        $outboxEmails = $this->loadModel('OutboxEmails');
        $data = $this->request->getData();
        $folders = $outboxEmails->checkEmailConfiguration($data);

        Log::debug('$folders:');
        Log::debug($folders);

        /*-------------------------------------------------------------
         * aqui pra pegar o cep nao usa nem compact nem serialize
         */
        //$this->set('response', json_encode($status));
        $this->set('response', json_encode($folders));
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*-----------------------------------------------------------*/
    }

    public function testShipping()
    {
        $outboxEmails = $this->loadModel('OutboxEmails');
        $data = $this->request->getData();
        Log::debug('entrou');
        $outboxEmails->testShipping($data);

        Log::debug('$data:');
        Log::debug($data);

        /*-------------------------------------------------------------
         * aqui pra pegar o cep nao usa nem compact nem serialize
         */
        //$this->set('response', json_encode($status));
        $this->set('response', json_encode($data));
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*-----------------------------------------------------------*/
    }

    // public function edit($id = null)
    // {
    //     $systemConfiguration = $this->SystemConfigurations
    //                          ->find()
    //                          ->where(['model' => $this->SystemConfigurations->registryAlias()])
    //                          ->first();

    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $data = $this->request->getData();
    //         if ($data['configuration']['address']['city_id'] != '') {
    //             $cities = $this->loadModel('Cities');
    //             $city = $cities->get($data['configuration']['address']['city_id']);
    //             $data['configuration']['address']['city_name'] = $city['name'];
    //         }
    //         $errors = $this->validateData($data['configuration']);
    //         if(empty($errors[0])) { // fiz pq estava devolvendo um array[0] vazio
    //             $systemConfiguration = $this->SystemConfigurations->patchEntity($systemConfiguration, $data);
    //             if ($this->SystemConfigurations->save($systemConfiguration)) {

    //                 if(Configure::check('system_configurations')) {
    //                     Configure::write('system_configurations', $systemConfiguration->toArray());
    //                     //                        Configure::dump('system_configurations', 'SystemConf');

    //                     $this->Flash->success(__('A configuração foi salva corretamente.'));
    //                     return $this->redirect(['action' => 'edit']);
    //                 }
    //             }
    //         }
    //         $errors[] = $systemConfiguration->errors();
    //         // Log::debug($errors);
    //         $this->Flash->error(__('A configuração não pode ser salva. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
    //             'clear' => true,
    //             'params' => [
    //                 'messages' => $errors,
    //             ]]);
    //     }

    //     $modelStates = $this->loadModel('States');
    //     $states = $modelStates->find('list')->toArray();
    //     $this->set(compact('states'));

    //     $modelCities = $this->loadModel('Cities');
    //     if (!empty($systemConfiguration['configuration']['address']['state_id'])) {
    //         $cities = $modelCities->find('list')->toArray();
    //     } else {
    //         $cities = [];
    //     }
    //     $this->set(compact('cities'));

    //     $this->set(compact('systemConfiguration'));
    // }

    //    public function validateAddress($data)
    public function validateData($data)
    {
        $validator = new Validator();

        // identificação
        $validator
            ->scalar('name')
            ->maxLength('name', 80)
            ->requirePresence('name')
            ->notEmptyString('name', 'O nome deve ser digitado');

        $validator->provider('br', BrValidation::Class);
        $validator
            ->requirePresence('cnpj', false)
            ->allowEmpty('cnpj')
            ->add('cnpj', 'myCustomNameForcnpj', [
                'rule' => 'cnpj',
                'provider' => 'br',
                'message' => __('O CNPJ deve ser válido.') // TRADUZIR
            ]);

        $validator
            ->scalar('phone')
            ->maxLength('phone', 32)
            ->allowEmptyString('phone');

        $validator
            ->scalar('phone2')
            ->maxLength('phone2', 32)
            ->allowEmptyString('phone2');

        $validator
            ->requirePresence('email', false)
            ->email('email', false, __('O email deve ser válido')) // TRADUZIR
            ->allowEmpty('email');

        $validator
            ->integer('deadline')
            ->requirePresence('deadline', 'O prazo de entrega precisa ser informado'); // TRADUZIR

        $validator
            ->email('imap_email', false, __('O email de requisição deve ser válido')) // TRADUZIR
            ->allowEmpty('imap_email');

        $validator
            ->email('imap_user', false, __('O usuário do email de requisição deve ser um email válido.')) // TRADUZIR
            ->allowEmpty('imap_user');

        $validator
            ->url('imap_server', 'O servidor de entrada dever ser válido')
            ->allowEmpty('imap_server');

        $validator
            ->integer('imap_port', 'A porta do servidor de entrada deve ser válida.')
            ->allowEmpty('imap_port');

        $validator
            ->url('smtp_server', 'O servidor de saída deve ser válido')
            ->allowEmpty('smtp_server');

        $validator
            ->integer('smtp_port', 'A porta do servidor de saída deve ser válida.')
            ->allowEmpty('smtp_port');

        $validator
            ->decimal('freight')
            ->greaterThanOrEqual('freight', 0)
            ->notEmptyString('freight', 'O valor do frete deve ser informado, coloque zero caso não deseje cobrá-lo');

        $errors = $validator->errors($data);

        $errors[] = $this->validateAddress($data['address']);
        // Log::debug($errors);
        return $errors;
    }

    public function validateAddress($data)
    {
        // limpa máscara de formatação
        if(isset($data['postal_code'])) {
            $data['postal_code'] = preg_replace("/[^0-9]/", "", $data['postal_code']);
        }

        $validator = new Validator();
        $validator
            ->scalar('postal_code')
            ->maxLength('postal_code', 8, 'CEP inválido')
            ->allowEmptyString('postal_code');

        $validator
            ->scalar('state_id', 'invalid type')
            ->allowEmptyString('state_id');

        $validator
            ->integer('city_id')
            ->allowEmptyString('city_id');

        $validator
            ->scalar('city_name')
            ->maxLength('city_name', 80)
            ->allowEmptyString('city_name');

        $validator
            ->scalar('neighborhood')
            ->maxLength('neighborhood', 80)
            ->allowEmptyString('neighborhood');

        $validator
            ->scalar('thoroughfare')
            ->maxLength('thoroughfare', 80)
            ->allowEmptyString('thoroughfare');

        $validator
            ->scalar('complement')
            ->maxLength('complement', 120)
            ->allowEmptyString('complement');

        $validator
            ->scalar('reference')
            ->allowEmptyString('reference');

        $validator
            ->boolean('valid_address')
            ->allowEmptyString('valid_address');


        $errors = $validator->errors($data);
        // Log::debug($errors);
        return $errors;
    }

    // /**
    //  * Delete method
    //  *
    //  * @param string|null $id System Configuration id.
    //  * @return \Cake\Http\Response|null Redirects to index.
    //  * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //  */
    // public function delete($id = null)
    // {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $systemConfiguration = $this->SystemConfigurations->get($id);
    //     if ($this->SystemConfigurations->delete($systemConfiguration)) {
    //         $this->Flash->success(__('The system configuration has been deleted.'));
    //     } else {
    //         $this->Flash->error(__('The system configuration could not be deleted. Please, try again.'));
    //     }

    //     return $this->redirect(['action' => 'index']);
    // }
}
