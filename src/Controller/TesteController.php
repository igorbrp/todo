<?php
namespace App\Controller;
use Cake\I18n\Time;
use Cake\I18n\Date;

use Cake\Collection\Collection;

use App\Controller\AppController;

/**
 * Teste Controller
 *
 *
 * @method \App\Model\Entity\Teste[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TesteController extends AppController
{
    public function config()
    {
        die();
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $colors = [
            0 => [
                'color'    => '#f56954',
                'highlight'=> '#f56954',
            ],
            1 => [
                'color'    => '#00a65a',
                'highlight'=> '#00a65a',
            ],
            2 => [
                'color'    => '#f39c12',
                'highlight'=> '#f39c12',
            ],
            3 => [
                'color'    => '#00c0ef',
                'highlight'=> '#00c0ef',
            ],
            4 => [
                'color'    => '#3c8dbc',
                'highlight'=> '#3c8dbc',
            ],
            5 => [
                'color'    => '#d2d6de',
                'highlight'=> '#d2d6de',
            ]
        ];


        $this->loadModel('Orders');

        $firstDay = new Date();
        $firstDay->setDate(2020, $firstDay->month, 1);

        $query = $this->Orders->find();
        $query->select([
            'value' => $query->func()->sum('Items.price'),
            // 'order_total_qtd' => $query->func()->sum(
            //     $query->identifier('Items.qtd')
            // ),
            'label' => 'Subsidiaries.name'
        ])
              ->group('label')
              ->innerJoinWith('Subsidiaries')
              ->innerJoinWith('Items')
              ->order(['value' => 'DESC'])
              ->hydrate(false)
              ->all();
        
        $order_amount = $query->toList();
        // $collection = new Collection($query->toList());
        // $subsidiaries = $collection->extract('subsidiary_name')->toList();        
        for ($i = 0; $i < count($order_amount); $i++) {
            $order_amount[$i]['color'] = $colors[$i]['color'];
            $order_amount[$i]['highlight'] = $colors[$i]['highlight'];
        }

        $this->set('order_amount', $order_amount);
        // $this->set('subsidiaries', $subsidiaries);
    }

    /**
     * View method
     *
     * @param string|null $id Teste id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $teste = $this->Teste->get($id, [
            'contain' => [],
        ]);

        $this->set('teste', $teste);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $teste = $this->Teste->newEntity();
        if ($this->request->is('post')) {
            $teste = $this->Teste->patchEntity($teste, $this->request->getData());
            if ($this->Teste->save($teste)) {
                $this->Flash->success(__('The teste has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The teste could not be saved. Please, try again.'));
        }
        $this->set(compact('teste'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Teste id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $teste = $this->Teste->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $teste = $this->Teste->patchEntity($teste, $this->request->getData());
            if ($this->Teste->save($teste)) {
                $this->Flash->success(__('The teste has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The teste could not be saved. Please, try again.'));
        }
        $this->set(compact('teste'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Teste id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $teste = $this->Teste->get($id);
        if ($this->Teste->delete($teste)) {
            $this->Flash->success(__('The teste has been deleted.'));
        } else {
            $this->Flash->error(__('The teste could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
