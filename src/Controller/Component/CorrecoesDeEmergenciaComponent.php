<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

use Cake\I18n\I18n;
use Cake\I18n\Date;

/**
 * CorrecoesDeEmergencia component
 */
class CorrecoesDeEmergenciaComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function BrCurrencyToValue($get_valor)
    {
        $valor = str_replace('.', '', $get_valor);
        $valor = str_replace(',', '.', $valor);

        return $valor;
    }

    public function ValueToBrCurrency($get_valor)
    {
        $valor = str_replace('.', ',', $get_valor);

        return $valor;
    }

    public function BrDateToUtcFormat($get_valor)
    {
        $valor = str_replace('/', '-', $get_valor);

        $dt = new Date($valor);
        return $dt->i18nFormat('yyyy-MM-dd');
    }
}
