<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

use Cake\Log\Log;

/**
 * HandlerSku component
 */
class HandlerSkuComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function buildSkuList($t, $prefix = '')
    {
        // Log::debug('handlerskucomponent');
        // Log::debug($t);
        $result = array(); // empty array
        $result[0] = $this->sku_generator($t, false, $prefix);
        // Log::debug(explode(" ", $t));
        $nw = count(explode(" ", $t));
        if($nw == 1)
            //            $result[1] = strtoupper(substr($this->remove_special($t, 0, 4)));
            $result[1] = strtoupper(substr($this->remove_special($t), 0, 4));
        else
            if($nw == 2)
                $result[1] = $this->sku_generator($t, false, '', 2, 3);
            else
                for($i = 3; $i < $nw; $i++)
                    $result[$i - 2] = $this->sku_generator($t, false, '', $i + 1);
        $nw = count($result);
        if($nw < 5)
            $result = $result + $this->sku_generator_incremented($result[0], 5 - $nw, 2);

        return $result;
    }

    public function sku_generator_incremented($s, $l, $d)
    {
        $result = array(); // empty array
        for($i = 5 - $l; $i <= $l; $i++)
            $result[$i] = $s . sprintf("%02s", $i);
        return $result;
    }

    /************************************************************************
     * sku_generator
     * gera codigos sku
     * $s: string original
     * $v: bool se vai tirar vogais
     * $p: string prefixo
     * $nw: numero de palavras da string original a serem investigadas
     * $l: numero de letras de cada palavra para gerar o codigo
     ************************************************************************/
    public function sku_generator($s, $v, $p = '', $nw = 3, $l = 2)
    {
        $cod = '';
        $result = '';
        $vowels = array('A', 'e', 'E', 'i', 'I', 'o', 'O', 'u', 'U', 'y', 'Y');
        $s = strtoupper($this->remove_acentos($s));
        $m = explode(" ", $s);

        for($i = 0; $i < count($m); $i++){
            $m[$i] = $this->remove_special($m[$i]);
            if($v)
                $m[$i] = str_replace($vowels, '', $m[$i]);
            $cod .= substr($m[$i],0,$l);
        }
        $i = ($nw * $l);
        $result = $p . substr($cod, 0, $i);

        return $result;
    }

    public function remove_acentos($s)
    {
        $acent = Array('Á','È','ô','Ç','á','è','Ò','ç','Â','Ë','ò','â','ë','Ø','Ñ','À','Ð',
                       'ø','ñ','à','ð','Õ','Å','õ','Ý','å','Í','Ö','ý','Ã','í','ö','ã','Î',
                       'Ä','î','Ú','ä','Ì','ú','Æ','ì','Û','æ','Ï','û','ï','Ù','É','ù','é',
                       'Ó','Ü','Ê','ó','ü','ê','Ô');
        $clear = Array('A','E','o','C','a','e','O','c','A','E','o','a','e','O','N','A','D',
                       'o','n','a','o','O','A','o','Y','a','I','O','y','A','i','o','a','I',
                       'A','i','U','a','I','u','A','i','U','a','I','u','i','U','E','u','e',
                       'O','U','E','o','u','e','O');
        return str_replace($acent, $clear, $s);
    }

    public function remove_special($s)
    {
        $special = Array(" ",",",".","'","\"","&","|","!","#","$","¨","*","(",")","`","´",
                         "<",">",";","=","+","§","{","}","[","]","^","~","?","%","®","©",
                         'Þ','þ','‘','’','‚','“','”','„','-');
            return str_replace($special, "", trim($s));
    }
}
