<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PaymentMethods Controller
 *
 * @property \App\Model\Table\PaymentMethodsTable $PaymentMethods
 *
 * @method \App\Model\Entity\PaymentMethod[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PaymentMethodsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {

        // $products = $this->Products->find('search', ['search' => $data])
        //                            ->contain(['Supplies', 'ProductLines', 'Files'])
        //                            ->where(['title IS NOT' => null])
        //                            ->formatResults(
        //                                function (\Cake\Collection\CollectionInterface $results) {
        //                                    return $results->map(function ($row) {
        //                                        if (is_null($row['price'])) {
        //                                            $row['price'] = $this->getPrice($row['root_price'], $row['supply_id'], $row['product_line_id']);
        //                                        }
        //                                        return $row;
        //                                    });
        //                                });

        // $this->set('q', isset($data['q']) ? $data['q'] : '');
        // $this->set(compact('products'), $this->paginate($products));

        // $paymentMethods = $this->paginate($this->PaymentMethods);
        // $this->set(compact('paymentMethods'));
        $query = $this->PaymentMethods
               ->find('search', ['search' => $this->request->getQuery()]);

        $this->set('paymentMethods', $this->paginate($query));

        //        $this->set(compact('paymentmethods'));
        $this->set('q', isset($data['q']) ? $data['q'] : '');
        $this->set('_serialize', ['paymentMethods']);
    }

    /**
     * View method
     *
     * @param string|null $id Payment Method id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $paymentMethod = $this->PaymentMethods->get($id, [
            'contain' => [],
        ]);

        $this->set('paymentMethod', $paymentMethod);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $paymentMethod = $this->PaymentMethods->newEntity();
        if ($this->request->is('post')) {
            $paymentMethod = $this->PaymentMethods->patchEntity($paymentMethod, $this->request->getData());
            if ($this->PaymentMethods->save($paymentMethod)) {
                $this->Flash->success(__('O método de pagamento foi salvo.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O método de pagamento não pode ser salvo. Por favor, verifique os erros e tente novamente.'));
        }
        $this->set(compact('paymentMethod'));
        $this->render('addedit');
    }

    /**
     * Edit method
     *
     * @param string|null $id Payment Method id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $paymentMethod = $this->PaymentMethods->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $paymentMethod = $this->PaymentMethods->patchEntity($paymentMethod, $this->request->getData());
            if ($this->PaymentMethods->save($paymentMethod)) {
                $this->Flash->success(__('O método de pagamento foi salvo.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('O método de pagamento não pode ser salvo. Por favor, verifique os erros e tente novamente.'));
        }
        $this->set(compact('paymentMethod'));
        $this->render('addedit');
    }

    /**
     * Delete method
     *
     * @param string|null $id Payment Method id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $paymentMethod = $this->PaymentMethods->get($id);
        if ($this->PaymentMethods->delete($paymentMethod)) {
            $this->Flash->success(__('The payment method has been deleted.'));
        } else {
            $this->Flash->error(__('The payment method could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
