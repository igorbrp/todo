<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\I18n\Date;
use Cake\I18n\Time;
use Cake\I18n\Number;
use Cake\Collection\Collection;
use \Cake\ORM\Query;

// use ArrayObject;
// use Cake\Event\Event;

use Cake\Log\Log;

/**
 * Migrate Controller
 *
 *
 * @method \App\Model\Entity\Migrate[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MigrateController extends AppController
{
    public function orders()
    {
        $qr = $this->request->getQuery();
        $id = $qr['id'];

        if(empty($id) || is_null($id))
            $id = 0;

        $contained = [
            'TblDetalhesdopedido',
            'TblDetalhesdopedido.TblUnidadesdoitem',
            'TblDetalhesdopedido.TblRoteiros',
            'TblClientes',
            'TblClientes.TblEnderecosdosclientes',
            'TblLojas',
            'TblVendedores',
            'TblCheques',
        ];

        $associated = [
            'Customers',
            'Customers.Addresses',
            'Items',
            'Items.Outputs',
            'Items.ItemHistories',
            'Deliveries',
            'Deliveries.Addresses',
            'Payments',
            'Payments.PaymentMethods',
            'Comments',
        ];

        $conn_ong = ConnectionManager::get('default');
        if($id == 0) {
            $stmt = $conn_ong->query('TRUNCATE TABLE orders');
            $stmt = $conn_ong->query('TRUNCATE TABLE customers');
            $stmt = $conn_ong->query('TRUNCATE TABLE items');
            $stmt = $conn_ong->query('TRUNCATE TABLE item_histories');
            $stmt = $conn_ong->query('TRUNCATE TABLE deliveries');
            $stmt = $conn_ong->query('TRUNCATE TABLE comments');
            $stmt = $conn_ong->query('TRUNCATE TABLE payments');
            $stmt = $conn_ong->query('TRUNCATE TABLE addresses');
            $stmt = $conn_ong->query('TRUNCATE TABLE errors');
        }

        $orders = TableRegistry::get('Orders');

        $tblPedidos = TableRegistry::get('TblPedidos')
                    ->find()
                    ->order(['NumeroDoPedidoKey' => 'ASC'])
                    ->where(['NumeroDoPedidoKey > ' => $id])
                    // ->andWhere(['numerodopedido > ' => '8810'])
                    // ->andWhere(['numerodopedido < ' => '8820'])
                    ->contain($contained)
                    ->limit(10);

        foreach($tblPedidos as $pedido) {
            // telefones
            $arrPhones = $this->verifyPhones(
                $pedido['tbl_cliente']['codigoDDDCliente1'],
                $pedido['tbl_cliente']['telefoneDoCliente1'],
                $pedido['tbl_cliente']['codigoDDDCliente2'],
                $pedido['tbl_cliente']['telefoneDoCliente2'],
                $pedido['tbl_cliente']['codigoDDDCliente3'],
                $pedido['tbl_cliente']['telefoneDoCliente3']
            );

            $phone = $arrPhones[0];
            $mobile = $arrPhones[1];
            $fax = $arrPhones[2];

            if(is_null($pedido['tbl_cliente']['tbl_enderecosdocliente'])) {
                $postal_code = preg_replace('/\D/', '', $pedido['tbl_cliente']['cepDoCliente']);
                $estado = $pedido['tbl_cliente']['codigoDoEstadoDoCliente'];
                $city = $pedido['tbl_cliente']['municipioDoCliente'];
                $neighborhood = $pedido['tbl_cliente']['bairroDoCliente'];
                $thoroughfare = $pedido['tbl_cliente']['enderecoDoCliente'];
                $number = null;
                $complement = $pedido['tbl_cliente']['complementoDoEnderecoDoCliente'];
                $reference = $pedido['referenciaEndereco'];
            } else {

                $postal_code = preg_replace('/\D/', '', $pedido['tbl_cliente']['Cep']);
                $estado = $pedido['tbl_cliente']['SIGLA'];
                $city = $pedido['tbl_cliente']['LOCALIDADE'];
                $neighborhood = $pedido['tbl_cliente']['BAIRRO'];
                $thoroughfare = $pedido['tbl_cliente']['LOGRADOURO'];
                $number = $pedido['tbl_cliente']['NUMERO'];
                $complement = $pedido['tbl_cliente']['COMPLEMENTO'];
                $reference = $pedido['referenciaEndereco'];
            }

            $state = TableRegistry::get('States')
                   ->find()
                   // ->where(['uf' => $estado])
                   ->where(['id' => $estado])
                   ->first();

            // loja
            $subsidiary = TableRegistry::get('Subsidiaries')
                        ->find()
                        ->where(['name' => $pedido['tbl_loja']['nomeDaLoja']])
                        ->first();

            // funcionario
            $employee = TableRegistry::get('Employees')
                      ->find()
                      ->where(['name' => $pedido['tbl_vendedore']['nomeCompleto']])
                      ->first();

            // detalhes e unidades
            $dt_items = [];
            foreach($pedido['tbl_detalhesdopedido'] as $detalhe) {
                // funcionario
                $product = TableRegistry::get('Products')
                         ->find()
                         ->where(['supply_sku' => $detalhe['codigoDoFornecedor']])
                         ->andWhere(['sku' => $detalhe['codigoDoProduto']])
                         ->first();
                // corrige algumas datas de pedidos
                // if($pedido['numeroDoPedido'] == 12981) $pedido['datadopedido'] = '2007-06-11';
                // if($pedido['numeroDoPedido'] == 5151) $pedido['datadopedido'] = '2011-01-23';
                // if($pedido['numeroDoPedido'] == 96832) $pedido['datadopedido'] = '2007-02-12';
                // if($pedido['numeroDoPedido'] == 2088) $pedido['datadopedido'] = '2010-02-26';
                // if($pedido['numeroDoPedido'] == 61501) $pedido['datadopedido'] = '2006-04-20';
                // if($pedido['numeroDoPedido'] == 10947) $pedido['datadopedido'] = '2006-04-20';
                // if($pedido['numeroDoPedido'] == 20037) $pedido['datadopedido'] = '2013-12-24';
                // if($pedido['numeroDoPedido'] == 60895) $pedido['datadopedido'] = '2006-04-20';

                // datas de recebimento, expedicao e conclusao
                foreach($detalhe['tbl_unidadesdoitem'] as $u) {
                    $dt_pendente = new Time($pedido['datadopedido']);
                    $dt_requisicao = is_null($u['DataDaRequisicao']) ? null : new Time($u['DataDaRequisicao']);
                    $dt_recebimento = is_null($u['DataDoRecebimento']) ? null : new Time($u['DataDoRecebimento']);
                    $dt_expedicao = is_null($u['DataDaExpedicao']) ? null : new Time($u['DataDaExpedicao']);
                    $dt_conclusao = is_null($u['DataDaConclusao']) ? null : new Time($u['DataDaConclusao']);

                    $dt_requisicao = (!is_null($dt_requisicao) && $dt_requisicao->year <  2000) ? null : $dt_requisicao;
                    $dt_recebimento = (!is_null($dt_recebimento) && $dt_recebimento->year < 2000) ? null : $dt_recebimento;
                    $dt_expedicao = (!is_null($dt_expedicao) && $dt_expedicao->year < 2000) ? null : $dt_expedicao;
                    $dt_conclusao = (!is_null($dt_conclusao) && $dt_conclusao->year < 2000) ? null : $dt_conclusao;


                    // echo "pendente - lido: " . $pedido['datadopedido'] . " convertido: " . $dt_pendente . "<br>";
                    // echo "requisicao - lido: " . $u['DataDaRequisicao'] . " convertido: " . $dt_requisicao . "<br>";
                    // echo "recebimento - lido: " . $u['DataDoRecebimento'] . " convertido: " . $dt_recebimento . "<br>";
                    // echo "expedicao - lido: " . $u['DataDaExpedicao'] . " convertido: " . $dt_expedicao . "<br>";
                    // echo "conclusao - lido: " . $u['DataDaConclusao'] . " convertido: " . $dt_conclusao . "<br>";


                    if(!is_null($dt_requisicao) ||
                       !is_null($dt_recebimento)  ||
                       !is_null($dt_expedicao) ||
                       !is_null($dt_conclusao)) {
                        if((!is_null($dt_requisicao) && $dt_requisicao->year <  2000) ||
                           is_null($dt_requisicao) && (!is_null($dt_recebimento) || !is_null($dt_expedicao) || !is_null($dt_conclusao))) {
                            $dt_requisicao = $dt_pendente;
                            $dt_requisicao->addSecond(1);
                        }
                        if((!is_null($dt_recebimento) && $dt_recebimento->year < 2000) ||
                           is_null($dt_recebimento) && (!is_null($dt_expedicao) || !is_null($dt_conclusao))) {
                            $dt_recebimento = $dt_requisicao;
                            $dt_recebimento->addSecond(1);
                        }
                        if((!is_null($dt_expedicao) && $dt_expedicao->year < 2000) ||
                           is_null($dt_expedicao) && (!is_null($dt_conclusao))){
                            $dt_expedicao = $dt_recebimento;
                            $dt_expedicao->addSecond(1);
                        }
                        if(!is_null($dt_conclusao) && $dt_conclusao->year < 2000){
                            $dt_conclusao = $dt_recebimento;
                            $dt_conclusao->addSecond(1);
                        }
                    }
                    $u['DataDaRequisicao'] = $dt_requisicao;
                    $u['DataDoRecebimento'] = $dt_recebimento;
                    $u['DataDaExpedicao'] = $dt_expedicao;
                    $u['DataDaConclusao'] = $dt_conclusao;
                }

                $collect = new Collection($detalhe['tbl_unidadesdoitem']);
                $arr_unidades = [];
                $dataDoPedido = new Time($pedido['datadopedido']);
                $arr_unidades[] =   array(
                    array(
                        'qtd' => count($detalhe['tbl_unidadesdoitem']),
                        'status' => 'uncovered',
                        'deadline' => (int) 0,
                        'created' => $dataDoPedido->i18nFormat('yyyy-MM-dd HH:mm:ss'),
                        'modified' => $dataDoPedido->i18nFormat('yyyy-MM-dd HH:mm:ss'),
                    )
                );

                $arr_unidades[] = $this->getArray($this->getHistoricoRequisicoes($collect), 'required');
                $arr_unidades[] = $this->getArray($this->getHistoricoRecebimentos($collect), 'received');
                $arr_unidades[] = $this->getArray($this->getHistoricoExpedicoes($collect), 'dispatched');
                $arr_unidades[] = $this->getArray($this->getHistoricoConclusoes($collect), 'completed');

                $col_unidades = new Collection($arr_unidades);
                $extr = $col_unidades->unfold();

                $uncovereds = $col_unidades->unfold()->filter(function ($key) {
                    return $key['status'] === 'uncovered';
                })->sumOf(function ($key) {
                    return $key['qtd'];
                });

                $requireds = $col_unidades->unfold()->filter(function ($key) {
                    return $key['status'] === 'required';
                })->sumOf(function ($key) {
                    return $key['qtd'];
                });

                $receiveds = $col_unidades->unfold()->filter(function ($key) {
                    return $key['status'] === 'received';
                })->sumOf(function ($key) {
                    return $key['qtd'];
                });

                $dispatcheds = $col_unidades->unfold()->filter(function ($key) {
                    return $key['status'] === 'dispatched';
                })->sumOf(function ($key) {
                    return $key['qtd'];
                });

                $completeds = $col_unidades->unfold()->filter(function ($key) {
                    return $key['status'] === 'completed';
                })->sumOf(function ($key) {
                    return $key['qtd'];
                });

                $total_uncovereds = ($uncovereds - $requireds) < 0 ? 0 : ($uncovereds - $requireds);
                $total_requireds = ($requireds - $receiveds) < 0 ? 0 : ($requireds - $receiveds);
                $total_receiveds = ($receiveds - $dispatcheds) < 0 ? 0 : ($receiveds - $dispatcheds);
                $total_dispatcheds = ($dispatcheds - $completeds) < 0 ? 0 : ($dispatcheds - $completeds);
                $total_completeds = $completeds;

                if($total_uncovereds > 0)
                    $status = 'uncovered';
                elseif($total_requireds > 0)
                    $status = 'required';
                elseif($total_receiveds > 0)
                    $status = 'received';
                elseif($total_dispatcheds > 0)
                    $status = 'dispatched';
                elseif($total_completeds > 0)
                    $status = 'completed';

                //                debug($tmp_itemshistory);

                $tmp_itemshistory = [];
                foreach($extr as $ex) {
                    $tmp_itemshistory[] = [
                        'qtd' => $ex['qtd'],
                        'status' => $ex['status'],
                        'deadline' => $ex['deadline'],
                        'user_id' =>  'c9050abf-1aef-4ea4-822c-7f5c7d48bfca',
                        'created' => new Time($ex['created']),
                        'modified' => new Time($ex['modified']),
                    ];
                }

                //                echo $pedido['numeroDoPedido'] . "<br>";
                //                debug($tmp_itemshistory);

                $collection_items_history = new Collection($tmp_itemshistory);
                $tmp_itemshistory = $collection_items_history->sortBy('created', SORT_DESC)->toArray();

                $dt_items[] = [
                    'qtd' => $detalhe['quantidade'],
                    'status' => $status,
                    'total_uncovered' => $total_uncovereds,
                    'total_required' => $total_requireds,
                    'total_received' => $total_receiveds,
                    'total_dispatched' => $total_dispatcheds,
                    'total_completed' => $total_completeds,
                    'product_id' => (!is_null($product['id'])) ? $product['id'] : null,
                    'supply_id' => (!is_null($product['supply_id'])) ? $product['supply_id'] : null,
                    'sku' => $detalhe['codigoDoProduto'],
                    'supply_code' => $detalhe['codigoDoFornecedor'],
                    'supply_name' => $detalhe['nomeDoFornecedor'],
                    'title' => $detalhe['descricaoDoProduto'],
                    'description' => null,
                    'complement' => $detalhe['complementoDoProduto'],
                    'deadline' => null,
                    'price' => $detalhe['valor'],
                    'original_price' => 0,
                    'root_price' => $detalhe['custo'],
                    'origin' => 'request',
                    'minimum_unitary_price' => 0.00,
                    'item_histories' => $tmp_itemshistory,
                ];
            }

            $dt_pagamentos = [];
            if($pedido['valorEmDinheiro'] > 0) {
                // payment em dinheiro payment_method_id = 1
                $dt_pagamentos[] = [
                    'payment_methods_id' => '1',
                    'value' => $pedido['valorEmDinheiro'],
                    'installments' => '1',
                    'fator' => '1',
                    'registry' => null,
                ];
            }
            if($pedido['valorNoCartao'] > 0) {
                // payment com cartao de credito payment_method_id = 2
                $dt_pagamentos[] = [
                    'payment_methods_id' => '2',
                    'value' => $pedido['valorNoCartao'],
                    'installments' => '1',
                    'fator' => '1',
                    'registry' => null,
                ];
            }
            if(!empty($pedido['tbl_cheques'])) {
                // payment em cheques payment_method_id = 2
                $i = 0;
                $tot_pay_cheque = 0;
                foreach($pedido['tbl_cheques'] as $cheque) {
                    $dt_cheques[] = [
                        'rec_date' => $cheque['dataDoCheque'],
                        'rec_value' => $cheque['valorDoCheque'],
                        'rec_number' => $cheque['numeroDoCheque'],
                        'rec_bank' => $cheque['numeroDoBanco'],
                        'rec_cod' => $cheque['CodigoDoTipoDoCheque'],
                    ];
                    $i++;
                    $tot_pay_cheque += $cheque['valorDoCheque'];
                }
                $dt_pagamentos[] = [
                    'payment_methods_id' => '4',
                    'value' => $tot_pay_cheque,
                    'installments' => $i,
                    'fator' => '1',
                    'registry' => $dt_cheques,
                ];
            }

            $data = [
                'number' => $pedido['numeroDoPedido'],
                'date_order' => $pedido['datadopedido'],
                'customer' => [
                    'name' => $pedido['tbl_cliente']['nomeDoCliente'],
                    'cpf_cnpj' => preg_replace('/\D/', '', $pedido['tbl_cliente']['cpfDoCliente']),
                    'birth_date' => $pedido['tbl_cliente']['dataDeNascimento'],
                    'phone' => $phone,
                    'mobile' => $mobile,
                    'fax' => $fax,
                    //                    'customer_address' => [
                    'address' => [
                        'model' => 'Customers',
                        'postal_code' => $postal_code,
                        'state_id' => $state['id'],
                        'city' => $city,
                        'neighborhood' => $neighborhood,
                        'thoroughfare' => $thoroughfare,
                        'number' => $number,
                        'complement' => $complement,
                        'reference' => $reference,
                    ],
                ],
                'subsidiary_id' => $subsidiary['id'],
                'employee_id' => $employee['id'],
                'user_id' =>  'c9050abf-1aef-4ea4-822c-7f5c7d48bfca',
                'discount' => $pedido['desconto'],
                'items' => $dt_items,
                'payments' => $dt_pagamentos,
                'deliveries' => [
                    (int) 0 => [
                        'same_customer_address' => '1',
                        'foreseen_date' => '10/10/2018', // date_order + $pedido['prazoDeEntrega']
                        'foreseen_horary' => 'any',
                        'address' => [
                            'model' => 'Deliveries',
                            'postal_code' => $postal_code,
                            'state_id' => $state['id'],
                            'city' => $city,
                            'neighborhood' => $neighborhood,
                            'thoroughfare' => $thoroughfare,
                            'number' => $number,
                            'complement' => $complement,
                            'reference' => $reference,
                        ]
                    ]
                ],
                'comments' => [
                    (int) 0 => [
                        'model' => 'Orders',
                        'body' => $pedido['observacao'],
                        'user_id' =>  'c9050abf-1aef-4ea4-822c-7f5c7d48bfca'
                    ]
                ]
            ];

            if(!empty($reference)) {
                $data['comments'][0][] = [
                    'body' => "REFERENCIA DE ENDEREÇO\n" . $reference,
                    'user_id' =>  'c9050abf-1aef-4ea4-822c-7f5c7d48bfca'
                ];
            }
            $order = $orders->newEntity(['associated' => $associated]);
            $order = $orders->patchEntity($order, $data, ['associated' => $associated]);

            // verifica erros e corrige alguns
            if($list_errors = $order->getErrors()){
                foreach($list_errors as $lt) {
                    $flat = $this->flatten($lt);
                    while(count($flat) > 0) {
                        $desc_erro = [key($list_errors)];
                        $ff = array_slice($flat, 0, array_search('@', $flat));
                        foreach($ff as $f)
                            $desc_erro[] = $f;
                        $flat = array_diff($flat, $ff);
                        array_shift($flat);
                        $str = '';
                        for($i = 0; $i < count($desc_erro) - 2; $i++)
                            $str .= "['" . $desc_erro[$i] . "']";
                        eval("\$error_value = \$data$str;");

                        // verifica se cpf é invalido, se for seta como null
                        // if(strpos($desc_erro[count($desc_erro) - 2], 'myCustomNameForcpf') !== false) {
                        if(array_search('myCustomNameForcpf', $desc_erro) !== false) {
                            eval("\$data$str = null;"); // coloco cpf = null
                            $data['comments'][0][] = [ // incluo msa em comments
                                'body' => "PEDIDO COM CPF INVÁLIDO NO GCOMM: " . $error_value,
                                'user_id' =>  'c9050abf-1aef-4ea4-822c-7f5c7d48bfca'
                            ];
                            $order = $orders->newEntity(['associated' => $associated]);
                            $order = $orders->patchEntity($order, $data, ['associated' => $associated]); // jogo de novo em order
                            // } elseif ($desc_erro[count($desc_erro) - 2] == 'date') { // se dataDeNascimento for invalida seta como null
                        } elseif (array_search('date', $desc_erro) !== false) { // se dataDeNascimento for invalida seta como null
                            eval("\$data$str = null;"); // coloco data = null
                            $data['comments'][0][] = [ // incluo msa em comments
                                'body' => "PEDIDO COM DATA DE NASCIMENTO INVÁLIDA NO GCOMM: " . $error_value,
                                'user_id' =>  'c9050abf-1aef-4ea4-822c-7f5c7d48bfca'
                            ];
                            $order = $orders->newEntity(['associated' => $associated]);
                            $order = $orders->patchEntity($order, $data, ['associated' => $associated]); // jogo de novo em order
                        }
                    }
                    next($list_errors);
                }
            }

            if($cliente = $this->verificaSeClienteExiste($data)) {
                Log::debug('<<< cliente existe');
                Log::debug($data['customer']);
                Log::debug('>>>');
                $data['customer'] = '';
                if(is_int($cliente))
                    $data['customer_id'] = $cliente;
                else
                    $data['customer'] = $cliente;
                $order = $orders->patchEntity($order, $data, ['associated' => $associated]); // jogo de novo em order
                //                $order->customer['id'] = $data['customer_id'];
            }

            if(isset($data['customer']['cpf_cnpj']))
                if($duplicado = $this->verificaSeCpfExiste($data)) {
                    Log::debug('<<< cpf existe');
                    Log::debug($data['customer']);
                    Log::debug('>>>');
                    // Log::debug($duplicado);
                    // die();
                    $data['customer']['cpf_cnpj'] = null;
                    $data['comments'][0][] = [
                        'body' => "cpf duplicado:" . $duplicado['cpf_cnpj'] . "|cliente:" . $duplicado['id'],
                        'user_id' =>  'c9050abf-1aef-4ea4-822c-7f5c7d48bfca'
                    ];
                    // if($pedido['numeroDoPedido'] == '4129' || $pedido['numeroDoPedido'] == '4902'){
                    //     Log::debug('path entity para customer se CPF existe');
                    // }
                    $order = $orders->patchEntity($order, $data, ['associated' => $associated]); // jogo de novo em order
                    // if($pedido['numeroDoPedido'] == '4129' || $pedido['numeroDoPedido'] == '4902'){
                    //     Log::debug('pasou CPF existe');
                    // }
            //                    Log::debug("entrou: " . $data['number']);
                    //                    Log::debug($data['customer']);
                }

            $order->user_id = 'c9050abf-1aef-4ea4-822c-7f5c7d48bfca';


            //            Log::debug('cliente: ' . $pedido['tbl_cliente']['nomeDoCliente'] . '  |  pedido: ' . $pedido['numeroDoPedido']);

            // if($pedido['numeroDoPedido'] == '4129' || $pedido['numeroDoPedido'] == '4902'){
            //     Log::debug($order);
            // }
            if (is_null($order->customer) || !$orders->save($order)) {
                Log::debug('NÃO GRAVOU');
                //                echo "<h3>ERRO: " . $order['number'] . "</h3>";
                //                debug($order->errors());
                Log::debug($order->errors());
                $errors = TableRegistry::get('Errors');
                $error = $errors->newEntity();
                $dt = [];
                $dt['module'] = 'Order.MigrateGcomm';
                $dt['reference'] = $order['number'];
                $dt['error'] = json_encode($order->errors());
                //                debug($dt);
                $error = $errors->patchEntity($error, $dt);
                $errors->save($error);
                //                debug($error);
            }
            //            Log::debug('GRAVOU: ' . $pedido['tbl_cliente']['nomeDoCliente'] . '  |  pedido: ' . $pedido['numeroDoPedido'] . '   | ORDER: ' . $order['number'] . ' CUSTOMER: ' . $order['customer']['id']);
        }

        $errors = TableRegistry::get('Errors')->find()->count();
        $last_key = $pedido['NumeroDoPedidoKey'];
        $tblRestantes = TableRegistry::get('TblPedidos')
                      ->find()
                      ->order(['NumeroDoPedidoKey' => 'ASC'])
                      ->where(['NumeroDoPedidoKey > ' => $last_key]);

        $restantes = $tblRestantes->count();

        $count = $orders->find()->count();
        $total = $restantes + $count + $errors;

        if($restantes == 0)
            $continue = 0;
        else
            $continue = 1;

        $response['continue'] = $continue;

        $percent_count = ($count * 100) / $total;
        $percent_errors = ($errors * 100) / $total;
        $response['total'] = $total;
        $response['count'] = $count;
        $response['percount'] =  Number::precision($percent_count, 2);
        $response['width_percount'] = (int) $percent_count;
        $response['errors'] = $errors;
        $response['pererrors'] = Number::precision($percent_errors, 2);
        $response['width_pererrors'] = (int) $percent_errors;
        $response['last_key'] = $last_key;

        if($continue == 0)
            $response['msg'] = 'Concluído!';

        /*--------------------------------------------------------*/
        $this->set('response', json_encode($response));
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/


    }

    public function products()
    {
        $qr = $this->request->getQuery();
        $id = $qr['id'];


        if(empty($id) || is_null($id) || $id === 0)
            $id = '0'; // codigo do produto é alfanumerico

        $products = TableRegistry::get('Products');

        $conn_ong = ConnectionManager::get('default');
        if($id === '0')  // codigo do produto é alfanumerico
            $stmt = $conn_ong->query('TRUNCATE TABLE products');

        $tblProdutos = TableRegistry::get('TblProdutos')
                     ->find()
                     ->order(['codigoDoProduto' => 'ASC'])
                     ->where(['codigoDoProduto > ' => $id])
                     ->limit(10);

        foreach($tblProdutos as $produto) {
            $querySupply = $products->Supplies->find()
                                              ->where(['code' => $produto['codigoDoFornecedor']]);
            $supply = $querySupply->first();

            if($supply) {
                $product = $products->newEntity();

                $product['sku'] = $produto['codigoDoProduto'];
                $product['automatic_sku'] = false;
                $product['supply_sku'] = $supply['code'];
                $product['title'] = $produto['descricaoDoProduto'];
                $product['description'] = '';
                $product['long_description'] = '';
                $product['root_price'] = $produto['precoMinimo'];
                $product['supply_id'] = $supply['id'];

                if(!$products->save($product)) {
                    $errors = TableRegistry::get('Errors');
                    $error = $errors->newEntity();
                    $dt = [];
                    $dt['module'] = 'Products';
                    $dt['reference'] = $product['descricaoDoProduto'];
                    $dt['error'] = json_encode($order->errors());
                    $error = $errors->patchEntity($error, $dt);
                    $errors->save($error);
                }
            }
        }

        $errors = TableRegistry::get('Errors')->find()->count();
        $last_key = $produto['codigoDoProduto'];
        $tblRestantes = TableRegistry::get('TblProdutos')
                      ->find()
                      ->order(['codigoDoProduto' => 'ASC'])
                      ->where(['codigoDoProduto > ' => $last_key]);

        $restantes = $tblRestantes->count();

        $count = $products->find()->count();

        $total = $restantes + $count + $errors;

        if($restantes == 0)
            $continue = 0;
        else
            $continue = 1;

        $response['continue'] = $continue;

        $percent_count = ($count * 100) / $total;
        $percent_errors = ($errors * 100) / $total;
        $response['total'] = $total;
        $response['count'] = $count;
        $response['percount'] =  Number::precision($percent_count, 2);
        $response['width_percount'] = (int) $percent_count;
        $response['errors'] = $errors;
        $response['pererrors'] = Number::precision($percent_errors, 2);
        $response['width_pererrors'] = (int) $percent_errors;
        $response['last_key'] = $last_key;

        if($continue == 0)
            $response['msg'] = 'Concluído!';

        /*--------------------------------------------------------*/
        $this->set('response', json_encode($response));
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/

    }

    public function supplies()
    {
        $qr = $this->request->getQuery();
        $id = $qr['id'];


        if(empty($id) || is_null($id) || $id === 0)
            $id = 'aa'; // codigo do fornecedor é alfanumerico

        $supplies = TableRegistry::get('Supplies');

        $conn_ong = ConnectionManager::get('default');
        if($id == 'aa')  // codigo do fornecedor é alfanumerico
            $stmt = $conn_ong->query('TRUNCATE TABLE supplies');

        $tblFornecedores = TableRegistry::get('TblFornecedores')
                         ->find()
                         ->order(['codigoDoFornecedor' => 'ASC'])
                         ->where(['codigoDoFornecedor > ' => $id])
                         ->limit(10);

        foreach($tblFornecedores as $fornecedor) {
            $supply = $supplies->newEntity();
            $supply['code'] = $fornecedor['codigoDoFornecedor'];
            $supply['name'] = $fornecedor['nomeDoFornecedor'];
            $supply['cnpj'] = '';
            $supply['phone_ddd'] = $fornecedor['codigoDDDFornecedor'];
            $supply['phone'] = $fornecedor['telefoneDoFornecedor1'];
            $supply['mobile_ddd'] = '';
            $supply['mobile'] = '';
            $supply['fax_ddd'] = $fornecedor['codigoDDDFornecedor'];
            $supply['fax'] = $fornecedor['telefoneDoFornecedor2'];
            $supply['email'] = $fornecedor['emailDoFornecedor'];
            $supply['contact'] = $fornecedor['contadoDoFornecedor'];

            if(!$supplies->save($supply)) {
                $errors = TableRegistry::get('Errors');
                $error = $errors->newEntity();
                $dt = [];
                $dt['module'] = 'Supplies';
                $dt['reference'] = $supply['nomeDoFornecedor'];
                $dt['error'] = json_encode($order->errors());
                $error = $errors->patchEntity($error, $dt);
                $errors->save($error);
            }
        }

        $errors = TableRegistry::get('Errors')->find()->count();
        $last_key = $fornecedor['codigoDoFornecedor'];
        $tblRestantes = TableRegistry::get('TblFornecedores')
                      ->find()
                      ->order(['codigoDoFornecedor' => 'ASC'])
                      ->where(['codigoDoFornecedor > ' => $last_key]);

        $restantes = $tblRestantes->count();

        $count = $supplies->find()->count();
        $total = $restantes + $count + $errors;

        if($restantes == 0)
            $continue = 0;
        else
            $continue = 1;

        $response['continue'] = $continue;

        $percent_count = ($count * 100) / $total;
        $percent_errors = ($errors * 100) / $total;
        $response['total'] = $total;
        $response['count'] = $count;
        $response['percount'] =  Number::precision($percent_count, 2);
        $response['width_percount'] = (int) $percent_count;
        $response['errors'] = $errors;
        $response['pererrors'] = Number::precision($percent_errors, 2);
        $response['width_pererrors'] = (int) $percent_errors;
        $response['last_key'] = $last_key;

        if($continue == 0)
            $response['msg'] = 'Concluído!';

        /*--------------------------------------------------------*/
        $this->set('response', json_encode($response));
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/

    }

    public function subsidiaries()
    {
        $qr = $this->request->getQuery();
        $id = $qr['id'];

        if(empty($id) || is_null($id))
            $id = 0;

        $subsidiaries = TableRegistry::get('Subsidiaries');

        $conn_ong = ConnectionManager::get('default');
        if($id == 0)
            $stmt = $conn_ong->query('TRUNCATE TABLE subsidiaries');

        $tblLojas = TableRegistry::get('TblLojas')
                  ->find()
                  ->order(['codigoDaLoja' => 'ASC'])
                  ->where(['codigoDaLoja > ' => $id])
                  ->limit(10);

        foreach($tblLojas as $loja) {
            $subsidiary = $subsidiaries->newEntity();
            $subsidiary['name'] = $loja['nomeDaLoja'];
            $subsidiary['cnpj'] = null;
            $subsidiary['phone'] = null;
            $subsidiary['mobile'] = null;
            $subsidiary['fax'] = null;

            if(!$subsidiaries->save($subsidiary)) {
                $errors = TableRegistry::get('Errors');
                $error = $errors->newEntity();
                $dt = [];
                $dt['module'] = 'Subsidiaries';
                $dt['reference'] = $subsidiary['nomeDaLoja'];
                $dt['error'] = json_encode($order->errors());
                $error = $errors->patchEntity($error, $dt);
                $errors->save($error);
            }
        }

        $errors = TableRegistry::get('Errors')->find()->count();
        $last_key = $loja['codigoDaLoja'];
        $tblRestantes = TableRegistry::get('TblLojas')
                      ->find()
                      ->order(['codigoDaLoja' => 'ASC'])
                      ->where(['codigoDaLoja > ' => $last_key]);

        $restantes = $tblRestantes->count();

        $count = $subsidiaries->find()->count();
        $total = $restantes + $count + $errors;

        if($restantes == 0)
            $continue = 0;
        else
            $continue = 1;

        $response['continue'] = $continue;

        $percent_count = ($count * 100) / $total;
        $percent_errors = ($errors * 100) / $total;
        $response['total'] = $total;
        $response['count'] = $count;
        $response['percount'] =  Number::precision($percent_count, 2);
        $response['width_percount'] = (int) $percent_count;
        $response['errors'] = $errors;
        $response['pererrors'] = Number::precision($percent_errors, 2);
        $response['width_pererrors'] = (int) $percent_errors;
        $response['last_key'] = $last_key;

        if($continue == 0)
            $response['msg'] = 'Concluído!';

        /*--------------------------------------------------------*/
        $this->set('response', json_encode($response));
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/

    }

    public function employees()
    {
        $qr = $this->request->getQuery();
        $id = $qr['id'];

        if(empty($id) || is_null($id))
            $id = 0;

        $employees = TableRegistry::get('Employees');

        $conn_ong = ConnectionManager::get('default');
        if($id == 0)
            $stmt = $conn_ong->query('TRUNCATE TABLE employees');


        $tblVendedores = TableRegistry::get('TblVendedores')
                       ->find()
                       ->order(['codigoDoVendedor' => 'ASC'])
                       ->where(['codigoDoVendedor > ' => $id])
                       ->limit(10);

        foreach($tblVendedores as $vendedor) {
            $employee = $employees->newEntity();
            $employee['name'] = $vendedor['nomeCompleto'];
            $employee['nick_name'] = $vendedor['conhecidoPor'];
            $employee['cpf'] = null;
            $employee['birth_date'] = null;
            $employee['phone'] = null;
            $employee['mobile'] = '';
            $employee['fax'] = null;
            $employee['user_id'] = null;

            if(!$employees->save($employee)) {
                //                echo "<h3>ERRO: " . $order['number'] . "</h3>";
                //                debug($order->errors());
                $errors = TableRegistry::get('Errors');
                $error = $errors->newEntity();
                $dt = [];
                $dt['module'] = 'Employees.MigrateGcomm';
                $dt['reference'] = $employee['nick_name'];
                $dt['error'] = json_encode($order->errors());
                //                debug($dt);
                $error = $errors->patchEntity($error, $dt);
                $errors->save($error);
                Log::debug('Erro ao salvar employee: ' . $employee['name'] . ' - vendedor: ' . $vendedor['nomeCompleto']);

            } else {
                Log::debug('GRAVOU employee: ' . $employee['name']);
            }
        }

        $errors = TableRegistry::get('Errors')->find()->count();
        $last_key = $vendedor['codigoDoVendedor'];
        $tblRestantes = TableRegistry::get('TblVendedores')
                      ->find()
                      ->order(['codigoDoVendedor' => 'ASC'])
                      ->where(['codigoDoVendedor > ' => $last_key]);

        $restantes = $tblRestantes->count();

        $count = $employees->find()->count();
        $total = $restantes + $count + $errors;

        if($restantes == 0)
            $continue = 0;
        else
            $continue = 1;

        //        $qtd_errors = $errors->find()->select()->count();
        $response['continue'] = $continue;

        $percent_count = ($count * 100) / $total;
        $percent_errors = ($errors * 100) / $total;
        $response['total'] = $total;
        $response['count'] = $count;
        $response['percount'] =  Number::precision($percent_count, 2);
        $response['width_percount'] = (int) $percent_count;
        $response['errors'] = $errors;
        $response['pererrors'] = Number::precision($percent_errors, 2);
        $response['width_pererrors'] = (int) $percent_errors;
        $response['last_key'] = $last_key;

        if($continue == 0)
            $response['msg'] = 'Concluído!';

        /*--------------------------------------------------------*/
        $this->set('response', json_encode($response));
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/

    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {

    }

    /**
     * View method
     *
     * @param string|null $id Migration id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $migration = $this->Migrations->get($id, [
            'contain' => []
        ]);

        $this->set('migration', $migration);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $migration = $this->Migrations->newEntity();
        if ($this->request->is('post')) {
            $migration = $this->Migrations->patchEntity($migration, $this->request->getData());
            if ($this->Migrations->save($migration)) {
                $this->Flash->success(__('The migration has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The migration could not be saved. Please, try again.'));
        }
        $this->set(compact('migration'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Migration id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $migration = $this->Migrations->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $migration = $this->Migrations->patchEntity($migration, $this->request->getData());
            if ($this->Migrations->save($migration)) {
                $this->Flash->success(__('The migration has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The migration could not be saved. Please, try again.'));
        }
        $this->set(compact('migration'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Migration id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $migration = $this->Migrations->get($id);
        if ($this->Migrations->delete($migration)) {
            $this->Flash->success(__('The migration has been deleted.'));
        } else {
            $this->Flash->error(__('The migration could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    private function verifyPhones($ddd1, $tel1, $ddd2, $tel2, $ddd3, $tel3)
    {
        $phone = $mobile = $other = '';
        $tel1 = str_replace("-", "", $tel1);
        $tel2 = str_replace("-", "", $tel2);
        $tel3 = str_replace("-", "", $tel3);

        if(strlen($tel1) < 8) $tel1 = "";
        if(strlen($tel2) < 8) $tel2 = "";
        if(strlen($tel3) < 8) $tel3 = "";

        if($tel1 != "") {
            if(substr($tel1, 0, 1) == "9" || substr($tel1, 0, 1) == "8" ||
               substr($tel1, 0, 1) == "7" || substr($tel1, 0, 1) == "6") {
                if(strlen($tel1) == 8)
                    $tel1 = "9" . $tel1;
                $mobile = $ddd1 . $tel1;
            } else {
                if(strlen($tel1) > 7) {
                    $phone = $ddd1 . $tel1;
                }
            }
        }

        if($tel2 != "") {
            if(substr($tel2, 0, 1) == "9" || substr($tel2, 0, 1) == "8" ||
               substr($tel2, 0, 1) == "7" || substr($tel2, 0, 1) == "6") {
                if(strlen($tel2) == 8)
                    $tel2 = "9" . $tel2;
                if($mobile == "")
                    $mobile = $ddd2 . $tel2;
            } else {
                if(strlen($tel2) > 7) {
                    if($phone == "")
                        $phone = $ddd2 . $tel2;
                    else
                        $other = $ddd2 . $tel2;
                }
            }
        }

        if($tel3 != "") {
            if(substr($tel3, 0, 1) == "9" || substr($tel3, 0, 1) == "8" ||
               substr($tel3, 0, 1) == "7" || substr($tel3, 0, 1) == "6") {
                if(strlen($tel3) == 8)
                    $tel3 = "9" . $tel3;
                if($mobile == "")
                    $mobile = $ddd3 . $tel3;
            } else {
                if(strlen($tel3) > 7) {
                    if($phone == "")
                        $phone = $ddd3 . $tel3;
                    else
                        if($other == "")
                            $other = $ddd3 . $tel3;
                }
            }
        }
        return [$phone, $mobile, $other];
    }

    private function getHistoricoRequisicoes($collect)
    {
        $countRequisicao = $collect->countBy(function ($value) {
            return is_null($value->DataDaRequisicao) ? null : $value->DataDaRequisicao->i18nFormat('yyyy-MM-dd HH:mm:ss');
        });
        return $countRequisicao->toArray();
    }

    private function getHistoricoRecebimentos($collect)
    {
        $countRecebimentos = $collect->countBy(function ($value) {
            return is_null($value->DataDoRecebimento) ? null : $value->DataDoRecebimento->i18nFormat('yyyy-MM-dd HH:mm:ss');
        });
        return $countRecebimentos->toArray();
    }

    private function getHistoricoExpedicoes($collect)
    {
        $countExpedicoes = $collect->countBy(function ($value) {
            return is_null($value->DataDaExpedicao) ? null : $value->DataDaExpedicao->i18nFormat('yyyy-MM-dd HH:mm:ss');
        });
        return $countExpedicoes->toArray();
    }

    private function getHistoricoConclusoes($collect)
    {
        $countConclusoes = $collect->countBy(function ($value) {
            return is_null($value->DataDaConclusao) ? null : $value->DataDaConclusao->i18nFormat('yyyy-MM-dd HH:mm:ss');
        });
        return $countConclusoes->toArray();
    }

    private function getArray($arr, $status)
    {
        $i = 0;
        $arr_new = [];
        foreach($arr as $key => $value) {
            if(!empty($key)) {
                $arr_new[$i]['qtd'] = $value;
                $arr_new[$i]['status'] = $status;
                $arr_new[$i]['deadline'] = 0;
                $arr_new[$i]['created'] = $key;
                $arr_new[$i]['modified'] = $key;
                $i++;
            }
        }
        return $arr_new;
    }

    private function flatten($array)
    {
        $return = [];
        foreach ($array as $key => $value) {
            $return[] = $key;
            if (is_array($value)){
                $return = array_merge($return, $this->flatten($value));
            } else {
                $return[] = $value;
                $return[] = '@';
            }
        }
        return $return;
    }
    private function verificaSeClienteExiste($new_order)
    {
        // verifica se existe pedido com cpf e nome iguais no sistema
        // se houver, vê o pedido mais recente com data < hoje (pra evitar 2027, etc...)
        // se for o pedido atual, salva com edit os dados novos do cliente, tira os
        // dados de 'customer' e seta customer_id
        // senão, se não for o pedido atual apenas pega customer_id do existente e seta
        // nos dois casos grava os dados 'perdidos' num log com o registro do pedido

        // $conn_ong = ConnectionManager::get('default');
        // $cliente = $conn_ong->execute("select customers.id as id, orders.date_order as date_order from customers inner join orders on customers.id = orders.customer_id where customers.cpf_cnpj = '" . $new_order['customer']['cpf_cnpj'] . "' and TRIM(name) = '" . $new_order['customer']['name'] ."' order by orders.date_order DESC limit 1")->fetchAll('assoc');

        $cliente = TableRegistry::get('Customers')
                 ->find()
                 ->contain([
                     'Orders' => function(Query $q) {
                         return $q->order(['date_order' => 'DESC']);
                     },
                     'Addresses',
                 ])
                 ->where(['cpf_cnpj' => $new_order['customer']['cpf_cnpj']])
                 ->andWhere(['name' => $new_order['customer']['name']])
                 ->first();

        if(!empty($cliente)) {
            $dt_ped_gravado = new Date($cliente['orders'][0]['date_order']);
            $dt_ped_atual = new Date($new_order['date_order']);
            if($dt_ped_atual > $dt_ped_gravado)
                return $cliente;
            else
                return $cliente['id'];

        } else {
            return false;
        }
    }

    private function verificaSeCpfExiste($new_order)
    {
        // verifica se apenas o cpf é igual, se for, seta nulo e grava em comentários
        // $conn_ong = ConnectionManager::get('default');
        // $cliente = $conn_ong->execute("select customers.id as id, orders.date_order as date_order from customers inner join orders on customers.id = orders.customer_id where customers.cpf_cnpj = '" . $new_order['customer']['cpf_cnpj'] . "' and TRIM(name) = '" . $new_order['customer']['name'] ."' order by orders.date_order DESC limit 1")->fetchAll('assoc');

        $cliente = TableRegistry::get('Customers')
                 ->find()
                 ->contain([
                     'Orders' => function(Query $q) {
                         return $q->order(['date_order' => 'DESC']);
                     },
                     'Addresses',
                 ])
                 ->where(['cpf_cnpj' => $new_order['customer']['cpf_cnpj']])
                 ->first();

        if(!empty($cliente)) {
            return $cliente;
        } else {
            return false;
        }
    }
    // public function beforeFilter(Event $event)
    // {
    //     $this->getEventManager()->off($this->Csrf);
    // }
}
