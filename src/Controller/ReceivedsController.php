<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\I18n\Number;

use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;

/**
 * Receiveds Controller
 *
 * @property \App\Model\Table\ReceivedsTable $Receiveds
 *
 * @method \App\Model\Entity\Received[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReceivedsController extends AppController
{
    private function totalItemsReceived($purchase_id)
    {
        $query = $this->Receiveds->find();
        $receivedsSum = $query->select([
            'total_items_received' => $query->func()->sum('Receiveds.qtd')
        ])->where([
            'Receiveds.purchase_id' => $purchase_id,
            'Receiveds.canceled' => 0,
        ])->first();

        return $receivedsSum->total_items_received;
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function insertReceived()
    {
        // Log::debug('entrou em ReceivedsController->insertReceived');
        $purchaseId = $this->request->query('p');
        $qtd = $this->request->query('q');

        $received = $this->Receiveds->newEntity();

        $query = $this->Receiveds->Purchases->find();

        $purchase = $query->select([
            'product_id',
            'qtd',
        ])->where([
            'id' => $purchaseId,
        ])->first();
        // Log::debug($purchase);

        $received->product_id = $purchase->product_id;
        $received->purchase_id = $purchaseId;
        $received->qtd = $qtd;

        if($this->Receiveds->save($received)) {
            // Log::debug('salvou');
            $savedReceived = $this->Receiveds->find()->where([
                'purchase_id' => $purchaseId,
            ])->order([
                'created' => 'DESC',
            ])->first();

            $total_items_received = $this->totalItemsReceived($purchaseId);

            $data['id'] = $savedReceived->id;
            $data['qtd'] = $savedReceived->qtd;
            $data['created'] = $savedReceived->created;
            $data['product_id'] = $received->product_id;
            $data['created_formated'] = $savedReceived->created->format('d/m/Y \à\s H:i');
            $data['total'] = $purchase->qtd;
            $data['recSum'] = $total_items_received;
            $data['success']['message'] = __('The received was successfully');
        } else {
            Log::debug('não salvou');
            $data['error'] = "Não é possível executar o recebimento.";
        }
        //        $this->set(compact('data')); // Pass $data to the view
        //        $this->set('_serialize', 'data'); // Let the JsonView class know what variable to use

        /*-------------------------------------------------------------
         * Depois de longo e tenebroso inverno, esse trecho funcionou.
         * Depende do layout ajax.ctp e do render json
         */
        $this->set('response', json_encode($data));
        //        $this->set(compact('response')); // Pass $data to the view
        $this->set('_serialize', 'response');
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/
    }

    public function cancelReceived()
    {
        // Log::write("debug", "ReceivedsController - cancelReceived - entrou");

        $purchaseId = $this->request->query('p');
        $qtd = $this->request->query('q');
        //        $received_id = $this->request->query('id');

        $cancelReceived = $this->Receiveds->newEntity();

        $query = $this->Receiveds->Purchases->find();

        $purchase = $query->select([
            'product_id',
            'qtd',
        ])->where([
            'id' => $purchaseId,
        ])->first();

        $cancelReceived->product_id = $purchase->product_id;
        $cancelReceived->purchase_id = $purchaseId;
        $cancelReceived->qtd = $qtd * -1;
        $cancelReceived->canceled = 1;

        if($this->Receiveds->save($cancelReceived)) {
            // Log::debug('salvou');
            $savedReceived = $this->Receiveds->find()->where([
                'purchase_id' => $purchaseId,
            ])->order([
                'created' => 'DESC',
            ])->first();

            $total_items_received = $this->totalItemsReceived($purchaseId);

            $data['id'] = $savedReceived->id;
            $data['qtd'] = $savedReceived->qtd;
            $data['created'] = $savedReceived->created;
            $data['product_id'] = $cancelReceived->product_id;
            $data['created_formated'] = $savedReceived->created->format('d/m/Y \à\s H:i');
            $data['total'] = $purchase->qtd;
            $data['recSum'] = $total_items_received;
            $data['success']['message'] = __('The received was successfully');
        } else {
            Log::debug('não salvou');
            $data['error'] = "Não é possível executar o recebimento.";
        }
        //        $this->set(compact('data')); // Pass $data to the view
        //        $this->set('_serialize', 'data'); // Let the JsonView class know what variable to use

        /*-------------------------------------------------------------
         * Depois de longo e tenebroso inverno, esse trecho funcionou.
         * Depende do layout ajax.ctp e do render json
         */
        $this->set('response', json_encode($data));
        //        $this->set(compact('response')); // Pass $data to the view
        $this->set('_serialize', 'response');
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/
        






        // $receivedOld = $this->Receiveds->findById($received_id)->first();

        // $current_qtd_stock = $this->Receiveds->Stocks->totalStock($receivedOld->product_id);

        // Log::write("debug", "ReceivedsController - cancelReceived - estoque atual = " . $current_qtd_stock);
        // Log::write("debug", "ReceivedsController - cancelReceived - receivedOld->qtd = " . $receivedOld->qtd);

        // if($receivedOld->qtd > $current_qtd_stock) {
        //     $data['error'] = "Não é possível executar o cancelamento. Só existem $current_qtd_stock unidades deste produto no estoque.";
        //     Log::write("debug", $data['error']);
        // } else {
        //     $received = $this->Receiveds->newEntity();
        //     $received->product_id = $receivedOld->product_id;
        //     $received->purchase_id = $receivedOld->purchase_id;
        //     $received->qtd = $receivedOld->qtd * -1;

        //     if($this->Receiveds->save($received)){
        //         $savedCancel = $this->Receiveds->find()->where([
        //             'purchase_id' => $received->purchase_id,
        //         ])->order([
        //             'created' => 'DESC',
        //         ])->first();

        //         Log::write("debug","ReceivedsController: gravou received");
        //         $data['success']['message'] = __('Cancellation has been successfully.');

        //         // total da compra
        //         $query = $this->Receiveds->Purchases->find();
        //         $purchase = $query->select([
        //             'qtd',
        //         ])->where([
        //             'id' => $received->purchase_id,
        //         ])->first();

        //         $data['qtd'] = $received->qtd;
        //         $data['created'] = $savedCancel->created;
        //         $data['product_id'] = $received->product_id;
        //         $data['created_formated'] = $savedCancel->created->format('d/m/Y \à\s H:i');
        //         $data['total'] = $purchase->qtd;
        //         $data['recSum'] = $this->totalItemsReceived($receivedOld->purchase_id);

        //         Log::write("debug", "ReceivedsController - cancelReceived - saindo...");
        //     } else {
        //         Log::write("debug","ReceivedsController: deu merda no cancelamento do received");
        //         $data['error'] = "Não foi possível cancelar o recebimento";
        //     }
        // }

        // // $this->set(compact('data')); // Pass $data to the view
        // // $this->set('_serialize', 'data'); // Let the JsonView class know what variable to use



        // Log::write("debug", "--------------------------------------------------------------");
        // Log::write("debug", "data['success']= " . $data['success']['message']);
        // Log::write("debug", "data['total']= " . $data['total']);
        // Log::write("debug", "data['recSum']= " . $data['recSum']);
        // Log::write("debug", "data['created_formated']= " . $data['created_formated']);
        // Log::write("debug", "--------------------------------------------------------------");
        // /*-------------------------------------------------------------
        //  * Depois de longo e tenebroso inverno, esse trecho funcionou.
        //  * Depende do layout ajax.ctp e do render json
        //  */
        // $this->set('response', json_encode($data));
        // //        $this->set(compact('response')); // Pass $data to the view
        // $this->set('_serialize', 'response');
        // $this->viewBuilder()->setLayout('ajax');
        // $this->render('/Element/json');
        // /*--------------------------------------------------------*/
        // Log::write("debug", "ReceivedsController - cancelReceived - saiu!");
    }
}
