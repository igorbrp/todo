<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\I18n\I18n;
use Cake\I18n\Date;
use Cake\ORM\Query;

use Cake\Database\Expression\QueryExpression;

use Cake\Log\Log;

/**
 * Deliveries Controller
 *
 * @property \App\Model\Table\DeliveriesTable $Deliveries
 *
 * @method \App\Model\Entity\Delivery[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DeliveriesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('CorrecoesDeEmergencia');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $data = $this->request->getQuery();
        $deliveries = $this->getDeliveryModel($data);
        
        if (isset($data['foreseen'])) {
            $dt = $this->CorrecoesDeEmergencia->BrDateToUtcFormat(rawurldecode($data['foreseen']));
            $deliveries->where(['Deliveries.foreseen_date' => $dt]);
        }

        if (isset($data['scheduled'])) {
            $dt = $this->CorrecoesDeEmergencia->BrDateToUtcFormat(rawurldecode($data['scheduled']));
            $deliveries->where(['DeliveryMades.scheduled_date' => $dt]);
        }

        if (isset($data['delivery'])) {
            $dt = $this->CorrecoesDeEmergencia->BrDateToUtcFormat(rawurldecode($data['delivery']));
            $deliveries->where(['DeliveryMades.delivery_date' => $dt]);
        }

        if (isset($data['vehicle'])) {
            $deliveries->where(['DeliveryMades.vehicle_id' => rawurldecode($data['vehicle'])]);
        }

        if (!isset($data['completeds']) || !$data['completeds']) {
            $deliveries->where(['DeliveryMades.delivery_date IS' => null]);
        } 
            
        $deliveries->group('Deliveries.order_id');

        $vehiclesModel = $this->loadModel('Vehicles');
        $vehicles = $vehiclesModel->find('list');
        $this->set(compact('vehicles'));

        if (isset($data['print']) && $data['print']) {
            $this->set(compact('deliveries'));
            $this->render('index-print');
        } else {
            $this->set(compact('deliveries'), $this->paginate($deliveries, [
                'sortWhitelist' => [
                    'Orders.number',
                    // // 'Customers.name',
                    // // 'Addresses.neighborhood',
                    // 'foreseen_date',
                    // 'foreseen_horary',
                    // 'DeliveryMades.scheduled_date',
                    // 'DeliveryMades.scheduled_horary',
                    // 'Vehicles.identification',
                ]
            ]));
        }
    }

    public function romance($order_id)
    {
        $orderModel = $this->loadModel('Orders');
        $queryOrder = $orderModel
                    ->find()
                    ->contain(['Customers.Addresses', 'Deliveries.Addresses'])
                    ->where(['Orders.id' => $order_id]);

        $deliveryMadesModel = $this->loadModel('DeliveryMades');

        $queryDeliveryMades = $deliveryMadesModel
                 ->find()
                 ->contain([
                     // 'ItemHistories',
                     // 'ItemHistories.Items',
                     'ItemHistories.Items.Orders' => [
                         'fields' => ['id'],
                     ],
                 ]);

        $queryDeliveryMades->where(['Orders.id' => $order_id])
              ->andWhere(['delivery_date IS' => null]);

        $order = $queryOrder->first();
        $romance = $queryDeliveryMades->all();

        if ($order['customer']['address']['city_id'] != '') {
            $cities = $this->loadModel('Cities');
            $city = $cities->get($order['customer']['address']['city_id']);
            $cityNameOfCustomer = $city['name'];
        }

        $this->set('cityNameOfCustomer', $cityNameOfCustomer);

        $this->set('romance', $romance);
        $this->set('order', $order);

        $this->render('romance_print');

    }

    private function getDeliveryModel($data)
    {
        return $this->Deliveries
            ->find('search', ['search' => $data])
            ->contain([
                'Orders' => function (Query $q) {
                    return $q
                        ->select(['id', 'number', 'date_order']);
                },
                'Orders.Customers' => function (Query $q) {
                    return $q
                        ->select(['name']);
                },
                'Orders.Customers.Addresses' => function (Query $q) {
                    return $q
                        ->select(['neighborhood']);
                },
                'DeliveryMades' => function(Query $q) {
                    return $q
                        ->select(['delivery_id', 'vehicle_id', 'scheduled_date', 'scheduled_horary', 'delivery_date', 'delivery_horary'])
                        ->group(['vehicle_id', 'scheduled_date', 'scheduled_horary']);
                },
                'DeliveryMades.ItemHistories' => function (Query $q) {
                    return $q
                        ->select(['item_id']);
                },
                'DeliveryMades.ItemHistories.Items' => function (Query $q) {
                    return $q
                        ->select(['sum_qtd' => $q->func()->sum($q->identifier('Items.qtd'))]);
                },
            ])
            ->leftJoinWith('DeliveryMades')
            ->innerJoinWith('Orders.Customers.Addresses');
    }

    public function indexPrint()
    {
        $data = $this->request->getQuery();
        $deliveries = $this->getDeliveryModel($data);
        
        if (isset($data['foreseen'])) {
            $dt = $this->CorrecoesDeEmergencia->BrDateToUtcFormat(rawurldecode($data['foreseen']));
            $deliveries->where(['Deliveries.foreseen_date' => $dt]);
        }

        if (isset($data['scheduled'])) {
            $dt = $this->CorrecoesDeEmergencia->BrDateToUtcFormat(rawurldecode($data['scheduled']));
            $deliveries->where(['DeliveryMades.scheduled_date' => $dt]);
        }

        if (isset($data['delivery'])) {
            $dt = $this->CorrecoesDeEmergencia->BrDateToUtcFormat(rawurldecode($data['delivery']));
            $deliveries->where(['DeliveryMades.delivery_date' => $dt]);
        }

        if (isset($data['vehicle'])) {
            $deliveries->where(['DeliveryMades.vehicle_id' => $data['vehicle']]);
        }

        if (!isset($data['completeds']) || !$data['completeds']) {
            $deliveries->where(['DeliveryMades.delivery_date IS' => null]);
        } 
            
        $deliveries->group('Deliveries.order_id');

        $this->set(compact('deliveries'), $this->paginate($deliveries, [
            'sortWhitelist' => [
                'Orders.number',
                'Customers.name',
                'Addresses.neighborhood',
                'foreseen_date',
                'foreseen_horary',
                'DeliveryMades.scheduled_date',
                'DeliveryMades.scheduled_horary',
                'Vehicles.identification',
            ]
        ]));

        $vehicles = $this->Deliveries->DeliveryMades->Vehicles->find('list')->limit(['200']);
        $this->set(compact('vehicles'));
    }

    // public function updateScheduled()
    // {
    //     $this->request->allowMethod('ajax');
    //     $deliveryMadeId = $this->request->query('delivery_id');
    //     $scheduledDate = $this->request->query('scheduled_date');
    //     $scheduledHorary = $this->request->query('scheduled_horary');

    //     $deliveryMade = $this->Deliveries->DeliveryMades->get($deliveryMadeId);
    //     $scheduledDate = new Date($this->CorrecoesDeEmergencia->BrDateToUtcFormat($scheduledDate));

    //     $deliveryMade->scheduled_date = $scheduledDate;
    //     if ($this->Deliveries->DeliveryMades->save($deliveryMade)) {
    //         $data = true;
    //     } else {
    //         $data = false;
    //     }

    //     /*-------------------------------------------------------------
    //      * Depois de longo e tenebroso inverno, esse trecho funcionou.
    //      * Depende do layout ajax.ctp e do render json
    //      */
    //     $this->set('response', json_encode($data));
    //     //        $this->set(compact('response')); // Pass $data to the view
    //     //        $this->set('_serialize', 'response');
    //     $this->viewBuilder()->setLayout('ajax');
    //     $this->render('/Element/json');
    //     /*--------------------------------------------------------*/
    // }

    public function updateVehicle()
    {
        $this->request->allowMethod('ajax');
        $deliveryMadeId = $this->request->query('delivery_made_id');
        $vehicleId = $this->request->query('vehicle_id');

        // Log::debug('$deliveryMadeId: ' . $deliveryMadeId);
        // Log::debug('$vehicleId: ' . $vehicleId);

        $deliveryMade = $this->Deliveries->DeliveryMades->get($deliveryMadeId);
        // Log::debug($deliveryMade);
        $deliveryMade->vehicle_id = $vehicleId;
        if ($this->Deliveries->DeliveryMades->save($deliveryMade)) {
            $data = true;
        } else {
            $data = false;
        }

        /*-------------------------------------------------------------
         * Depois de longo e tenebroso inverno, esse trecho funcionou.
         * Depende do layout ajax.ctp e do render json
         */
        $this->set('response', json_encode($data));
        //        $this->set(compact('response')); // Pass $data to the view
        //        $this->set('_serialize', 'response');
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/
    }

    /**
     * View method
     *
     * @param string|null $id Delivery id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $delivery = $this->Deliveries->get($id, [
            'contain' => ['Orders', 'Addresses'],
        ]);

        $this->set('delivery', $delivery);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $delivery = $this->Deliveries->newEntity();
        if ($this->request->is('post')) {
            $delivery = $this->Deliveries->patchEntity($delivery, $this->request->getData());
            if ($this->Deliveries->save($delivery)) {
                $this->Flash->success(__('The delivery has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The delivery could not be saved. Please, try again.'));
        }
        $orders = $this->Deliveries->Orders->find('list', ['limit' => 200]);
        $this->set(compact('delivery', 'orders'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Delivery id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $delivery = $this->Deliveries->get($id, [
            'contain' => [
                'Orders' => function(Query $q) {
                    return $q
                        ->select(['id', 'number', 'date_order']);
                },
                'DeliveryMades' => function(Query $q) {
                    return $q
                        ->select(['delivery_id', 'vehicle_id', 'scheduled_date', 'scheduled_horary', 'delivery_date', 'delivery_horary'])
                        ->group(['vehicle_id', 'scheduled_date', 'scheduled_horary']);
                },
                'DeliveryMades.ItemHistories' => function (Query $q) {
                    return $q
                        ->select(['item_id']);
                },
                'DeliveryMades.ItemHistories.Items' => function (Query $q) {
                    return $q
                        ->select(['sum_qtd' => $q->func()->sum($q->identifier('Items.qtd'))]);
                }
            ]
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $delivery = $this->Deliveries->patchEntity($delivery, $this->request->getData());
            if ($this->Deliveries->save($delivery)) {
                $this->Flash->success(__('The delivery has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The delivery could not be saved. Please, try again.'));
        }

        $vehiclesModel = $this->loadModel('Vehicles');
        $vehicles = $vehiclesModel->find('list');
        $this->set(compact('vehicles'));

        $this->set(compact('delivery'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Delivery id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $delivery = $this->Deliveries->get($id);
        if ($this->Deliveries->delete($delivery)) {
            $this->Flash->success(__('The delivery has been deleted.'));
        } else {
            $this->Flash->error(__('The delivery could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
