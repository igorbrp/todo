<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Log\Log;

/**
 * Stocks Controller
 *
 * @property \App\Model\Table\StocksTable $Stocks
 *
 * @method \App\Model\Entity\Stock[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class StocksController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $data = $this->request->getQuery();
        // Log::debug($data);
        $stocks = $this->Stocks
                ->find('search', ['search' => $data])
                ->contain(['Products', 'Products.Supplies']);

        $stocks->where($this->buildWhere($data));

        if (!isset($data['sort'])) {
            $concat = $stocks->func()->concat([
                'Supplies.name' => 'identifier',
                'Products.title' => 'identifier',
            ]);
            $stocks->orderAsc($concat);
            // $stocks
            //     ->innerJoinWith('Products.Supplies', function ($q) {
            //         return $q->order(['Products.Supplies.name' => 'asc']);
            //     })
            //     ->innerJoinWith('Products', function ($q) {
            //         return $q->order(['Products.title' => 'asc']);
            //     });
        }

        $supplies = $this->Stocks->Products->Supplies
                  ->find('list', ['limit' => 200])
                  // ->contain(['Products', 'Products.Stocks'])
                  //                  ->where(['Products.is_stock' => true])
                  //                  ->andWhere(['Products.Stocks.qtd > ' => 0])
                  ->innerJoinWith('Products', function ($q) {
                      return $q->where(['Products.is_stock' => true]);
                  })
                  ->innerJoinWith('Products.Stocks', function ($q) {
                      return $q->where(['Stocks.qtd >' => 0]);
                  });
        
        $this->set(compact('supplies'));


        if (isset($data['print']) && $data['print']) {
            $this->set(compact('stocks'));
            $this->render('index-print');
        } else {
            $this->set(compact('stocks'), $this->paginate($stocks, [
                'sortWhitelist' => [
                    'Products.sku', 'Products.title', 'Supplies.name', 'qtd'
                ]
            ]));
        }
    }

    public function indexPrint()
    {
        $data = $this->request->getQuery();
        // Log::debug($data);
        $stocks = $this->Stocks
                ->find('search', ['search' => $data])
                ->contain(['Products', 'Products.Supplies']);

        $stocks->where($this->buildWhere($data));

        if (!isset($data['sort'])) {
            $stocks
                ->innerJoinWith('Products', function ($q) {
                    return $q->order(['Products.title' => 'asc']);
                })
                ->innerJoinWith('Products.Supplies', function ($q) {
                    return $q->order(['Products.Supplies.name' => 'asc']);
                });
        }

        $supplies = $this->Stocks->Products->Supplies
                  ->find('list', ['limit' => 200])
                  // ->contain(['Products', 'Products.Stocks'])
                  //                  ->where(['Products.is_stock' => true])
                  //                  ->andWhere(['Products.Stocks.qtd > ' => 0])
                  ->innerJoinWith('Products', function ($q) {
                      return $q->where(['Products.is_stock' => true]);
                  })
                  ->innerJoinWith('Products.Stocks', function ($q) {
                      return $q->where(['Stocks.qtd >' => 0]);
                  });
        
        
        $this->set(compact('supplies'));

        $this->set(compact('stocks'), $this->paginate($stocks));
    }
    public function buildWhere($data)
    {
        $where = ['1']; // importante

        if(!isset($data['selectsupply'])) {
            $data['selectsupply'] = '';
            //            return $where;
        } else {
            $where = ['Supplies.id' => $data['selectsupply']];
        }
        
        return $where;
    }
    /**
     * View method
     *
     * @param string|null $id Stock id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $stock = $this->Stocks->get($id, [
            'contain' => [],
        ]);

        $this->set('stock', $stock);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $stock = $this->Stocks->newEntity();
        if ($this->request->is('post')) {
            $stock = $this->Stocks->patchEntity($stock, $this->request->getData());
            if ($this->Stocks->save($stock)) {
                $this->Flash->success(__('The stock has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The stock could not be saved. Please, try again.'));
        }
        $this->set(compact('stock'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Stock id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $stock = $this->Stocks->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $stock = $this->Stocks->patchEntity($stock, $this->request->getData());
            if ($this->Stocks->save($stock)) {
                $this->Flash->success(__('The stock has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The stock could not be saved. Please, try again.'));
        }
        $this->set(compact('stock'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Stock id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $stock = $this->Stocks->get($id);
        if ($this->Stocks->delete($stock)) {
            $this->Flash->success(__('The stock has been deleted.'));
        } else {
            $this->Flash->error(__('The stock could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function StockQtd()
    {
        $this->request->allowMethod('ajax');
		$product_id = $this->request->query('product_id');
        $result = $this->Stocks
                ->find()
                ->where(['product_id' => $product_id])
                ->first();
        
        $data = is_null($result['qtd']) ? 0 : $result['qtd'];

        /*-------------------------------------------------------------
         * Depois de longo e tenebroso inverno, esse trecho funcionou.
         * Depende do layout ajax.ctp e do render json
         */
        $this->set('response', json_encode($data));
        //        $this->set(compact('response')); // Pass $data to the view
        //        $this->set('_serialize', 'response');
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/
    }
}
