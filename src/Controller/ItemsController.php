<?php
namespace App\Controller;

use App\Controller\AppController;
use \Cake\Collection\CollectionInterface;
use \Cake\Database\Expression\QueryExpression;
use \Cake\ORM\Query;

use \Cake\Log\Log;

/**
 * Items Controller
 *
 * @property \App\Model\Table\ItemsTable $Items
 *
 * @method \App\Model\Entity\Item[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ItemsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $data = $this->request->getQuery();

        $query = $this->Items
               ->find('search', ['search' => $data])
               ->contain(['Products', 'Supplies', 'Orders']);

        $query->where($this->buildWhere($data));

        $supplies = $this->Items->Supplies->find('list', ['limit' => 200]);
        $this->set(compact('supplies'));

        $this->set('items', $this->paginate($query));

        $modelVehicles = $this->loadModel('Vehicles');
        $vehicles = $modelVehicles->find('list');

        $this->set(compact('vehicles'));
        $this->set('q', isset($data['q']) ? $data['q'] : '');
        $this->set('_serialize', ['items']);
    }

    public function buildWhere($data)
    {
        $where = ['1']; // importante

        if(!isset($data['items_tot_uncovered']) && // pesquisa padrão
           !isset($data['items_tot_required']) &&
           !isset($data['items_tot_received']) &&
           !isset($data['items_tot_dispatched']) &&
           !isset($data['items_partially']) &&
           !isset($data['items_completed'])) {

            $data['items_tot_uncovered'] = $data['items_tot_required'] = $data['items_tot_received'] = $data['items_tot_dispatched'] = $data['items_partially'] = $data['items_completed'] = 0;

        } // else {
        if($data['items_tot_uncovered']) { // pendentes
            if($data['items_partially']) {
                $where = ['total_uncovered > ' => 0];
            } else {
                //                $where = ['qtd' => new \Cake\Database\Expression\IdentifierExpression('total_uncovered')];
                $where = ['qtd = total_uncovered'];
            }
        } else {
            if($data['items_tot_required']) { // requisitados
                if($data['items_partially']) {
                    $where = ['total_required > ' => 0];
                } else {
                    //                    $where = ['qtd' => new \Cake\Database\Expression\IdentifierExpression('total_required')];
                    $where = ['qtd = total_required'];
                }
            } else {
                if($data['items_tot_received']) { // recebidos
                    if($data['items_partially']) {
                        $where = ['total_received > ' => 0];
                    } else {
                        //                        $where = ['qtd' => new \Cake\Database\Expression\IdentifierExpression('total_received')];
                        $where = ['qtd = total_received'];
                    }
                } else {
                    if($data['items_tot_dispatched']) { // despachados
                        if($data['items_partially']) {
                            $where = ['total_dispatched > ' => 0];
                        } else {
                            //                            $where = ['qtd' => new \Cake\Database\Expression\IdentifierExpression('total_dispatched')];
                            $where = ['qtd = total_dispatched'];
                        }
                    } else {
                        if($data['items_completed']) { // inclusive concluidos
                        } else {
                            // créditos a esse link: https://stackoverflow.com/questions/43725445/how-to-compare-two-fields-columns-in-a-condition/43726213#43726213
                            //                            $where = ['qtd != ' => new \Cake\Database\Expression\IdentifierExpression('total_completed')];
                            $where = ['qtd != total_completed'];
                        }
                    }
                }
            }
        }
        return $where;
    }

    public function changeStatus()
    {
        if($this->request->is('post')) {
            $data = $this->request->getData();
            $item_id = isset($data['item_id']) ? $data['item_id'] : null;
            $from = isset($data['from']) ? $data['from'] : null;
            $to = isset($data['to']) ? $data['to'] : null;
            $qtd = isset($data['qtd']) ? $data['qtd'] : null;
        }
    }

    // /**
    //  * View method
    //  *
    //  * @param string|null $id Item id.
    //  * @return \Cake\Http\Response|null
    //  * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //  */
    // public function view($id = null)
    // {
    //     $item = $this->Items->get($id, [
    //         'contain' => ['Products', 'Supplies', 'Orders']
    //     ]);

    //     $this->set('item', $item);
    // }

    // /**
    //  * Add method
    //  *
    //  * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
    //  */
    // public function add()
    // {
    //     $item = $this->Items->newEntity();
    //     if ($this->request->is('post')) {
    //         $item = $this->Items->patchEntity($item, $this->request->getData());
    //         if ($this->Items->save($item)) {
    //             $this->Flash->success(__('The item has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The item could not be saved. Please, try again.'));
    //     }
    //     $products = $this->Items->Products->find('list', ['limit' => 200]);
    //     $supplies = $this->Items->Supplies->find('list', ['limit' => 200]);
    //     $orders = $this->Items->Orders->find('list', ['limit' => 200]);
    //     $this->set(compact('item', 'products', 'supplies', 'orders'));
    // }

    // /**
    //  * Edit method
    //  *
    //  * @param string|null $id Item id.
    //  * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
    //  * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //  */
    // public function edit($id = null)
    // {
    //     $item = $this->Items->get($id, [
    //         'contain' => []
    //     ]);
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $item = $this->Items->patchEntity($item, $this->request->getData());
    //         if ($this->Items->save($item)) {
    //             $this->Flash->success(__('The item has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The item could not be saved. Please, try again.'));
    //     }
    //     $products = $this->Items->Products->find('list', ['limit' => 200]);
    //     $supplies = $this->Items->Supplies->find('list', ['limit' => 200]);
    //     $orders = $this->Items->Orders->find('list', ['limit' => 200]);
    //     $this->set(compact('item', 'products', 'supplies', 'orders'));
    // }

    // /**
    //  * Delete method
    //  *
    //  * @param string|null $id Item id.
    //  * @return \Cake\Http\Response|null Redirects to index.
    //  * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //  */
    // public function delete($id = null)
    // {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $item = $this->Items->get($id);
    //     if ($this->Items->delete($item)) {
    //         $this->Flash->success(__('The item has been deleted.'));
    //     } else {
    //         $this->Flash->error(__('The item could not be deleted. Please, try again.'));
    //     }

    //     return $this->redirect(['action' => 'index']);
    // }
}
