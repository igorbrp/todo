<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Collection\Collection;
use Cake\I18n\Number;

use Cake\Core\Configure;

use Cake\ORM\Query;
use Cake\Database\Expression\QueryExpression;

use Cake\Log\Log;

/**
 * Products Controller
 *
 * @property \App\Model\Table\ProductsTable $Products
 *
 * @method \App\Model\Entity\Product[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('HandlerSku');
        $this->loadComponent('CorrecoesDeEmergencia');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $data = $this->request->getQuery();

        $products = $this->Products->find('search', ['search' => $data])
                                   ->contain([
                                       'Supplies',
                                       'Stocks',
                                   ])
                                   ->formatResults(
                                       function (\Cake\Collection\CollectionInterface $results) {
                                           return $results->map(function ($row) {
                                               // if (is_null($row['price'])) {
                                               if ($row['price_calculated']) {
                                                   $row['price'] = $this->getPrice($row['root_price'], $row['supply_id'], $row['product_line_id']);
                                                   Log::debug('entrou com price == null: ' . $row['price_calculated']);
                                               }
                                               return $row;
                                           });
                                       });

        // Log::debug('productscontroller index');
        $products->where($this->buildWhere($data));

        //        $this->set('q', isset($data['q']) ? $data['q'] : '');
        $supplies = $this->Products->Supplies->find('list', ['limit' => 200]);
        $this->set(compact('supplies'));
        //        Log::debug($supplies);
        $this->set(compact('products'), $this->paginate($products, [
            'sortWhitelist' => [
                'sku', 'title', 'Supplies.name', 'Stocks.qtd', 'price'
                ]
        ]));
    }

    public function buildWhere($data)
    {
        $where = ['1']; // importante

        if(!isset($data['select_type'])) {
            $data['select_type'] = '';
            //            return $where;
        }

        // Log::debug('================================');
        // Log::debug('buildWhere');
        // Log::debug('================================');
        // Log::debug($data['select_type']);
        
        if($data['select_type'] == 'default') {
            $where = ['is_stock' => '0'];
        } elseif ($data['select_type'] == 'stock') {
            $where = ['is_stock' => '1'];
        }

        return $where;
    }
    /**
     * View method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $product = $this->Products->get($id, [
            'contain' => [/*'ProductTypes',*/ 'Supplies', 'ProductLines', 'Files', 'Items']
            ]);

        $this->set('product', $product);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
		$userId = $this->Auth->user('id');

        $product = $this->Products->newEntity();

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            // Log::debug($data['root_price']);
            // Log::debug($data['price']);
            $data['root_price'] = $this->CorrecoesDeEmergencia->BrCurrencyToValue($data['root_price']);
            $data['price'] = $this->CorrecoesDeEmergencia->BrCurrencyToValue($data['price']);
            // Log::debug($data['root_price']);
            // Log::debug($data['price']);

            if ($data['price_calculated'] == 1)
                $data['price'] = null;

            if (!empty($data['file']['submittedfile']) && $data['file']['submittedfile']['error'] == 0) {
                $data['file']['user_id'] = $userId; // Optional
                $data['file']['model'] = $this->Products->registryAlias();
            }

            //            $product = $this->Products->patchEntity($product, $this->request->getData());

            $product = $this->Products->patchEntity($product, $data, [
                'associated' => [
                    'Files'
                    ]
            ]);

            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }

        $productTypes = $this->Products->ProductTypes->find('list', ['limit' => 200]);
        $supplies = $this->Products->Supplies->find('list', ['limit' => 200]);
        $productLines = $this->Products->ProductLines->find('list', ['limit' => 200]);
        $this->set(compact('product', 'productTypes', 'supplies', 'productLines'));

        $this->render('addedit');
    }

    /**
     * Edit method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
		$userId = $this->Auth->user('id');

        $product = $this->Products->get($id, [
            'contain' => [
                'Files'
            ]
        ]);

        if($product->is_stock) {
            $originalProduct = $this->Products->get($id);
            $this->set(compact('originalProduct'));
        }            

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            // debug($data['root_price']);
            // debug($data['price']);
            $data['root_price'] = $this->CorrecoesDeEmergencia->BrCurrencyToValue($data['root_price']);

            $data['price'] = $this->CorrecoesDeEmergencia->BrCurrencyToValue($data['price']);

            // debug($data['root_price']);
            // debug($data['price']);die();

            if ($data['price_calculated'] == 1)
                $data['price'] = null;

            if (!empty($data['file']['submittedfile']) && $data['file']['submittedfile']['error'] == 0) {
                $data['file']['user_id'] = $userId; // Optional
                $data['file']['model'] = $this->Products->registryAlias();
            }

            $product = $this->Products->patchEntity($product, $data, [
                'associated' => [
                    'Files',
                ]
            ]);

            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        }

        //        $taxes = $this->getTaxes($product->supply_id, $product->product_line_id);

        if ($product->price_calculated == true)
            $product->price = $this->getPrice($product->root_price, $product->supply_id, $product->product_line_id);

        //        $this->set(compact('taxes'));
        $product->root_price = $this->CorrecoesDeEmergencia->ValueToBrCurrency($product->root_price);
        $product->price = $this->CorrecoesDeEmergencia->ValueToBrCurrency($product->price);
        //        debug($product);die();
        $productTypes = $this->Products->ProductTypes->find('list', ['limit' => 200]);
        $supplies = $this->Products->Supplies->find('list', ['limit' => 200]);
        $productLines = $this->Products->ProductLines->find('list', ['limit' => 200]);
        $this->set(compact('product', 'productTypes', 'supplies', 'productLines'));

        $this->render('addedit');
    }

    public function buildStock($id = null)
    {
		$userId = $this->Auth->user('id');

        $product = $this->Products->newEntity();

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            // Log::debug($data['root_price']);
            // Log::debug($data['price']);
            $data['root_price'] = $this->CorrecoesDeEmergencia->BrCurrencyToValue($data['root_price']);
            $data['price'] = $this->CorrecoesDeEmergencia->BrCurrencyToValue($data['price']);
            // Log::debug($data['root_price']);
            // Log::debug($data['price']);

            if ($data['price_calculated'] == 1)
                $data['price'] = null;

            if (!empty($data['file']['submittedfile']) && $data['file']['submittedfile']['error'] == 0) {
                $data['file']['user_id'] = $userId; // Optional
                $data['file']['model'] = $this->Products->registryAlias();
            }

            //            $product = $this->Products->patchEntity($product, $this->request->getData());

            $product = $this->Products->patchEntity($product, $data, [
                'associated' => [
                    'Files'
                    ]
            ]);

            if ($this->Products->save($product)) {
                $this->Flash->success(__('The product has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The product could not be saved. Please, try again.'));
        } else {
            $originalProduct = $this->Products->get($id, [
                'contain' => [
                    'Files'
                ]
            ]);

            $product = $this->Products->patchEntity($product, $originalProduct->toArray(), [
                'associated' => [
                    'Files',
                ]
            ]);

            $sku_list = $this->HandlerSku->buildSkuList($originalProduct->title);
            foreach($sku_list as $sl) {
                $s = $this->Products->find()->where([
                    'sku' => $sl,
                ])->first();
                if(empty($s)) {
                    $sku = $sl;
                    break;
                }
            }
            // if(empty($sku)) {
            //     $data['error'] = "Nao foi possivel gerar automaticamente o SKU.";
            //     $data['code'] = '';
            // } else {
            //     $data['code'] = $sku;
            // }
            $product->derived_from = $originalProduct->id;
            $product->is_stock = true;
            $product->sku = $sku;
            $product->id = '';
            $product->isNew();
            
            if ($product->price_calculated == true)
                $product->price = $this->getPrice($product->root_price, $product->supply_id, $product->product_line_id);
            
            $productTypes = $this->Products->ProductTypes->find('list', ['limit' => 200]);
            $supplies = $this->Products->Supplies->find('list', ['limit' => 200]);
            $productLines = $this->Products->ProductLines->find('list', ['limit' => 200]);
            $this->set(compact('product', 'productTypes', 'supplies', 'productLines', 'originalProduct'));
            
            debug($originalProduct);
            $this->render('build_stock');
        }
    }
        



    /**
     * Delete method
     *
     * @param string|null $id Product id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $product = $this->Products->get($id);
        if ($this->Products->delete($product)) {
            $this->Flash->success(__('The product has been deleted.'));
        } else {
            $this->Flash->error(__('The product could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deleteSomeRecords($result = null)
    {
        if ($this->request->is('post')) {
            $ids = explode(",", $this->request->getData('ids'));

            $query = $this->Products->query();
            $query->delete();

            foreach($ids as $id)
                $query->orWhere(['id' => $id]);

            if($query->execute()) {
                $this->Flash->success(__('The products has been deleted.'));
            } else {
                $this->Flash->error(__('The products could not be deleted. Please, try again.'));
            }
        } else {
            $data = '';
        }

        return $this->redirect(['action' => 'index']);
    }

    public function generateSku()
    {
        $this->request->allowMethod('ajax');
        $title = $this->request->query('title');
        $sku_list = $this->HandlerSku->buildSkuList($title);

        foreach($sku_list as $sl) {
            $s = $this->Products->find()->where([
                'sku' => $sl,
            ])->first();
            if(empty($s)) {
                $sku = $sl;
                break;
            }
        }
        if(empty($sku)) {
            $data['error'] = "Nao foi possivel gerar automaticamente o SKU.";
            $data['code'] = '';
        } else {
            $data['code'] = $sku;
        }

        /*-------------------------------------------------------------
         * Depois de longo e tenebroso inverno, esse trecho funcionou.
         * Depende do layout ajax.ctp e do render json
         */
        $this->set('response', json_encode($data));
        //        $this->set(compact('response')); // Pass $data to the view
        //        $this->set('_serialize', 'response');
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/
    }

    public function getTaxes($supply_id, $product_line_id = null)
    {
        $taxes = [];

        $globalModel = $this->loadModel('Price.Taxes');
        $globalTaxes = $globalModel
                     ->find()
                     ->contain(['Prices' => ['fields' => ['id', 'name', 'sequence', 'active']]])
                     ->order(['Prices.sequence'])
                     ->where(['Prices.active' => true])
                     ->all();

        $supplyModel = $this->loadModel('Supply.SupplyTaxes');
        $supplyTaxes = $supplyModel
                      ->find()
                     // ->contain(['Prices' => ['fields' => ['id', 'name', 'sequence', 'active']]])
                     // ->order(['Prices.sequence'])
                      ->where(['supply_id' => $supply_id])
                      ->all();

        foreach ($globalTaxes as $tax) {
            $tax['inherited'] = 1;
            $tax['inherited_value'] = $tax['value'];
            $tax['cost_name'] = $tax['price']['name'];
            $taxes[] = $tax;
        }

        $names = array_column($taxes, 'name');

        foreach ($supplyTaxes as $supplyTax) {
            $key = array_search(trim($supplyTax['name']), $names);
            if ($key !== false) {
                // $taxes[$key]['model'] = $supplyTax['model'];
                $taxes[$key]['name'] = $supplyTax['name'];
                $taxes[$key]['value'] = $supplyTax['value'];
                $taxes[$key]['calculation'] = $supplyTax['calculation'];
            }
        }

        if (!is_null($product_line_id)) {
            $productLineTaxModel = $this->loadModel('ProductLine.ProductLineTaxes');
            $productLineTaxTaxes = $productLineTaxModel
                                 ->find()
                                 ->where(['product_line_id' => $product_line_id])
                                 ->all();

            foreach ($productLineTaxTaxes as $productLineTax) {
                $key = array_search(trim($productLineTax['name']), $names);
                if ($key !== false) {
                    $taxes[$key]['name'] = $productLineTax['name'];
                    $taxes[$key]['value'] = $productLineTax['value'];
                    $taxes[$key]['calculation'] = $productLineTax['calculation'];
                }
            }
        }

        return $taxes;
    }

    public function getPrice($root_price, $supply_id, $product_line_id = null) {
        $taxes = $this->getTaxes($supply_id, $product_line_id);
        $finalPrice = $price = $root_price;
        $oldPriceId = '';
        for($i = 0; $i < count($taxes); $i++) {
            if ($taxes[$i]['price_id'] != $oldPriceId) {
                $oldPriceId = $taxes[$i]['price_id'];
                $price = $finalPrice;
            }
            if ($taxes[$i]['calculation'] == '%') {
                $finalPrice += $price * ($taxes[$i]['value'] / 100);
            } else {
                $finalPrice += $taxes[$i]['value'];
            }
        }
        return $finalPrice;
    }

    public function getAjaxPrice()
    {
        $this->request->allowMethod('ajax');
        $rootPrice = $this->request->query('root_price');
        $supplyId = $this->request->query('supply_id');
        $productLineId = $this->request->query('product_line_id');
        $data['price'] = $this->getPrice($rootPrice, $supplyId, $productLineId);
        /*-------------------------------------------------------------
         * Depois de longo e tenebroso inverno, esse trecho funcionou.
         * Depende do layout ajax.ctp e do render json
         */
        $this->set('response', json_encode($data));
        //        $this->set(compact('response')); // Pass $data to the view
        //        $this->set('_serialize', 'response');
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/
    }

    public function find()
    {
        $this->request->allowMethod('ajax');
        $products = $this->Products
                  ->find('search', ['search' => $this->request->query()])
                  ->contain(['Supplies', 'ProductLines', 'Files', 'Stocks'])
                  ->formatResults(
                      function (\Cake\Collection\CollectionInterface $results) {
                          return $results->map(function ($row) {
                              if (!is_null($row['file'])) {
                                  $row['image'] = DS . $row['file']['path'] . DS . $row['file']['hash'] . '.media.' . $row['file']['extension'];
                              } else {
                                  $row['image'] = null;
                              }
                              // if (is_null($row['price'])) {
                              if ($row['price_calculated']) {
                                  $row['price'] = $this->getPrice($row['root_price'], $row['supply_id'], $row['product_line_id']);
                              }

                              if (is_null($row['maximum_discount'])) {
                                  if (is_null($row['product_line']) || is_null($row['product_line']['maximum_discount'])) {
                                      if (is_null($row['supply']['maximum_discount'])) {
                                          $row['maximum_discount'] = 20; // mudar para global
                                      } else {
                                          $row['maximum_discount'] = $row['supply']['maximum_discount']; // recebe supply
                                      }
                                  } else {
                                      $row['maximum_discount'] = $row['product_line']['maximum_discount'];
                                  }
                              }

                              if (is_null($row['deadline'])) {
                                  if (is_null($row['product_line']) || is_null($row['product_line']['deadline'])) {
                                      if (is_null($row['supply']['deadline'])) {
                                          $row['deadline'] =  Configure::read('system_configurations.configuration.deadline'); // deadline global
                                      } else {
                                          $row['deadline'] = $row['supply']['deadline']; // recebe supply
                                      }
                                  } else {
                                      $row['deadline'] = $row['product_line']['deadline'];
                                  }
                              }
                              $row['minimum_unitary_price'] = $row['price'] - ($row['price'] * ($row['maximum_discount'] / 100));
                              
                              if(is_null($row['stock'])) { // não é produto de estoque
                                  $row['stock_qtd'] = null;
                                  $row['show_line'] = '1';
                              } else {
                                  if($row['stock']['qtd'] > 0) {
                                      $row['show_line'] = '1';
                                      $row['stock_qtd'] = $row['stock']['qtd'];
                                  } else {
                                      $row['stock_qtd'] = null;
                                      $row['show_line'] = '0';
                                  }
                              }
                              return $row;
                          });
                      });
                  //                  ->where(['is_stock' => 0])
        $products->where(function (QueryExpression $exp, Query $products) {
    //         $author = $query->newExpr()->or(['author_id' => 3])->add(['author_id' => 2]);
    // $published = $query->newExpr()->and(['published' => true, 'view_count' => 10]);

            // $default = $products->newExpr()->eq('is_stock', 0);
            // $stock = $products->newExpr()->and(['is_stock' => 1], ['Stocks.qtd > ' => 0]);
            // return $exp->or([
            //     $default,
            //     $stock
            // ]);
            // $default = $products->newExpr()->eq('is_stock', 0);
            // $stock = $products->newExpr()->and(['is_stock' => 1], ['Stocks.qtd > ' => 0]);
            return $exp->or([
                'is_stock' => 0,
                $products->newExpr()->and([['is_stock' => 1], ['Stocks.qtd > ' => 0]])
            ]);

        })->limit(20);

        /*-------------------------------------------------------------
         * Depois de longo e tenebroso inverno, esse trecho funcionou.
         * Depende do layout ajax.ctp e do render json
         */
        $this->set('response', json_encode($products));
        //        $this->set(compact('response')); // Pass $data to the view
        //        $this->set('_serialize', 'response');
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*--------------------------------------------------------*/

    }

    public function selectProductsBySupplyHtml()
    {
        $this->request->allowMethod('ajax');
		$supply_id = $this->request->query('supply_id');
        $result = $this->Products->find('list')
                ->where(['supply_id' => $supply_id])->all();

        $data['results'] = $result;

        /*-------------------------------------------------------------
         * Depois de longo e tenebroso inverno, esse trecho funcionou.
         * Depende do layout ajax.ctp e do render json
         */
        //        $this->set('response', json_encode($data));

        // mudei para select2 para receber como array
        $this->set('response', $data);
        // comentei a linha abaixo pq passou a dar erro em dezembro de 2018
        // nao sei se eh pelo php7.3 ou outra coisa qualquer
        //         $this->set(compact('response')); // Pass $data to the view
        $this->set('_serialize', 'response');
        $this->viewBuilder()->setLayout('ajax');
        // $this->render('/Element/json');
        $this->render('/Element/select');
        /*--------------------------------------------------------*/

    }

    public function selectStockProductsBySupplyHtml()
    {
        $this->request->allowMethod('ajax');
		$supply_id = $this->request->query('supply_id');
        $result = $this->Products
                ->find('list')
                ->where(['supply_id' => $supply_id])
                ->andWhere(['is_stock' => '1'])
                ->all();

        $data['results'] = $result;

        /*-------------------------------------------------------------
         * Depois de longo e tenebroso inverno, esse trecho funcionou.
         * Depende do layout ajax.ctp e do render json
         */
        //        $this->set('response', json_encode($data));

        // mudei para select2 para receber como array
        $this->set('response', $data);
        // comentei a linha abaixo pq passou a dar erro em dezembro de 2018
        // nao sei se eh pelo php7.3 ou outra coisa qualquer
        //         $this->set(compact('response')); // Pass $data to the view
        $this->set('_serialize', 'response');
        $this->viewBuilder()->setLayout('ajax');
        // $this->render('/Element/json');
        $this->render('/Element/select');
        /*--------------------------------------------------------*/

    }
}
