<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Collection\Collection;

use Cake\I18n\Date;
use Cake\I18n\Time;

use Cake\Error\Debugger;

use Cake\Log\Log;

/**
 * Purchases Controller
 *
 * @property \App\Model\Table\PurchasesTable $Purchases
 *
 * @method \App\Model\Entity\Purchase[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PurchasesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('CorrecoesDeEmergencia');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $data = $this->request->getQuery();

        $purchases = $this->Purchases->find('search', ['search' => $data]);

        $purchases->contain([
            'Products',
            'Products.Supplies',
        ]);
    
        $this->set('purchases', $this->paginate($purchases));
        $this->set('_serialize', ['purchases']);
    }

    /**
     * View method
     *
     * @param string|null $id Purchase id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // $purchase = $this->Purchases->get($id, [
        //     'contain' => ['Supplies', 'Products', 'Receiveds', 'Requisitions'],
        // ]);

        // $this->set('purchase', $purchase);

        $purchase = $this->Purchases->get($id, [
            'contain' => [
                'Products',
                'Products.Supplies',
                'Receiveds',
                'Requisitions'
            ]
        ]);

        // totalização dos itens recebidos na compra
        $query = $this->Purchases->Receiveds->find();
        $receivedsSum = $query->select([
            'recSum' => $query->func()->sum('Receiveds.qtd')
        ])->where([
            'Receiveds.purchase_id' => $id,
            //            'Receiveds.canceled' => 0,
        ])->first();


        $this->set('purchase', $purchase);
        $this->set(compact('receivedsSum'));
        $this->set('_serialize', ['purchase']);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $purchase = $this->Purchases->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $supply = $this->Purchases->Products->Supplies->get($data['supply_id']);
            $product = $this->Purchases->Products->get($data['product_id']);
            $data['total_price'] = $this->CorrecoesDeEmergencia->BrCurrencyToValue($data['total_price']);
            $message = 'Olá, ' . $supply['contact'] . '\r\n\r\n';
            $message .= 'Estamos requisitando o(s) produto(s) descrito(s) abaixo:\r\n';
            $message .= 'estoque\r\n';
            $message .= 'quantidade: ' . $data['qtd'] . '\r\n';
            $message .= 'produto: ' . $product['title'] . '\r\n';

            $tmpRequisition = [
                'required' => Time::now(),
                'outbox_email' => [
                    'model' => 'Requisitions',
                    'subject' => 'Requisição de material a ' . $supply['name'],
                    'recipient' => $supply['email'],
                    'message' => $message,
                    'sented' => 0,
                    'fails' => 0,
                    'last_message' => '',
                ]
            ];

            $data['requisitions'][] = $tmpRequisition;

            $purchase = $this->Purchases->patchEntity($purchase, $data, [
                'associated' => [
                    'Requisitions',
                    'Requisitions.OutboxEmails',
                ],
            ]);

            if ($this->Purchases->save($purchase)) {
                $this->Flash->success(__('A compra para o estoque foi salva.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('A compra para o estoque não pode ser salva. Por favor, verifique os erros e tente novamente.'));
            }
                    
        }
        $supplies = $this->Purchases->Supplies
                  ->find('list', ['limit' => 200])
                  ->where(['Products.is_stock' => '1'])
                  ->innerJoinWith('Products');
        
        $products = $this->Purchases->Products
                  ->find('list', ['limit' => 200])
                  ->where(['is_stock' => '1']);
        
        $this->set(compact('purchase', 'products', 'supplies'));
        $this->set('_serialize', ['purchase']);

        $this->render('addedit');
    }

    /**
     * Edit method
     *
     * @param string|null $id Purchase id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // $purchase = $this->Purchases->get($id, [
        //     'contain' => [],
        // ]);
        // if ($this->request->is(['patch', 'post', 'put'])) {
        //     $purchase = $this->Purchases->patchEntity($purchase, $this->request->getData());
        //     if ($this->Purchases->save($purchase)) {
        //         $this->Flash->success(__('The purchase has been saved.'));

        //         return $this->redirect(['action' => 'index']);
        //     }
        //     $this->Flash->error(__('The purchase could not be saved. Please, try again.'));
        // }
        // $supplies = $this->Purchases->Supplies->find('list', ['limit' => 200]);
        // $products = $this->Purchases->Products->find('list', ['limit' => 200]);
        // $this->set(compact('purchase', 'supplies', 'products'));
        $purchase = $this->Purchases->get($id, [
            'contain' => ['Receiveds'],
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {

            $data = $this->request->getData();
            $data['total_price'] = $this->CorrecoesDeEmergencia->BrCurrencyToValue($data['total_price']);


            $purchase = $this->Purchases->patchEntity($purchase, $data);
            if ($this->Purchases->save($purchase)) {
                $this->Flash->success(__('A compra do estoque foi salva.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('A compra do estoque não pode ser salva. Por favor, verifique os erros e tente novamente.'));
        }

        $supplies = $this->Purchases->Supplies->find('list', ['limit' => 200]);

        if (!empty($purchase['supply_id'])) {
            $products = $this->Purchases->Products
                      ->find('list', ['limit' => 200])
                      ->where(['supply_id' => $purchase['supply_id']]);
        } else {
            $products = [];
        }

        $receiveds = $this->Purchases->Receiveds->find();// ->select([
        //     '*',
        // ])->where([
        //     'Receiveds.canceled' => 0,
        // ]);

        // totalização dos itens recebidos na compra
        $query = $this->Purchases->Receiveds->find();
        $receivedsSum = $query->select([
            'recSum' => $query->func()->sum('Receiveds.qtd')
        ])->where([
            'Receiveds.purchase_id' => $id,
            //            'Receiveds.canceled' => 0,
        ])->first();

        //        debug($purchase->supply_id);die();
        $supply_name = $this->Purchases->Supplies->get($purchase->supply_id)->name;
        $product_title = $this->Purchases->Products->get($purchase->product_id)->title;

        $this->set(compact('purchase', 'products', 'product_title', 'supplies', 'supply_name', 'receiveds', 'product_title'));
        $this->set(compact('receivedsSum'));
        //        $this->set('_serialize', ['total']);
        $this->set('_serialize', ['purchase']);

        $this->render('addedit');
    }

    /**
     * Delete method
     *
     * @param string|null $id Purchase id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $purchase = $this->Purchases->get($id);
        if ($this->Purchases->delete($purchase)) {
            $this->Flash->success(__('A compra para o estoque foi deletada.'));
        } else {
            $this->Flash->error(__('A compra para o estoque não pode ser deletada. Por favor, verifique os erros e tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);

        // $this->request->allowMethod(['post', 'delete']);
        // $purchase = $this->Purchases->get($id);
        // if ($this->Purchases->delete($purchase)) {
        //     $this->Flash->success(__('The purchase has been deleted.'));
        // } else {
        //     $this->Flash->error(__('The purchase could not be deleted. Please, try again.'));
        // }

        // return $this->redirect(['action' => 'index']);
        // if ($this->request->is(['patch', 'post', 'put'])) {
        //     //            $id = $this->request->getData('id');
        //     $purchase = $this->Purchases->get($id);
        //     if ($this->Purchases->delete($purchase))
        //         $response = true;
        //     else
        //         $response = false;
        //     $this->set(compact('response')); // Pass $data to the view
        //     $this->set('_serialize', 'response');
        //     $this->viewBuilder()->setLayout('ajax');
        //     $this->render('/Element/json');
        // } else {
        //     if ($result)
        //         $this->Flash->success(__('The purchase has been deleted.'));
        //     else
        //         $this->Flash->error(__('The purchase could not be deleted. Please, try again.'));
        //     return $this->redirect(['action' => 'index']);
        // }
    }
}
