<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Log\Log;

/**
 * Customers Controller
 *
 * @property \App\Model\Table\CustomersTable $Customers
 *
 * @method \App\Model\Entity\Customer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CustomersController extends AppController
{

    public function findcpfcnpj($cpfCnpj)
    {
        Log::debug('CustomersController: ' . $cpfCnpj);

        $query = $this->Customers
                  ->find('all')
        ->contain([
            'Addresses',
        ])
                  ->where(['cpf_cnpj' => $cpfCnpj]);
        $customer = $query->first();
        Log::debug('fez oquery');
        $data = $customer;
        Log::debug('checou cpf $data: ');
        Log::debug($data);

        /*-------------------------------------------------------------
         * aqui pra pegar o cep nao usa nem compact nem serialize
         */
        $this->set('response', json_encode($data));
        // $this->set('response', $data);
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*-----------------------------------------------------------*/

    }
}
