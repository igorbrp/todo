<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Network\Http\Client;

use Cake\Log\Log;

/**
 * Addresses Controller
 *
 * @property \App\Model\Table\AddressesTable $Addresses
 *
 * @method \App\Model\Entity\Address[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AddressesController extends AppController
{
    public function findCep($cep)
    {
        $this->request->allowMethod('ajax');
        $http = new Client();

        // limpa mascara de formatacao
        $cep = preg_replace("/[^0-9]/", "", $cep);

        $url = "https://viacep.com.br/ws/$cep/json/";
        $response = $http->get($url);
        $data = $response->json;
        $query = $this->Addresses->States->find('all')
                                         ->where(['States.id' => $data['uf']]);
        $data['coduf'] = $query->first()->id;

        /*-------------------------------------------------------------
         * aqui pra pegar o cep nao usa nem compact nem serialize
         */
        $this->set('response', json_encode($data));
        $this->viewBuilder()->setLayout('ajax');
        $this->render('/Element/json');
        /*-----------------------------------------------------------*/
    }

    public function getCitiesByState($id)
    {
        $this->request->allowMethod('ajax');
        $state_id = $this->request->getData('state_id');
        $result = $this->Addresses->Cities->find('list')
                ->where(['state_id' => $id])->all();

        $data['results'] = $result;

        //        }
        /*-------------------------------------------------------------
         * Depois de longo e tenebroso inverno, esse trecho funcionou.
         * Depende do layout ajax.ctp e do render json
         */
        //        $this->set('response', json_encode($data));

        // mudei para select2 para receber como array
        $this->set('response', $data);
        // comentei a linha abaixo pq passou a dar erro em dezembro de 2018
        // nao sei se eh pelo php7.3 ou outra coisa qualquer
        //         $this->set(compact('response')); // Pass $data to the view
        $this->set('_serialize', 'response');
        $this->viewBuilder()->setLayout('ajax');
        // $this->render('/Element/json');
        $this->render('/Element/select');
        /*--------------------------------------------------------*/
    }

    // /**
    //  * Index method
    //  *
    //  * @return \Cake\Http\Response|null
    //  */
    // public function index()
    // {
    //     $this->paginate = [
    //         'contain' => ['Countries', 'States', 'Cities']
    //     ];
    //     $addresses = $this->paginate($this->Addresses);

    //     $this->set(compact('addresses'));
    // }

    // /**
    //  * View method
    //  *
    //  * @param string|null $id Address id.
    //  * @return \Cake\Http\Response|null
    //  * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //  */
    // public function view($id = null)
    // {
    //     $address = $this->Addresses->get($id, [
    //         'contain' => ['Countries', 'States', 'Cities']
    //     ]);

    //     $this->set('address', $address);
    // }

    // /**
    //  * Add method
    //  *
    //  * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
    //  */
    // public function add()
    // {
    //     $address = $this->Addresses->newEntity();
    //     if ($this->request->is('post')) {
    //         $address = $this->Addresses->patchEntity($address, $this->request->getData());
    //         if ($this->Addresses->save($address)) {
    //             $this->Flash->success(__('The address has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The address could not be saved. Please, try again.'));
    //     }
    //     $countries = $this->Addresses->Countries->find('list', ['limit' => 200]);
    //     $states = $this->Addresses->States->find('list', ['limit' => 200]);
    //     $cities = $this->Addresses->Cities->find('list', ['limit' => 200]);
    //     $this->set(compact('address', 'countries', 'states', 'cities'));
    // }

    // /**
    //  * Edit method
    //  *
    //  * @param string|null $id Address id.
    //  * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
    //  * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //  */
    // public function edit($id = null)
    // {
    //     $address = $this->Addresses->get($id, [
    //         'contain' => []
    //     ]);
    //     if ($this->request->is(['patch', 'post', 'put'])) {
    //         $address = $this->Addresses->patchEntity($address, $this->request->getData());
    //         if ($this->Addresses->save($address)) {
    //             $this->Flash->success(__('The address has been saved.'));

    //             return $this->redirect(['action' => 'index']);
    //         }
    //         $this->Flash->error(__('The address could not be saved. Please, try again.'));
    //     }
    //     $countries = $this->Addresses->Countries->find('list', ['limit' => 200]);
    //     $states = $this->Addresses->States->find('list', ['limit' => 200]);
    //     $cities = $this->Addresses->Cities->find('list', ['limit' => 200]);
    //     $this->set(compact('address', 'countries', 'states', 'cities'));
    // }

    // /**
    //  * Delete method
    //  *
    //  * @param string|null $id Address id.
    //  * @return \Cake\Http\Response|null Redirects to index.
    //  * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    //  */
    // public function delete($id = null)
    // {
    //     $this->request->allowMethod(['post', 'delete']);
    //     $address = $this->Addresses->get($id);
    //     if ($this->Addresses->delete($address)) {
    //         $this->Flash->success(__('The address has been deleted.'));
    //     } else {
    //         $this->Flash->error(__('The address could not be deleted. Please, try again.'));
    //     }

    //     return $this->redirect(['action' => 'index']);
    // }
}
