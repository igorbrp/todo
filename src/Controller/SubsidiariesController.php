<?php
namespace App\Controller;

use App\Controller\AppController;

use Cake\Utility\Inflector;

// teste
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
// fim teste

use Cake\Event\Event;
use Cake\Log\Log;

/**
 * Subsidiaries Controller
 *
 * @property \App\Model\Table\SubsidiariesTable $Subsidiaries
 *
 * @method \App\Model\Entity\Subsidiary[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SubsidiariesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        // $subsidiaries = $this->paginate($this->Subsidiaries);

        // $this->set(compact('subsidiaries'));
        $query = $this->Subsidiaries
               ->find('search', ['search' => $this->request->getQuery()]);

        $this->set('subsidiaries', $this->paginate($query));
        $this->set('q', isset($data['q']) ? $data['q'] : '');
        $this->set('_serialize', ['subsidiaries']);
    }

    /**
     * View method
     *
     * @param string|null $id Subsidiary id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $subsidiary = $this->Subsidiaries->get($id, [
            'contain' => ['Addresses'// , 'Orders'
            ]
        ]);

        $this->set('subsidiary', $subsidiary);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
		$userId = $this->Auth->user('id');

        $subsidiary = $this->Subsidiaries->newEntity();

        if ($this->request->is('post')) {
            $data = $this->request->getData();

            if (!empty(implode("", array_values($data['address'])))) {
                $data['address']['model'] = $this->Subsidiaries->registryAlias();
            } else {
                $data['address'] = null;
            }

            $subsidiary = $this->Subsidiaries->patchEntity($subsidiary, $data, [
                'associated' => [
                    'Addresses',
                ]
            ]);

            if ($this->Subsidiaries->save($subsidiary)) {
                $this->Flash->success(__('A filial foi salva com sucesso.')); // TRADUZIR
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('A filial não pode ser salva. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $subsidiary->errors(),
            ]]);
        }

        $modelStates = $this->loadModel('States');
        $states = $modelStates->find('list')->toArray();
        $this->set(compact('states'));

        $modelCities = $this->loadModel('Cities');
        if (!empty($subsidiary['address']['state_id'])) {
            $cities = $modelCities->find('list')->toArray();
        } else {
            $cities = [];
        }
        $this->set(compact('cities'));
        $this->set(compact('subsidiary'));

        $this->render('addedit');
    }

    /**
     * Edit method
     *
     * @param string|null $id Subsidiary id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
		$userId = $this->Auth->user('id');

        $subsidiary = $this->Subsidiaries->get($id, [
            'contain' => [
                'Addresses'
            ]
        ]);

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();

            if (!empty(implode("", array_values($data['address'])))) {
                $data['address']['model'] = $this->Subsidiaries->registryAlias();
            } else {
                $data['address'] = null;
            }

            $subsidiary = $this->Subsidiaries->patchEntity($subsidiary, $data, [
                'associated' => [
                    'Addresses'
                ]
            ]);

            if ($this->Subsidiaries->save($subsidiary)) {
                $this->Flash->success(__('A filial foi salva com sucesso.')); // TRADUZIR
                return $this->redirect(['action' => 'index']);
            }

            $this->Flash->error(__('A filial não pode ser salva. Por favor, verifique os erros e tente novamente.'), [ // TRADUZIR
                'clear' => true,
                'params' => [
                    'messages' => $subsidiary->errors(),
            ]]);
        }

        $modelStates = $this->loadModel('States');
        $states = $modelStates->find('list')->toArray();
        $this->set(compact('states'));

        $modelCities = $this->loadModel('Cities');
        if (!empty($subsidiary['address']['state_id'])) {
            $cities = $modelCities->find('list')->toArray();
        } else {
            $cities = [];
        }
        $this->set(compact('cities'));
        $this->set(compact('subsidiary'));

        $this->render('addedit');
    }

    /**
     * Delete method
     *
     * @param string|null $id Subsidiary id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $subsidiary = $this->Subsidiaries->get($id);
        if ($this->Subsidiaries->delete($subsidiary)) {
            $this->Flash->success(__('A filial foi excluída.'));
        } else {
            $this->Flash->error(__('A filial não pode ser excluída. Por favor, verifique os erros e tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function deleteSomeRecords($result = null)
    {
        if ($this->request->is('post')) {
            $ids = explode(",", $this->request->getData('ids'));

            $query = $this->Subsidiaries->query();
            $query->delete();

            foreach($ids as $id)
                $query->orWhere(['id' => $id]);

            if($query->execute()) {
                $this->Flash->success(__('As filiais foram excluídas.'));
            } else {
                $this->Flash->error(__('As filiais não puderam ser excluídas. Por favor, verifique os erros e tente novamente.'));
            }
        } else {
            $data = '';
        }

        return $this->redirect(['action' => 'index']);
    }

    public function changeActivity()
    {
        $this->request->allowMethod(['post']);
        $id = $this->request->getData('change-id');
        $subsidiary = $this->Subsidiaries->get($id);
        $subsidiary->active = ($subsidiary->active == 1 ? 0 : 1);
        if ($this->Subsidiaries->save($subsidiary)) {
            $this->Flash->success(__('O status da filial foi alterado.'));
        } else {
            $this->Flash->error(__('O status da filial não pôde ser alterado. Por favor, verifique os erros e tente novamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
