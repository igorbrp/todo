<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Employee Entity
 *
 * @property int $id
 * @property string $name
 * @property string $nick_name
 * @property string|null $cpf
 * @property \Cake\I18n\FrozenDate|null $birth_date
 * @property string|null $phone
 * @property string|null $mobile
 * @property string|null $fax
 * @property string|null $user_id
 * @property bool $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \SystemUser\Model\Entity\SystemUser $user
 * @property \App\Model\Entity\Order[] $orders
 */
class Employee extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'nick_name' => true,
        'cpf' => true,
        'birth_date' => true,
        'phone' => true,
        'mobile' => true,
        'fax' => true,
        'user_id' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'user' => true,
        'address' => true,
        'orders' => true
    ];
}
