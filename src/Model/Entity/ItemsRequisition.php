<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ItemsRequisition Entity
 *
 * @property int $id
 * @property int $item_id
 * @property int $requisition_id
 *
 * @property \App\Model\Entity\Item $item
 * @property \App\Model\Entity\Requisition $requisition
 */
class ItemsRequisition extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'item_id' => true,
        'requisition_id' => true,
        'item' => true,
        'requisition' => true,
    ];
}
