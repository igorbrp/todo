<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * StockAdjustment Entity
 *
 * @property int $id
 * @property int $qtd
 * @property int $supply_id
 * @property int $product_id
 * @property string $user_id
 * @property string $comment
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Supply $supply
 * @property \App\Model\Entity\Product $product
 * @property \SystemUser\Model\Entity\SystemUser $user
 */
class StockAdjustment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'qtd' => true,
        'supply_id' => true,
        'product_id' => true,
        'user_id' => true,
        'comment' => true,
        'created' => true,
        'modified' => true,
        'supply' => true,
        'product' => true,
        'user' => true,
    ];
}
