<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Financier Entity
 *
 * @property int $id
 * @property string|null $code
 * @property string $name
 * @property int $payment_method_id
 * @property string|null $url
 * @property bool $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\PaymentMethod[] $payment_methods
 */
class Financier extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'code' => true,
        'name' => true,
        'payment_method_id' => true,
        'url' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'payment_methods' => true,
    ];
}
