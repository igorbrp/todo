<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ItemHistory Entity
 *
 * @property int $id
 * @property int $item_id
 * @property int $qtd
 * @property string $status
 * @property int $deadline
 * @property int|null $delivery_made_id
 * @property string $user_id
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Item $item
 * @property \App\Model\Entity\DeliveryMade $delivery_made
 * @property \SystemUser\Model\Entity\SystemUser $user
 * @property \App\Model\Entity\CopyRequisition[] $copy_requisitions
 */
class ItemHistory extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'item_id' => true,
        'qtd' => true,
        'status' => true,
        'deadline' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'item' => true,
        'delivery_made' => true,
        'user' => true,
        'invoice_id' => true,
    ];
}
