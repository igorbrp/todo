<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Item Entity
 *
 * @property int $id
 * @property int $qtd
 * @property string $status
 * @property int|null $total_uncovered
 * @property int|null $total_required
 * @property int|null $total_received
 * @property int|null $total_dispatched
 * @property int|null $total_completed
 * @property int|null $product_id
 * @property string $sku
 * @property int|null $supply_id
 * @property string $supply_code
 * @property string $supply_name
 * @property string $title
 * @property string|null $description
 * @property string|null $complement
 * @property int|null $deadline
 * @property float|null $price
 * @property float|null $original_price
 * @property float $minimum_unitary_price
 * @property float|null $root_price
 * @property int|null $order_id
 * @property string $origin
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Supply $supply
 * @property \App\Model\Entity\Order $order
 */
class Item extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'qtd' => true,
        'status' => true,
        'total_uncovered' => true,
        'total_required' => true,
        'total_received' => true,
        'total_dispatched' => true,
        'total_completed' => true,
        'product_id' => true,
        'sku' => true,
        'supply_id' => true,
        'supply_code' => true,
        'supply_name' => true,
        'title' => true,
        'description' => true,
        'complement' => true,
        'deadline' => true,
        'price' => true,
        'unit_original_price' => true,
        'minimum_unitary_price' => true,
        'root_price' => true,
        'order_id' => true,
        'origin' => true,
        'created' => true,
        'modified' => true,
        'product' => true,
        'supply' => true,
        'item_histories' => true,
        'order' => true,
        'output' => true,
        'requisitions' => true,
    ];
}
