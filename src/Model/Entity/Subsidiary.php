<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Subsidiary Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $cnpj
 * @property string|null $phone
 * @property string|null $mobile
 * @property string|null $phone2
 * @property string|null $mobile2
 * @property string|null $email
 * @property bool $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Order[] $orders
 */
class Subsidiary extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'cnpj' => true,
        'phone' => true,
        'mobile' => true,
        'phone2' => true,
        'mobile2' => true,
        'email' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'orders' => true,
        'address' => true
    ];
}
