<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Order Entity
 *
 * @property int $id
 * @property string $number
 * @property \Cake\I18n\FrozenDate|null $date_order
 * @property int $customer_id
 * @property int $subsidiary_id
 * @property int $employee_id
 * @property string $user_id
 * @property float $discount
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Customer $customer
 * @property \Order\Model\Entity\Subsidiary $subsidiary
 * @property \Order\Model\Entity\Employee $employee
 * @property \Order\Model\Entity\User $user
 * @property \Order\Model\Entity\Comment[] $comments
 * @property \Order\Model\Entity\Delivery[] $deliveries
 * @property \Order\Model\Entity\Item[] $items
 * @property \Order\Model\Entity\Payment[] $payments
 */
class Order extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'number' => true,
        'date_order' => true,
        'customer_id' => true,
        'subsidiary_id' => true,
        'employee_id' => true,
        'user_id' => true,
        'discount' => true,
        'created' => true,
        'modified' => true,
        'customer' => true,
        'subsidiary' => true,
        'employee' => true,
        'user' => true,
        'comments' => true,
        'delivery' => true,
        'items' => true,
        'unnecessary_customer_address' => true,
        'delivery_less' => true,
        'payments' => true
    ];
}
