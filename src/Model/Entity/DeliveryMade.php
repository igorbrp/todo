<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * DeliveryMade Entity
 *
 * @property int $id
 * @property int $delivery_id
 * @property int $vehicle_id
 * @property \Cake\I18n\FrozenDate $delivery_date
 * @property string $delivery_horary
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenDate $modified
 *
 * @property \App\Model\Entity\Delivery $delivery
 * @property \App\Model\Entity\Vehicle $vehicle
 * @property \App\Model\Entity\ItemHistory[] $item_histories
 */
class DeliveryMade extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'delivery_id' => true,
        'vehicle_id' => true,
        'item_history_id' => true,
        'scheduled_date' => true,
        'scheduled_horary' => true,
        'delivery_date' => true,
        'delivery_horary' => true,
        'created' => true,
        'modified' => true,
        'delivery' => true,
        'vehicle' => true,
    ];
}
