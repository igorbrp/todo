<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * OutboxEmail Entity
 *
 * @property int $id
 * @property bool $sented
 * @property int $fails
 * @property string|null $last_message
 * @property int $requisition_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Requisition $requisition
 */
class OutboxEmail extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'foreign_key' => true,
        'model' => true,
        'sented' => true,
        'fails' => true,
        'last_message' => true,
        'subject' => true,
        'recipient' => true,
        'message' => true,
        'created' => true,
        'modified' => true,
    ];
}
