<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Output Entity
 *
 * @property int $id
 * @property int $product_id
 * @property int|null $item_id
 * @property int $qtd
 * @property bool $canceled
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Item $item
 */
class Output extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'product_id' => true,
        'item_id' => true,
        'qtd' => true,
        'canceled' => true,
        'created' => true,
        'modified' => true,
        'product' => true,
        'item' => true,
    ];
}
