<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Purchase Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate $date_purchase
 * @property int $qtd
 * @property int $supply_id
 * @property int $product_id
 * @property \Cake\I18n\FrozenDate $forecast_date
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Supply $supply
 * @property \App\Model\Entity\Product $product
 * @property \App\Model\Entity\Received[] $receiveds
 * @property \App\Model\Entity\Requisition[] $requisitions
 */
class Purchase extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'date_purchase' => true,
        'qtd' => true,
        'supply_id' => true,
        'product_id' => true,
        'forecast_date' => true,
        'forecast_days' => true,
        'working_days' => true,
        'created' => true,
        'modified' => true,
        'supply' => true,
        'product' => true,
        'receiveds' => true,
        'requisitions' => true,
        'total_price' => true,
    ];
}
