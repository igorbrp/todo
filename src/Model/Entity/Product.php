<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Product Entity
 *
 * @property int $id
 * @property string $sku
 * @property bool|null $automatic_sku
 * @property string|null $supply_sku
 * @property string $title
 * @property string|null $slug
 * @property string|null $description
 * @property string|null $long_description
 * @property float $root_price
 * @property float|null $cost_price
 * @property float|null $price
 * @property bool $price_calculated
 * @property int|null $deadline
 * @property bool $working_days
 * @property bool $deadline_inherited
 * @property float|null $maximum_discount
 * @property bool $maximum_discount_inherited
 * @property int $product_type_id
 * @property int $supply_id
 * @property int|null $product_line_id
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\ProductType $product_type
 * @property \App\Model\Entity\Supply $supply
 * @property \App\Model\Entity\ProductLine $product_line
 * @property \App\Model\Entity\Item[] $items
 */
class Product extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'sku' => true,
        'automatic_sku' => true,
        'supply_sku' => true,
        'title' => true,
        'derived_from' => true,
        'slug' => true,
        'description' => true,
        'long_description' => true,
        'root_price' => true,
        'cost_price' => true,
        'price' => true,
        'price_calculated' => true,
        'deadline' => true,
        'working_days' => true,
        'deadline_inherited' => true,
        'maximum_discount' => true,
        'maximum_discount_inherited' => true,
        'product_type_id' => true,
        'supply_id' => true,
        'product_line_id' => true,
        'created' => true,
        'modified' => true,
        'product_type' => true,
        'supply' => true,
        'product_line' => true,
        'items' => true,
        'is_stock' => true,
        'complement' => true,
        'file' => true
    ];
}
