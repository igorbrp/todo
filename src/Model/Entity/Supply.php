<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Supply Entity
 *
 * @property int $id
 * @property string|null $code
 * @property string $name
 * @property string|null $cnpj
 * @property string|null $phone
 * @property string|null $mobile
 * @property string|null $phone2
 * @property string|null $mobile2
 * @property string|null $email
 * @property string|null $contact
 * @property string|null $mobile_contact
 * @property int|null $agent_id
 * @property int|null $deadline
 * @property bool $working_days
 * @property float|null $maximum_discount
 * @property bool $email_request
 * @property bool $print_request
 * @property bool $active
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Agent $agent
 * @property \App\Model\Entity\Item[] $items
 * @property \App\Model\Entity\ProductLine[] $product_lines
 * @property \App\Model\Entity\Product[] $products
 * @property \App\Model\Entity\SupplyTax[] $supply_taxes
 */
class Supply extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'code' => true,
        'name' => true,
        'cnpj' => true,
        'phone' => true,
        'mobile' => true,
        'phone2' => true,
        'mobile2' => true,
        'email' => true,
        'contact' => true,
        'mobile_contact' => true,
        'agent_id' => true,
        'deadline' => true,
        'working_days' => true,
        'maximum_discount' => true,
        'email_request' => true,
        'print_request' => true,
        'active' => true,
        'created' => true,
        'modified' => true,
        'agent' => true,
        'items' => true,
        'product_lines' => true,
        'products' => true,
        'address' => true,
        'file' => true,
        'supply_taxes' => true
    ];
}
