<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Payment Entity
 *
 * @property int $id
 * @property int $order_id
 * @property int $payment_methods_id
 * @property float $value
 * @property int $installments
 * @property float|null $fator
 * @property string|null $registry
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Order $order
 * @property \App\Model\Entity\PaymentMethod $payment_method
 */
class Payment extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'order_id' => true,
        'payment_methods_id' => true,
        'value' => true,
        'installments' => true,
        'fator' => true,
        'registry' => true,
        'created' => true,
        'modified' => true,
        'order' => true,
        'details' => true,
        'payment_method' => true,
        'financier_id' => true,
    ];
}
