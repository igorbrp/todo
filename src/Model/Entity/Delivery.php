<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Delivery Entity
 *
 * @property int $id
 * @property int $order_id
 * @property \Cake\I18n\FrozenDate|null $foreseen_date
 * @property string|null $foreseen_horary
 * @property \Cake\I18n\FrozenDate|null $scheduled_date
 * @property string|null $scheduled_horary
 * @property \Cake\I18n\FrozenDate|null $delivery_date
 * @property string|null $delivery_horary
 * @property bool $same_customer_address
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Order $order
 */
class Delivery extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'address' => true,
        'delivery_mades' => true,
        'freight' => true,
        'id' => false,
        // 'order_id' => true,
        // 'foreseen_date' => true,
        // 'foreseen_horary' => true,
        // 'scheduled_date' => true,
        // 'scheduled_horary' => true,
        // 'delivery_date' => true,
        // 'delivery_horary' => true,
        // 'same_customer_address' => true,
        // 'created' => true,
        // 'modified' => true,
        // 'order' => true
    ];
}
