<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Address Entity
 *
 * @property int $id
 * @property string $model
 * @property int $foreign_key
 * @property string|null $postal_code
 * @property int|null $country_id
 * @property string|null $state_id
 * @property string|null $city_id
 * @property string|null $city_name
 * @property string|null $neighborhood
 * @property string|null $thoroughfare
 * @property int|null $number
 * @property string|null $complement
 * @property string|null $reference
 * @property bool|null $valid_address
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \Address\Model\Entity\Country $country
 * @property \Address\Model\Entity\State $state
 * @property \Address\Model\Entity\City $city
 * @property \App\Model\Entity\Customer $customer
 */
class Address extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'model' => true,
        'foreign_key' => true,
        'postal_code' => true,
        'country_id' => true,
        'state_id' => true,
        'city_id' => true,
        'city_name' => true,
        'neighborhood' => true,
        'thoroughfare' => true,
        'number' => true,
        'complement' => true,
        'reference' => true,
        'valid_address' => true,
        'created' => true,
        'modified' => true,
        'country' => true,
        'state' => true,
        'city' => true,
        'customer' => true
    ];
}
