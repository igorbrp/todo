<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * City Entity
 *
 * @property string $id
 * @property string $name
 * @property int $uf
 * @property string $state_id
 *
 * @property \App\Model\Entity\State $state
 * @property \App\Model\Entity\Address[] $addresses
 * @property \App\Model\Entity\CustomerAddress[] $customer_addresses
 */
class City extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'uf' => true,
        'state_id' => true,
        'state' => true,
        'addresses' => true,
        'customer_addresses' => true
    ];
}
