<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Requisition Entity
 *
 * @property int $id
 * @property int|null $item_id
 * @property int|null $purchase_id
 * @property \Cake\I18n\FrozenTime|null $required
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Item $item
 * @property \App\Model\Entity\Purchase $purchase
 * @property \App\Model\Entity\CopyItemHistory[] $copy_item_histories
 * @property \App\Model\Entity\CopyOutboxEmail[] $copy_outbox_emails
 * @property \App\Model\Entity\OutboxEmail[] $outbox_emails
 */
class Requisition extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        //        'foreign_key' => true,
        //        'model' => true,
        'required' => true,
        'created' => true,
        'modified' => true,
        'outbox_email' => true,
        'items' => true,
    ];
}
