<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Log\Log;

/**
 * Stocks Model
 *
 * @method \App\Model\Entity\Stock get($primaryKey, $options = [])
 * @method \App\Model\Entity\Stock newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Stock[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Stock|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Stock saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Stock patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Stock[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Stock findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class StocksTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('stocks');
        $this->setDisplayField('product_id');
        $this->setPrimaryKey('product_id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
        ]);

        $this->hasMany('Receiveds', [
            'foreignKey' => 'product_id',
            'bindingKey' => 'product_id',
        ]);

        $this->hasMany('Outputs', [
            'foreignKey' => 'product_id',
            'bindingKey' => 'product_id',
        ]);

        $this->hasMany('StockAdjustments', [
            'foreignKey' => 'product_id',
            'bindingKey' => 'product_id',
        ]);

        // friendsofcake / search
        // Add the behaviour to your table
        $this->addBehavior('Search.Search');
        // Setup search filter using search manager
        $this->searchManager()
            //             ->value('supply_id')
             ->value('product_id')
            // Here we will alias the 'q' query param to search the `Articles.title`
            // field and the `Articles.content` field, using a LIKE match, with `%`
            // both before and after.
             ->add('q', 'Search.Like', [
                 'before' => true,
                 'after' => true,
                 'mode' => 'or',
                 'comparison' => 'LIKE',
                 'wildcardAny' => '*',
                 'wildcardOne' => '?',
                 'field' => ['Products.sku', 'Products.title', 'Products.complement']
             ])
             ->add('select_supply', 'Search.Value', [
                 'mode' => 'and',
                 'field' => [
                     'Supplies.id'
                 ]
             ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('product_id')
            ->allowEmptyString('product_id', null, 'create');

        $validator
            ->integer('qtd')
            ->notEmptyString('qtd');

        return $validator;
    }
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_id'], 'Products'));

        return $rules;
    }

    public function updateStock($e)
    {
        $currentStock = $this->totalStock($e->product_id);

        if($currentStock < 0) {
            return false;
        }

        // verifica se produto ja existe no estoque
        $queryStock = $this->find()->where([
            'product_id' => $e->product_id,
        ])->first();

        if(empty($queryStock)) {
            // nao existe, grava novo
            $stock = $this->newEntity();
            $stock->product_id = $e->product_id;
        } else {
            // existe, atualiza ou deleta registro
            $stock = $this->get($e->product_id);
            if($currentStock == 0) { // deleta registro do produto no estoque
                if($this->delete($stock)) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        $stock->qtd = $currentStock;
        if(!$this->save($stock)) {
            return false;
        }
        return true;
    }

    public function totalReceiveds($p)
    {
        // totalizando entradas
        $query = $this->Receiveds->find();

        $rsum = $query->select([
            's' => $query->func()->sum('qtd')
        ])->where([
            'product_id' => $p,
            //            'canceled' => 0,
        ])->first();

        return is_null($rsum->s) ? 0 : $rsum->s;
    }

    public function totalOutputs($p)
    {
        // totalizando saídas
        $query = $this->Outputs->find();

        $osum = $query->select([
            's' => $query->func()->sum('qtd')
        ])->where([
            'product_id' => $p,
            //            'canceled' => 0,
        ])->first();

        return is_null($osum->s) ? 0 : $osum->s;
    }

    public function totalAdjustment($p)
    {
        // totalizando ajustes
        $query = $this->StockAdjustments->find();

        $asum = $query->select([
            's' => $query->func()->sum('qtd')
        ])->where([
            'product_id' => $p,
        ])->first();

        return is_null($asum->s) ? 0 : $asum->s;
    }

    public function totalStock($p)
    {
        return (($this->totalReceiveds($p) - $this->totalOutputs($p)) + $this->totalAdjustment($p));
    }
}
