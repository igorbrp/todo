<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Deliveries Model
 *
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \App\Model\Entity\Delivery get($primaryKey, $options = [])
 * @method \App\Model\Entity\Delivery newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Delivery[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Delivery|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Delivery saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Delivery patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Delivery[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Delivery findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DeliveriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('deliveries');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id',
            'joinType' => 'INNER'
        ]);
        $this->hasOne('Addresses', [
            'foreignKey' => 'foreign_key',
            'bindingKey' => 'id',
            'className' => 'Addresses',
            'conditions' => ['Addresses.model' => 'Deliveries'],
            'cascadeCallbacks' => true,
            'dependent' => true,
        ]);
        $this->hasMany('DeliveryMades', [
            'foreignKey' => 'delivery_id',
        ]);
        // friendsofcake / search
        // Add the behaviour to your table
        $this->addBehavior('Search.Search');
        // Setup search filter using search manager
        $this->searchManager()
             ->value('order_id')
            // Here we will alias the 'q' query param to search the `Articles.title`
            // field and the `Articles.content` field, using a LIKE match, with `%`
            // both before and after.
             ->add('q', 'Search.Like', [
                 'before' => true,
                 'after' => true,
                 'mode' => 'or',
                 'comparison' => 'LIKE',
                 'wildcardAny' => '*',
                 'wildcardOne' => '?',
                 'field' => ['Orders.number', 'Customers.name', 'Addresses.neighborhood']
             ])
             // ->add('select_supply', 'Search.Value', [
             //     'mode' => 'and',
             //     'field' => [
             //         'Supplies.id'
             //     ]
             // ])
            ;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->date('foreseen_date', ['dmy'], __('Foreseen date must be valid.'))
            ->allowEmpty('foreseen_date');

        $validator
            ->scalar('foreseen_horary', __('Birth date must be valid.'))
            ->allowEmptyString('foreseen_horary');

        $validator
            ->date('scheduled_date', ['dmy'], __('Scheduled date must be valid.'))
            ->allowEmptyDate('scheduled_date');

        $validator
            ->scalar('scheduled_horary')
            ->allowEmptyString('scheduled_horary');

        $validator
            ->date('delivery_date')
            ->allowEmptyDate('delivery_date');

        $validator
            ->scalar('delivery_horary')
            ->allowEmptyString('delivery_horary');

        $validator
            ->boolean('same_customer_address')
            ->allowEmpty('same_customer_address');

        $validator
            ->decimal('freight')
            ->greaterThanOrEqual('freight', 0)
            ->allowEmptyString('freight');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['order_id'], 'Orders'));

        return $rules;
    }

    // public function afterSave(Event $event, EntityInterface $entity, ArrayObject $options)
    // {
    //     $event = new Event('Model.afterSaveDelivery', $this, ['params' => $entity]);
    //     $this->getEventManager()->dispatch($event);
    // }
}
