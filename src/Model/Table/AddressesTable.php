<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use ArrayObject;
use Cake\Event\Event;

use Cake\Log\Log;

/**
 * Addresses Model
 *
 * @property \Address\Model\Table\CountriesTable&\Cake\ORM\Association\BelongsTo $Countries
 * @property \Address\Model\Table\StatesTable&\Cake\ORM\Association\BelongsTo $States
 * @property &\Cake\ORM\Association\BelongsTo $Cities
 *
 * @method \App\Model\Entity\Address get($primaryKey, $options = [])
 * @method \App\Model\Entity\Address newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Address[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Address|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Address saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Address patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Address[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Address findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AddressesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('addresses');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id'
        ]);
        $this->belongsTo('States', [
            'foreignKey' => 'state_id'
        ]);
        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('model')
            ->maxLength('model', 128)
            ->requirePresence('model', 'create')
            ->notEmptyString('model');

        $validator
            ->scalar('postal_code')
            ->maxLength('postal_code', 8)
            ->allowEmptyString('postal_code');

        $validator
            ->scalar('state_id', 'invalid type')
            ->notEmpty('state_id', 'O estado deve ser informado.'); // TRADUZIIR

        $validator
            ->integer('city_id')
            ->notEmpty('city_id', 'A cidade deve ser informada'); // TRADUZIR

        $validator
            ->scalar('city_name')
            ->maxLength('city_name', 80)
            ->allowEmptyString('city_name');

        $validator
            ->scalar('neighborhood')
            ->maxLength('neighborhood', 80)
            ->notEmpty('neighborhood', 'O bairro deve ser informado.'); // TRADUZIR

        $validator
            ->scalar('thoroughfare')
            ->maxLength('thoroughfare', 80)
            ->notEmpty('thoroughfare', 'O logradouro deve ser informado.'); // TRADUZIR

        $validator
            ->integer('number')
            ->allowEmptyString('number');

        $validator
            ->scalar('complement')
            ->maxLength('complement', 120)
            ->allowEmptyString('complement');

        $validator
            ->scalar('reference')
            ->allowEmptyString('reference');

        $validator
            ->boolean('valid_address')
            ->allowEmptyString('valid_address');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {

        $rules->add($rules->existsIn(['country_id'], 'Countries', 'Invalid country'));
        $rules->add($rules->existsIn(['state_id'], 'States', 'Invalid state'));
        $rules->add($rules->existsIn(['city_id'], 'Cities', 'Invalid city'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        // limpa máscara de formatação
        if(isset($data['postal_code'])) {
            $data['postal_code'] = preg_replace("/[^0-9]/", "", $data['postal_code']);
        }

        if(empty($data['state_id'])) {
            $data['state_id'] = null;
        }

        if(empty($data['city_id'])) {
            $data['city_id'] = null;
        }

        if(empty($data['country_id'])) {
            $data['country_id'] = null;
        }

        // sugestão da documentação
        // trim em todo mundo
        foreach ($data as $key => $value) {
            if (is_string($value)) {
                $data[$key] = trim($value);
            }
        }
    }
}
