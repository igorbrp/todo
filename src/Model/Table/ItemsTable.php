<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;

use Cake\Log\Log;

/**
 * Items Model
 *
 * @property \App\Model\Table\ProductsTable&\Cake\ORM\Association\BelongsTo $Products
 * @property \App\Model\Table\SuppliesTable&\Cake\ORM\Association\BelongsTo $Supplies
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\BelongsTo $Orders
 *
 * @method \App\Model\Entity\Item get($primaryKey, $options = [])
 * @method \App\Model\Entity\Item newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Item[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Item|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Item saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Item patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Item[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Item findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ItemsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('items');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Products', [
            'foreignKey' => 'product_id'
        ]);
        $this->belongsTo('Supplies', [
            'foreignKey' => 'supply_id'
        ]);
        $this->belongsTo('Orders', [
            'foreignKey' => 'order_id'
        ]);
        $this->hasOne('Outputs', [
            'foreignKey' => 'item_id',
            'bindingKey' => 'id',
            'className' => 'Outputs',
            'cascadeCallbacks' => true,
            'dependent' => true,
        ]);
        $this->hasMany('ItemHistories', [
            'foreignKey' => 'item_id',
            'bindingKey' => 'id',
            'className' => 'ItemHistories',
            'cascadeCallbacks' => true,
            'dependent' => true,
        ]);
        $this->belongsToMany('Requisitions', [
            'joinTable' => 'items_requisitions',
            'foreignKey' => 'item_id',
            'targetForeignKey' => 'requisition_id',
            'className' => 'Requisitions',
            'cascadeCallbacks' => true,
            'dependent' => true,
            'propertyName' => 'requisitions',
        ]);

        // friendsofcake / search
        // Add the behaviour to your table
        $this->addBehavior('Search.Search');
        // Setup search filter using search manager
        $this->searchManager()
            //             ->value('supply_id')
             ->value('item_id')
            // Here we will alias the 'q' query param to search the `Articles.title`
            // field and the `Articles.content` field, using a LIKE match, with `%`
            // both before and after.
             ->add('q', 'Search.Like', [
                 'before' => true,
                 'after' => true,
                 'mode' => 'or',
                 'comparison' => 'LIKE',
                 'wildcardAny' => '*',
                 'wildcardOne' => '?',
                 'field' => ['sku', 'title', 'Orders.number']
             ])
             ->add('select_supply', 'Search.Value', [
                 'mode' => 'and',
                 'field' => ['Supplies.id']
             ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->nonNegativeInteger('qtd')
            ->requirePresence('qtd', 'create')
            ->notEmpty('qtd');

        $validator
            ->scalar('status')
            ->requirePresence('status', 'create')
            ->notEmptyString('status');

        $validator
            ->nonNegativeInteger('total_uncovered')
            ->allowEmpty('total_uncovered');

        $validator
            ->nonNegativeInteger('total_required')
            ->allowEmpty('total_required');

        $validator
            ->nonNegativeInteger('total_received')
            ->allowEmpty('total_received');

        $validator
            ->nonNegativeInteger('total_dispatched')
            ->allowEmpty('total_dispatched');

        $validator
            ->nonNegativeInteger('total_completed')
            ->allowEmpty('total_completed');

        $validator
            ->scalar('sku')
            ->maxLength('sku', 180)
            ->requirePresence('sku', 'create')
            ->notEmptyString('sku');

        $validator
            ->scalar('supply_code')
            ->maxLength('supply_code', 30)
            ->requirePresence('supply_code', 'create')
            ->notEmptyString('supply_code');

        $validator
            ->scalar('supply_name')
            ->maxLength('supply_name', 80)
            ->requirePresence('supply_name', 'create')
            ->notEmptyString('supply_name');

        $validator
            ->scalar('title')
            ->maxLength('title', 180)
            ->requirePresence('title', 'create')
            ->notEmptyString('title');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmptyString('description');

        $validator
            ->scalar('complement')
            ->maxLength('complement', 256)
            ->allowEmptyString('complement');

        $validator
            ->integer('deadline')
            ->allowEmptyString('deadline');

        $validator
            ->decimal('price')
            ->greaterThanOrEqual('price', 0)
            ->allowEmptyString('price');

        $validator
            ->decimal('unit_original_price')
            ->greaterThanOrEqual('unit_original_price', 0)
            ->allowEmptyString('unit_original_price');

        $validator
            ->decimal('minimum_unitary_price')
            ->requirePresence('minimum_unitary_price', 'create')
            ->notEmptyString('minimum_unitary_price');

        $validator
            ->decimal('root_price')
            ->greaterThanOrEqual('root_price', 0)
            ->allowEmptyString('root_price');

        $validator
            ->scalar('origin')
            ->requirePresence('origin', 'create')
            ->notEmptyString('origin');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_id'], 'Products'));
        $rules->add($rules->existsIn(['supply_id'], 'Supplies'));
        $rules->add($rules->existsIn(['order_id'], 'Orders'));

        return $rules;
    }
}
