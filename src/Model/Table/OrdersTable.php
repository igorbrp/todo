<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\I18n\Date;

/**
 * Orders Model
 *
 * @property \App\Model\Table\CustomersTable&\Cake\ORM\Association\BelongsTo $Customers
 * @property \Order\Model\Table\SubsidiariesTable&\Cake\ORM\Association\BelongsTo $Subsidiaries
 * @property \Order\Model\Table\EmployeesTable&\Cake\ORM\Association\BelongsTo $Employees
 * @property \Order\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \Order\Model\Table\CommentsTable&\Cake\ORM\Association\HasMany $Comments
 * @property \Order\Model\Table\DeliveriesTable&\Cake\ORM\Association\HasMany $Deliveries
 * @property \Order\Model\Table\ItemsTable&\Cake\ORM\Association\HasMany $Items
 * @property \Order\Model\Table\PaymentsTable&\Cake\ORM\Association\HasMany $Payments
 *
 * @method \App\Model\Entity\Order get($primaryKey, $options = [])
 * @method \App\Model\Entity\Order newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Order[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Order|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Order patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Order[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Order findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OrdersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('orders');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Customers', [
            'foreignKey' => 'customer_id',
            'joinType' => 'INNER',
            'cascadeCallbacks' => true,
            'dependent' => true,
        ]);
        $this->belongsTo('Subsidiaries', [
            'foreignKey' => 'subsidiary_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Employees', [
            'foreignKey' => 'employee_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Comments', [
            'foreignKey' => 'foreign_key',
            'bindingKey' => 'id',
            'className' => 'Comments',
            'conditions' => ['Comments.model' => 'Orders'],
            'cascadeCallbacks' => true,
            'dependent' => true,
            'joinType' => 'LEFT',
        ]);
        $this->hasOne('Deliveries', [
            'foreignKey' => 'order_id',
            'cascadeCallbacks' => true,
            'dependent' => true,
        ]);
        $this->hasMany('Items', [
            'foreignKey' => 'order_id',
            'bindingKey' => 'id',
            'className' => 'Items',
            'cascadeCallbacks' => true,
            'dependent' => true,
            'saveStrategy' => 'replace',
        ]);
        $this->hasMany('Payments', [
            'foreignKey' => 'order_id',
            'bindingKey' => 'id',
            'className' => 'Payments',
            'cascadeCallbacks' => true,
            'saveStrategy' => 'replace',
            'dependent' => true
        ]);
        // friendsofcake / search
        // Add the behaviour to your table
        $this->addBehavior('Search.Search');
        // Setup search filter using search manager
        $this->searchManager()
             ->value('order_id')
        // Here we will alias the 'q' query param to search the `Articles.title`
        // field and the `Articles.content` field, using a LIKE match, with `%`
        // both before and after.
             ->add('q', 'Search.Like', [
                 'before' => true,
                 'after' => true,
                 'mode' => 'or',
                 'comparison' => 'LIKE',
                 'wildcardAny' => '*',
                 'wildcardOne' => '?',
                 'field' => [
                     'number', 'Customers.name', 'Subsidiaries.name'
                 ]
             ])
             ->add('select_employee', 'Search.Value', [
                 'mode' => 'and',
                 'field' => ['Employees.id']
             ])
             ->add('select_subsidiary', 'Search.Value', [
                 'mode' => 'and',
                 'field' => ['Subsidiaries.id']
             ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            // trocar lihas abaixo para permitir numero do pedido alfanumerico
            ->scalar('number')
            ->maxLength('number', 180)
            ->requirePresence('number', 'create')
            ->notEmptyString('number', 'O número do pedido deve ser digitado.') // TRADUZIR
            ->add('number', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __('Já existe um pedido com esse número.')]); // TRADUZIR

        $validator
            ->integer('subsidiary_id')
            ->requirePresence('subsidiary_id', 'A filial deve ser informada.'); // TRADUZIR

        $validator
            ->integer('employee_id')
            ->requirePresence('employee_id', 'O vendedor deve ser informado.'); // TRADUZIR

        $validator
            ->date('date_order', ['dmy'], __('A data do pedido deve ser válida.')) // TRADUZIR
            ->notEmptyDate('date_order')
            ->add('date_order', 'custom', [
                'rule' => function ($value, $context) {
                    $dateOrder = new Date(str_replace('/', '-', $value), 'd-m-y');
                    $d = new Date();
                    if ($dateOrder > $d) {
                        return false;
                    }
                    return true;
                },
                'message' => 'Não é possível criar um pedido com uma data no futuro',
            ]);

        // $validator
        //     ->requirePresence('items', 'create', 'endereco no informated');

        $validator
            ->decimal('discount')
            //            ->notEmptyString('discount');
            ->allowEmpty('discount');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        // acho que nãio precisa dessa regra aqui
        //        $rules->add($rules->isUnique(['number'], 'O número do pedido deve ser único.')); // TRADUZIR
        $rules->add($rules->existsIn(['customer_id'], 'Customers'));
        $rules->add($rules->existsIn(['subsidiary_id'], 'Subsidiaries'));
        $rules->add($rules->existsIn(['employee_id'], 'Employees'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
