<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;

use Cake\Localized\Validation\BrValidation;

use Cake\Log\Log;

class ProfilesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }
    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 80)
            ->requirePresence('name', 'create')
            ->notEmptyString('name');

        $validator->setProvider('br', BrValidation::Class);

        $validator
            ->requirePresence('cpf', false)
            ->allowEmpty('cpf')
            ->add('cpf', 'myCustomNameForcpf', [
                'rule' => 'cpf',
                'provider' => 'br',
                'message' => __('O CPF deve ser válido.') // TRADUZIR
            ]);

        $validator
            ->requirePresence('cnpj', false)
            ->allowEmpty('cnpj')
            ->add('cnpj', 'myCustomNameForcnpj', [
                'rule' => 'cnpj',
                'provider' => 'br',
                'message' => __('O CNPJ deve ser válido.') // TRADUZIR
            ]);

        $validator
            ->requirePresence('cpf_cnpj', false)
            ->allowEmpty('cpf_cnpj')
            ->add('cpf_cnpj', 'myCustomNameForcpf', [
                'rule' => 'cpf',
                'provider' => 'br',
                'message' => __('O CPF deve ser válido.'), // TRADUZIR
                'on' => function($context) {
                    return (strlen($context['data']['cpf_cnpj']) < 12);
                }
            ])
            ->add('cpf_cnpj', 'myCustomNameForCpfCnpj', [
                'rule' => 'cnpj',
                'provider' => 'br',
                'message' => __('O CNPJ deve ser válido.'), // TRADUZIR
                'on' => function($context) {
                    return (strlen($context['data']['cpf_cnpj']) > 11);
                }
            ]);

        $validator
            ->date('birth_date', ['dmy'], __('A data de nascimento deve ser válida.')) // TRADUZIR
            ->allowEmpty('birth_date');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 32)
            ->allowEmptyString('phone');

        $validator
            ->scalar('phone2')
            ->maxLength('phone2', 32)
            ->allowEmptyString('phone2');

        $validator
            ->scalar('mobile')
            ->maxLength('mobile', 32)
            ->allowEmptyString('mobile');

        $validator
            ->scalar('mobile2')
            ->maxLength('mobile2', 32)
            ->allowEmptyString('mobile2');

        $validator
            ->requirePresence('email', false)
            ->email('email', false, __('O email deve ser válido')) // TRADUZIR
            ->allowEmpty('email');

        return $validator;
    }
    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['nick_name']));

        $rules->add(
            function ($entity, $options) use($rules) {
                if(!empty($entity->cpf_cnpj)) {
                    $rule = $rules->isUnique(['cpf_cnpj'], __('Este CPF/CNPJ já está em uso')); // TRADUZIR
                    return $rule($entity, $options);
                }
                return true;
            });

        $rules->add(
            function ($entity, $options) use($rules) {
                if(!empty($entity->cpf)) {
                    $rule = $rules->isUnique(['cpf'], __('Este CPF já está em uso'));
                    return $rule($entity, $options);
                }
                return true;
            });

        $rules->add(
            function ($entity, $options) use($rules) {
                if(!empty($entity->cnpj)) {
                    $rule = $rules->isUnique(['cnpj'], __('Este CNPJ já está em uso'));
                    return $rule($entity, $options);
                }
                return true;
            });

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        // limpa máscara de formatação
        if(isset($data['cpf'])) {
            $data['cpf'] = preg_replace("/[^0-9]/", "", $data['cpf']);
        }

        if(isset($data['cpf_cnpj'])) {
            $data['cpf_cnpj'] = preg_replace("/[^0-9]/", "", $data['cpf_cnpj']);
        }

        if(isset($data['cnpj'])) {
            $data['cnpj'] = preg_replace("/[^0-9]/", "", $data['cnpj']);
        }

        if(empty($data['cpf_cnpj'])) {
            $data['cpf_cnpj'] = null;
        }

        if(empty($data['cnpj'])) {
            $data['cnpj'] = null;
        }

        if(empty($data['cpf'])) {
            $data['cpf'] = null;
        }

        if(empty($data['email'])) {
            $data['email'] = null;
        }
    }
}
