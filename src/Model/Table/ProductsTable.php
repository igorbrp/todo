<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;

/**
 * Products Model
 *
 * @property \App\Model\Table\ProductTypesTable&\Cake\ORM\Association\BelongsTo $ProductTypes
 * @property \App\Model\Table\SuppliesTable&\Cake\ORM\Association\BelongsTo $Supplies
 * @property \App\Model\Table\ProductLinesTable&\Cake\ORM\Association\BelongsTo $ProductLines
 * @property \App\Model\Table\ItemsTable&\Cake\ORM\Association\HasMany $Items
 *
 * @method \App\Model\Entity\Product get($primaryKey, $options = [])
 * @method \App\Model\Entity\Product newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Product[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Product|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Product patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Product[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Product findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('products');
        $this->setDisplayField('title');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasOne('Files', [
            'foreignKey' => 'foreign_key',
            'bindingKey' => 'id',
            'className' => 'FileManager.Files',
            'conditions' => ['Files.model' => 'Products'],
            'cascadeCallbacks' => true,
            //            'dependent' => true
        ]);
        $this->belongsTo('ProductTypes', [
            'foreignKey' => 'product_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Supplies', [
            'foreignKey' => 'supply_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ProductLines', [
            'foreignKey' => 'product_line_id'
        ]);
        $this->hasMany('Items', [
            'foreignKey' => 'product_id'
        ]);
        $this->hasOne('Stocks', [
        'foreignKey' => 'product_id'
        ]);
        
        // friendsofcake / search
        // Add the behaviour to your table
        $this->addBehavior('Search.Search');
        // Setup search filter using search manager
        $this->searchManager()
            //             ->value('supply_id')
             ->value('product_id')
            // Here we will alias the 'q' query param to search the `Articles.title`
            // field and the `Articles.content` field, using a LIKE match, with `%`
            // both before and after.
             ->add('q', 'Search.Like', [
                 'before' => true,
                 'after' => true,
                 'mode' => 'or',
                 'comparison' => 'LIKE',
                 'wildcardAny' => '*',
                 'wildcardOne' => '?',
                 'field' => ['sku', 'title', 'complement']
             ])
             ->add('selectsupply', 'Search.Value', [
                 'mode' => 'and',
                 'field' => ['Supplies.id']
             ]);
             // ->add('s', 'Search.Value', [
             //     'mode' => 'and',
             //     'field' => ['Supplies.id']
             // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('sku')
            ->maxLength('sku', 180)
            ->requirePresence('sku', 'create')
            ->notEmptyString('sku');

        $validator
            ->boolean('automatic_sku')
            ->allowEmptyString('automatic_sku');

        $validator
            ->scalar('supply_sku')
            ->maxLength('supply_sku', 180)
            ->allowEmptyString('supply_sku');

        $validator
            ->scalar('title')
            ->maxLength('title', 180)
            ->requirePresence('title', 'create')
            ->notEmptyString('title');

        $validator
            ->scalar('slug')
            ->maxLength('slug', 255)
            ->allowEmptyString('slug');

        $validator
            ->scalar('description')
            ->maxLength('description', 255)
            ->allowEmptyString('description');

        $validator
            ->scalar('long_description')
            ->maxLength('long_description', 16777215)
            ->allowEmptyString('long_description');

        $validator
            ->add('root_price', 'decimal', [
                'message' => 'O preço de custo deve ser numérico.'
            ]);

        $validator
            ->greaterThanOrEqual('root_price', 0, 'O preço de custo deve ser positivo.');

        $validator
            ->notEmptyString('root_price', 'O preço de custo deve ser informado.');

        $validator
            ->add('price', 'decimal', [
                'message' => 'O preço de venda deve ser numérico.'
            ]);

        $validator
            ->greaterThanOrEqual('price', 0, 'O preço de venda deve ser positivo.');

        $validator
            ->notEmptyString('price', 'O preço de venda deve ser informado.');

        // $validator->allowEmptyString('stock_sub_title', function ($context) {
        //     return isset($context['data']['is_stock']) && !$context['data']['is_stock'];
        // },
        // 'O subtítulo do estoque deve ser digitado.');

        // $validator
        //     ->decimal('cost_price')
        //     ->greaterThanOrEqual('cost_price', 0)
        //     ->allowEmptyString('cost_price');

        // $validator
        //     ->decimal('price')
        //     ->greaterThanOrEqual('price', 0)
        //     ->allowEmptyString('price');

        $validator
            ->boolean('price_calculated')
            ->notEmptyString('price_calculated');

        $validator
            ->nonNegativeInteger('deadline')
            ->allowEmptyString('deadline');

        $validator
            ->boolean('working_days')
            ->notEmptyString('working_days');

        $validator
            ->boolean('deadline_inherited')
            ->notEmptyString('deadline_inherited');

        $validator
            ->decimal('maximum_discount')
            ->greaterThanOrEqual('maximum_discount', 0)
            ->allowEmptyString('maximum_discount');

        $validator
            ->boolean('maximum_discount_inherited')
            ->notEmptyString('maximum_discount_inherited');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['product_type_id'], 'ProductTypes'));
        $rules->add($rules->existsIn(['supply_id'], 'Supplies'));
        $rules->add($rules->existsIn(['product_line_id'], 'ProductLines'));

        return $rules;
    }

    public function afterDelete(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        $event = new Event('Model.afterDeleteAssociated', $this, ['params' => ['id' => $entity->id, 'model' => $this->registryAlias()]]);
        $this->getEventManager()->dispatch($event);
    }
}
