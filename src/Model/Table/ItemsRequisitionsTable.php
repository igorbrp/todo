<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;

use Cake\Log\Log;

/**
 * ItemsRequisitions Model
 *
 * @property \App\Model\Table\ItemsTable&\Cake\ORM\Association\BelongsTo $Items
 * @property \App\Model\Table\RequisitionsTable&\Cake\ORM\Association\BelongsTo $Requisitions
 *
 * @method \App\Model\Entity\ItemsRequisition get($primaryKey, $options = [])
 * @method \App\Model\Entity\ItemsRequisition newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ItemsRequisition[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ItemsRequisition|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ItemsRequisition saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ItemsRequisition patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ItemsRequisition[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ItemsRequisition findOrCreate($search, callable $callback = null, $options = [])
 */
class ItemsRequisitionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('items_requisitions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Items', [
            'foreignKey' => 'item_id',
            'joinType' => 'INNER',
        ]);

        $this->belongsTo('Requisitions', [
            'foreignKey' => 'requisition_id',
            'joinType' => 'INNER',
            'cascadeCallbacks' => true,
            'dependent' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['item_id'], 'Items'));
        $rules->add($rules->existsIn(['requisition_id'], 'Requisitions'));

        return $rules;
    }

    public function findRequisitionByItemId(Query $query, array $options)
    {
        $itemId = $options['item_id'];
        return $query->where(['item_id' => $itemId]);
    }

    public function findItemByRequisitionId(Query $query, array $options)
    {
        $requisitionId = $options['requisition_id'];
        return $query->where(['requisition_id' => $requisitionId]);
    }
}
