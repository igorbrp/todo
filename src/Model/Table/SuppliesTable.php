<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;

/**
 * Supplies Model
 *
 * @property \App\Model\Table\AgentsTable&\Cake\ORM\Association\BelongsTo $Agents
 * @property \App\Model\Table\ItemsTable&\Cake\ORM\Association\HasMany $Items
 * @property \App\Model\Table\ProductLinesTable&\Cake\ORM\Association\HasMany $ProductLines
 * @property \App\Model\Table\ProductsTable&\Cake\ORM\Association\HasMany $Products
 * @property \App\Model\Table\SupplyTaxesTable&\Cake\ORM\Association\HasMany $SupplyTaxes
 *
 * @method \App\Model\Entity\Supply get($primaryKey, $options = [])
 * @method \App\Model\Entity\Supply newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Supply[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Supply|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Supply saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Supply patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Supply[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Supply findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SuppliesTable extends ProfilesTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('supplies');
        $this->setDisplayField('name');

        $this->hasOne('Files', [
            'foreignKey' => 'foreign_key',
            'bindingKey' => 'id',
            'className' => 'FileManager.Files',
            'conditions' => ['Files.model' => 'Supplies'],
            'cascadeCallbacks' => true,
            //            'dependent' => true
        ]);

        $this->hasOne('Addresses', [
            'foreignKey' => 'foreign_key',
            'bindingKey' => 'id',
            'className' => 'Addresses',
            'conditions' => ['Addresses.model' => 'Supplies'],
            'cascadeCallbacks' => true,
            'dependent' => true,
        ]);

        $this->belongsTo('Agents', [
            'foreignKey' => 'agent_id'
        ]);
        $this->hasMany('Items', [
            'foreignKey' => 'supply_id'
        ]);
        $this->hasMany('ProductLines', [
            'foreignKey' => 'supply_id'
        ]);
        $this->hasMany('Products', [
            'foreignKey' => 'supply_id'
        ]);
        $this->hasMany('SupplyTaxes', [
            'foreignKey' => 'supply_id',
            'cascadeCallbacks' => true,
            'dependent' => true,
            'saveStrategy' => 'replace'
        ]);

        // friendsofcake / search
        // Add the behaviour to your table
        $this->addBehavior('Search.Search');
        // Setup search filter using search manager
        $this->searchManager()
             ->value('supply_id')
        // Here we will alias the 'q' query param to search the `Articles.title`
        // field and the `Articles.content` field, using a LIKE match, with `%`
        // both before and after.
             ->add('q', 'Search.Like', [
                 'before' => true,
                 'after' => true,
                 'mode' => 'or',
                 'comparison' => 'LIKE',
                 'wildcardAny' => '*',
                 'wildcardOne' => '?',
                 'field' => [
                     'name', 'code',
                 ]
             ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        parent::validationDefault($validator);

        $validator
            ->scalar('code')
            ->maxLength('code', 30)
            ->allowEmptyString('code')
            ->add('code', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('contact')
            ->maxLength('contact', 180)
            ->allowEmptyString('contact');

        $validator
            ->scalar('mobile_contact')
            ->maxLength('mobile_contact', 32)
            ->allowEmptyString('mobile_contact');

        $validator
            ->integer('deadline')
            ->allowEmptyString('deadline');

        $validator
            ->boolean('working_days')
            ->notEmptyString('working_days');

        $validator
            ->decimal('maximum_discount')
            ->greaterThanOrEqual('maximum_discount', 0)
            ->allowEmptyString('maximum_discount');

        $validator
            ->boolean('email_request')
            //            ->requirePresence('email_request', 'create')
            ->allowEmptyString('email_request');

        $validator
            ->boolean('print_request')
            //            ->requirePresence('print_request', 'create')
            ->allowEmptyString('print_request');

        $validator
            ->boolean('active')
            ->notEmptyString('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        parent::buildRules($rules);

        //        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['code'], __('O código deve ser único'))); // TRADUZIR
        $rules->add($rules->existsIn(['agent_id'], 'Agents'));

        return $rules;
    }

    public function afterDelete(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        $event = new Event('Model.afterDeleteAssociated', $this, ['params' => ['id' => $entity->id, 'model' => $this->registryAlias()]]);
        $this->getEventManager()->dispatch($event);
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        parent::beforeMarshal($event, $data, $options);

        // // limpa máscara de formatação
        // if(isset($data['cnpj'])) {
        //     $data['cnpj'] = preg_replace("/[^0-9]/", "", $data['cnpj']);
        // }
    }
}
