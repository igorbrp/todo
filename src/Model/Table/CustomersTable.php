<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;

use Cake\Localized\Validation\BrValidation;

use Cake\Log\Log;

/**
 * Customers Model
 *
 * @property \App\Model\Table\CustomerAddressesTable&\Cake\ORM\Association\HasMany $CustomerAddresses
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\Customer get($primaryKey, $options = [])
 * @method \App\Model\Entity\Customer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Customer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Customer|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Customer saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Customer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Customer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Customer findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CustomersTable extends ProfilesTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('customers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        // $this->hasOne('CustomerAddresses', [
        //     'foreignKey' => 'customer_id'
        // ]);
        $this->hasOne('Addresses', [
            'foreignKey' => 'foreign_key',
            'bindingKey' => 'id',
            'className' => 'Addresses',
            'conditions' => ['Addresses.model' => 'Customers'],
            'cascadeCallbacks' => true,
            'dependent' => true,
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'customer_id',
            // 'bindingKey' => 'id',
            // 'className' => 'Orders',
            // 'cascadeCallbacks' => true,
            // 'dependent' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        parent::validationDefault($validator);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        parent::buildRules($rules);

        // $rules->add(
        //     function ($entity, $options) use($rules) {
        //         if(!empty($entity->cpf_cnpj)) {
        //             $rule = $rules->isUnique(['cpf_cnpj']);
        //             return $rule($entity, $options);
        //         }
        //         return true;
        //     });

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        parent::beforeMarshal($event, $data, $options);

        // // limpa máscara de formatação
        // if(isset($data['cpf_cnpj'])) {
        //     $data['cpf_cnpj'] = preg_replace("/[^0-9]/", "", $data['cpf_cnpj']);
        // }
        // if(empty($data['cpf_cnpj'])) {
        //     $data['cpf_cnpj'] = null;
        // }
        // if(empty($data['email'])) {
        //     $data['email'] = null;
        // }
    }
}
