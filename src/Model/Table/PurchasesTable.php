<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;

//use Cake\Localized\Validation\BrValidation;

use Cake\Log\Log;

/**
 * Purchases Model
 *
 * @property \App\Model\Table\SuppliesTable&\Cake\ORM\Association\BelongsTo $Supplies
 * @property \App\Model\Table\ProductsTable&\Cake\ORM\Association\BelongsTo $Products
 * @property \App\Model\Table\ReceivedsTable&\Cake\ORM\Association\HasMany $Receiveds
 * @property \App\Model\Table\RequisitionsTable&\Cake\ORM\Association\HasMany $Requisitions
 *
 * @method \App\Model\Entity\Purchase get($primaryKey, $options = [])
 * @method \App\Model\Entity\Purchase newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Purchase[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Purchase|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Purchase saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Purchase patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Purchase[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Purchase findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PurchasesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('purchases');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Supplies', [
            'foreignKey' => 'supply_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Products', [
            'foreignKey' => 'product_id',
            'joinType' => 'INNER',
        ]);
        $this->hasMany('Receiveds', [
            'foreignKey' => 'purchase_id',
            'cascadeCallbacks' => true,
            'dependent' => true,
        ]);
        $this->belongsToMany('Requisitions', [
            'joinTable' => 'purchases_requisitions',
            // 'through' => 'ItemsRequisitions',
            'foreignKey' => 'purchase_id',
            'targetForeignKey' => 'purchase_id',
            'className' => 'Requisitions',
            'cascadeCallbacks' => true,
            'dependent' => true,
            'propertyName' => 'requisitions',
        ]);

        // Add the behaviour to your table
        $this->addBehavior('Search.Search');

        // Setup search filter using search manager
        $this->searchManager()
             ->value('product_id')
        // Here we will alias the 'q' query param to search the `Articles.title`
        // field and the `Articles.content` field, using a LIKE match, with `%`
        // both before and after.
             ->add('q', 'Search.Like', [
                 'before' => true,
                 'after' => true,
                 'mode' => 'or',
                 'comparison' => 'LIKE',
                 'wildcardAny' => '*',
                 'wildcardOne' => '?',
                 'field' => ['Products.sku', 'Products.title', 'Products.complement']
             ]);
    }


    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        // não entendi pq as datas não estão sendo validadas
        $validator
            ->date('date_purchase', ['dmy'], __('A data da compra deve ser válida.'//, ['provider' => 'br']
            )) // TRADUZIR
            ->requirePresence('date_purchase', 'create')
            ->notEmptyDate('date_purchase', "A data não pode estar vazia.");

        $validator
            ->nonNegativeInteger('qtd')
            ->requirePresence('qtd', 'create')
            ->notEmptyString('qtd', ['message' => 'A quantidade deve ser definida.']);
        $validator
            ->date('forecast_date', ['dmy'], __('A data da previsão de recebimento deve ser válida.')) // TRADUZIR
            ->requirePresence('forecast_date', 'create')
            ->notEmptyDate('forecast_date', "A data não pode estar vazia.");

        $validator
            ->decimal('total_price')
            ->greaterThanOrEqual('total_price', 0)
            ->notEmptyString('total_price');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['supply_id'], 'Supplies'));
        $rules->add($rules->existsIn(['product_id'], 'Products'));

        return $rules;
    }

    /**
     * Modifying Request Data Before Building Entities em
     * http://book.cakephp.org/3.0/en/orm/saving-data.html#modifying-request-data-before-building-entities
     */
    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        // sugestão da documentação
        // trim em todo mundo
        foreach ($data as $key => $value) {
            if (is_string($value)) {
                $data[$key] = trim($value);
            }
        }
    }

    public function afterDelete(Event $event, EntityInterface $entity, ArrayObject $options)
    {
        $retorno = $this->Receiveds->Stocks->updateStock($entity);
    }
}
