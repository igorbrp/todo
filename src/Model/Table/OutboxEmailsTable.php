<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;

use Cake\Mailer\Email;
use Cake\Mailer\TransportFactory;
use Cake\Mailer\Transport\MailTransport;

use Cake\Core\Configure;

use Cake\Log\Log;

/**
 * OutboxEmails Model
 *
 * @property \App\Model\Table\RequisitionsTable&\Cake\ORM\Association\BelongsTo $Requisitions
 *
 * @method \App\Model\Entity\OutboxEmail get($primaryKey, $options = [])
 * @method \App\Model\Entity\OutboxEmail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\OutboxEmail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\OutboxEmail|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OutboxEmail saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\OutboxEmail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\OutboxEmail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\OutboxEmail findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class OutboxEmailsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('outbox_emails');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Requisitions', [
            'foreignKey' => 'foreign_key',
            'bindingKey' => 'id',
            'joinType' => 'INNER',
        ]);
        // friendsofcake / search
        // Add the behaviour to your table
        $this->addBehavior('Search.Search');
        // Setup search filter using search manager
        $this->searchManager()
            //             ->value('supply_id')
             ->value('outbox_emails_id')
            // Here we will alias the 'q' query param to search the `Articles.title`
            // field and the `Articles.content` field, using a LIKE match, with `%`
            // both before and after.
             ->add('q', 'Search.Like', [
                 'before' => true,
                 'after' => true,
                 'mode' => 'or',
                 'comparison' => 'LIKE',
                 'wildcardAny' => '*',
                 'wildcardOne' => '?',
                 'field' => ['Orders.number']
             ])
             ->add('selectsupply', 'Search.Value', [
                 'mode' => 'and',
                 'field' => ['Supplies.id']
             ]);
             // ->add('s', 'Search.Value', [
             //     'mode' => 'and',
             //     'field' => ['Supplies.id']
             // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->boolean('sented')
            ->allowEmptyString('sented');

        $validator
            ->integer('fails')
            ->allowEmptyString('fails');

        $validator
            ->scalar('last_message')
            ->allowEmptyString('last_message');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['requisition_id'], 'Requisitions'));

        return $rules;
    }

    public function sentEmailsOnOutbox()
    {
        $outboxEmailsTable = TableRegistry::getTableLocator()->get('OutboxEmails');
        $outboxEmails = $outboxEmailsTable
                      ->find()
                      ->contain([
                          'Requisitions',
                          'Requisitions.Items' => [
                              'strategy' => 'subquery',
                          ],
                          'Requisitions.Items.Supplies',
                          'Requisitions.Purchases' => [
                              'strategy' => 'subquery',
                          ],
                          'Requisitions.Purchases.Supplies',
                          'Requisitions.Purchases.Products',
                      ])
                      ->where(['sented' => 0])
                      ->andWhere(['fails < ' => 10])
                      ->andWhere(['recipient !=' => '']);

        foreach ($outboxEmails as $outboxEmail) {
            $this->sendMail($outboxEmail);
        }
        return $outboxEmails;
    }

    public function sendMail($mail)
    {
        Log::debug('entrou em sendmail ============================================================');
        $result = 0;
        $system_configuration = Configure::read('system_configurations.configuration');
        TransportFactory::drop('mail');
        
        // TransportFactory::setConfig('mail', [
        //     'host' => 'smtp://' . trim($system_configuration['smtp_server']),
        //     'port' => trim($system_configuration['smtp_port']),
        //     'timeout' => 30,
        //     'username' => trim($system_configuration['imap_user']),
        //     'password' => trim($system_configuration['imap_password']),
        //     'className' => MailTransport::class,
        //     //            'tls' => false,
        //     // 'client' => null,
        //     // 'tls' => true,
        //     //            'url' => env('EMAIL_TRANSPORT_DEFAULT_URL', null),
        // ]);

        $strDns = 'smtp://' . trim($system_configuration['imap_user']) . ':' . trim($system_configuration['imap_password']) . '@' . trim($system_configuration['smtp_server']) . ':' . trim($system_configuration['smtp_port']);

        Log::debug(' $strDns  ============================================================');
        Log::debug($strDns);
        Log::debug(' $strDns  ============================================================');

        TransportFactory::setConfig('mail', [
            'url' => $strDns,
        ]);

        Log::debug('configurou  ============================================================');
        $modelOrders = TableRegistry::getTableLocator()->get('Orders');

        if(!empty($mail['requisition']['items'])) { // items
            $contact = $mail['requisition']['items'][0]['supply']['contact'];
            $supply_name = $mail['requisition']['items'][0]['supply']['name'];
            $items = $mail['requisition']['items'];
            $order = $modelOrders->get($mail['requisition']['items'][0]['order_id']);
            $ref = 'Requisição para pedido: ' . $order['number'] . ' - ' . $supply_name;
        } else {
            $contact = $mail['requisition']['purchases'][0]['supply']['contact'];
            $items[] = [
                'qtd' => $mail['requisition']['purchases'][0]['qtd'],
                'title' => $mail['requisition']['purchases'][0]['product']['title'],
                'complement' => $mail['requisition']['purchases'][0]['product']['complement'],
            ];
            $supply_name = $mail['requisition']['purchases'][0]['supply']['name'];
            $ref = 'Requisição para estoque: ' .  $mail['requisition']['purchases']['id'] . ' - ' . $supply_name;
        }

        Log::debug('vai mandar  ============================================================');
        $email = new Email();
        $email->setTransport('mail')
              ->setTo($mail['recipient'])
              ->setFrom($system_configuration['imap_email'])
              ->setSubject($ref)
              ->setTemplate('requisition', 'default') // depreciado
            // ->viewBuilder()->setTemplate('requisition', 'default')
            //              ->emailFormat('both')
              ->setEmailFormat('both')
              ->setViewVars([
                  'contact' => $contact,
                  'items' => $items,
                  'company' => $system_configuration['name'],
                  'thoroughfare' => $system_configuration['address']['thoroughfare'],
                  'number' => $system_configuration['address']['number'],
                  'complement' => $system_configuration['address']['complement'],
                  'state_id' => $system_configuration['address']['state_id'],
                  'city_name' => $system_configuration['address']['city_name'],
                  'neighborhood' => $system_configuration['address']['neighborhood'],
                  'postal_code' => $system_configuration['address']['postal_code'],
                  'nick_name' => 'Administração', //$_SESSION['Auth']['User']['nick_name'],
              ]);

        $emailSent = $email->send();

        if ($emailSent !== false) { // email foi enviado
            $outboxEmailsTable = TableRegistry::getTableLocator()->get('OutboxEmails');
            $outboxEmail = $outboxEmailsTable->get($mail->id);
            $outboxEmail->sented = '1';
            $outboxEmailsTable->save($outboxEmail);
            
            $result = 1; // marca retorno enviou email
            $imap = $system_configuration['imap_server'];
            $port = $system_configuration['imap_port'];
            $service = 'imap';
            // funcionando pra bienestar
            //            $validation = 'novalidate-cert';
            // testando para quartzomoveis
            //            $validation = 'notls';
            //            $box = 'INBOX.Sent';
            $box = $system_configuration['sents_folder'];
            $layer = $system_configuration['imap_layer'] == true ? '/ssl' : '';
            $validation = $system_configuration['imap_layer'] == true ? '/novalidate-cert' : '/notls';
            $username = $system_configuration['imap_user'];
            $password = $system_configuration['imap_password'];
            
            //            $mailbox = '{' . $imap . ':' . $port . '/' . $service . $layer . '/' . $validation . '}' . $box;
            $mailbox = '{' . $imap . ':' . $port . '/' . $service . $layer . $validation . '}' . $box;
            Log::debug($mailbox);
            $stream = imap_open($mailbox, $username, $password);

            if ($stream !== false) {
                $check = imap_check($stream);

                $content = 'From: ' . $system_configuration['imap_email'] . '\r\n' 
                         . 'To: ' . $mail['recipient'] . '\r\n' 
                         . 'Subject: ' . $mail['subject'] . '\r\n' 
                         . '\r\n' 
                         . $mail['message'];

                $result_append = imap_append($stream, $mailbox, str_replace('\r\n', PHP_EOL, $content));

                $check = imap_check($stream);

                imap_close($stream);

                $result = 2; // NÃO ME LEMBRO PQ COLOQUEI ISSO
            } else {
                Log::debug('Deu erro abrindo imap ' . $mailbox . ' | username: ' . $username . ' | password: ' . $password);
            }
        }
        return $result;
    }

    public function checkEmailConfiguration($data)
    {
        $authhost = '{' . $data['check-imap-server'] . ':' . $data['check-imap-port'] . '/imap';
        $authhost .= $data['check-imap-layer'] ? '/ssl/novalidate-cert' : '/notls';
        $authhost .= '}';

        $user = trim($data['check-imap-user']);
        $pass = trim($data['check-imap-password']);

        $folders = [];
        if ($mbox=imap_open( $authhost, $user, $pass, OP_DEBUG )) {
            $list = imap_listmailbox($mbox, $authhost, "*");
            if ($list !== false) {
                foreach ($list as $val) {
                    $folders[] = substr($val, strlen($authhost), strlen($val) - strlen($authhost));
                }
                imap_close($mbox);
                return $folders;
            }
            imap_close($mbox);
        }
        return false;
    }

    public function testShipping($data)
    {
        $strDns = 'smtp://' . trim($data['test-imap-user']) . ':' . trim($data['test-imap-password']) . '@' . trim($data['test-smtp-server']) . ':' . trim($data['test-smtp-port']);

        Log::debug(' $strDns  ============================================================');
        Log::debug($strDns);
        Log::debug(' $strDns  ============================================================');

        TransportFactory::setConfig('mail', [
            'url' => $strDns,
        ]);
        
        Log::debug('vai mandar  ============================================================');
        $email = new Email();
        $email->setTransport('mail')
              ->setTo($data['target-email'])
              ->setFrom($data['test-imap-email'])
              ->setSubject('Teste de envio de ' . $data['test-imap-email'])
              ->setTemplate('default') // depreciado
            // ->viewBuilder()->setTemplate('requisition', 'default')
            //              ->emailFormat('both')
              ->setEmailFormat('text');
              // ->setViewVars([
              //     'contact' => $contact,
              //     'items' => $items,
              //     'company' => $system_configuration['name'],
              //     'thoroughfare' => $system_configuration['address']['thoroughfare'],
              //     'number' => $system_configuration['address']['number'],
              //     'complement' => $system_configuration['address']['complement'],
              //     'state_id' => $system_configuration['address']['state_id'],
              //     'city_name' => $system_configuration['address']['city_name'],
              //     'neighborhood' => $system_configuration['address']['neighborhood'],
              //     'postal_code' => $system_configuration['address']['postal_code'],
              //     'nick_name' => 'Administração', //$_SESSION['Auth']['User']['nick_name'],
              // ]);

        $emailSent = $email->send();

        Log::debug("==============================================================");
        Log::debug($emailSent);
        Log::debug("==============================================================");

        if ($emailSent !== false) { // email foi enviado
            // $outboxEmailsTable = TableRegistry::getTableLocator()->get('OutboxEmails');
            // $outboxEmail = $outboxEmailsTable->get($mail->id);
            // $outboxEmail->sented = '1';
            // $outboxEmailsTable->save($outboxEmail);
            
            $result = 1; // marca retorno enviou email
            $imap = $data['test-imap-server'];
            $port = $data['test-imap-port'];
            $service = 'imap';
            // funcionando pra bienestar
            // testando para quartzomoveis
            $box = $data['test-sents-folder'];
            $layer = $data['test-imap-layer'] == true ? '/ssl' : '';
            $validation = $data['test-imap-layer'] == true ? '/novalidate-cert' : '/notls';
            $username = $data['test-imap-user'];
            $password = $data['test-imap-password'];
            
            $mailbox = '{' . $imap . ':' . $port . '/' . $service . $layer . $validation . '}' . $box;
            Log::debug($mailbox);
            $stream = imap_open($mailbox, $username, $password);

            if ($stream !== false) {
                $check = imap_check($stream);

                $content = 'From: ' . $system_configuration['imap_email'] . '\r\n' 
                         . 'To: ' . $mail['recipient'] . '\r\n' 
                         . 'Subject: ' . $mail['subject'] . '\r\n' 
                         . '\r\n' 
                         . $mail['message'];
                $content = utf8_decode($content);

                $result_append = imap_append($stream, $mailbox, str_replace('\r\n', PHP_EOL, $content));

                $check = imap_check($stream);

                imap_close($stream);

                $result = 2; // NÃO ME LEMBRO PQ COLOQUEI ISSO
            } else {
                Log::debug('Deu erro abrindo imap ' . $mailbox . ' | username: ' . $username . ' | password: ' . $password);
            }
        }
    }

    public function beforeFilter(Event $event)
    {
        $this->getEventManager()->off($this->Csrf);
    }
}
