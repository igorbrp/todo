<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Localized\Validation\BrValidation;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;

use Cake\Log\Log;

/**
 * Employees Model
 *
 * @property \SystemUser\Model\Table\SystemUsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\Employee get($primaryKey, $options = [])
 * @method \App\Model\Entity\Employee newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Employee[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Employee|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Employee saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Employee patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Employee[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Employee findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class EmployeesTable extends ProfilesTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('employees');
        $this->setDisplayField('nick_name');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->hasMany('Orders', [
            'foreignKey' => 'employee_id'
        ]);
        $this->hasOne('Addresses', [
            'foreignKey' => 'foreign_key',
            'bindingKey' => 'id',
            'className' => 'Addresses',
            'conditions' => ['Addresses.model' => 'Employees'],
            'cascadeCallbacks' => true,
            'dependent' => true,
        ]);

        // friendsofcake / search
        // Add the behaviour to your table
        $this->addBehavior('Search.Search');
        // Setup search filter using search manager
        $this->searchManager()
             ->value('employee_id')
        // Here we will alias the 'q' query param to search the `Articles.title`
        // field and the `Articles.content` field, using a LIKE match, with `%`
        // both before and after.
             ->add('q', 'Search.Like', [
                 'before' => true,
                 'after' => true,
                 'mode' => 'or',
                 'comparison' => 'LIKE',
                 'wildcardAny' => '*',
                 'wildcardOne' => '?',
                 'field' => [
                     'name', 'nick_name',
                 ]
             ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        parent::validationDefault($validator);

        $validator
            ->scalar('nick_name')
            ->maxLength('nick_name', 60)
            ->requirePresence('nick_name', 'create')
            ->notEmptyString('nick_name')
            ->add('nick_name', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->boolean('active')
            ->notEmptyString('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        parent::buildRules($rules);

        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        parent::beforeMarshal($event, $data, $options);

        // Log::debug($data['birth_date']);
        // // limpa máscara de formatação
        // if(isset($data['cpf'])) {
        //     $data['cpf'] = preg_replace("/[^0-9]/", "", $data['cpf']);
        // }
    }
}
