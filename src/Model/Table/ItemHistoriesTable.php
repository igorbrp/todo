<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Log\Log;

/**
 * ItemHistories Model
 *
 * @property \App\Model\Table\ItemsTable&\Cake\ORM\Association\BelongsTo $Items
 * @property \App\Model\Table\DeliveryMadesTable&\Cake\ORM\Association\BelongsTo $DeliveryMades
 * @property \SystemUser\Model\Table\SystemUsersTable&\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\CopyRequisitionsTable&\Cake\ORM\Association\HasMany $CopyRequisitions
 *
 * @method \App\Model\Entity\ItemHistory get($primaryKey, $options = [])
 * @method \App\Model\Entity\ItemHistory newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ItemHistory[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ItemHistory|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
nn * @method \App\Model\Entity\ItemHistory saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ItemHistory patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ItemHistory[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ItemHistory findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ItemHistoriesTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('item_histories');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Items', [
            'foreignKey' => 'item_id',
            'joinType' => 'INNER',
        ]);
        $this->hasOne('DeliveryMades', [
            'foreignKey' => 'item_history_id',
            'cascadeCallbacks' => true,
            'dependent' => true,
        ]);
        $this->belongsTo('Invoices', [
            'foreignKey' => 'invoice_id',
            'cascadeCallbacks' => true,
            'dependent' => true,
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('qtd')
            ->requirePresence('qtd', 'create')
            ->notEmptyString('qtd');

        $validator
            ->scalar('status')
            ->requirePresence('status', 'create')
            ->notEmptyString('status');

        $validator
            ->nonNegativeInteger('deadline')
            ->requirePresence('deadline', 'create')
            ->notEmptyString('deadline');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['item_id'], 'Items'));
        $rules->add($rules->existsIn(['delivery_made_id'], 'DeliveryMades'));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function updateItemStatus($item_id)
    {
        $item_histories_status = $this->getItemHistoryStatus($item_id);
        $updateItem['total_uncovered'] = $item_histories_status['total_uncovered'];
        $updateItem['total_required'] = $item_histories_status['total_required'];
        $updateItem['total_received'] = $item_histories_status['total_received'];
        $updateItem['total_dispatched'] = $item_histories_status['total_dispatched'];
        $updateItem['total_completed'] = $item_histories_status['total_completed'];

        $item = $this->Items->get($item_id);
        $item = $this->Items->patchEntity($item, $updateItem);

        if($this->Items->save($item)) {
            Log::write("debug","salvou item!");
        } else {
            Log::write("debug","deu merda pra salavar o  item!");
        }
    }

    public function getItemHistory($item_id = null)
    {
        //        $query = $this->ItemHistories
        $query = $this->find()
               ->contain([
                   'Users' => [
                       'fields' => [
                           'id',
                           'nick_name',
                       ]
                   ],
                   'Invoices',
               ])
               ->where([
                   'item_id' => $item_id,
               ])
               ->order([
                   'ItemHistories.created' => 'desc'
               ])->all();

        return $query;
    }

    protected function get_sum_uncovered($item_id)
    {
        /* sum_uncovered
         * soma total dos uncovereds
         */
        $sum_uncovered = $this->find();
        $sum_uncovered->select([
            'sum_qtd' => $sum_uncovered->func()->sum('qtd')
        ])->where([
            'item_id' => $item_id,
            'status' => 'uncovered',
        ]);
        $sum = $sum_uncovered->first()->sum_qtd;

        return is_null($sum) ? 0 : $sum;
    }

    protected function get_sum_required($item_id)
    {
        /* sum_required
         * soma total dos requireds
         */
        $sum_required = $this->find();
        $sum_required->select([
            'sum_qtd' => $sum_required->func()->sum('qtd')
        ])->where([
            'item_id' => $item_id,
            'status' => 'required',
        ])->all();
        $sum = $sum_required->first()->sum_qtd;

        return is_null($sum) ? 0 : $sum;
    }

    protected function get_sum_received($item_id)
    {
        /* sum_received
         * soma total dos receiveds
         */
        $sum_received = $this->find();
        $sum_received->select([
            'sum_qtd' => $sum_received->func()->sum('qtd')
        ])->where([
            'item_id' => $item_id,
            'status' => 'received',
        ]);
        $sum = $sum_received->first()->sum_qtd;

        return is_null($sum) ? 0 : $sum;
    }

    protected function get_sum_dispatched($item_id)
    {
        $sum_dispatched = $this->find();
        $sum_dispatched->select([
            'sum_qtd' => $sum_dispatched->func()->sum('qtd')
        ])->where([
            'item_id' => $item_id,
            'status' => 'dispatched',
        ]);

        $sum = $sum_dispatched->first()->sum_qtd;

        return is_null($sum) ? 0 : $sum;
    }

    protected function get_sum_completed($item_id)
    {
        /* sum_completed
         * soma total dos completeds
         */
        $sum_completed = $this->find();
        $sum_completed->select([
            'sum_qtd' => $sum_completed->func()->sum('qtd')
        ])->where([
            'item_id' => $item_id,
            'status' => 'completed',
        ]);
        $sum = $sum_completed->first()->sum_qtd;

        return is_null($sum) ? 0 : $sum;
    }

    protected function get_total_uncovered($item_id)
    {
        return $this->get_sum_uncovered($item_id) - $this->get_sum_required($item_id);
    }

    protected function get_total_required($item_id)
    {
        $required = $this->get_sum_required($item_id);
        $received = $this->get_sum_received($item_id);
        if ($required < $received) { // estoque
            return 0;
        }
        return $required - $received;
    }

    protected function get_total_received($item_id)
    {
        return $this->get_sum_received($item_id) - $this->get_sum_dispatched($item_id);
    }

    protected function get_total_dispatched($item_id)
    {
        return $this->get_sum_dispatched($item_id) - $this->get_sum_completed($item_id);
    }

    protected function get_total_completed($item_id)
    {
        return $this->get_sum_completed($item_id);
    }

    public function getItemHistoryStatus($item_id = null)
    {
        $item_history_status['item_id'] = $item_id;
        $item_history_status['total_uncovered'] = $this->get_total_uncovered($item_id);
        $item_history_status['total_required'] = $this->get_total_required($item_id);
        $item_history_status['total_received'] = $this->get_total_received($item_id);
        $item_history_status['total_dispatched'] = $this->get_total_dispatched($item_id);
        $item_history_status['total_completed'] = $this->get_total_completed($item_id);

        return $item_history_status;
    }
}
