<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Errors Model
 *
 * @method \App\Model\Entity\Error get($primaryKey, $options = [])
 * @method \App\Model\Entity\Error newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Error[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Error|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Error saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Error patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Error[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Error findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ErrorsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('errors');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('module')
            ->maxLength('module', 120)
            ->requirePresence('module', 'create')
            ->notEmptyString('module');

        $validator
            ->scalar('reference')
            ->maxLength('reference', 250)
            ->requirePresence('reference', 'create')
            ->notEmptyString('reference');

        $validator
            ->scalar('error')
            ->requirePresence('error', 'create')
            ->notEmptyString('error');

        return $validator;
    }
}
