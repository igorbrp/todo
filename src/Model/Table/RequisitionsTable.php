<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\ORM\TableRegistry;
use Cake\Collection\Collection;

use Cake\I18n\Time;

use Cake\Event\Event;
use Cake\Datasource\EntityInterface;
use ArrayObject;

use Cake\Log\Log;

use Cake\Http\Exception\NotImplementedException;

/**
 * Requisitions Model
 *
 * @property \App\Model\Table\ItemsTable&\Cake\ORM\Association\BelongsTo $Items
 * @property \App\Model\Table\PurchasesTable&\Cake\ORM\Association\BelongsTo $Purchases
 * @property \App\Model\Table\OutboxEmailsTable&\Cake\ORM\Association\HasMany $OutboxEmails
 *
 * @method \App\Model\Entity\Requisition get($primaryKey, $options = [])
 * @method \App\Model\Entity\Requisition newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Requisition[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Requisition|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Requisition saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Requisition patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Requisition[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Requisition findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RequisitionsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('requisitions');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        // $this->belongsTo('Purchases', [
        //     'foreignKey' => 'foreign_key',
        //     'bindingKey' => 'id',
        //     //            'conditions' => ['Requisitions.model' => 'Purchases'],
        // ]);
        $this->hasOne('OutboxEmails', [
            'foreignKey' => 'foreign_key',
            'bindingKey' => 'id',
            'className' => 'OutboxEmails',
            'conditions' => ['OutboxEmails.model' => 'Requisitions'],
            'cascadeCallbacks' => true,
            'dependent' => true,
        ]);
        $this->belongsToMany('Items', [
            'joinTable' => 'items_requisitions',
            // 'through' => 'ItemsRequisitions',
            'foreignKey' => 'requisition_id',
            'targetForeignKey' => 'item_id',
            'className' => 'Items',
            // 'cascadeCallbacks' => true,
            'propertyName' => 'items',
        ]);
        $this->belongsToMany('Purchases', [
            'joinTable' => 'purchases_requisitions',
            // 'through' => 'ItemsRequisitions',
            'foreignKey' => 'requisition_id',
            'targetForeignKey' => 'purchase_id',
            'className' => 'Purchases',
            'cascadeCallbacks' => true,
            'propertyName' => 'purchases',
        ]);
        // $this->belongsTo('Items', [
        //     'foreignKey' => 'foreign_key',
        //     'bindingKey' => 'id',
        //     'conditions' => ['Requisitions.model' => 'Items'],
        //     'className' => 'Items',
        // ]);
        // friendsofcake / search
        // Add the behaviour to your table
        $this->addBehavior('Search.Search');
        // Setup search filter using search manager
        $this->searchManager()
            //             ->value('supply_id')
             ->value('requisition_id')
            // Here we will alias the 'q' query param to search the `Articles.title`
            // field and the `Articles.content` field, using a LIKE match, with `%`
            // both before and after.
             ->add('q', 'Search.Like', [
                 'before' => true,
                 'after' => true,
                 'mode' => 'or',
                 'comparison' => 'LIKE',
                 'wildcardAny' => '*',
                 'wildcardOne' => '?',
                 'field' => ['order_or_purchase']
             ])
             ->add('selectsupply', 'Search.Value', [
                 'mode' => 'and',
                 'field' => ['Supplies.id']
             ]);
             // ->add('s', 'Search.Value', [
             //     'mode' => 'and',
             //     'field' => ['Supplies.id']
             // ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->nonNegativeInteger('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->dateTime('required')
            ->allowEmptyDateTime('required');

        return $validator;
    }

    public function createRequisition($data)
    {
        $model = TableRegistry::getTableLocator()->get($data['model']);
        $origin = $model
                ->find()
                ->contain([
                    'Products' => [
                        'fields' => [
                            'title',
                            'complement'
                        ],
                    ],
                    'Supplies' => [
                        'fields' => ['name', 'contact', 'email'],
                    ],
                ])
                ->where([$data['model'] . '.id' => $data['foreign_key']])
                ->first();
            if ($data['model'] == 'Items') { // pedido
                $ref = 'pedido de referência: ' . $origin['order_id'] . '\r\n';
            } else {
                $ref = 'REPOSIÇÃO DE ESTOQUE\r\n' ;
            }
        
            $data['outbox_email']['subject'] = 'Requisição de material a '. $origin->supply->name;
            $data['outbox_email']['recipient'] = $origin->supply->email;
            $data['outbox_email']['message'] = 'Olá, ' . $origin->supply->contact . '.\r\n\r\n';
            $data['outbox_email']['message'] .= 'Estamos requisitando o(s) produto(s) descrito(s) abaixo:\r\n';
            $data['outbox_email']['message'].= $ref . '\r\n';
            $data['outbox_email']['message'] .= 'quantidade: ' . $origin->qtd . '\r\n';
            $data['outbox_email']['message'] .= 'produto: ' . $origin->product->title . '\r\n';

        if (!empty($origin->complement)) {
            $data['outbox_email']['message'] .= 'complemento: ' . $origin->complement . '\r\n';
        }

        $data['outbox_email']['message'] .= '\r\n';
        $requisition = $this->newEntity($data);
        
        if(!$this->save($requisition)) {
            Log::debug('a requisição não foi gravada e ainda não sei como manipular o erro e dar uma mensagem');
        }
    }

    public function cancelRequisition($data)
    {
        $requisition = $this->find()
                            ->contain([
                                'Items' => [
                                    'strategy' => 'subquery',
                                    'queryBuilder' => function ($q) {
                                        return $q->where(['Items.id' => new \Cake\Database\Expression\IdentifierExpression('item_id')]);
                                    }
                                ]
                            ])
                            ->first();

        if (!is_null($requisition)) {
            if ($this->delete($requisition)) {
                Log::debug('Excluiu requisitions');
            } else {
                Log::debug('ERRO AO EXCLUIR REQUISITION, NÃO SEI COMO TRATAR');
            }
        }
    }
}
