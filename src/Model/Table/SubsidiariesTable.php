<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

use Cake\Localized\Validation\BrValidation;

use ArrayObject;
use Cake\Event\Event;

/**
 * Subsidiaries Model
 *
 * @property \App\Model\Table\OrdersTable&\Cake\ORM\Association\HasMany $Orders
 *
 * @method \App\Model\Entity\Subsidiary get($primaryKey, $options = [])
 * @method \App\Model\Entity\Subsidiary newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Subsidiary[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Subsidiary|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Subsidiary saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Subsidiary patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Subsidiary[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Subsidiary findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class SubsidiariesTable extends ProfilesTable
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('subsidiaries');
        $this->setDisplayField('name');
        //        $this->setPrimaryKey('id');

        //        $this->addBehavior('Timestamp');

        $this->hasMany('Orders', [
            'foreignKey' => 'subsidiary_id'
        ]);

        $this->hasOne('Addresses', [
            'foreignKey' => 'foreign_key',
            'bindingKey' => 'id',
            'className' => 'Addresses',
            'conditions' => ['Addresses.model' => 'Subsidiaries'],
            'cascadeCallbacks' => true,
            'dependent' => true,
        ]);

        // friendsofcake / search
        // Add the behaviour to your table
        $this->addBehavior('Search.Search');
        // Setup search filter using search manager
        $this->searchManager()
            //             ->value('supply_id')
             ->value('subsidiary_id')
            // Here we will alias the 'q' query param to search the `Articles.title`
            // field and the `Articles.content` field, using a LIKE match, with `%`
            // both before and after.
             ->add('q', 'Search.Like', [
                 'before' => true,
                 'after' => true,
                 'mode' => 'or',
                 'comparison' => 'LIKE',
                 'wildcardAny' => '*',
                 'wildcardOne' => '?',
                 'field' => ['name']
             ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        parent::validationDefault($validator);

        // $validator
        //     ->nonNegativeInteger('id')
        //     ->allowEmptyString('id', null, 'create');

        // $validator
        //     ->scalar('name')
        //     ->maxLength('name', 80)
        //     ->requirePresence('name', 'create')
        //     ->notEmptyString('name');

        // $validator
        //     ->scalar('cnpj')
        //     ->maxLength('cnpj', 14, __('CNPJ must be no longer than 14 digits'))
        //     ->allowEmptyString('cnpj');

        //        $validator->provider('br', BrValidation::Class);

        // $validator
        //     ->requirePresence('cnpj', false)
        //     ->allowEmpty('cnpj')
        //     ->add('cnpj', 'myCustomNameForcnpj', [
        //         'rule' => 'cnpj',
        //         'provider' => 'br',
        //         'message' => __('CNPJ must be valid.')
        //     ]);

        // $validator
        //     ->scalar('phone')
        //     ->maxLength('phone', 32)
        //     ->allowEmptyString('phone');

        // $validator
        //     ->scalar('mobile')
        //     ->maxLength('mobile', 32)
        //     ->allowEmptyString('mobile');

        // $validator
        //     ->scalar('phone2')
        //     ->maxLength('phone2', 32)
        //     ->allowEmptyString('phone2');

        // $validator
        //     ->scalar('mobile2')
        //     ->maxLength('mobile2', 32)
        //     ->allowEmptyString('mobile2');

        // $validator
        //     ->email('email')
        //     ->allowEmptyString('email');

        $validator
            ->boolean('active')
            ->notEmptyString('active');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        parent::buildRules($rules);

        $rules->add($rules->isUnique(['email'], __('Este email já está em uso.')));

        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data, ArrayObject $options)
    {
        parent::beforeMarshal($event, $data, $options);

        // // limpa máscara de formatação
        // if(isset($data['cnpj'])) {
        //     $data['cnpj'] = preg_replace("/[^0-9]/", "", $data['cnpj']);
        // }
    }

    // referencia para o afterDelete dos outros que usam filemanager
    // public function afterDelete(Event $event, EntityInterface $entity, ArrayObject $options)
    // {
    //     $event = new Event('Model.afterDeleteAssociated', $this, ['params' => ['id' => $entity->id, 'model' => $this->registryAlias()]]);
    //     $this->getEventManager()->dispatch($event);
    // }
}
