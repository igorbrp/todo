<?php
namespace App\Command;

use Cake\Console\Arguments;
use Cake\Console\Command;
use Cake\Console\ConsoleIo;
use Cake\Console\ConsoleOptionParser;
use Cake\Collection\Collection;

use Cake\Log\Log;

/**
 * TesteOutbox command.
 */
class TesteOutboxCommand extends Command
{
    public function initialize()
    {
        $this->loadModel('OutboxEmails');
    }

    /**
     * Hook method for defining this command's option parser.
     *
     * @see https://book.cakephp.org/3.0/en/console-and-shells/commands.html#defining-arguments-and-options
     *
     * @param \Cake\Console\ConsoleOptionParser $parser The parser to be defined
     * @return \Cake\Console\ConsoleOptionParser The built parser.
     */
    public function buildOptionParser(ConsoleOptionParser $parser)
    {
        $parser = parent::buildOptionParser($parser);

        return $parser;
    }

    /**
     * Implement this method with your command's logic.
     *
     * @param \Cake\Console\Arguments $args The command arguments.
     * @param \Cake\Console\ConsoleIo $io The console io
     * @return null|int The exit code or null for success
     */
    public function execute(Arguments $args, ConsoleIo $io)
    {
        $query = $this->OutboxEmails->find();
        $query->contain([
                  'Requisitions',
                  'Requisitions.Items' => [
                      'strategy' => 'subquery',
                  ],
                  'Requisitions.Items.Supplies',
                  'Requisitions.Purchases' => [
                      'strategy' => 'subquery',
                  ],
                  'Requisitions.Purchases.Supplies',
                  'Requisitions.Purchases.Products',
              ])
              ->formatResults(
                  function (\Cake\Collection\CollectionInterface $results) {
                      return $results->map(function ($row) {
                          $email_request = 0;
                          if(!empty($row['requisition']['items'])) {
                              $email_request = $row['requisition']['items'][0]['supply']['email_request'];
                          } elseif (!empty($row['requisition']['purchases'])) {
                              $email_request = $row['requisition']['purchases'][0]['supply']['email_request'];
                          }
                          if ($email_request == 1) {
                              return $row;
                          } else {
                              return false;
                          }
                      });
                  })
              ->where(['sented' => 0])
              ->andWhere(['fails < ' => 10])
              ->andWhere(['recipient !=' => '']);
        
        $result = $query->all();
        $collection = new Collection($result);
        $validas = $collection->reject(function ($linha, $key) {
            return empty($linha);
        });
        $io->out('registros >>>>>>>>');
        $io->out(count($validas->toArray()));
        $io->out('>>>>>>>>');
        $lines = $query->count();
        $io->out($lines);
        $io->out('>>>>>>>>');
        print_r($result->toArray());
        $io->out('>>>>>>>>');
    }
}
