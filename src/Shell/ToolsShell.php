<?php
namespace App\Shell;

use Cake\Console\Shell;

use Cake\Filesystem\Folder;
use Cake\Filesystem\File;


class ToolsShell extends Shell
{
    public function main()
    {
        $this->out('Hello world.');
    }

    public function update()
    {
        if (!$this->checkPackages())
            return;

        $returns = [];

        $last_line = system('cd ' . ROOT . ' && composer update', $retval);
        $returns['composer'] = ['return' => $retval, 'last_line' => $last_line];

        $last_line = system('cd ' . ROOT . ' && git pull', $retval);
        $returns['git_pull'] = ['return' => $retval, 'last_line' => $last_line];

        $last_line = system('cd ' . ROOT . DS . 'webroot' . DS . ' && npm update', $retval);
        $returns['npm'] = ['return' => $retval, 'last_line' => $last_line];

        $last_line = system('cd ' . ROOT . DS . 'webroot' . DS . 'third-party'. DS . ' && bower update', $retval);
        $returns['bower'] = ['return' => $retval, 'last_line' => $last_line];

        foreach ($returns as $key => $val) {
            $this->out($key . ' => [' . $val['return'] . ', ' . $val['last_line'] . ']');
        }
    }

    public function checkPackages() {
        $returns = [];

        $last_line = system('npm -v', $retval);
        $returns['npm'] = [$retval, $last_line]; // == 0 ? $last_line : $retval;

        $last_line = system('bower -v', $retval);
        $returns['bower'] = [$retval, $last_line]; // == 0 ? $last_line : $retval;

        $last_line = system('composer -V', $retval);
        $returns['composer'] = [$retval, $last_line]; // == 0 ? $last_line : $retval;

        // criar codigo para gravar
        // em uma variavel de configuração o resultado, ou seja versões dos pacotes ou erros

        return ($returns['composer'][0] == 0 && $returns['bower'][0] == 0 && $returns['npm'][0] == 0) ? 1 : 0;
    }

    public function verifyUpdates() {
        $need_update = 0;
        $returns = [];

        $last_line = system('npm outdated', $retval);
        $returns['npm'] = [$retval, $last_line]; // == 0 ? $last_line : $retval;
        $need_update = ($retval == 0 && $last_line != '') ? 1 : 0;

    }
}
