<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * HtmlElement helper
 */
class HtmlElementHelper extends Helper
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    function mk_dropdown_bulk($ord)
    {
        $colors = [
            'uncovered' => '#ff0000',
            'required' => '#f4c113',
            'received' => '#008000',
            'dispatched' => '#ff6680',
            'completed' => '#3333ff',
        ];

        $li = '';
        if ($ord['order_total_uncovered'] > 0) {
            $li .= '<li><a href="#" onclick="confirmChangeBulk('. $ord['id'] . ', \'required\');"><span style="color: ' . $colors['required'] . '; font-weight:bold">' . __('requisitar') . '</span></a></li>';
        }
        if ($ord['order_total_required'] > 0) {
            $li .= '<li><a href="#" onclick="confirmChangeBulk('. $ord['id'] . ', \'received\');"><span style="color: ' . $colors['received'] . '; font-weight:bold">' . __('receber') . '</span></a></li>';
        }
        if ($ord['order_total_received'] > 0) {
            $li .= '<li><a href="#" onclick="confirmChangeBulk('. $ord['id'] . ', \'dispatched\');"><span style="color: ' . $colors['dispatched'] . '; font-weight:bold">' . __('despachar') . '</span></a></li>';
        }
        if ($ord['order_total_dispatched'] > 0) {
            $li .= '<li><a href="#" onclick="confirmChangeBulk('. $ord['id'] . ', \'completed\');"><span style="color: ' . $colors['completed'] . '; font-weight:bold">' . __('concluir') . '</span></a></li>';
        }

        $dropdown = '<span class="dropdown" style="display:inline;">' /* era inline-block e passei para inline*/
                  . '<a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false">'
                  . '<span class="text-muted">escolha<b class="caret"></b></span>'
                  . '</a>'
                  . '<ul class="dropdown-menu ">'
                  . $li
                  . '</ul>'
                  . '</span>';
        return $dropdown;
    }
    function statusCancel($s)
    {
        switch ($s){
        case 'uncovered':
            $o = 'uncovered';
            break;
        case 'required':
            $o = 'uncovered';
            break;
        case 'received':
            $o = 'required';
            break;
        case 'dispatched':
            $o = 'received';
            break;
        case 'completed':
            $o = 'dispatched';
            break;
        }
        return $o;
    }

    function getOptions($status)
    {
        switch ($status){
        case 'uncovered':
            $o = ['required' => __('required')];
            break;
        case 'required':
            $o = ['received' => __('received'), 'uncovered' => __('uncovered')];
            break;
        case 'received':
            $o = ['dispatched' => __('dispatched'), 'required' => __('required')];
            break;
        case 'dispatched':
            $o = ['received' => __('received'), 'completed' => __('completed')];
            break;
        case 'completed':
            $o = ['dispatched' => __('dispatched')];
            break;
        }
        return $o;
    }

    function getTextStatus($s)
    {
        switch ($s){
        case 'uncovered':
            $status = __('cadastrou'); // TRADUZIR
            break;
        case 'required':
            $status = __('requisitou'); // TRADUZIR
            break;
        case 'received':
            $status = __('recebeu'); // TRADUZIR
            break;
        case 'dispatched':
            $status = __('despachou'); // TRADUZIR
            break;
        case 'completed':
            $status = __('concluiu'); // TRADUZIR
            break;
        }
        return $status;
    }

    function headerdropdown($color, $t) {
        return '<span class="dropdown" style="display:inline-block;"><a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-expanded="false"><span style="color:' . $color . '; font-weight:bold;">' . $t . '<b class="caret"></b></span><ul class="dropdown-menu "><li class="dropdown-header">' . __('escolha um ação') . '</li>';
    }

    function footerdropdown() {
        return '</ul></a></span></div>';
    }

    function mk_dropdown_status($s, $h, $colors, $o) {

        $unique_qtd = false;
    
        switch ($s) {
        case 'uncovered':
            // estava lixo e msg embaixo não sei pq
            //            $li = '<li><a href="#" rel="itemHistory/item_history_details" _ext="json" onclick="confirmChangeStatus(' . $h['item_id'] . ', \'uncovered\', \'required\', \'lixo\', \'Confirma a requisicao do item?\');"><span style="color: ' . $colors['required'] . '; font-weight:bold">' . __('requisitar') . '</span></a></li>';
            $li = '<li><a href="#" rel="itemHistory/item_history_details" _ext="json" onclick="confirmChangeStatus(' . $h['item_id'] . ', \'uncovered\', \'required\', $(this));"><span style="color: ' . $colors['required'] . '; font-weight:bold">' . __('requisitar') . '</span></a></li>';
            $unique_qtd = true;
            $v = $h['total_uncovered'];
            $t = __('pendente');
            $color = $colors['uncovered'];
            break;
        case 'required':
            $li = '<li><a href="#" rel="/itemHistory/item_history_details" _ext="json" onclick="confirmChangeStatus(' . $h['item_id'] . ', \'required\', \'received\', $(this));"><span style="color: ' . $colors['received'] . '; font-weight:bold">' . __('receber') . '</span></a></li>';
            $li .= '<li class="divider"></li>';
            $li .= '<li><a href="#" rel="/itemHistory/item_history_details" _ext="json" onclick="confirmChangeStatus(' . $h['item_id'] . ', \'required\', \'uncovered\', $(this));"><span style="color: ' . $colors['uncovered'] . '; font-weight:bold">' . __('retornar a pendente') . '</span></a></li>';
            $v = $h['total_required'];
            $t = __('requisitado');
            $color = $colors['required'];
            break;
        case 'received':
            $li = '<li><a href="#" rel="/itemHistory/item_history_details" _ext="json" onclick="confirmChangeStatus(' . $h['item_id'] . ', \'received\', \'dispatched\', $(this));"><span style="color: ' . $colors['dispatched'] . '; font-weight:bold">' . __('despachar') . '</span></a></li>';
            $li .= '<li class="divider"></li>';
            if ($o == 'stock') {
                $li .= '<li><span style="color: ' . $colors['required'] . '; font-weight:bold;text-decoration-line:line-through;">&nbsp;&nbsp;' . __('retornar a requisitado') . '</span></li>';
            } else {
                $li .= '<li><a href="#" rel="/itemHistory/item_history_details" _ext="json" onclick="confirmChangeStatus(' . $h['item_id'] . ', \'received\', \'required\', $(this));"><span style="color: ' . $colors['required'] . '; font-weight:bold">' . __('retornar a requisitado') . '</span></a></li>';
            }
            $v = $h['total_received'];
            $t = __('recebido');
            $color = $colors['received'];
            break;
        case 'dispatched':
            $li = '<li><a href="#" rel="/itemHistory/item_history_details" _ext="json" onclick="confirmChangeStatus(' . $h['item_id'] . ', \'dispatched\', \'completed\', $(this));"><span style="color: ' . $colors['completed'] . '; font-weight:bold">' . __('concluir') . '</span></a></li>';
            $li .= '<li class="divider"></li>';
            $li .= '<li><a href="#" rel="/itemHistory/item_history_details" _ext="json" onclick="confirmChangeStatus(' . $h['item_id'] . ', \'dispatched\', \'received\', $(this));"><span style="color: ' . $colors['received'] . '; font-weight:bold">' . __('retornar a recebido') . '</span></a></li>';
            $v = $h['total_dispatched'];
            $t = __('despachado');
            $color = $colors['dispatched'];
            break;
        case 'completed':
            $li = '<li><a href="#" rel="/itemHistory/item_history_details" _ext="json" onclick="confirmChangeStatus(' . $h['item_id'] . ', \'completed\', \'dispatched\', $(this));"><span style="color: ' . $colors['dispatched'] . '; font-weight:bold">' . __('retornar a despachado') . '</span></a></li>';
            $v = $h['total_completed'];
            $t = __('concluído');
            $color = $colors['completed'];
            break;
        }

        $head = $this->headerdropdown($color, $t);
        $foot = $this->footerdropdown();

        $input = '<div style="margin-top:10px;" class="select-action"><input style="width:60px;display:inline-block;margin-right:10px;" class="form-control" id="' . $s . '_recqtd_' . $h['item_id'] . '" type="number" min="1" max="' . $v . '" value="' . $v . '" ' . ($unique_qtd ? 'disabled' : '') . '>&nbsp;&nbsp;';
        return  $input . $head . $li . $foot;
    }
}
