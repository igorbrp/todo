<?php
namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

/**
 * Format helper
 */
class FormatHelper extends Helper
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function mask($val, $type)
    {
        switch($type) {
        case 'cep':
            $mask = '#####-###';
            break;
        case 'data':
            $mask = '##/##/####';
            break;
        case 'phone':
            if(strlen($val) == 10) // default
                $mask = '(##) ####-####';
            else // mobile
                $mask = '(##) # ####-####';
            break;
        case 'cnpj':
            $mask = '##.###.###/####-##';
            break;
        case 'cpf':
            $mask = '###.###.###-##';
            break;
        case 'cpf_cnpj':
            if(strlen($val) == 11) // cpf
                $mask = '###.###.###-##';
            else
                if(strlen($val) == 14) // cnpj
                    $mask = '##.###.###/####-##';
            break;
        }

        $maskared = '';
        $k = 0;
        for($i = 0; $i<=strlen($mask)-1; $i++)
        {
            if($mask[$i] == '#')
            {
                if(isset($val[$k]))
                    $maskared .= $val[$k++];
            }
            else
            {
                if(isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }
}
