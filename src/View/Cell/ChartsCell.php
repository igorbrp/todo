<?php
namespace App\View\Cell;

use Cake\I18n\Time;
use Cake\I18n\Date;
use Cake\Collection\Collection;

use Cake\View\Cell;
use Cake\Log\Log;

/**
 * Charts cell
 */
class ChartsCell extends Cell
{
    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Initialization logic run at the end of object construction.
     *
     * @return void
     */
    public function initialize()
    {
    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
    }

    /**
     * Default display method.
     *
     * @return void
     */
    public function salesThisYear()
    {
        $this->loadModel('Orders');

        $firstDay = new Date();
        $firstDay->setDate(2020, $firstDay->month, 1);

        $query = $this->Orders->find();
        $query->select([
            'order_amount' => $query->func()->sum('Items.price'),
            'month' => 'MONTH(date_order)'
        ])
              ->group('month')
              ->innerJoinWith('Items')
              ->hydrate(false)
              ->all();
        
        $collection = new Collection($query->toList());
        $order_amount = $collection->extract('order_amount');
        
        $totalSalesThisMonth = $order_amount->toList();
        for ($i = 0; $i < 12; $i++) {
            if (!isset($totalSalesThisMonth[$i])) {
                $totalSalesThisMonth[$i] = 0;
            }
        }

        $this->set('totalSalesThisMonth', $totalSalesThisMonth);
    }

    public function salesBySubsidiaries()
    {
        $colors = [
            0 => [
                'color'    => '#f56954',
                'highlight'=> '#f56954',
            ],
            1 => [
                'color'    => '#00a65a',
                'highlight'=> '#00a65a',
            ],
            2 => [
                'color'    => '#f39c12',
                'highlight'=> '#f39c12',
            ],
            3 => [
                'color'    => '#00c0ef',
                'highlight'=> '#00c0ef',
            ],
            4 => [
                'color'    => '#3c8dbc',
                'highlight'=> '#3c8dbc',
            ],
            5 => [
                'color'    => '#d2d6de',
                'highlight'=> '#d2d6de',
            ]
        ];

        $this->loadModel('Orders');

        $firstDay = new Date();
        $firstDay->setDate(2020, $firstDay->month, 1);

        $query = $this->Orders->find();
        $query->select([
            'value' => $query->func()->sum('Items.price'),
            // 'order_total_qtd' => $query->func()->sum(
            //     $query->identifier('Items.qtd')
            // ),
            'label' => 'Subsidiaries.name'
        ])
              ->group('label')
              ->innerJoinWith('Subsidiaries')
              ->innerJoinWith('Items')
              ->order(['value' => 'DESC'])
              ->hydrate(false)
              ->all();
        
        $sales_by_subsidiaries = $query->toList();

        for ($i = 0; $i < count($sales_by_subsidiaries); $i++) {
            $sales_by_subsidiaries[$i]['color'] = $colors[$i]['color'];
            $sales_by_subsidiaries[$i]['highlight'] = $colors[$i]['highlight'];
        }

        $this->set('sales_by_subsidiaries', $sales_by_subsidiaries);
        Log::debug($sales_by_subsidiaries);
    }

    public function ordersThisYear()
    {
        $this->loadModel('Orders');

        // $firstDay = new Date();
        // $firstDay->setDate(2020, $firstDay->month, 1);

        $query = $this->Orders->find();
        $query->select([
            'order_qtd' => $query->func()->count('id'),
            'month' => 'MONTH(date_order)'
        ])
              ->group('month')
            //              ->innerJoinWith('Items')
              ->hydrate(false)
              ->all();
        
        $collection = new Collection($query->toList());
        $order_qtd = $collection->extract('order_qtd');
        
        $totalOrdersThisMonth = $order_amount->toList();
        for ($i = 0; $i < 12; $i++) {
            if (!isset($totalOrdersThisMonth[$i])) {
                $totalOrdersThisMonth[$i] = 0;
            }
        }

        $this->set('totalOrdersThisMonth', $totalOrdersThisMonth);
    }

    public function ordersThisMonth()
    {
        $this->loadModel('Orders');

        $firstDay = new Date();
        $onLastMonth = new Date();
        $firstDayOnLastMonth = new Date();

        $firstDay->setDate(2020, $firstDay->month, 1);

        $onLastMonth->subMonth();

        $firstDayOnLastMonth->subMonth();
        $firstDayOnLastMonth->setDate($firstDayOnLastMonth->year, $firstDayOnLastMonth->month, 1);

        $salesMonth = $this->Orders
                    ->find()
                    ->where(['date_order >=' => $firstDay])
                    ->count();

        $salesLastMonth = $this->Orders
                        ->find()
                        ->where(['date_order >=' => $firstDayOnLastMonth])
                        ->andWhere(['date_order <=' => $onLastMonth])
                        ->count();

        $this->set('firstDayOnLastMonth', $firstDayOnLastMonth);
        $this->set('firstDay', $firstDay);
        $this->set('onLastMonth', $onLastMonth);
        
        $this->set('salesMonth', $salesMonth);
        $this->set('salesLastMonth', $salesLastMonth);
    }

    public function todayScheduled()
    {
        $this->loadModel('Deliveries');

        $today = new Date();
        $today->setDate(2020, 2, 12);

        $scheduledDeliveries = $this->Deliveries
                    ->find()
                    ->contain([
                        'DeliveryMades'
                    ])
                    ->where(['DeliveryMades.scheduled_date' => $today])
                    ->group('Deliveries.id')
                    ->innerJoinWith('DeliveryMades')
                    ->count();
        $this->set('scheduledDeliveries', $scheduledDeliveries);
    }

    public function todayForeseen()
    {
        $this->loadModel('Deliveries');

        $today = new Date();
        $today->setDate(2020, 2, 12);

        $foreseenDeliveries = $this->Deliveries
                    ->find()
                    // ->contain([
                    //     'DeliveryMades'
                    // ])
                            //                    ->where(['DeliveryMades.foreseen_date' => $today])
                    ->where(['foreseen_date' => $today])
                            //                    ->group('Deliveries.id')
                            //                    ->innerJoinWith('DeliveryMades')
                    ->count();
        $this->set('foreseenDeliveries', $foreseenDeliveries);
    }

    public function uncovereds()
    {
        $this->loadModel('Orders');

        $totalUncovereds = $this->Orders
                    ->find()
                    ->contain([
                        'Items'
                    ])
                    ->where(['Items.total_uncovered >' => 0])
                    ->innerJoinWith('Items')
                    ->count();
        $this->set('totalUncovereds', $totalUncovereds);
    }
}
