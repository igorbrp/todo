<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.3.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App;

use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Error\Middleware\ErrorHandlerMiddleware;
use Cake\Http\BaseApplication;
use Cake\Routing\Middleware\AssetMiddleware;
use Cake\Routing\Middleware\RoutingMiddleware;
use Cake\Http\Middleware\CsrfProtectionMiddleware;

// para deletar itemhistories
use Cake\Event\Event;
use Cake\Event\EventManager;
use App\Model\Table\ItemHistoriesTable as ItemHistories;
use App\Model\Table\RequisitionsTable as Requisitions;
use App\Model\Table\OutboxEmailsTable as OutboxEmails;
// use App\Model\Table\DeliveriesTable as Deliveries;

// tipo customizado
// https://book.cakephp.org/4/en/orm/database-basics.html#adding-custom-types
use Cake\Database\Type;

use Cake\Core\Configure\Engine\PhpConfig;

/**
 * Application setup class.
 *
 * This defines the bootstrapping logic and middleware layers you
 * want to use in your application.
 */
class Application extends BaseApplication
{
    /**
     * {@inheritDoc}
     */
    public function bootstrap()
    {
        // $this->addPlugin('CakePdf', ['bootstrap' => true]);

        // Configure::write('CakePdf', [ 
        //     'engine'   =>   [ 
        //         'className'   => 'CakePdf.WkHtmlToPdf', 
        //         'binary' => '/usr/bin/wkhtmltopdf',
        //         'options' => [ 
        //             'print-media-type' => false, 
        //             'outline' => true, 
        //             'dpi' => 96 
        //         ], 
        //     ], 
        //     'download' => true 
        // ]);
 //        Configure::write('CakePdf', [
        //     'engine' => 'CakePdf.WkHtmlToPdf',
        //     'binary' => '/usr/bin/wkhtmltopdf',
        //     'margin' => [
        //         'bottom' => 15,
        //         'left' => 50,
        //         'right' => 30,
        //         'top' => 45
        //     ],
        //     'orientation' => 'portrait',
        //     'download' => true
        // ]);

        // $this->addPlugin('Widgets');

        // $this->addPlugin('Widgets');

        // $this->addPlugin('Charts');

        // $this->addPlugin('Maintenance');

        // tipo customizado
        // https://book.cakephp.org/4/en/orm/database-basics.html#adding-custom-types
        Type::map('json', 'App\Database\Type\JsonType');

        $this->addPlugin('Cake/Localized');

        $this->addPlugin('AdminLTE');

        // $this->addPlugin('Search');

        $this->addPlugin('ProductLine');

        $this->addPlugin('Price');

        $this->addPlugin('FileManager', ['bootstrap' => true]);

        /**
         * Image resizing configuration
         * https://github.com/burzum/cakephp-file-storage/blob/3.0/docs/Tutorials/Quick-Start.md
         */
        $this->addPlugin('Burzum/Imagine');

        // $this->addPlugin('Burzum/Imagine');
        // Configure::write('FileStorage', array(
        //     'imageSizes' => [
        //         'Avatar' => [
        //             'crop180' => ['squareCenterCrop' => ['size' => 180]],
        //             'crop100' => ['squareCenterCrop' => ['size' => 100]],
        //             'crop40' => ['squareCenterCrop' => ['size' => 40]]
        //         ]
        //     ]
        // ));

        $this->addPlugin('CakeDC/Users', ['bootstrap' => true]);

        $this->addPlugin('CakeDC/Auth', ['bootstrap' => true]);

        Configure::write('Users.config', ['users']);

        Configure::read('Users.Auth.permissions', ['permissions']);

        $this->addPlugin('SystemUser', ['bootstrap' => true]);


        $this->addPlugin('Menus', ['routes' => true]);

        // Call parent to load bootstrap from files.
        parent::bootstrap();

        if (PHP_SAPI === 'cli') {
            $this->bootstrapCli();
        }

        // Configure::config('SystemConf', new PhpConfig());
        // Configure::load('system_configurations', 'SystemConf', false);

        // try {
        //     Configure::config('default', new PhpConfig());
            //            Configure::load('user_preferences/be19b64f-3954-427b-ace3-3862a503513e', 'default');
            //                       Configure::load('user_preferences' . DS . $this->request->session()->read('Auth.User.id'), 'default');
            //            Configure::load('user_preferences' . DS . $_SESSION['Auth']['User']['id']);
        // } catch (\Exception $e) {}

        /*
         * Only try to load DebugKit in development mode
         * Debug Kit should not be installed on a production system
         */
        if (Configure::read('debug')) {
            $this->addPlugin(\DebugKit\Plugin::class);
        }

        // Load more plugins here

        // ---------------------------------------------------------------------
        // Events
        // para deletear itemhistories
        // EventManager::instance()->on(
        //     'Model.afterDeleteItem',
        //     function ($event, $params) {
        //         $itemHistories = new ItemHistories();
        //         $itemHistories->deleteItemHistories($params);
        //     }
        // );

        // para gerar requisições
        // EventManager::instance()->on(
        //     'Model.afterSaveItemHistory',
        //     function ($event, $params) {
        //         $requisitions = new Requisitions();
        //         $requisitions->createRequisition($params);
        //     }
        // );

        // // para gerar entregas
        // EventManager::instance()->on(
        //     'Model.afterSaveDelivery',
        //     function ($event, $params) {
        //         $deliveries = new Deliveries();
        //         $deliveries->createDispatch($params);
        //     }
        // );

        // enviar emails para OutboxEmails
        // EventManager::instance()->on(
        //     'Model.afterSaveRequisition',
        //     function ($event) {
        //         $outboxEmail = new OutboxEmails();
        //         $outboxEmail->sentEmailsOnOutbox();
        //     }
        // );
    }

    /**
     * Setup the middleware queue your application will use.
     *
     * @param \Cake\Http\MiddlewareQueue $middlewareQueue The middleware queue to setup.
     * @return \Cake\Http\MiddlewareQueue The updated middleware queue.
     */
    public function middleware($middlewareQueue)
    {
        $middlewareQueue
            // Catch any exceptions in the lower layers,
            // and make an error page/response
            ->add(new ErrorHandlerMiddleware(null, Configure::read('Error')))

            // Handle plugin/theme assets like CakePHP normally does.
            ->add(new AssetMiddleware([
                'cacheTime' => Configure::read('Asset.cacheTime')
            ]))

            // Add routing middleware.
            // If you have a large number of routes connected, turning on routes
            // caching in production could improve performance. For that when
            // creating the middleware instance specify the cache config name by
            // using it's second constructor argument:
            // `new RoutingMiddleware($this, '_cake_routes_')`
            ->add(new RoutingMiddleware($this));

        /* CSRF
         * https://book.cakephp.org/3.0/en/controllers/middleware.html#cross-site-request-forgery-csrf-middleware
         */
        // $options = [
        //     // ...
        // ];
        // $csrf = new CsrfProtectionMiddleware($options);

        // $csrf = new CsrfProtectionMiddleware();

        // $middlewareQueue->add($csrf);


        return $middlewareQueue;
    }

    /**
     * @return void
     */
    protected function bootstrapCli()
    {
        try {
            $this->addPlugin('Bake');
        } catch (MissingPluginException $e) {
            // Do not halt if the plugin is missing
        }

        $this->addPlugin('Migrations');

        // Load more plugins here
    }
}
