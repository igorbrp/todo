<?php
use Cake\Event\EventListenerInterface;

use Cake\Log\Log;

class EvOne implements EventListenerInterface
{
    public function implementedEvents()
    {
        return [
            'Model.Subsidiary.afterPlace' => 'updateBuyStatistic',
        ];
    }

    public function updateBuyStatistic($event)
    {
        Log::write('debug', 'evento: updateBuyStatistic');
    }
}
