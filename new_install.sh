# plugins
composer require burzum/cakephp-imagine-plugin:3.0.*
composer require cakedc/users:^8.4
composer require cakephp/localized:^3.5
composer require friendsofcake/search:^5.3
composer require maiconpinto/cakephp-adminlte-theme:^1.1
cd webroot/
mkdir third-party/
cd third-party/
npm install inputmask
npm install load-image
bower install javascript-auto-complete
