<?php
/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

/*
 * IMPORTANT:
 * This is an example configuration file. Copy this file into your config directory and edit to
 * setup your app permissions.
 *
 * This is a quick roles-permissions implementation
 * Rules are evaluated top-down, first matching rule will apply
 * Each line define
 *      [
 *          'role' => 'role' | ['roles'] | '*'
 *          'prefix' => 'Prefix' | , (default = null)
 *          'plugin' => 'Plugin' | , (default = null)
 *          'controller' => 'Controller' | ['Controllers'] | '*',
 *          'action' => 'action' | ['actions'] | '*',
 *          'allowed' => true | false | callback (default = true)
 *      ]
 * You could use '*' to match anything
 * 'allowed' will be considered true if not defined. It allows a callable to manage complex
 * permissions, like this
 * 'allowed' => function (array $user, $role, Request $request) {}
 *
 * Example, using allowed callable to define permissions only for the owner of the Posts to edit/delete
 *
 * (remember to add the 'uses' at the top of the permissions.php file for Hash, TableRegistry and Request
   [
        'role' => ['user'],
        'controller' => ['Posts'],
        'action' => ['edit', 'delete'],
        'allowed' => function(array $user, $role, Request $request) {
            $postId = Hash::get($request->params, 'pass.0');
            $post = TableRegistry::getTableLocator()->get('Posts')->get($postId);
            $userId = Hash::get($user, 'id');
            if (!empty($post->user_id) && !empty($userId)) {
                return $post->user_id === $userId;
            }
            return false;
        }
    ],
 */

return [
    'CakeDC/Auth.permissions' => [

        /*
         * Board
         */
        //board role allowed to all the things
        [
            'role' => 'board',
            'prefix' => '*',
            'extension' => '*',
            'plugin' => '*',
            'controller' => [
                'Addresses',
                'Customers',
                'Deliveries',
                'DeliveryMades',
                'Employees',
                'Files',
                'Financiers',
                'Items',
                'ItemHistories',
                'OutboxEmails',
                'Outputs',
                'Orders',
                'Payments',
                'PaymentMethods',
                'Prices',
                'Products',
                'ProductLines',
                'Purchases',
                'Receiveds',
                'Requisitions',
                'Stocks',
                'StockAdjustments',
                'Subsidiaries',
                'Supplies',
                'SystemConfigurations',
                'SystemUsers',
                'Vehicles',
            ],
            'action' => '*',
        ],

        [
            'role' => 'board',
            'controller' => 'Pages',
            'action' => ['display'],
        ],

        [
            'role' => 'board',
            'plugin' => 'CakeDC/Users',
            'controller' => 'Users',
            'action' => ['profile', 'logout'],
        ],

        [
            'role' => 'board',
            'controller' => 'Customers',
            'action' => ['findcpfcnpj'],
        ],

        // ************************************************************************

        /*
         * Admin
         */
        //admin role allowed to all the things
        [
            'role' => 'admin',
            'prefix' => '*',
            'extension' => '*',
            'plugin' => '*',
            'controller' => [
                'Addresses',
                'Customers',
                'Deliveries',
                'DeliveryMades',
                'Employees',
                'Files',
                'Items',
                'ItemHistories',
                'OutboxEmails',
                'Outputs',
                'Orders',
                'Payments',
                'PaymentMethods',
                'Products',
                'ProductLines',
                'Purchases',
                'Receiveds',
                'Reports',
                'Requisitions',
                'Stocks',
                'Subsidiaries',
                'Supplies',
                'SystemUsers',
                'Vehicles',
            ],
            'action' => '*',
        ],

        [
            'role' => 'admin',
            'controller' => 'Financiers',
            'action' => ['index', 'view'],
        ],

        [
            'role' => 'admin',
            'controller' => 'Pages',
            'action' => ['display'],
        ],

        [
            'role' => 'admin',
            'plugin' => 'CakeDC/Users',
            'controller' => 'Users',
            'action' => ['profile', 'logout'],
        ],

        [
            'role' => 'admin',
            'controller' => 'Customers',
            'action' => ['findcpfcnpj'],
        ],

        [
            'role' => 'admin',
            'controller' => 'SystemConfigurations',
            'action' => ['aparence', 'collapseToggle'],
        ],

        /* *****************************************        
         * Vendor
         */
        //specific actions allowed for the all roles in Users plugin
        // geral para user
        [
            'role' => 'vendor',
            'controller' => [
                'Addresses',
                'Customers',
                'Payments',
            ],
            'action' => '*',
        ],

        [
            'role' => 'vendor',
            'controller' => [
                'Agents',
                'Employees',
                'Financiers',
                'PaymentMethods',
                'Products',
                'Supplies',
                'Subsidiaries',
                'Supplies',
                'Vehicles',
            ],
            'action' => ['index', 'view'],
        ],

        // [
        //     'role' => 'vendor',
        //     'controller' => [
        //         'Payments',
        //     ],
        //     'action' => ['add', 'edit'],
        // ],
        // [
        //     'role' => 'vendor',
        //     'action' => 'json',
        // ],

        [
            'role' => 'vendor',
            'plugin' => 'CakeDC/Users',
            'controller' => 'Users',
            'action' => ['logout'],
        ],

        [
            'role' => 'vendor',
            'controller' => 'SystemUsers',
            'action' => ['profile'],
        ],

        [
            'role' => 'vendor',
            'controller' => 'Products',
            'action' => ['add'],
        ],

        [
            'role' => 'vendor',
            'controller' => 'Products',
            'action' => ['find', 'select-products-by-supply-html'],
        ],

        // [
        //     'role' => 'vendor',
        //     'controller' => 'Customers',
        //     'action' => ['findcpfcnpj'],
        // ],

        [
            'role' => 'vendor',
            'controller' => 'Orders',
            'action' => ['add', 'index', 'view'],
        ],

        [
            'role' => 'vendor',
            'controller' => 'Stocks',
            'action' => ['index'],
        ],

        [
            'role' => 'vendor',
            'controller' => 'Addresses',
            'action' => ['findCep', 'getCitiesByState'],
        ],

        [
            'role' => 'vendor',
            'controller' => 'Pages',
            'action' => ['display'],
        ],

        [
            'role' => 'vendor',
            'controller' => 'SystemConfigurations',
            'action' => ['aparence', 'collapseToggle'],
        ],

        /* *****************************************        
         * All
         */
        //specific actions allowed for the all roles 


        // // pesquisa cep para vendor
        // ['role' => 'vendor', 'controller' => ['Addresses'], 'action' => ['findCep']],

        // // pesquisa produtos para vendor
        // ['role' => 'vendor', 'controller' => ['Products'], 'action' => ['find']],

        // //specific actions allowed for the all roles in Users plugin
        // [
        //     'role' => '*',
        //     'plugin' => 'CakeDC/Users',
        //     'controller' => 'Users',
        //     'action' => ['profile', 'logout', 'linkSocial', 'callbackLinkSocial'],
        // ],

        // [
        //     'role' => '*',
        //     'plugin' => 'CakeDC/Users',
        //     'controller' => 'Users',
        //     'action' => 'resetGoogleAuthenticator',
        //     'allowed' => function (array $user, $role, \Cake\Http\ServerRequest $request) {
        //         $userId = \Cake\Utility\Hash::get($request->getAttribute('params'), 'pass.0');
        //         if (!empty($userId) && !empty($user)) {
        //             return $userId === $user['id'];
        //         }

        //         return false;
        //     }
        // ],
        // //all roles allowed to Pages/display
        // [
        //     'role' => '*',
        //     'controller' => 'Pages',
        //     'action' => 'display',
        // ],
    ]
];
