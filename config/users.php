<?php

/**
 * Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright 2010 - 2017, Cake Development Corporation (https://www.cakedc.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
use Cake\Core\Configure;
use Cake\Routing\Router;

// return [
//     'Users.table' => 'SystemUsers',
// ];

$config = [
    'Users' => [
        // Table used to manage users
        //'table' => 'CakeDC/Users.Users',
        // https://github.com/CakeDC/users/blob/master/Docs/Documentation/Extending-the-Plugin.md
        //        'table' => 'SystemUsers',
        'table' => 'SystemUser.SystemUsers',
        // Controller used to manage users plugin features & actions
        'controller' => 'CakeDC/Users.Users',
        // https://github.com/CakeDC/users/blob/master/Docs/Documentation/Extending-the-Plugin.md#extending-the-controller
        //        'controller' => 'SystemUsers',
        // configure Auth component
        'auth' => true,
        // Password Hasher
        'passwordHasher' => '\Cake\Auth\DefaultPasswordHasher',
        // token expiration, 1 hour
        'Token' => ['expiration' => 3600],
        'Email' => [
            // determines if the user should include email
            'required' => true,
            // determines if registration workflow includes email validation
            'validate' => true,
        ],
        'Registration' => [
            // determines if the register is enabled
            'active' => false,
            // determines if the reCaptcha is enabled for registration
            'reCaptcha' => false,
            // allow a logged in user to access the registration form
            'allowLoggedIn' => false,
            //ensure user is active (confirmed email) to reset his password
            'ensureActive' => false,
            // default role name used in registration
            'defaultRole' => 'user',
        ],
        'reCaptcha' => [
            // reCaptcha key goes here
            'key' => null,
            // reCaptcha secret
            'secret' => null,
            // use reCaptcha in registration
            'registration' => false,
            // use reCaptcha in login, valid values are false, true
            'login' => false,
        ],
        'Tos' => [
            // determines if the user should include tos accepted
            'required' => true,
        ],
        'Social' => [
            // enable social login
            'login' => false,
            // enable social login
            'authenticator' => 'CakeDC/Users.Social',
        ],
        'Profile' => [
            // Allow view other users profiles
            'viewOthers' => true,
            'route' => ['plugin' => 'CakeDC/Users', 'controller' => 'Users', 'action' => 'profile'],
        ],
        'Key' => [
            'Session' => [
                // session key to store the social auth data
                'social' => 'Users.social',
                // userId key used in reset password workflow
                'resetPasswordUserId' => 'Users.resetPasswordUserId',
            ],
            // form key to store the social auth data
            'Form' => [
                'social' => 'social'
            ],
            'Data' => [
                // data key to store the users email
                'email' => 'email',
                // data key to store email coming from social networks
                'socialEmail' => 'info.email',
                // data key to check if the remember me option is enabled
                'rememberMe' => 'remember_me',
            ],
        ],
        // Avatar placeholder
        'Avatar' => ['placeholder' => 'CakeDC/Users.avatar_placeholder.png'],
    ],
    'U2f' => [
        'enabled' => false,
        'checker' => \CakeDC\Users\Auth\DefaultU2fAuthenticationChecker::class,
        'startAction' => [
            'plugin' => 'CakeDC/Users',
            'controller' => 'Users',
            'action' => 'u2f',
            'prefix' => false,
        ]
    ],
    // default configuration used to auto-load the Auth Component, override to change the way Auth works
    'Auth' => [
        'loginAction' => [
            'plugin' => 'CakeDC/Users',
            'controller' => 'Users',
            'action' => 'login',
            'prefix' => false
        ],

        'authenticate' => [
            'all' => [
                'finder' => 'active',
            ],
            'CakeDC/Auth.ApiKey',
            'CakeDC/Auth.RememberMe',
            'Form',
        ],
        'authorize' => [
            'CakeDC/Auth.Superuser',
            'CakeDC/Auth.SimpleRbac',
        ],

    ],
];

return $config;
