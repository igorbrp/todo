<?php
return [
    'Theme' => [
        'title' => 'Bienestar',
        'logo' => [
            'mini' => '<b>B</b>r',
            'large' => '<b>Bienestar</b>'
        ],
        'login' => [
            'show_remember' => true,
            'show_register' => false,
            'show_social' => false
        ],
        'folder' => ROOT,
        'skin' => 'blue',
        'layout' => 'default',
        'collapsedBar' => ''
    ]
];
