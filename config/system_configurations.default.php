<?php
return array (
  'id' => 1,
  'model' => 'SystemConfigurations',
  'configuration' => 
  array (
    'name' => '',
    'cnpj' => '',
    'phone' => '',
    'phone2' => '',
    'email' => '',
    'address' => 
    array (
      'postal_code' => '',
      'state_id' => '',
      'city_id' => '',
      'neighborhood' => '',
      'thoroughfare' => '',
      'number' => '',
      'complement' => '',
      'reference' => '',
      'city_name' => ''
    ),
    'deadline' => '',
    'imap_email' => '',
    'imap_user' => '',
    'imap_password' => '',
    'imap_server' => '',
    'imap_port' => '',
    'imap_layer' => '',
    'smtp_server' => '',
    'smtp_port' => '',
    'freight' => ''
  ),
  'created' => '',
  'modified' => '',
);
