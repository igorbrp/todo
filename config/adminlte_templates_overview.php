<?php
return [
    // 'checkbox' => '<input type="checkbox" name="{{name}}" value="{{value}}" {{attrs}} class="minimal">',
    // 'checkboxFormGroup' => '{{input}}{{label}}',
    // 'checkboxContainer' => '<div class="form-group  icheckbox {{required}}">{{content}}</div>',

    'input' => '<input class="form-control {{otherClass}}" type="{{type}}" name="{{name}}"{{attrs}}/>',
    'inputContainerError' => '<div class="form-group input {{type}}{{required}} has-error">{{content}}<i class="fa fa-times-circle-o" style="display:inline-block;margin-right:5px;color:red;"></i><span class="help-block" style="display:inline-block;">{{error}}</span></div>',

    // select2 ainda não tá legal. não recebe foco e ainda não entendi como se alinha corretamente
    // ainda assim vou usando
    //    'select' => '<select class="form-control select2 {{otherClass}}" name="{{name}}"{{attrs}}>{{content}}</select>',
    //'select' => '<select class="form-control" name="{{name}}"{{attrs}}>{{content}}</select>',

    'textarea' => '<textarea class="form-control" name="{{name}}"{{attrs}}>{{value}}</textarea>',
];
