<?php
namespace App\Test\TestCase\Model\Entity;

use App\Model\Entity\Orders;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Entity\Orders Test Case
 */
class OrdersTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Entity\Orders
     */
    public $Orders;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->Orders = new Orders();
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Orders);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
