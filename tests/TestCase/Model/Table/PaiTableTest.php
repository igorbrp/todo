<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PaiTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PaiTable Test Case
 */
class PaiTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PaiTable
     */
    public $Pai;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Pai',
        'app.Filho'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Pai') ? [] : ['className' => PaiTable::class];
        $this->Pai = TableRegistry::getTableLocator()->get('Pai', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Pai);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
