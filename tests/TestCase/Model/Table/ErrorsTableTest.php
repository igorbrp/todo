<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ErrorsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ErrorsTable Test Case
 */
class ErrorsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ErrorsTable
     */
    public $Errors;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Errors',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Errors') ? [] : ['className' => ErrorsTable::class];
        $this->Errors = TableRegistry::getTableLocator()->get('Errors', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Errors);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
