<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OutboxEmailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OutboxEmailsTable Test Case
 */
class OutboxEmailsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\OutboxEmailsTable
     */
    public $OutboxEmails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.OutboxEmails',
        'app.Requisitions',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('OutboxEmails') ? [] : ['className' => OutboxEmailsTable::class];
        $this->OutboxEmails = TableRegistry::getTableLocator()->get('OutboxEmails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OutboxEmails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
