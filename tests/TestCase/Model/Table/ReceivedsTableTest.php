<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReceivedsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReceivedsTable Test Case
 */
class ReceivedsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ReceivedsTable
     */
    public $Receiveds;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Receiveds',
        'app.Products',
        'app.Purchases',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Receiveds') ? [] : ['className' => ReceivedsTable::class];
        $this->Receiveds = TableRegistry::getTableLocator()->get('Receiveds', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Receiveds);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
