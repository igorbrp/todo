<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FilhosTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FilhosTable Test Case
 */
class FilhosTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FilhosTable
     */
    public $Filhos;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Filhos',
        'app.Pais'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Filhos') ? [] : ['className' => FilhosTable::class];
        $this->Filhos = TableRegistry::getTableLocator()->get('Filhos', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Filhos);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
