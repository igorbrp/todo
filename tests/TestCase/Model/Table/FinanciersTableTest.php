<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FinanciersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FinanciersTable Test Case
 */
class FinanciersTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FinanciersTable
     */
    public $Financiers;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Financiers',
        'app.PaymentMethods',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Financiers') ? [] : ['className' => FinanciersTable::class];
        $this->Financiers = TableRegistry::getTableLocator()->get('Financiers', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Financiers);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
