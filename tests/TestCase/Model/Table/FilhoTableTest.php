<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\FilhoTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\FilhoTable Test Case
 */
class FilhoTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\FilhoTable
     */
    public $Filho;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Filho',
        'app.Pais'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Filho') ? [] : ['className' => FilhoTable::class];
        $this->Filho = TableRegistry::getTableLocator()->get('Filho', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Filho);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
