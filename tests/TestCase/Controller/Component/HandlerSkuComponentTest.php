<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\HandlerSkuComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\HandlerSkuComponent Test Case
 */
class HandlerSkuComponentTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Controller\Component\HandlerSkuComponent
     */
    public $HandlerSku;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->HandlerSku = new HandlerSkuComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HandlerSku);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
