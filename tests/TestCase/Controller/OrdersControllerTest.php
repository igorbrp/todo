<?php
namespace App\Test\TestCase\Controller;

use App\Controller\OrdersController;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\OrdersController Test Case
 *
 * @uses \App\Controller\OrdersController
 */
class OrdersControllerTest extends TestCase
{
    use IntegrationTestTrait;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Orders',
        'app.Customers',
        'app.Subsidiaries',
        'app.Employees',
        'app.Users',
        'app.Comments',
        'app.Deliveries',
        'app.Items',
        'app.Payments',
    ];

    /**
     * Test buildOrderLine method
     *
     * @return void
     */
    public function testBuildOrderLine()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildHaving method
     *
     * @return void
     */
    public function testBuildHaving()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test romance method
     *
     * @return void
     */
    public function testRomance()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test deleteSomeRecords method
     *
     * @return void
     */
    public function testDeleteSomeRecords()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test changeStatusBulk method
     *
     * @return void
     */
    public function testChangeStatusBulk()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test makeRequisition method
     *
     * @return void
     */
    public function testMakeRequisition()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test makeReception method
     *
     * @return void
     */
    public function testMakeReception()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test makeDispatch method
     *
     * @return void
     */
    public function testMakeDispatch()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test makeConclusion method
     *
     * @return void
     */
    public function testMakeConclusion()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test teste method
     *
     * @return void
     */
    public function testTeste()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
